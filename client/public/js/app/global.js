var Global =
{
	init: function()
	{
	},
	
	ajaxPost: function(options)
	{
		if( options.preload )
		{
			options.preload();
		}
		
		if( !options.type )
		{
			options.type = 'html';
		}
		
		if( !options.url )
		{
			options.url = '/ajax/handler';
		}
		
		jQuery.post( 
			options.url,
			{'data' : options.data}, 
			function(data) {
				
				if( options.callback )
				{
					options.callback(options, data);
				}
			},
			options.type
		);
	}		
};

//DOM
$(document).ready(function() {
	Global.init();
});