<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->tal->title = $this->config->item('title');
		$this->tal->base_url = substr(base_url(), 0, -1);		
	}
	
	public function index()
	{
		$this->load->library('wurfl');		
		$device = $this->wurfl->device();
		//debug($device->id);
		//debug($device->fallBack);
		//debug($device->getAllCapabilities());
		//debug($device->getCapability('mp3'));
		
		$this->tal->display('welcome/index.zpt');
	}
}