<?php
if ( ! function_exists('debug'))
{
	function debug($str)
	{
		echo '<pre>';
		print_r($str);
		echo '</pre>';		
	}
}

if ( ! function_exists('link_tag'))
{
	function link_tag($file = '')
	{
		$link = '';
		if($file != '')
		{
			$type = substr($file, -3);
			if($type == '.js')
			{
				// JS
				//$link = "<script type=\"text/javascript\" src=\"" . $CI->config->slash_item('base_url') . $CI->config->slash_item('js_path') . $file . "\"></script>\n";
				$link = "<script type=\"text/javascript\" src=\"" . $file . "?rand=" . rand(). "\"></script>\n";
			}
			
			if($type == 'css')
			{
				// CSS
				//$link = "<link rel=\"stylesheet\" type=\"text/css\" href=\"". $CI->config->slash_item('base_url') . $CI->config->slash_item('css_path') . $file ."\" />\n";
				$link = "<link rel=\"stylesheet\" type=\"text/css\" href=\"". $file . "?rand=" . rand(). "\" />\n";
			}
			
			if ($type == 'ico')
			{
				$link = "<link rel=\"icon\" href=\"". $file ."\" />\n";
			}

		}
		
		return $link;
	}
}

if ( ! function_exists('object_to_array'))
{

	function object_to_array($data) 
	{
  		if(is_array($data) || is_object($data))
  		{
    		$result = array(); 
    		foreach($data as $key => $value)
    		{ 
      			$result[$key] = object_to_array($value); 
    		}
    		return $result;
  		}
  
  	return $data;
	}
}

if ( ! function_exists('export_to_xls')) 
{
	function export_to_xls($report, $header, $data)
	{
		$report_data = "";
		for($i=0; $i<= count($header) - 1; $i++)
		{
			$report_data .= $header[$i] . "\t";
			if($i == count($header) - 1)
				$report_data = substr($report_data, 0, -1) . "\n";
		}
		
		
		foreach($data as $cell => $value)
		{
			$arr_val = array_values($value);
			for($i=0; $i<= count($arr_val) - 1; $i++)
			{
				$report_data .= $arr_val[$i] . "\t";
				if($i == count($arr_val) - 1)
					$report_data = substr($report_data, 0, -1) . "\n";
			}
		}
	
		//header("Content-type: application/octet-stream");
		//header("Content-Type: application/vnd.ms-excel");
		header('Content-type: application/ms-excel');
		
		//header("Content-length: " . strlen($report_data));
		header("Content-disposition: attachment; filename=\"" . $report . ".xls\""); 
		echo $report_data;
	}
}