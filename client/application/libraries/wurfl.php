<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//include 'wurfl/config.php';

class Wurfl {
		
	var $wurflManager;
	var $wurflDevice;
	
	public function __construct()
	{
		/*
		 * This is an example of configuring the WURFL PHP API
		 */
		
		// Enable all error logging while in development
		ini_set('display_errors', 'on');
		error_reporting(E_ALL);
		
		$wurflDir = dirname(__FILE__) . '/wurfl/WURFL';
		$resourcesDir = dirname(__FILE__) . '/wurfl/resources';
		
		require_once $wurflDir.'/Application.php';
		
		$persistenceDir = $resourcesDir.'/storage/persistence';
		$cacheDir = $resourcesDir.'/storage/cache';
			
		// Create WURFL Configuration
		$wurflConfig = new WURFL_Configuration_InMemoryConfig();
		
		// Set location of the WURFL File
		$wurflConfig->wurflFile($resourcesDir.'/wurfl.zip');
		
		// Set the match mode for the API ('performance' or 'accuracy')
		$wurflConfig->matchMode('performance');
		
		// Setup WURFL Persistence
		$wurflConfig->persistence('file', array('dir' => $persistenceDir));
		
		// Setup Caching
		$wurflConfig->cache('file', array('dir' => $cacheDir, 'expiration' => 36000));
		
		// Create a WURFL Manager Factory from the WURFL Configuration
		$wurflManagerFactory = new WURFL_WURFLManagerFactory($wurflConfig);
		
		// Create a WURFL Manager
		/* @var $wurflManager WURFL_WURFLManager */
		$this->wurflManager = $wurflManagerFactory->create();
		
		$this->wurflDevice = $this->wurflManager->getDeviceForHttpRequest($_SERVER);
	}
	
	public function info()
	{
		return $this->wurflManager->getWURFLInfo();
	}
	
	public function device()
	{
		return $this->wurflDevice;
	}
	
}
