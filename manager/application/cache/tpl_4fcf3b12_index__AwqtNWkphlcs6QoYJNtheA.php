<?php 
function tpl_4fcf3b12_index__AwqtNWkphlcs6QoYJNtheA(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<?php /* tag "link" from line 2 */; ?>
<link rel="stylesheet" href="/css/calendar/ui-lightness/jquery-ui-1.8.16.custom.css" media="screen"/>
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" href="/css/dashboard/style.css" media="screen"/>
<?php /* tag "link" from line 4 */; ?>
<link rel="stylesheet" href="/css/dashboard/barchart.css" media="screen"/>
<?php /* tag "link" from line 5 */; ?>
<link rel="stylesheet" href="/css/dashboard/chart.css" media="screen"/>

<?php /* tag "script" from line 7 */; ?>
<script src="/js/calendar/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
<!--<script src="/js/dashboard/chart.js" type="text/javascript"></script>-->

<?php /* tag "script" from line 10 */; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	  // for each result row..
	  $(".extras_result").each(function() {
	    // get the width of the bar from the span html
	    var length = $(this).find("span").html();
	    length = length.replace("%","");
	    length = length / 100;
	    var lengthPx = 400 * length;
	    
	    $(this).find("p").animate({'width':lengthPx}, 2000, function() {
	      // once the bar animation has finished, fade in the results
	      $(this).find("span").fadeIn(800);
	    });
	  });
	});
</script>


<?php /* tag "script" from line 29 */; ?>
<script>
    $(function() {
        $( "#startDate" ).datepicker();
        $( "#startDate" ).change(function() {
            $( "#startDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });     
	// for extraction form
    $(function() {
        $( "#endDate" ).datepicker();
        $( "#endDate" ).change(function() {
            $( "#endDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });        
</script>		
    
    <?php /* tag "h1" from line 45 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 45 */; ?>
<span style="float:left;">Dashboard</span></h1>
	<?php /* tag "div" from line 46 */; ?>
<div id="folderTab"><?php /* tag "div" from line 46 */; ?>
<div class="folderTabText">Quick View</div></div>
	<?php /* tag "br" from line 47 */; ?>
<br/>
<!--	<div class="dashboardTabMenu">
		<span><a href="admin#/dashboard/index">Home</a></span>
		<span><a class="selected" href="#">Analytics</a></span>
		<span><a href="#">Menu 3</a></span>
		<span><a href="#">Menu 4</a></span>
	</div>
	<div class="dimension">
		Dimensions<br/>
		Start Date: <input type="text" id="startDate" name="startDate"/>&nbsp;&nbsp;&nbsp;
		End Date: <input type="text" id="endDate" name="endDate"/>&nbsp;&nbsp;&nbsp;
		<input type="button" value="Query"/>
	</div>-->
	<?php /* tag "div" from line 60 */; ?>
<div class="reportContainer">
		<?php 
/* tag "tal:block" from line 61 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->detailMonthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

			<?php 
/* tag "div" from line 62 */ ;
if (null !== ($_tmp_2 = ($ctx->item))):  ;
$_tmp_2 = ' id="'.phptal_escape($_tmp_2).'"' ;
else:  ;
$_tmp_2 = '' ;
endif ;
?>
<div<?php echo $_tmp_2 ?>
></div>
		<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

		<?php /* tag "div" from line 64 */; ?>
<div class="tbl">
			<?php /* tag "table" from line 65 */; ?>
<table>
				<?php /* tag "tr" from line 66 */; ?>
<tr>
					<?php /* tag "td" from line 67 */; ?>
<td>
						<?php /* tag "table" from line 68 */; ?>
<table style="width:200px;">
							<?php /* tag "tr" from line 69 */; ?>
<tr><?php /* tag "td" from line 69 */; ?>
<td>&nbsp;</td></tr>
							<?php /* tag "tr" from line 70 */; ?>
<tr><?php /* tag "td" from line 70 */; ?>
<td>&nbsp;</td></tr>
							<?php /* tag "tr" from line 71 */; ?>
<tr><?php /* tag "th" from line 71 */; ?>
<th colspan="3"><?php /* tag "span" from line 71 */; ?>
<span class="tableTitle">Today Transaction</span></th></tr>
							<?php /* tag "tr" from line 72 */; ?>
<tr><?php /* tag "td" from line 72 */; ?>
<td colspan="3" align="center"><?php /* tag "span" from line 72 */; ?>
<span style="font-size:9px; font-style:Italic; font-weight:normal;"><?php /* tag "tal:block" from line 72 */; ?>
<?php echo phptal_escape($ctx->todayDate); ?>
</span></td></tr>
							<?php /* tag "tr" from line 73 */; ?>
<tr><?php /* tag "td" from line 73 */; ?>
<td>&nbsp;</td></tr>
							<?php /* tag "tr" from line 74 */; ?>
<tr>
								<?php /* tag "td" from line 75 */; ?>
<td>&nbsp;</td>
								<?php /* tag "td" from line 76 */; ?>
<td>
									<?php /* tag "table" from line 77 */; ?>
<table style="width:75px; text-align:center;">
										<?php /* tag "tr" from line 78 */; ?>
<tr>
											<?php /* tag "th" from line 79 */; ?>
<th>Hour</th><?php /* tag "th" from line 79 */; ?>
<th>Hits</th>
										</tr>
										<?php 
/* tag "tal:block" from line 81 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

											<?php 
/* tag "tal:block" from line 82 */ ;
if ($ctx->repeat->item->index <= 11):  ;
?>

											<?php /* tag "tr" from line 83 */; ?>
<tr><?php /* tag "td" from line 83 */; ?>
<td><?php /* tag "tal:block" from line 83 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 83 */; ?>
<td><?php /* tag "tal:block" from line 83 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
											<?php endif; ?>

										<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

									</table>
								</td>
								<?php /* tag "td" from line 88 */; ?>
<td valign="top">
									<?php /* tag "table" from line 89 */; ?>
<table style="width:75px;text-align:center;">
										<?php /* tag "th" from line 90 */; ?>
<th>Hour</th><?php /* tag "th" from line 90 */; ?>
<th>Hits</th>
										<?php 
/* tag "tal:block" from line 91 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

											<?php 
/* tag "tal:block" from line 92 */ ;
if ($ctx->repeat->item->index >= 12):  ;
?>

											<?php /* tag "tr" from line 93 */; ?>
<tr><?php /* tag "td" from line 93 */; ?>
<td><?php /* tag "tal:block" from line 93 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 93 */; ?>
<td><?php /* tag "tal:block" from line 93 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
											<?php endif; ?>

										<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 104 */; ?>
<div id="bargraph">
		<?php /* tag "br" from line 105 */; ?>
<br/>
			<?php /* tag "table" from line 106 */; ?>
<table style="text-align:left;">
				<?php /* tag "tr" from line 107 */; ?>
<tr>
					<?php /* tag "td" from line 108 */; ?>
<td>
						<?php /* tag "table" from line 109 */; ?>
<table class="extras_table" align="center">
							<?php /* tag "tr" from line 110 */; ?>
<tr>
								<?php /* tag "th" from line 111 */; ?>
<th colspan="2"><?php /* tag "span" from line 111 */; ?>
<span class="tableTitle">Top Followers</span></th>
							</tr>
							<?php /* tag "tr" from line 113 */; ?>
<tr><?php /* tag "td" from line 113 */; ?>
<td></td></tr>
						
							<?php 
/* tag "tal:block" from line 115 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->topFollowers)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

							<?php /* tag "tr" from line 116 */; ?>
<tr>
								<?php /* tag "th" from line 117 */; ?>
<th class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 117 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</th>
								<?php /* tag "td" from line 118 */; ?>
<td><?php /* tag "div" from line 118 */; ?>
<div class="extras_result"><?php /* tag "p" from line 118 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 118 */; ?>
<span><?php /* tag "tal:block" from line 118 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 118 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
							</tr>
							<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

							<?php /* tag "tr" from line 121 */; ?>
<tr><?php /* tag "td" from line 121 */; ?>
<td>&nbsp;</td></tr>
						</table>
						<?php /* tag "table" from line 123 */; ?>
<table class="extras_table" align="center">
							<?php /* tag "tr" from line 124 */; ?>
<tr>
								<?php /* tag "th" from line 125 */; ?>
<th colspan="2"><?php /* tag "span" from line 125 */; ?>
<span class="tableTitle">Tenant Total Hits</span></th>
							</tr>
							<?php 
/* tag "tal:block" from line 127 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->totalHitsPerTenant)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

							<?php /* tag "tr" from line 128 */; ?>
<tr>
								<?php /* tag "th" from line 129 */; ?>
<th class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 129 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</th>
								<?php /* tag "td" from line 130 */; ?>
<td><?php /* tag "div" from line 130 */; ?>
<div class="extras_result"><?php /* tag "p" from line 130 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 130 */; ?>
<span><?php /* tag "tal:block" from line 130 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 130 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
							</tr>
							<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

							<?php /* tag "tr" from line 133 */; ?>
<tr><?php /* tag "td" from line 133 */; ?>
<td>&nbsp;</td></tr>
						</table>
						<?php /* tag "table" from line 135 */; ?>
<table class="extras_table" align="center">
							<?php /* tag "tr" from line 136 */; ?>
<tr>
								<?php /* tag "th" from line 137 */; ?>
<th colspan="2"><?php /* tag "span" from line 137 */; ?>
<span class="tableTitle">Touch Point Total Hits</span></th>
							</tr>
							<?php 
/* tag "tal:block" from line 139 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->operatorTotalHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

							<?php /* tag "tr" from line 140 */; ?>
<tr>
								<?php /* tag "th" from line 141 */; ?>
<th class="extras_y-desc" scope="row">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php /* tag "tal:block" from line 141 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</th>
								<?php /* tag "td" from line 142 */; ?>
<td><?php /* tag "div" from line 142 */; ?>
<div class="extras_result"><?php /* tag "p" from line 142 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 142 */; ?>
<span><?php /* tag "tal:block" from line 142 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 142 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
							</tr>
							<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

							<?php /* tag "tr" from line 145 */; ?>
<tr><?php /* tag "td" from line 145 */; ?>
<td>&nbsp;</td></tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 151 */; ?>
<div id="monthlyReport">
			<?php /* tag "table" from line 152 */; ?>
<table style="width:880px; margin-left:20px;">
				<?php /* tag "tr" from line 153 */; ?>
<tr><?php /* tag "td" from line 153 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 154 */; ?>
<tr><?php /* tag "th" from line 154 */; ?>
<th colspan="12" align="center"><?php /* tag "span" from line 154 */; ?>
<span class="tableTitle">Total Hits By Month</span></th></tr>
				<?php /* tag "tr" from line 155 */; ?>
<tr><?php /* tag "td" from line 155 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 156 */; ?>
<tr>
					<?php 
/* tag "tal:block" from line 157 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->monthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

						<?php /* tag "td" from line 158 */; ?>
<td valign="top">
							<?php /* tag "table" from line 159 */; ?>
<table>
								<?php /* tag "tr" from line 160 */; ?>
<tr>
									<?php /* tag "th" from line 161 */; ?>
<th colspan="2" align="center"><?php /* tag "a" from line 161 */; ?>
<a href="javascript:;" onclick="alert($(this).html());"><?php /* tag "tal:block" from line 161 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</a></th>
								</tr>
								<?php 
/* tag "tal:block" from line 163 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->h as $ctx->h): ;
?>

									<?php /* tag "tr" from line 164 */; ?>
<tr>
										<?php /* tag "td" from line 165 */; ?>
<td><?php /* tag "tal:block" from line 165 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'total')); ?>
</td>
										<?php /* tag "td" from line 166 */; ?>
<td><?php /* tag "tal:block" from line 166 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
									</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 169 */; ?>
<tr><?php /* tag "td" from line 169 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</td>
					<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>
			
				</tr>
			</table>
		</div>
	</div>
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/dashboard/index.zpt (edit that file instead) */; ?>