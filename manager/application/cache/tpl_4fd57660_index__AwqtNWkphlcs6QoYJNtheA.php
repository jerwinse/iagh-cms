<?php 
function tpl_4fd57660_index__AwqtNWkphlcs6QoYJNtheA(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<?php /* tag "link" from line 2 */; ?>
<link rel="stylesheet" href="/css/calendar/ui-lightness/jquery-ui-1.8.16.custom.css" media="screen"/>
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" href="/css/dashboard/style.css" media="screen"/>
<?php /* tag "link" from line 4 */; ?>
<link rel="stylesheet" href="/css/dashboard/barchart.css" media="screen"/>
<?php /* tag "link" from line 5 */; ?>
<link rel="stylesheet" href="/css/dashboard/chart.css" media="screen"/>

<?php /* tag "script" from line 7 */; ?>
<script src="/js/calendar/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
<!--<script src="/js/calendar/jquery.js" type="text/javascript"></script>-->
<?php /* tag "script" from line 9 */; ?>
<script src="/js/dashboard/global.js" type="text/javascript"></script>

<!--<script src="/js/dashboard/chart.js" type="text/javascript"></script>-->

<?php /* tag "script" from line 13 */; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	  // for each result row..
	  $(".extras_result").each(function() {
	    // get the width of the bar from the span html
	    var length = $(this).find("span").html();
	    length = length.replace("%","");
	    length = length / 100;
	    var lengthPx = 400 * length;
	    
	    $(this).find("p").animate({'width':lengthPx}, 2000, function() {
	      // once the bar animation has finished, fade in the results
	      $(this).find("span").fadeIn(800);
	    });
	  });
	});
</script>


<?php /* tag "script" from line 32 */; ?>
<script>
    $(function() {
        $( "#startDate" ).datepicker();
        $( "#startDate" ).change(function() {
            $( "#startDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });     
	// for extraction form
    $(function() {
        $( "#endDate" ).datepicker();
        $( "#endDate" ).change(function() {
            $( "#endDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });        
    
	var sOperator = $("#severityOperator").val();
	var sObj = jQuery.parseJSON(sOperator);
	var sArr = jQuery.makeArray(sObj);
	
	var jData = $("#severityData").val();
	var obj = jQuery.parseJSON(jData);
	var graphBySeverity = jQuery.makeArray(obj);
    
	var statusData = $("#statusData").val();
	var statusObj = jQuery.parseJSON(statusData);
	var graphByStatus = jQuery.makeArray(statusObj);
    
</script>
    
    <?php /* tag "h1" from line 61 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 61 */; ?>
<span style="float:left;">Dashboard</span></h1>
	<?php /* tag "div" from line 62 */; ?>
<div id="folderTab"><?php /* tag "div" from line 62 */; ?>
<div class="folderTabText">Quick View</div></div>
	<?php /* tag "br" from line 63 */; ?>
<br/>
	<?php /* tag "div" from line 64 */; ?>
<div class="dashboardTabMenu">
		<?php /* tag "span" from line 65 */; ?>
<span id="followersLink"><?php /* tag "a" from line 65 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('followers')">Top Follower</a></span>
		<?php /* tag "span" from line 66 */; ?>
<span id="tenantHitsLink"><?php /* tag "a" from line 66 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('tenantHits')">Tenant Hits</a></span>
		<?php /* tag "span" from line 67 */; ?>
<span id="touchpointHitsLink"><?php /* tag "a" from line 67 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('touchpointHits')">Touchpoint Hits</a></span>
<!--		<span id="feedbackSeverityLink"><a href="javascript:;" onclick="global.displayTabMenuReports('feedbackStatus')">Feedback</a></span>-->
		<?php /* tag "span" from line 69 */; ?>
<span id="feedbackStatusLink"><?php /* tag "a" from line 69 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('feedbackStatus')">Feedback</a></span>
	</div>
<!--	<div class="dimension">
		Dimensions<br/>
		Start Date: <input type="text" id="startDate" name="startDate"/>&nbsp;&nbsp;&nbsp;
		End Date: <input type="text" id="endDate" name="endDate"/>&nbsp;&nbsp;&nbsp;
		<input type="button" value="Query"/>
	</div>-->
	<?php /* tag "tal:block" from line 77 */; ?>

		<?php 
/* tag "input" from line 78 */ ;
if (null !== ($_tmp_1 = ($ctx->statusHits))):  ;
$_tmp_1 = ' value="'.phptal_escape($_tmp_1).'"' ;
else:  ;
$_tmp_1 = '' ;
endif ;
?>
<input type="hidden" name="statusData" id="statusData"<?php echo $_tmp_1 ?>
/>
		<?php 
/* tag "input" from line 79 */ ;
if (null !== ($_tmp_1 = ($ctx->severityHits))):  ;
$_tmp_1 = ' value="'.phptal_escape($_tmp_1).'"' ;
else:  ;
$_tmp_1 = '' ;
endif ;
?>
<input type="hidden" name="severityData" id="severityData"<?php echo $_tmp_1 ?>
/>
		<?php 
/* tag "input" from line 80 */ ;
if (null !== ($_tmp_1 = ($ctx->severityOperators))):  ;
$_tmp_1 = ' value="'.phptal_escape($_tmp_1).'"' ;
else:  ;
$_tmp_1 = '' ;
endif ;
?>
<input type="hidden" name="severityOperator" id="severityOperator"<?php echo $_tmp_1 ?>
/>
	
	<?php /* tag "div" from line 82 */; ?>
<div class="reportContainer">
		<?php /* tag "div" from line 83 */; ?>
<div class="tbl">
			<?php 
/* tag "tal:block" from line 84 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->detailMonthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

				<?php 
/* tag "div" from line 85 */ ;
if (null !== ($_tmp_2 = ($ctx->repeat->item->key))):  ;
$_tmp_2 = ' id="'.phptal_escape($_tmp_2).'"' ;
else:  ;
$_tmp_2 = '' ;
endif ;
?>
<div style="display:none;"<?php echo $_tmp_2 ?>
>
					<?php /* tag "table" from line 86 */; ?>
<table style="width:200px; text-align:center;">
						<?php /* tag "tr" from line 87 */; ?>
<tr><?php /* tag "td" from line 87 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 87 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 88 */; ?>
<tr><?php /* tag "td" from line 88 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 88 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 89 */; ?>
<tr><?php /* tag "th" from line 89 */; ?>
<th colspan="3" align="center"><?php /* tag "span" from line 89 */; ?>
<span class="tableTitle" style="font-size:14px;"><?php /* tag "tal:block" from line 89 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</span></th></tr>
						<?php /* tag "tr" from line 90 */; ?>
<tr><?php /* tag "td" from line 90 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 90 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 91 */; ?>
<tr><?php /* tag "th" from line 91 */; ?>
<th>Name</th><?php /* tag "th" from line 91 */; ?>
<th>Hits</th></tr>
						<?php 
/* tag "tal:block" from line 92 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

							<?php /* tag "tr" from line 93 */; ?>
<tr>
								<?php /* tag "td" from line 94 */; ?>
<td width="50%"><?php /* tag "tal:block" from line 94 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'name')); ?>
</td>
								<?php /* tag "td" from line 95 */; ?>
<td width="50%"><?php /* tag "tal:block" from line 95 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
							</tr>
						<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

					</table>
				</div>
			<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

			<?php /* tag "div" from line 101 */; ?>
<div id="todayTransaction">
				<?php /* tag "table" from line 102 */; ?>
<table>
					<?php /* tag "tr" from line 103 */; ?>
<tr>
						<?php /* tag "td" from line 104 */; ?>
<td>
							<?php /* tag "table" from line 105 */; ?>
<table style="width:200px;">
								<?php /* tag "tr" from line 106 */; ?>
<tr><?php /* tag "th" from line 106 */; ?>
<th colspan="3"><?php /* tag "span" from line 106 */; ?>
<span class="tableTitle">Today Transaction</span></th></tr>
								<?php /* tag "tr" from line 107 */; ?>
<tr><?php /* tag "td" from line 107 */; ?>
<td colspan="3" align="center"><?php /* tag "span" from line 107 */; ?>
<span style="font-size:9px; font-style:Italic; font-weight:normal;"><?php /* tag "tal:block" from line 107 */; ?>
<?php echo phptal_escape($ctx->todayDate); ?>
</span></td></tr>
								<?php /* tag "tr" from line 108 */; ?>
<tr><?php /* tag "td" from line 108 */; ?>
<td>&nbsp;</td></tr>
								<?php /* tag "tr" from line 109 */; ?>
<tr>
									<?php /* tag "td" from line 110 */; ?>
<td>&nbsp;</td>
									<?php /* tag "td" from line 111 */; ?>
<td>
										<?php /* tag "table" from line 112 */; ?>
<table style="width:75px; text-align:center;">
											<?php /* tag "tr" from line 113 */; ?>
<tr>
												<?php /* tag "th" from line 114 */; ?>
<th>Hour</th><?php /* tag "th" from line 114 */; ?>
<th>Hits</th>
											</tr>
											<?php 
/* tag "tal:block" from line 116 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

												<?php 
/* tag "tal:block" from line 117 */ ;
if ($ctx->repeat->item->index <= 11):  ;
?>

												<?php /* tag "tr" from line 118 */; ?>
<tr><?php /* tag "td" from line 118 */; ?>
<td><?php /* tag "tal:block" from line 118 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 118 */; ?>
<td><?php /* tag "tal:block" from line 118 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
												<?php endif; ?>

											<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

										</table>
									</td>
									<?php /* tag "td" from line 123 */; ?>
<td valign="top">
										<?php /* tag "table" from line 124 */; ?>
<table style="width:75px;text-align:center;">
											<?php /* tag "th" from line 125 */; ?>
<th>Hour</th><?php /* tag "th" from line 125 */; ?>
<th>Hits</th>
											<?php 
/* tag "tal:block" from line 126 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

												<?php 
/* tag "tal:block" from line 127 */ ;
if ($ctx->repeat->item->index >= 12):  ;
?>

												<?php /* tag "tr" from line 128 */; ?>
<tr><?php /* tag "td" from line 128 */; ?>
<td><?php /* tag "tal:block" from line 128 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 128 */; ?>
<td><?php /* tag "tal:block" from line 128 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
												<?php endif; ?>

											<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<?php /* tag "div" from line 140 */; ?>
<div id="bargraph">
		<?php /* tag "br" from line 141 */; ?>
<br/><?php /* tag "br" from line 141 */; ?>
<br/>
			<?php /* tag "table" from line 142 */; ?>
<table style="text-align:left;">
				<?php /* tag "tr" from line 143 */; ?>
<tr>
					<?php /* tag "td" from line 144 */; ?>
<td>
						<?php /* tag "div" from line 145 */; ?>
<div id="followers">
							<?php /* tag "table" from line 146 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 147 */; ?>
<tr>
									<?php /* tag "th" from line 148 */; ?>
<th colspan="2"><?php /* tag "span" from line 148 */; ?>
<span class="tableTitle">Top Followers</span></th>
								</tr>
								<?php /* tag "tr" from line 150 */; ?>
<tr><?php /* tag "td" from line 150 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 151 */; ?>
<tr><?php /* tag "td" from line 151 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 152 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->topFollowers)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 153 */; ?>
<tr>
									<?php /* tag "td" from line 154 */; ?>
<td class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 154 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 155 */; ?>
<td><?php /* tag "div" from line 155 */; ?>
<div class="extras_result"><?php /* tag "p" from line 155 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 155 */; ?>
<span><?php /* tag "tal:block" from line 155 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 155 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 158 */; ?>
<tr><?php /* tag "td" from line 158 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 161 */; ?>
<div id="tenantHits" style="display:none">
							<?php /* tag "table" from line 162 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 163 */; ?>
<tr>
									<?php /* tag "th" from line 164 */; ?>
<th colspan="2"><?php /* tag "span" from line 164 */; ?>
<span class="tableTitle">Tenant Hits</span></th>
								</tr>
								<?php /* tag "tr" from line 166 */; ?>
<tr><?php /* tag "td" from line 166 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 167 */; ?>
<tr><?php /* tag "td" from line 167 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 168 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->totalHitsPerTenant)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 169 */; ?>
<tr>
									<?php /* tag "td" from line 170 */; ?>
<td class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 170 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 171 */; ?>
<td><?php /* tag "div" from line 171 */; ?>
<div class="extras_result"><?php /* tag "p" from line 171 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 171 */; ?>
<span><?php /* tag "tal:block" from line 171 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 171 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 174 */; ?>
<tr><?php /* tag "td" from line 174 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 177 */; ?>
<div id="touchpointHits" style="display:none">
							<?php /* tag "table" from line 178 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 179 */; ?>
<tr>
									<?php /* tag "th" from line 180 */; ?>
<th colspan="2"><?php /* tag "span" from line 180 */; ?>
<span class="tableTitle">Touch Point Hits</span></th>
								</tr>
								<?php /* tag "tr" from line 182 */; ?>
<tr><?php /* tag "td" from line 182 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 183 */; ?>
<tr><?php /* tag "td" from line 183 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 184 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->operatorTotalHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 185 */; ?>
<tr>
									<?php /* tag "td" from line 186 */; ?>
<td class="extras_y-desc" scope="row">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php /* tag "tal:block" from line 186 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 187 */; ?>
<td><?php /* tag "div" from line 187 */; ?>
<div class="extras_result"><?php /* tag "p" from line 187 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 187 */; ?>
<span><?php /* tag "tal:block" from line 187 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 187 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 190 */; ?>
<tr><?php /* tag "td" from line 190 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 193 */; ?>
<div id="feedbackSeverity" style="display:none"></div>
						<?php /* tag "div" from line 194 */; ?>
<div id="feedbackStatus" style="display:none;">
							<?php /* tag "ul" from line 195 */; ?>
<ul style="margin-left:520px;font-size:16px;">
								<?php /* tag "li" from line 196 */; ?>
<li><?php /* tag "span" from line 196 */; ?>
<span class="tableTitle">Status</span></li>
								<?php /* tag "li" from line 197 */; ?>
<li><?php /* tag "span" from line 197 */; ?>
<span class="tableTitle">Severity</span></li>
							</ul>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 204 */; ?>
<div id="monthlyReport">
			<?php /* tag "table" from line 205 */; ?>
<table style="width:880px; margin-left:20px;">
				<?php /* tag "tr" from line 206 */; ?>
<tr><?php /* tag "td" from line 206 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 207 */; ?>
<tr><?php /* tag "th" from line 207 */; ?>
<th colspan="12" align="center"><?php /* tag "span" from line 207 */; ?>
<span class="tableTitle">Total Hits By Month</span></th></tr>
				<?php /* tag "tr" from line 208 */; ?>
<tr><?php /* tag "td" from line 208 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 209 */; ?>
<tr>
					<?php 
/* tag "tal:block" from line 210 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->monthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

						<?php /* tag "td" from line 211 */; ?>
<td valign="top">
							<?php /* tag "table" from line 212 */; ?>
<table>
								<?php /* tag "tr" from line 213 */; ?>
<tr>
									<?php /* tag "th" from line 214 */; ?>
<th colspan="2" align="center"><?php /* tag "a" from line 214 */; ?>
<a href="javascript:;" onclick="global.displayTransContainer($(this).html());"><?php /* tag "tal:block" from line 214 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</a></th>
								</tr>
								<?php 
/* tag "tal:block" from line 216 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

									<?php /* tag "tr" from line 217 */; ?>
<tr>
										<?php /* tag "td" from line 218 */; ?>
<td><?php /* tag "tal:block" from line 218 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'total')); ?>
</td>
										<?php /* tag "td" from line 219 */; ?>
<td><?php /* tag "tal:block" from line 219 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
									</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 222 */; ?>
<tr><?php /* tag "td" from line 222 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</td>
					<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>
			
				</tr>
			</table>
		</div>
	</div>
<?php /* tag "script" from line 230 */; ?>
<script src="/js/dashboard/jquery.js" type="text/javascript"></script>
<?php /* tag "script" from line 231 */; ?>
<script src="/js/dashboard/jqBarGraph.1.1.js" type="text/javascript"></script>
<?php /* tag "script" from line 232 */; ?>
<script>
			
//	$("#feedbackSeverity").jqBarGraph({
//		data: graphBySeverity,
//		width: 550,
//		height: 250,
//		colors: ['#122A47','#185aac','#5fa6fd'],
//		color: '#000000',
//		legends: ['low','medium','high'],
//		legend: true,
//		type:'multi'
//	});
	
	$("#feedbackStatus").jqBarGraph({
		data: graphByStatus,
		width: 550,
		height: 250,
		colors: ['#122A47','#185aac'],
		color: '#000000',
		legends: ['open','closed'],
		legend: true,
		type:'multi'
	});
</script>	
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/dashboard/index.zpt (edit that file instead) */; ?>