<?php 
function tpl_4fd1f129_index__AwqtNWkphlcs6QoYJNtheA(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<?php /* tag "link" from line 2 */; ?>
<link rel="stylesheet" href="/css/calendar/ui-lightness/jquery-ui-1.8.16.custom.css" media="screen"/>
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" href="/css/dashboard/style.css" media="screen"/>
<?php /* tag "link" from line 4 */; ?>
<link rel="stylesheet" href="/css/dashboard/barchart.css" media="screen"/>
<?php /* tag "link" from line 5 */; ?>
<link rel="stylesheet" href="/css/dashboard/chart.css" media="screen"/>

<?php /* tag "script" from line 7 */; ?>
<script src="/js/calendar/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
<!--<script src="/js/calendar/jquery.js" type="text/javascript"></script>-->
<?php /* tag "script" from line 9 */; ?>
<script src="/js/dashboard/global.js" type="text/javascript"></script>

<!--<script src="/js/dashboard/chart.js" type="text/javascript"></script>-->

<?php /* tag "script" from line 13 */; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	  // for each result row..
	  $(".extras_result").each(function() {
	    // get the width of the bar from the span html
	    var length = $(this).find("span").html();
	    length = length.replace("%","");
	    length = length / 100;
	    var lengthPx = 400 * length;
	    
	    $(this).find("p").animate({'width':lengthPx}, 2000, function() {
	      // once the bar animation has finished, fade in the results
	      $(this).find("span").fadeIn(800);
	    });
	  });
	});
</script>


<?php /* tag "script" from line 32 */; ?>
<script>
    $(function() {
        $( "#startDate" ).datepicker();
        $( "#startDate" ).change(function() {
            $( "#startDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });     
	// for extraction form
    $(function() {
        $( "#endDate" ).datepicker();
        $( "#endDate" ).change(function() {
            $( "#endDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });        
    
	var sOperator = $("#severityOperator").val();
	var sObj = jQuery.parseJSON(sOperator);
	var sArr = jQuery.makeArray(sObj);
	
	var jData = $("#severityData").val();
	var obj = jQuery.parseJSON(jData);
	var graphByMonth = jQuery.makeArray(obj);
    
</script>
    
    <?php /* tag "h1" from line 57 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 57 */; ?>
<span style="float:left;">Dashboard</span></h1>
	<?php /* tag "div" from line 58 */; ?>
<div id="folderTab"><?php /* tag "div" from line 58 */; ?>
<div class="folderTabText">Quick View</div></div>
	<?php /* tag "br" from line 59 */; ?>
<br/>
	<?php /* tag "div" from line 60 */; ?>
<div class="dashboardTabMenu">
		<?php /* tag "span" from line 61 */; ?>
<span id="followersLink"><?php /* tag "a" from line 61 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('followers')">Top Follower</a></span>
		<?php /* tag "span" from line 62 */; ?>
<span id="tenantHitsLink"><?php /* tag "a" from line 62 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('tenantHits')">Tenant Hits</a></span>
		<?php /* tag "span" from line 63 */; ?>
<span id="touchpointHitsLink"><?php /* tag "a" from line 63 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('touchpointHits')">Touchpoint Hits</a></span>
		<?php /* tag "span" from line 64 */; ?>
<span id="feedbackLink"><?php /* tag "a" from line 64 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('feedbackSeverity')">Feedback</a></span>
	</div>
<!--	<div class="dimension">
		Dimensions<br/>
		Start Date: <input type="text" id="startDate" name="startDate"/>&nbsp;&nbsp;&nbsp;
		End Date: <input type="text" id="endDate" name="endDate"/>&nbsp;&nbsp;&nbsp;
		<input type="button" value="Query"/>
	</div>-->
	<?php /* tag "tal:block" from line 72 */; ?>

		<?php 
/* tag "input" from line 73 */ ;
if (null !== ($_tmp_1 = ($ctx->severityHits))):  ;
$_tmp_1 = ' value="'.phptal_escape($_tmp_1).'"' ;
else:  ;
$_tmp_1 = '' ;
endif ;
?>
<input type="hidden" name="severityData" id="severityData"<?php echo $_tmp_1 ?>
/>
		<?php 
/* tag "input" from line 74 */ ;
if (null !== ($_tmp_1 = ($ctx->severityOperators))):  ;
$_tmp_1 = ' value="'.phptal_escape($_tmp_1).'"' ;
else:  ;
$_tmp_1 = '' ;
endif ;
?>
<input type="hidden" name="severityOperator" id="severityOperator"<?php echo $_tmp_1 ?>
/>
	
	<?php /* tag "div" from line 76 */; ?>
<div class="reportContainer">
		<?php /* tag "div" from line 77 */; ?>
<div class="tbl">
			<?php 
/* tag "tal:block" from line 78 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->detailMonthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

				<?php 
/* tag "div" from line 79 */ ;
if (null !== ($_tmp_2 = ($ctx->repeat->item->key))):  ;
$_tmp_2 = ' id="'.phptal_escape($_tmp_2).'"' ;
else:  ;
$_tmp_2 = '' ;
endif ;
?>
<div style="display:none;"<?php echo $_tmp_2 ?>
>
					<?php /* tag "table" from line 80 */; ?>
<table style="width:200px; text-align:center;">
						<?php /* tag "tr" from line 81 */; ?>
<tr><?php /* tag "td" from line 81 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 81 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 82 */; ?>
<tr><?php /* tag "td" from line 82 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 82 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 83 */; ?>
<tr><?php /* tag "th" from line 83 */; ?>
<th colspan="3" align="center"><?php /* tag "span" from line 83 */; ?>
<span class="tableTitle" style="font-size:14px;"><?php /* tag "tal:block" from line 83 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</span></th></tr>
						<?php /* tag "tr" from line 84 */; ?>
<tr><?php /* tag "td" from line 84 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 84 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 85 */; ?>
<tr><?php /* tag "th" from line 85 */; ?>
<th>Name</th><?php /* tag "th" from line 85 */; ?>
<th>Hits</th></tr>
						<?php 
/* tag "tal:block" from line 86 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

							<?php /* tag "tr" from line 87 */; ?>
<tr>
								<?php /* tag "td" from line 88 */; ?>
<td width="50%"><?php /* tag "tal:block" from line 88 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'name')); ?>
</td>
								<?php /* tag "td" from line 89 */; ?>
<td width="50%"><?php /* tag "tal:block" from line 89 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
							</tr>
						<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

					</table>
				</div>
			<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

			<?php /* tag "div" from line 95 */; ?>
<div id="todayTransaction">
				<?php /* tag "table" from line 96 */; ?>
<table>
					<?php /* tag "tr" from line 97 */; ?>
<tr>
						<?php /* tag "td" from line 98 */; ?>
<td>
							<?php /* tag "table" from line 99 */; ?>
<table style="width:200px;">
								<?php /* tag "tr" from line 100 */; ?>
<tr><?php /* tag "th" from line 100 */; ?>
<th colspan="3"><?php /* tag "span" from line 100 */; ?>
<span class="tableTitle">Today Transaction</span></th></tr>
								<?php /* tag "tr" from line 101 */; ?>
<tr><?php /* tag "td" from line 101 */; ?>
<td colspan="3" align="center"><?php /* tag "span" from line 101 */; ?>
<span style="font-size:9px; font-style:Italic; font-weight:normal;"><?php /* tag "tal:block" from line 101 */; ?>
<?php echo phptal_escape($ctx->todayDate); ?>
</span></td></tr>
								<?php /* tag "tr" from line 102 */; ?>
<tr><?php /* tag "td" from line 102 */; ?>
<td>&nbsp;</td></tr>
								<?php /* tag "tr" from line 103 */; ?>
<tr>
									<?php /* tag "td" from line 104 */; ?>
<td>&nbsp;</td>
									<?php /* tag "td" from line 105 */; ?>
<td>
										<?php /* tag "table" from line 106 */; ?>
<table style="width:75px; text-align:center;">
											<?php /* tag "tr" from line 107 */; ?>
<tr>
												<?php /* tag "th" from line 108 */; ?>
<th>Hour</th><?php /* tag "th" from line 108 */; ?>
<th>Hits</th>
											</tr>
											<?php 
/* tag "tal:block" from line 110 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

												<?php 
/* tag "tal:block" from line 111 */ ;
if ($ctx->repeat->item->index <= 11):  ;
?>

												<?php /* tag "tr" from line 112 */; ?>
<tr><?php /* tag "td" from line 112 */; ?>
<td><?php /* tag "tal:block" from line 112 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 112 */; ?>
<td><?php /* tag "tal:block" from line 112 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
												<?php endif; ?>

											<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

										</table>
									</td>
									<?php /* tag "td" from line 117 */; ?>
<td valign="top">
										<?php /* tag "table" from line 118 */; ?>
<table style="width:75px;text-align:center;">
											<?php /* tag "th" from line 119 */; ?>
<th>Hour</th><?php /* tag "th" from line 119 */; ?>
<th>Hits</th>
											<?php 
/* tag "tal:block" from line 120 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

												<?php 
/* tag "tal:block" from line 121 */ ;
if ($ctx->repeat->item->index >= 12):  ;
?>

												<?php /* tag "tr" from line 122 */; ?>
<tr><?php /* tag "td" from line 122 */; ?>
<td><?php /* tag "tal:block" from line 122 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 122 */; ?>
<td><?php /* tag "tal:block" from line 122 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
												<?php endif; ?>

											<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<?php /* tag "div" from line 134 */; ?>
<div id="bargraph">
		<?php /* tag "br" from line 135 */; ?>
<br/><?php /* tag "br" from line 135 */; ?>
<br/>
			<?php /* tag "table" from line 136 */; ?>
<table style="text-align:left;">
				<?php /* tag "tr" from line 137 */; ?>
<tr>
					<?php /* tag "td" from line 138 */; ?>
<td>
						<?php /* tag "div" from line 139 */; ?>
<div id="followers">
							<?php /* tag "table" from line 140 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 141 */; ?>
<tr>
									<?php /* tag "th" from line 142 */; ?>
<th colspan="2"><?php /* tag "span" from line 142 */; ?>
<span class="tableTitle">Top Followers</span></th>
								</tr>
								<?php /* tag "tr" from line 144 */; ?>
<tr><?php /* tag "td" from line 144 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 145 */; ?>
<tr><?php /* tag "td" from line 145 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 146 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->topFollowers)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 147 */; ?>
<tr>
									<?php /* tag "td" from line 148 */; ?>
<td class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 148 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 149 */; ?>
<td><?php /* tag "div" from line 149 */; ?>
<div class="extras_result"><?php /* tag "p" from line 149 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 149 */; ?>
<span><?php /* tag "tal:block" from line 149 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 149 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 152 */; ?>
<tr><?php /* tag "td" from line 152 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 155 */; ?>
<div id="tenantHits" style="display:none">
							<?php /* tag "table" from line 156 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 157 */; ?>
<tr>
									<?php /* tag "th" from line 158 */; ?>
<th colspan="2"><?php /* tag "span" from line 158 */; ?>
<span class="tableTitle">Tenant Hits</span></th>
								</tr>
								<?php /* tag "tr" from line 160 */; ?>
<tr><?php /* tag "td" from line 160 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 161 */; ?>
<tr><?php /* tag "td" from line 161 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 162 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->totalHitsPerTenant)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 163 */; ?>
<tr>
									<?php /* tag "td" from line 164 */; ?>
<td class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 164 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 165 */; ?>
<td><?php /* tag "div" from line 165 */; ?>
<div class="extras_result"><?php /* tag "p" from line 165 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 165 */; ?>
<span><?php /* tag "tal:block" from line 165 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 165 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 168 */; ?>
<tr><?php /* tag "td" from line 168 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 171 */; ?>
<div id="touchpointHits" style="display:none">
							<?php /* tag "table" from line 172 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 173 */; ?>
<tr>
									<?php /* tag "th" from line 174 */; ?>
<th colspan="2"><?php /* tag "span" from line 174 */; ?>
<span class="tableTitle">Touch Point Hits</span></th>
								</tr>
								<?php /* tag "tr" from line 176 */; ?>
<tr><?php /* tag "td" from line 176 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 177 */; ?>
<tr><?php /* tag "td" from line 177 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 178 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->operatorTotalHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 179 */; ?>
<tr>
									<?php /* tag "td" from line 180 */; ?>
<td class="extras_y-desc" scope="row">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php /* tag "tal:block" from line 180 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 181 */; ?>
<td><?php /* tag "div" from line 181 */; ?>
<div class="extras_result"><?php /* tag "p" from line 181 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 181 */; ?>
<span><?php /* tag "tal:block" from line 181 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 181 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 184 */; ?>
<tr><?php /* tag "td" from line 184 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 187 */; ?>
<div id="feedbackSeverity" style="display:none">
							<?php /* tag "div" from line 188 */; ?>
<div style="margin-right:-100px;">Status Stats</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 194 */; ?>
<div id="monthlyReport">
			<?php /* tag "table" from line 195 */; ?>
<table style="width:880px; margin-left:20px;">
				<?php /* tag "tr" from line 196 */; ?>
<tr><?php /* tag "td" from line 196 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 197 */; ?>
<tr><?php /* tag "th" from line 197 */; ?>
<th colspan="12" align="center"><?php /* tag "span" from line 197 */; ?>
<span class="tableTitle">Total Hits By Month</span></th></tr>
				<?php /* tag "tr" from line 198 */; ?>
<tr><?php /* tag "td" from line 198 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 199 */; ?>
<tr>
					<?php 
/* tag "tal:block" from line 200 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->monthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

						<?php /* tag "td" from line 201 */; ?>
<td valign="top">
							<?php /* tag "table" from line 202 */; ?>
<table>
								<?php /* tag "tr" from line 203 */; ?>
<tr>
									<?php /* tag "th" from line 204 */; ?>
<th colspan="2" align="center"><?php /* tag "a" from line 204 */; ?>
<a href="javascript:;" onclick="global.displayTransContainer($(this).html());"><?php /* tag "tal:block" from line 204 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</a></th>
								</tr>
								<?php 
/* tag "tal:block" from line 206 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

									<?php /* tag "tr" from line 207 */; ?>
<tr>
										<?php /* tag "td" from line 208 */; ?>
<td><?php /* tag "tal:block" from line 208 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'total')); ?>
</td>
										<?php /* tag "td" from line 209 */; ?>
<td><?php /* tag "tal:block" from line 209 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
									</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 212 */; ?>
<tr><?php /* tag "td" from line 212 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</td>
					<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>
			
				</tr>
			</table>
		</div>
	</div>
<?php /* tag "script" from line 220 */; ?>
<script src="/js/dashboard/jquery.js" type="text/javascript"></script>
<?php /* tag "script" from line 221 */; ?>
<script src="/js/dashboard/jqBarGraph.1.1.js" type="text/javascript"></script>
<?php /* tag "script" from line 222 */; ?>
<script>
			
	$("#feedbackSeverity").jqBarGraph({
		data: graphByMonth,
		width: 600,
		height: 250,
		colors: ['#122A47','#185aac','#5fa6fd'],
		color: '#000000',
		legends: ['low','medium','high'],
		legend: true,
		type:'multi',
		postfix: '%'
	});
</script>	
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/dashboard/index.zpt (edit that file instead) */; ?>