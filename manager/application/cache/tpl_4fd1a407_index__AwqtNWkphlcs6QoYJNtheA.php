<?php 
function tpl_4fd1a407_index__AwqtNWkphlcs6QoYJNtheA(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<?php /* tag "link" from line 2 */; ?>
<link rel="stylesheet" href="/css/calendar/ui-lightness/jquery-ui-1.8.16.custom.css" media="screen"/>
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" href="/css/dashboard/style.css" media="screen"/>
<?php /* tag "link" from line 4 */; ?>
<link rel="stylesheet" href="/css/dashboard/barchart.css" media="screen"/>
<?php /* tag "link" from line 5 */; ?>
<link rel="stylesheet" href="/css/dashboard/chart.css" media="screen"/>

<?php /* tag "script" from line 7 */; ?>
<script src="/js/calendar/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
<!--<script src="/js/calendar/jquery.js" type="text/javascript"></script>-->
<?php /* tag "script" from line 9 */; ?>
<script src="/js/dashboard/global.js" type="text/javascript"></script>
<?php /* tag "script" from line 10 */; ?>
<script src="/js/dashboard/jqBarGraph.1.1.js" type="text/javascript"></script>
<!--<script src="/js/dashboard/chart.js" type="text/javascript"></script>-->

<?php /* tag "script" from line 13 */; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	  // for each result row..
	  $(".extras_result").each(function() {
	    // get the width of the bar from the span html
	    var length = $(this).find("span").html();
	    length = length.replace("%","");
	    length = length / 100;
	    var lengthPx = 400 * length;
	    
	    $(this).find("p").animate({'width':lengthPx}, 2000, function() {
	      // once the bar animation has finished, fade in the results
	      $(this).find("span").fadeIn(800);
	    });
	  });
	});
</script>


<?php /* tag "script" from line 32 */; ?>
<script>
    $(function() {
        $( "#startDate" ).datepicker();
        $( "#startDate" ).change(function() {
            $( "#startDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });     
	// for extraction form
    $(function() {
        $( "#endDate" ).datepicker();
        $( "#endDate" ).change(function() {
            $( "#endDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });        
</script>
    
    <?php /* tag "h1" from line 48 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 48 */; ?>
<span style="float:left;">Dashboard</span></h1>
	<?php /* tag "div" from line 49 */; ?>
<div id="folderTab"><?php /* tag "div" from line 49 */; ?>
<div class="folderTabText">Quick View</div></div>
	<?php /* tag "br" from line 50 */; ?>
<br/>
	<?php /* tag "div" from line 51 */; ?>
<div class="dashboardTabMenu">
		<?php /* tag "span" from line 52 */; ?>
<span id="followersLink"><?php /* tag "a" from line 52 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('followers')">Top Follower</a></span>
		<?php /* tag "span" from line 53 */; ?>
<span id="tenantHitsLink"><?php /* tag "a" from line 53 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('tenantHits')">Tenant Hits</a></span>
		<?php /* tag "span" from line 54 */; ?>
<span id="touchpointHitsLink"><?php /* tag "a" from line 54 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('touchpointHits')">Touchpoint Hits</a></span>
		<?php /* tag "span" from line 55 */; ?>
<span id="feedbackLink"><?php /* tag "a" from line 55 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('feedback')">Feedback</a></span>
	</div>
<!--	<div class="dimension">
		Dimensions<br/>
		Start Date: <input type="text" id="startDate" name="startDate"/>&nbsp;&nbsp;&nbsp;
		End Date: <input type="text" id="endDate" name="endDate"/>&nbsp;&nbsp;&nbsp;
		<input type="button" value="Query"/>
	</div>-->
	<?php /* tag "div" from line 63 */; ?>
<div class="reportContainer">
		<?php /* tag "div" from line 64 */; ?>
<div class="tbl">
			<?php 
/* tag "tal:block" from line 65 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->detailMonthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

				<?php 
/* tag "div" from line 66 */ ;
if (null !== ($_tmp_2 = ($ctx->repeat->item->key))):  ;
$_tmp_2 = ' id="'.phptal_escape($_tmp_2).'"' ;
else:  ;
$_tmp_2 = '' ;
endif ;
?>
<div style="display:none;"<?php echo $_tmp_2 ?>
>
					<?php /* tag "table" from line 67 */; ?>
<table style="width:200px; text-align:center;">
						<?php /* tag "tr" from line 68 */; ?>
<tr><?php /* tag "td" from line 68 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 68 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 69 */; ?>
<tr><?php /* tag "td" from line 69 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 69 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 70 */; ?>
<tr><?php /* tag "th" from line 70 */; ?>
<th colspan="3" align="center"><?php /* tag "span" from line 70 */; ?>
<span class="tableTitle" style="font-size:14px;"><?php /* tag "tal:block" from line 70 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</span></th></tr>
						<?php /* tag "tr" from line 71 */; ?>
<tr><?php /* tag "td" from line 71 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 71 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 72 */; ?>
<tr><?php /* tag "th" from line 72 */; ?>
<th>Name</th><?php /* tag "th" from line 72 */; ?>
<th>Hits</th></tr>
						<?php 
/* tag "tal:block" from line 73 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

							<?php /* tag "tr" from line 74 */; ?>
<tr>
								<?php /* tag "td" from line 75 */; ?>
<td width="50%"><?php /* tag "tal:block" from line 75 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'name')); ?>
</td>
								<?php /* tag "td" from line 76 */; ?>
<td width="50%"><?php /* tag "tal:block" from line 76 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
							</tr>
						<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

					</table>
				</div>
			<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

			<?php /* tag "div" from line 82 */; ?>
<div id="todayTransaction">
				<?php /* tag "table" from line 83 */; ?>
<table>
					<?php /* tag "tr" from line 84 */; ?>
<tr>
						<?php /* tag "td" from line 85 */; ?>
<td>
							<?php /* tag "table" from line 86 */; ?>
<table style="width:200px;">
								<?php /* tag "tr" from line 87 */; ?>
<tr><?php /* tag "th" from line 87 */; ?>
<th colspan="3"><?php /* tag "span" from line 87 */; ?>
<span class="tableTitle">Today Transaction</span></th></tr>
								<?php /* tag "tr" from line 88 */; ?>
<tr><?php /* tag "td" from line 88 */; ?>
<td colspan="3" align="center"><?php /* tag "span" from line 88 */; ?>
<span style="font-size:9px; font-style:Italic; font-weight:normal;"><?php /* tag "tal:block" from line 88 */; ?>
<?php echo phptal_escape($ctx->todayDate); ?>
</span></td></tr>
								<?php /* tag "tr" from line 89 */; ?>
<tr><?php /* tag "td" from line 89 */; ?>
<td>&nbsp;</td></tr>
								<?php /* tag "tr" from line 90 */; ?>
<tr>
									<?php /* tag "td" from line 91 */; ?>
<td>&nbsp;</td>
									<?php /* tag "td" from line 92 */; ?>
<td>
										<?php /* tag "table" from line 93 */; ?>
<table style="width:75px; text-align:center;">
											<?php /* tag "tr" from line 94 */; ?>
<tr>
												<?php /* tag "th" from line 95 */; ?>
<th>Hour</th><?php /* tag "th" from line 95 */; ?>
<th>Hits</th>
											</tr>
											<?php 
/* tag "tal:block" from line 97 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

												<?php 
/* tag "tal:block" from line 98 */ ;
if ($ctx->repeat->item->index <= 11):  ;
?>

												<?php /* tag "tr" from line 99 */; ?>
<tr><?php /* tag "td" from line 99 */; ?>
<td><?php /* tag "tal:block" from line 99 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 99 */; ?>
<td><?php /* tag "tal:block" from line 99 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
												<?php endif; ?>

											<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

										</table>
									</td>
									<?php /* tag "td" from line 104 */; ?>
<td valign="top">
										<?php /* tag "table" from line 105 */; ?>
<table style="width:75px;text-align:center;">
											<?php /* tag "th" from line 106 */; ?>
<th>Hour</th><?php /* tag "th" from line 106 */; ?>
<th>Hits</th>
											<?php 
/* tag "tal:block" from line 107 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

												<?php 
/* tag "tal:block" from line 108 */ ;
if ($ctx->repeat->item->index >= 12):  ;
?>

												<?php /* tag "tr" from line 109 */; ?>
<tr><?php /* tag "td" from line 109 */; ?>
<td><?php /* tag "tal:block" from line 109 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 109 */; ?>
<td><?php /* tag "tal:block" from line 109 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
												<?php endif; ?>

											<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<?php /* tag "div" from line 121 */; ?>
<div id="bargraph">
		<?php /* tag "br" from line 122 */; ?>
<br/><?php /* tag "br" from line 122 */; ?>
<br/>
			<?php /* tag "table" from line 123 */; ?>
<table style="text-align:left;">
				<?php /* tag "tr" from line 124 */; ?>
<tr>
					<?php /* tag "td" from line 125 */; ?>
<td>
						<?php /* tag "div" from line 126 */; ?>
<div id="followers">
							<?php /* tag "table" from line 127 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 128 */; ?>
<tr>
									<?php /* tag "th" from line 129 */; ?>
<th colspan="2"><?php /* tag "span" from line 129 */; ?>
<span class="tableTitle">Top Followers</span></th>
								</tr>
								<?php /* tag "tr" from line 131 */; ?>
<tr><?php /* tag "td" from line 131 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 132 */; ?>
<tr><?php /* tag "td" from line 132 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 133 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->topFollowers)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 134 */; ?>
<tr>
									<?php /* tag "td" from line 135 */; ?>
<td class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 135 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 136 */; ?>
<td><?php /* tag "div" from line 136 */; ?>
<div class="extras_result"><?php /* tag "p" from line 136 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 136 */; ?>
<span><?php /* tag "tal:block" from line 136 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 136 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 139 */; ?>
<tr><?php /* tag "td" from line 139 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 142 */; ?>
<div id="tenantHits" style="display:none">
							<?php /* tag "table" from line 143 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 144 */; ?>
<tr>
									<?php /* tag "th" from line 145 */; ?>
<th colspan="2"><?php /* tag "span" from line 145 */; ?>
<span class="tableTitle">Tenant Hits</span></th>
								</tr>
								<?php /* tag "tr" from line 147 */; ?>
<tr><?php /* tag "td" from line 147 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 148 */; ?>
<tr><?php /* tag "td" from line 148 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 149 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->totalHitsPerTenant)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 150 */; ?>
<tr>
									<?php /* tag "td" from line 151 */; ?>
<td class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 151 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 152 */; ?>
<td><?php /* tag "div" from line 152 */; ?>
<div class="extras_result"><?php /* tag "p" from line 152 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 152 */; ?>
<span><?php /* tag "tal:block" from line 152 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 152 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 155 */; ?>
<tr><?php /* tag "td" from line 155 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 158 */; ?>
<div id="touchpointHits" style="display:none">
							<?php /* tag "table" from line 159 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 160 */; ?>
<tr>
									<?php /* tag "th" from line 161 */; ?>
<th colspan="2"><?php /* tag "span" from line 161 */; ?>
<span class="tableTitle">Touch Point Hits</span></th>
								</tr>
								<?php /* tag "tr" from line 163 */; ?>
<tr><?php /* tag "td" from line 163 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 164 */; ?>
<tr><?php /* tag "td" from line 164 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 165 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->operatorTotalHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 166 */; ?>
<tr>
									<?php /* tag "td" from line 167 */; ?>
<td class="extras_y-desc" scope="row">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php /* tag "tal:block" from line 167 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 168 */; ?>
<td><?php /* tag "div" from line 168 */; ?>
<div class="extras_result"><?php /* tag "p" from line 168 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 168 */; ?>
<span><?php /* tag "tal:block" from line 168 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 168 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 171 */; ?>
<tr><?php /* tag "td" from line 171 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 174 */; ?>
<div id="feedback" style="display:none">
							<?php /* tag "table" from line 175 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 176 */; ?>
<tr>
									<?php /* tag "th" from line 177 */; ?>
<th><?php /* tag "span" from line 177 */; ?>
<span class="tableTitle" style="text-decoration:underline;"><?php /* tag "a" from line 177 */; ?>
<a href="javascript:;">Severity</a></span></th>
									<?php /* tag "th" from line 178 */; ?>
<th>&nbsp;/&nbsp;</th>
																		<?php /* tag "th" from line 179 */; ?>
<th><?php /* tag "span" from line 179 */; ?>
<span class="tableTitle" style="text-decoration:underline;"><?php /* tag "a" from line 179 */; ?>
<a href="javascript:;">Status</a></span></th>
								</tr>
							</table>
							<?php /* tag "div" from line 182 */; ?>
<div id="severityHits">
								<?php /* tag "table" from line 183 */; ?>
<table>
									<?php /* tag "tr" from line 184 */; ?>
<tr>
										<?php /* tag "td" from line 185 */; ?>
<td></td>
									</tr>
								</table>
							</div>
		<?php /* tag "script" from line 189 */; ?>
<script>
			graphByMonth = new Array(
			    [[14,54,26],'2007'],
			    [[8,48,38],'2008'],
			    [[4,36,57],'2009']
			); 
		
			$("#hello").jqBarGraph({
				data: graphByMonth,
				width: 500,
				colors: ['#122A47','#1B3E69'],
				color: '#1A2944',
				barSpace: 5,
				title: '<?php /* tag "h3" from line 202 */; ?>
<h3>Number of visitors per month<?php /* tag "br" from line 202 */; ?>
<br/><?php /* tag "small" from line 202 */; ?>
<small>simple bar graph</small></h3>'
			});
		</script>
							<?php /* tag "div" from line 205 */; ?>
<div id="status">
								<?php /* tag "table" from line 206 */; ?>
<table>
									<?php /* tag "tr" from line 207 */; ?>
<tr>
										<?php /* tag "td" from line 208 */; ?>
<td></td>
									</tr>
								</table>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 217 */; ?>
<div id="monthlyReport">
			<?php /* tag "table" from line 218 */; ?>
<table style="width:880px; margin-left:20px;">
				<?php /* tag "tr" from line 219 */; ?>
<tr><?php /* tag "td" from line 219 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 220 */; ?>
<tr><?php /* tag "th" from line 220 */; ?>
<th colspan="12" align="center"><?php /* tag "span" from line 220 */; ?>
<span class="tableTitle">Total Hits By Month</span></th></tr>
				<?php /* tag "tr" from line 221 */; ?>
<tr><?php /* tag "td" from line 221 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 222 */; ?>
<tr>
					<?php 
/* tag "tal:block" from line 223 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->monthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

						<?php /* tag "td" from line 224 */; ?>
<td valign="top">
							<?php /* tag "table" from line 225 */; ?>
<table>
								<?php /* tag "tr" from line 226 */; ?>
<tr>
									<?php /* tag "th" from line 227 */; ?>
<th colspan="2" align="center"><?php /* tag "a" from line 227 */; ?>
<a href="javascript:;" onclick="global.displayTransContainer($(this).html());"><?php /* tag "tal:block" from line 227 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</a></th>
								</tr>
								<?php 
/* tag "tal:block" from line 229 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

									<?php /* tag "tr" from line 230 */; ?>
<tr>
										<?php /* tag "td" from line 231 */; ?>
<td><?php /* tag "tal:block" from line 231 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'total')); ?>
</td>
										<?php /* tag "td" from line 232 */; ?>
<td><?php /* tag "tal:block" from line 232 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
									</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 235 */; ?>
<tr><?php /* tag "td" from line 235 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</td>
					<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>
			
				</tr>
			</table>
		</div>
	</div>
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/dashboard/index.zpt (edit that file instead) */; ?>