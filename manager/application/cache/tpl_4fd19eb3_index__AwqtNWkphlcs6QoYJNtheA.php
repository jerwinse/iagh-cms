<?php 
function tpl_4fd19eb3_index__AwqtNWkphlcs6QoYJNtheA(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<?php /* tag "link" from line 2 */; ?>
<link rel="stylesheet" href="/css/calendar/ui-lightness/jquery-ui-1.8.16.custom.css" media="screen"/>
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" href="/css/dashboard/style.css" media="screen"/>
<?php /* tag "link" from line 4 */; ?>
<link rel="stylesheet" href="/css/dashboard/barchart.css" media="screen"/>
<?php /* tag "link" from line 5 */; ?>
<link rel="stylesheet" href="/css/dashboard/chart.css" media="screen"/>

<?php /* tag "script" from line 7 */; ?>
<script src="/js/calendar/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
<?php /* tag "script" from line 8 */; ?>
<script src="/js/dashboard/jqBarGraph.1.1.js" type="text/javascript"></script>
<?php /* tag "script" from line 9 */; ?>
<script src="/js/dashboard/global.js" type="text/javascript"></script>
<!--<script src="/js/dashboard/chart.js" type="text/javascript"></script>-->

<?php /* tag "script" from line 12 */; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	  // for each result row..
	  $(".extras_result").each(function() {
	    // get the width of the bar from the span html
	    var length = $(this).find("span").html();
	    length = length.replace("%","");
	    length = length / 100;
	    var lengthPx = 400 * length;
	    
	    $(this).find("p").animate({'width':lengthPx}, 2000, function() {
	      // once the bar animation has finished, fade in the results
	      $(this).find("span").fadeIn(800);
	    });
	  });
	});
</script>


<?php /* tag "script" from line 31 */; ?>
<script>
    $(function() {
        $( "#startDate" ).datepicker();
        $( "#startDate" ).change(function() {
            $( "#startDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });     
	// for extraction form
    $(function() {
        $( "#endDate" ).datepicker();
        $( "#endDate" ).change(function() {
            $( "#endDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });        
</script>

<?php /* tag "script" from line 47 */; ?>
<script>
	var arrayOfData = new Array(
	    [[14,54,26],'2007'],
	    [[8,48,38],'2008'],
	    [[4,36,57],'2009']
	); 

//	$('#severityHits').jqBarGraph({ data: arrayOfData }); 
//	data: arrayOfData, // array of data for your graph
//	title: false, // title of your graph, accept HTML
//	barSpace: 10, // this is default space between bars in pixels
//	width: 400, // default width of your graph
//	height: 200, //default height of your graph
//	color: '#000000', // if you don't send colors for your data this will be default bars color
//	colors: false, // array of colors that will be used for your bars and legends
//	lbl: '', // if there is no label in your array
//	sort: false, // sort your data before displaying graph, you can sort as 'asc' or 'desc'
//	position: 'bottom', // position of your bars, can be 'bottom' or 'top'. 'top' doesn't work for multi type
//	prefix: '', // text that will be shown before every label
//	postfix: '', // text that will be shown after every label
//	animate: true, // if you don't need animated appearance change to false
//	speed: 2, // speed of animation in seconds
//	legendWidth: 100, // width of your legend box
//	legend: false, // if you want legend change to true
//	legends: false, // array for legend. for simple graph type legend will be extracted from labels if you don't set this
//	type: false, // for multi array data default graph type is stacked, you can change to 'multi' for multi bar type
//	showValues: true, // you can use this for multi and stacked type and it will show values of every bar part
//	showValuesColor: '#fff' // color of font for values
	
</script>
    
    <?php /* tag "h1" from line 78 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 78 */; ?>
<span style="float:left;">Dashboard</span></h1>
	<?php /* tag "div" from line 79 */; ?>
<div id="folderTab"><?php /* tag "div" from line 79 */; ?>
<div class="folderTabText">Quick View</div></div>
	<?php /* tag "br" from line 80 */; ?>
<br/>
	<?php /* tag "div" from line 81 */; ?>
<div class="dashboardTabMenu">
		<?php /* tag "span" from line 82 */; ?>
<span id="followersLink"><?php /* tag "a" from line 82 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('followers')">Top Follower</a></span>
		<?php /* tag "span" from line 83 */; ?>
<span id="tenantHitsLink"><?php /* tag "a" from line 83 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('tenantHits')">Tenant Hits</a></span>
		<?php /* tag "span" from line 84 */; ?>
<span id="touchpointHitsLink"><?php /* tag "a" from line 84 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('touchpointHits')">Touchpoint Hits</a></span>
		<?php /* tag "span" from line 85 */; ?>
<span id="feedbackLink"><?php /* tag "a" from line 85 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('feedback')">Feedback</a></span>
	</div>
<!--	<div class="dimension">
		Dimensions<br/>
		Start Date: <input type="text" id="startDate" name="startDate"/>&nbsp;&nbsp;&nbsp;
		End Date: <input type="text" id="endDate" name="endDate"/>&nbsp;&nbsp;&nbsp;
		<input type="button" value="Query"/>
	</div>-->
	<?php /* tag "div" from line 93 */; ?>
<div class="reportContainer">
		<?php /* tag "div" from line 94 */; ?>
<div class="tbl">
			<?php 
/* tag "tal:block" from line 95 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->detailMonthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

				<?php 
/* tag "div" from line 96 */ ;
if (null !== ($_tmp_2 = ($ctx->repeat->item->key))):  ;
$_tmp_2 = ' id="'.phptal_escape($_tmp_2).'"' ;
else:  ;
$_tmp_2 = '' ;
endif ;
?>
<div style="display:none;"<?php echo $_tmp_2 ?>
>
					<?php /* tag "table" from line 97 */; ?>
<table style="width:200px; text-align:center;">
						<?php /* tag "tr" from line 98 */; ?>
<tr><?php /* tag "td" from line 98 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 98 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 99 */; ?>
<tr><?php /* tag "td" from line 99 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 99 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 100 */; ?>
<tr><?php /* tag "th" from line 100 */; ?>
<th colspan="3" align="center"><?php /* tag "span" from line 100 */; ?>
<span class="tableTitle" style="font-size:14px;"><?php /* tag "tal:block" from line 100 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</span></th></tr>
						<?php /* tag "tr" from line 101 */; ?>
<tr><?php /* tag "td" from line 101 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 101 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 102 */; ?>
<tr><?php /* tag "th" from line 102 */; ?>
<th>Name</th><?php /* tag "th" from line 102 */; ?>
<th>Hits</th></tr>
						<?php 
/* tag "tal:block" from line 103 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

							<?php /* tag "tr" from line 104 */; ?>
<tr>
								<?php /* tag "td" from line 105 */; ?>
<td width="50%"><?php /* tag "tal:block" from line 105 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'name')); ?>
</td>
								<?php /* tag "td" from line 106 */; ?>
<td width="50%"><?php /* tag "tal:block" from line 106 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
							</tr>
						<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

					</table>
				</div>
			<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

			<?php /* tag "div" from line 112 */; ?>
<div id="todayTransaction">
				<?php /* tag "table" from line 113 */; ?>
<table>
					<?php /* tag "tr" from line 114 */; ?>
<tr>
						<?php /* tag "td" from line 115 */; ?>
<td>
							<?php /* tag "table" from line 116 */; ?>
<table style="width:200px;">
								<?php /* tag "tr" from line 117 */; ?>
<tr><?php /* tag "th" from line 117 */; ?>
<th colspan="3"><?php /* tag "span" from line 117 */; ?>
<span class="tableTitle">Today Transaction</span></th></tr>
								<?php /* tag "tr" from line 118 */; ?>
<tr><?php /* tag "td" from line 118 */; ?>
<td colspan="3" align="center"><?php /* tag "span" from line 118 */; ?>
<span style="font-size:9px; font-style:Italic; font-weight:normal;"><?php /* tag "tal:block" from line 118 */; ?>
<?php echo phptal_escape($ctx->todayDate); ?>
</span></td></tr>
								<?php /* tag "tr" from line 119 */; ?>
<tr><?php /* tag "td" from line 119 */; ?>
<td>&nbsp;</td></tr>
								<?php /* tag "tr" from line 120 */; ?>
<tr>
									<?php /* tag "td" from line 121 */; ?>
<td>&nbsp;</td>
									<?php /* tag "td" from line 122 */; ?>
<td>
										<?php /* tag "table" from line 123 */; ?>
<table style="width:75px; text-align:center;">
											<?php /* tag "tr" from line 124 */; ?>
<tr>
												<?php /* tag "th" from line 125 */; ?>
<th>Hour</th><?php /* tag "th" from line 125 */; ?>
<th>Hits</th>
											</tr>
											<?php 
/* tag "tal:block" from line 127 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

												<?php 
/* tag "tal:block" from line 128 */ ;
if ($ctx->repeat->item->index <= 11):  ;
?>

												<?php /* tag "tr" from line 129 */; ?>
<tr><?php /* tag "td" from line 129 */; ?>
<td><?php /* tag "tal:block" from line 129 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 129 */; ?>
<td><?php /* tag "tal:block" from line 129 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
												<?php endif; ?>

											<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

										</table>
									</td>
									<?php /* tag "td" from line 134 */; ?>
<td valign="top">
										<?php /* tag "table" from line 135 */; ?>
<table style="width:75px;text-align:center;">
											<?php /* tag "th" from line 136 */; ?>
<th>Hour</th><?php /* tag "th" from line 136 */; ?>
<th>Hits</th>
											<?php 
/* tag "tal:block" from line 137 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

												<?php 
/* tag "tal:block" from line 138 */ ;
if ($ctx->repeat->item->index >= 12):  ;
?>

												<?php /* tag "tr" from line 139 */; ?>
<tr><?php /* tag "td" from line 139 */; ?>
<td><?php /* tag "tal:block" from line 139 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 139 */; ?>
<td><?php /* tag "tal:block" from line 139 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
												<?php endif; ?>

											<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<?php /* tag "div" from line 151 */; ?>
<div id="bargraph">
		<?php /* tag "br" from line 152 */; ?>
<br/><?php /* tag "br" from line 152 */; ?>
<br/>
			<?php /* tag "table" from line 153 */; ?>
<table style="text-align:left;">
				<?php /* tag "tr" from line 154 */; ?>
<tr>
					<?php /* tag "td" from line 155 */; ?>
<td>
						<?php /* tag "div" from line 156 */; ?>
<div id="followers">
							<?php /* tag "table" from line 157 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 158 */; ?>
<tr>
									<?php /* tag "th" from line 159 */; ?>
<th colspan="2"><?php /* tag "span" from line 159 */; ?>
<span class="tableTitle">Top Followers</span></th>
								</tr>
								<?php /* tag "tr" from line 161 */; ?>
<tr><?php /* tag "td" from line 161 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 162 */; ?>
<tr><?php /* tag "td" from line 162 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 163 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->topFollowers)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 164 */; ?>
<tr>
									<?php /* tag "td" from line 165 */; ?>
<td class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 165 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 166 */; ?>
<td><?php /* tag "div" from line 166 */; ?>
<div class="extras_result"><?php /* tag "p" from line 166 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 166 */; ?>
<span><?php /* tag "tal:block" from line 166 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 166 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 169 */; ?>
<tr><?php /* tag "td" from line 169 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 172 */; ?>
<div id="tenantHits" style="display:none">
							<?php /* tag "table" from line 173 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 174 */; ?>
<tr>
									<?php /* tag "th" from line 175 */; ?>
<th colspan="2"><?php /* tag "span" from line 175 */; ?>
<span class="tableTitle">Tenant Hits</span></th>
								</tr>
								<?php /* tag "tr" from line 177 */; ?>
<tr><?php /* tag "td" from line 177 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 178 */; ?>
<tr><?php /* tag "td" from line 178 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 179 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->totalHitsPerTenant)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 180 */; ?>
<tr>
									<?php /* tag "td" from line 181 */; ?>
<td class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 181 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 182 */; ?>
<td><?php /* tag "div" from line 182 */; ?>
<div class="extras_result"><?php /* tag "p" from line 182 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 182 */; ?>
<span><?php /* tag "tal:block" from line 182 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 182 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 185 */; ?>
<tr><?php /* tag "td" from line 185 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 188 */; ?>
<div id="touchpointHits" style="display:none">
							<?php /* tag "table" from line 189 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 190 */; ?>
<tr>
									<?php /* tag "th" from line 191 */; ?>
<th colspan="2"><?php /* tag "span" from line 191 */; ?>
<span class="tableTitle">Touch Point Hits</span></th>
								</tr>
								<?php /* tag "tr" from line 193 */; ?>
<tr><?php /* tag "td" from line 193 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 194 */; ?>
<tr><?php /* tag "td" from line 194 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 195 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->operatorTotalHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 196 */; ?>
<tr>
									<?php /* tag "td" from line 197 */; ?>
<td class="extras_y-desc" scope="row">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php /* tag "tal:block" from line 197 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 198 */; ?>
<td><?php /* tag "div" from line 198 */; ?>
<div class="extras_result"><?php /* tag "p" from line 198 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 198 */; ?>
<span><?php /* tag "tal:block" from line 198 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 198 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 201 */; ?>
<tr><?php /* tag "td" from line 201 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 204 */; ?>
<div id="feedback" style="display:none">
							<?php /* tag "table" from line 205 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 206 */; ?>
<tr>
									<?php /* tag "th" from line 207 */; ?>
<th><?php /* tag "span" from line 207 */; ?>
<span class="tableTitle" style="text-decoration:underline;"><?php /* tag "a" from line 207 */; ?>
<a href="javascript:;">Severity</a></span></th>
									<?php /* tag "th" from line 208 */; ?>
<th>&nbsp;/&nbsp;</th>
																		<?php /* tag "th" from line 209 */; ?>
<th><?php /* tag "span" from line 209 */; ?>
<span class="tableTitle" style="text-decoration:underline;"><?php /* tag "a" from line 209 */; ?>
<a href="javascript:;">Status</a></span></th>
								</tr>
							</table>
							<?php /* tag "div" from line 212 */; ?>
<div id="severityHits">
								<?php /* tag "table" from line 213 */; ?>
<table>
									<?php /* tag "tr" from line 214 */; ?>
<tr>
										<?php /* tag "td" from line 215 */; ?>
<td></td>
									</tr>
								</table>
							</div>
							<?php /* tag "div" from line 219 */; ?>
<div id="status">
								<?php /* tag "table" from line 220 */; ?>
<table>
									<?php /* tag "tr" from line 221 */; ?>
<tr>
										<?php /* tag "td" from line 222 */; ?>
<td></td>
									</tr>
								</table>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 231 */; ?>
<div id="monthlyReport">
			<?php /* tag "table" from line 232 */; ?>
<table style="width:880px; margin-left:20px;">
				<?php /* tag "tr" from line 233 */; ?>
<tr><?php /* tag "td" from line 233 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 234 */; ?>
<tr><?php /* tag "th" from line 234 */; ?>
<th colspan="12" align="center"><?php /* tag "span" from line 234 */; ?>
<span class="tableTitle">Total Hits By Month</span></th></tr>
				<?php /* tag "tr" from line 235 */; ?>
<tr><?php /* tag "td" from line 235 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 236 */; ?>
<tr>
					<?php 
/* tag "tal:block" from line 237 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->monthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

						<?php /* tag "td" from line 238 */; ?>
<td valign="top">
							<?php /* tag "table" from line 239 */; ?>
<table>
								<?php /* tag "tr" from line 240 */; ?>
<tr>
									<?php /* tag "th" from line 241 */; ?>
<th colspan="2" align="center"><?php /* tag "a" from line 241 */; ?>
<a href="javascript:;" onclick="global.displayTransContainer($(this).html());"><?php /* tag "tal:block" from line 241 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</a></th>
								</tr>
								<?php 
/* tag "tal:block" from line 243 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

									<?php /* tag "tr" from line 244 */; ?>
<tr>
										<?php /* tag "td" from line 245 */; ?>
<td><?php /* tag "tal:block" from line 245 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'total')); ?>
</td>
										<?php /* tag "td" from line 246 */; ?>
<td><?php /* tag "tal:block" from line 246 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
									</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 249 */; ?>
<tr><?php /* tag "td" from line 249 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</td>
					<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>
			
				</tr>
			</table>
		</div>
	</div>
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/dashboard/index.zpt (edit that file instead) */; ?>