<?php 
function tpl_4fd1eb1e_index__AwqtNWkphlcs6QoYJNtheA(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<?php /* tag "link" from line 2 */; ?>
<link rel="stylesheet" href="/css/calendar/ui-lightness/jquery-ui-1.8.16.custom.css" media="screen"/>
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" href="/css/dashboard/style.css" media="screen"/>
<?php /* tag "link" from line 4 */; ?>
<link rel="stylesheet" href="/css/dashboard/barchart.css" media="screen"/>
<?php /* tag "link" from line 5 */; ?>
<link rel="stylesheet" href="/css/dashboard/chart.css" media="screen"/>

<?php /* tag "script" from line 7 */; ?>
<script src="/js/calendar/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
<!--<script src="/js/calendar/jquery.js" type="text/javascript"></script>-->
<?php /* tag "script" from line 9 */; ?>
<script src="/js/dashboard/global.js" type="text/javascript"></script>

<!--<script src="/js/dashboard/chart.js" type="text/javascript"></script>-->

<?php /* tag "script" from line 13 */; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	  // for each result row..
	  $(".extras_result").each(function() {
	    // get the width of the bar from the span html
	    var length = $(this).find("span").html();
	    length = length.replace("%","");
	    length = length / 100;
	    var lengthPx = 400 * length;
	    
	    $(this).find("p").animate({'width':lengthPx}, 2000, function() {
	      // once the bar animation has finished, fade in the results
	      $(this).find("span").fadeIn(800);
	    });
	  });
	});
</script>


<?php /* tag "script" from line 32 */; ?>
<script>
    $(function() {
        $( "#startDate" ).datepicker();
        $( "#startDate" ).change(function() {
            $( "#startDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });     
	// for extraction form
    $(function() {
        $( "#endDate" ).datepicker();
        $( "#endDate" ).change(function() {
            $( "#endDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });        
</script>
    
    <?php /* tag "h1" from line 48 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 48 */; ?>
<span style="float:left;">Dashboard</span></h1>
	<?php /* tag "div" from line 49 */; ?>
<div id="folderTab"><?php /* tag "div" from line 49 */; ?>
<div class="folderTabText">Quick View</div></div>
	<?php /* tag "br" from line 50 */; ?>
<br/>
	<?php /* tag "div" from line 51 */; ?>
<div class="dashboardTabMenu">
		<?php /* tag "span" from line 52 */; ?>
<span id="followersLink"><?php /* tag "a" from line 52 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('followers')">Top Follower</a></span>
		<?php /* tag "span" from line 53 */; ?>
<span id="tenantHitsLink"><?php /* tag "a" from line 53 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('tenantHits')">Tenant Hits</a></span>
		<?php /* tag "span" from line 54 */; ?>
<span id="touchpointHitsLink"><?php /* tag "a" from line 54 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('touchpointHits')">Touchpoint Hits</a></span>
		<?php /* tag "span" from line 55 */; ?>
<span id="feedbackLink"><?php /* tag "a" from line 55 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('feedback')">Feedback</a></span>
	</div>
<!--	<div class="dimension">
		Dimensions<br/>
		Start Date: <input type="text" id="startDate" name="startDate"/>&nbsp;&nbsp;&nbsp;
		End Date: <input type="text" id="endDate" name="endDate"/>&nbsp;&nbsp;&nbsp;
		<input type="button" value="Query"/>
	</div>-->
	<?php /* tag "tal:block" from line 63 */; ?>

		<?php 
/* tag "input" from line 64 */ ;
if (null !== ($_tmp_1 = ($ctx->severityHits))):  ;
$_tmp_1 = ' value="'.phptal_escape($_tmp_1).'"' ;
else:  ;
$_tmp_1 = '' ;
endif ;
?>
<input type="hidden" name="severityData" id="severityData"<?php echo $_tmp_1 ?>
/>
	
	<?php /* tag "div" from line 66 */; ?>
<div class="reportContainer">
		<?php /* tag "div" from line 67 */; ?>
<div class="tbl">
			<?php 
/* tag "tal:block" from line 68 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->detailMonthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

				<?php 
/* tag "div" from line 69 */ ;
if (null !== ($_tmp_2 = ($ctx->repeat->item->key))):  ;
$_tmp_2 = ' id="'.phptal_escape($_tmp_2).'"' ;
else:  ;
$_tmp_2 = '' ;
endif ;
?>
<div style="display:none;"<?php echo $_tmp_2 ?>
>
					<?php /* tag "table" from line 70 */; ?>
<table style="width:200px; text-align:center;">
						<?php /* tag "tr" from line 71 */; ?>
<tr><?php /* tag "td" from line 71 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 71 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 72 */; ?>
<tr><?php /* tag "td" from line 72 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 72 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 73 */; ?>
<tr><?php /* tag "th" from line 73 */; ?>
<th colspan="3" align="center"><?php /* tag "span" from line 73 */; ?>
<span class="tableTitle" style="font-size:14px;"><?php /* tag "tal:block" from line 73 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</span></th></tr>
						<?php /* tag "tr" from line 74 */; ?>
<tr><?php /* tag "td" from line 74 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 74 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 75 */; ?>
<tr><?php /* tag "th" from line 75 */; ?>
<th>Name</th><?php /* tag "th" from line 75 */; ?>
<th>Hits</th></tr>
						<?php 
/* tag "tal:block" from line 76 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

							<?php /* tag "tr" from line 77 */; ?>
<tr>
								<?php /* tag "td" from line 78 */; ?>
<td width="50%"><?php /* tag "tal:block" from line 78 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'name')); ?>
</td>
								<?php /* tag "td" from line 79 */; ?>
<td width="50%"><?php /* tag "tal:block" from line 79 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
							</tr>
						<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

					</table>
				</div>
			<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

			<?php /* tag "div" from line 85 */; ?>
<div id="todayTransaction">
				<?php /* tag "table" from line 86 */; ?>
<table>
					<?php /* tag "tr" from line 87 */; ?>
<tr>
						<?php /* tag "td" from line 88 */; ?>
<td>
							<?php /* tag "table" from line 89 */; ?>
<table style="width:200px;">
								<?php /* tag "tr" from line 90 */; ?>
<tr><?php /* tag "th" from line 90 */; ?>
<th colspan="3"><?php /* tag "span" from line 90 */; ?>
<span class="tableTitle">Today Transaction</span></th></tr>
								<?php /* tag "tr" from line 91 */; ?>
<tr><?php /* tag "td" from line 91 */; ?>
<td colspan="3" align="center"><?php /* tag "span" from line 91 */; ?>
<span style="font-size:9px; font-style:Italic; font-weight:normal;"><?php /* tag "tal:block" from line 91 */; ?>
<?php echo phptal_escape($ctx->todayDate); ?>
</span></td></tr>
								<?php /* tag "tr" from line 92 */; ?>
<tr><?php /* tag "td" from line 92 */; ?>
<td>&nbsp;</td></tr>
								<?php /* tag "tr" from line 93 */; ?>
<tr>
									<?php /* tag "td" from line 94 */; ?>
<td>&nbsp;</td>
									<?php /* tag "td" from line 95 */; ?>
<td>
										<?php /* tag "table" from line 96 */; ?>
<table style="width:75px; text-align:center;">
											<?php /* tag "tr" from line 97 */; ?>
<tr>
												<?php /* tag "th" from line 98 */; ?>
<th>Hour</th><?php /* tag "th" from line 98 */; ?>
<th>Hits</th>
											</tr>
											<?php 
/* tag "tal:block" from line 100 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

												<?php 
/* tag "tal:block" from line 101 */ ;
if ($ctx->repeat->item->index <= 11):  ;
?>

												<?php /* tag "tr" from line 102 */; ?>
<tr><?php /* tag "td" from line 102 */; ?>
<td><?php /* tag "tal:block" from line 102 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 102 */; ?>
<td><?php /* tag "tal:block" from line 102 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
												<?php endif; ?>

											<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

										</table>
									</td>
									<?php /* tag "td" from line 107 */; ?>
<td valign="top">
										<?php /* tag "table" from line 108 */; ?>
<table style="width:75px;text-align:center;">
											<?php /* tag "th" from line 109 */; ?>
<th>Hour</th><?php /* tag "th" from line 109 */; ?>
<th>Hits</th>
											<?php 
/* tag "tal:block" from line 110 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

												<?php 
/* tag "tal:block" from line 111 */ ;
if ($ctx->repeat->item->index >= 12):  ;
?>

												<?php /* tag "tr" from line 112 */; ?>
<tr><?php /* tag "td" from line 112 */; ?>
<td><?php /* tag "tal:block" from line 112 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 112 */; ?>
<td><?php /* tag "tal:block" from line 112 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
												<?php endif; ?>

											<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<?php /* tag "div" from line 124 */; ?>
<div id="bargraph">
		<?php /* tag "br" from line 125 */; ?>
<br/><?php /* tag "br" from line 125 */; ?>
<br/>
			<?php /* tag "table" from line 126 */; ?>
<table style="text-align:left;">
				<?php /* tag "tr" from line 127 */; ?>
<tr>
					<?php /* tag "td" from line 128 */; ?>
<td>
						<?php /* tag "div" from line 129 */; ?>
<div id="followers">
							<?php /* tag "table" from line 130 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 131 */; ?>
<tr>
									<?php /* tag "th" from line 132 */; ?>
<th colspan="2"><?php /* tag "span" from line 132 */; ?>
<span class="tableTitle">Top Followers</span></th>
								</tr>
								<?php /* tag "tr" from line 134 */; ?>
<tr><?php /* tag "td" from line 134 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 135 */; ?>
<tr><?php /* tag "td" from line 135 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 136 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->topFollowers)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 137 */; ?>
<tr>
									<?php /* tag "td" from line 138 */; ?>
<td class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 138 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 139 */; ?>
<td><?php /* tag "div" from line 139 */; ?>
<div class="extras_result"><?php /* tag "p" from line 139 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 139 */; ?>
<span><?php /* tag "tal:block" from line 139 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 139 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 142 */; ?>
<tr><?php /* tag "td" from line 142 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 145 */; ?>
<div id="tenantHits" style="display:none">
							<?php /* tag "table" from line 146 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 147 */; ?>
<tr>
									<?php /* tag "th" from line 148 */; ?>
<th colspan="2"><?php /* tag "span" from line 148 */; ?>
<span class="tableTitle">Tenant Hits</span></th>
								</tr>
								<?php /* tag "tr" from line 150 */; ?>
<tr><?php /* tag "td" from line 150 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 151 */; ?>
<tr><?php /* tag "td" from line 151 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 152 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->totalHitsPerTenant)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 153 */; ?>
<tr>
									<?php /* tag "td" from line 154 */; ?>
<td class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 154 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 155 */; ?>
<td><?php /* tag "div" from line 155 */; ?>
<div class="extras_result"><?php /* tag "p" from line 155 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 155 */; ?>
<span><?php /* tag "tal:block" from line 155 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 155 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 158 */; ?>
<tr><?php /* tag "td" from line 158 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 161 */; ?>
<div id="touchpointHits" style="display:none">
							<?php /* tag "table" from line 162 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 163 */; ?>
<tr>
									<?php /* tag "th" from line 164 */; ?>
<th colspan="2"><?php /* tag "span" from line 164 */; ?>
<span class="tableTitle">Touch Point Hits</span></th>
								</tr>
								<?php /* tag "tr" from line 166 */; ?>
<tr><?php /* tag "td" from line 166 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 167 */; ?>
<tr><?php /* tag "td" from line 167 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 168 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->operatorTotalHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 169 */; ?>
<tr>
									<?php /* tag "td" from line 170 */; ?>
<td class="extras_y-desc" scope="row">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php /* tag "tal:block" from line 170 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 171 */; ?>
<td><?php /* tag "div" from line 171 */; ?>
<div class="extras_result"><?php /* tag "p" from line 171 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 171 */; ?>
<span><?php /* tag "tal:block" from line 171 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 171 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 174 */; ?>
<tr><?php /* tag "td" from line 174 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 177 */; ?>
<div id="feedback" style="display:none">
							<?php /* tag "br" from line 178 */; ?>
<br/><?php /* tag "br" from line 178 */; ?>
<br/><?php /* tag "br" from line 178 */; ?>
<br/>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 184 */; ?>
<div id="monthlyReport">
			<?php /* tag "table" from line 185 */; ?>
<table style="width:880px; margin-left:20px;">
				<?php /* tag "tr" from line 186 */; ?>
<tr><?php /* tag "td" from line 186 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 187 */; ?>
<tr><?php /* tag "th" from line 187 */; ?>
<th colspan="12" align="center"><?php /* tag "span" from line 187 */; ?>
<span class="tableTitle">Total Hits By Month</span></th></tr>
				<?php /* tag "tr" from line 188 */; ?>
<tr><?php /* tag "td" from line 188 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 189 */; ?>
<tr>
					<?php 
/* tag "tal:block" from line 190 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->monthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

						<?php /* tag "td" from line 191 */; ?>
<td valign="top">
							<?php /* tag "table" from line 192 */; ?>
<table>
								<?php /* tag "tr" from line 193 */; ?>
<tr>
									<?php /* tag "th" from line 194 */; ?>
<th colspan="2" align="center"><?php /* tag "a" from line 194 */; ?>
<a href="javascript:;" onclick="global.displayTransContainer($(this).html());"><?php /* tag "tal:block" from line 194 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</a></th>
								</tr>
								<?php 
/* tag "tal:block" from line 196 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

									<?php /* tag "tr" from line 197 */; ?>
<tr>
										<?php /* tag "td" from line 198 */; ?>
<td><?php /* tag "tal:block" from line 198 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'total')); ?>
</td>
										<?php /* tag "td" from line 199 */; ?>
<td><?php /* tag "tal:block" from line 199 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
									</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 202 */; ?>
<tr><?php /* tag "td" from line 202 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</td>
					<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>
			
				</tr>
			</table>
		</div>
	</div>
<?php /* tag "script" from line 210 */; ?>
<script src="/js/dashboard/jquery.js" type="text/javascript"></script>
<?php /* tag "script" from line 211 */; ?>
<script src="/js/dashboard/jqBarGraph.1.1.js" type="text/javascript"></script>
<?php /* tag "script" from line 212 */; ?>
<script>

//	var jData = $("#severityData").val();
	var j = '[[[2,0,0],"globe"],[[5,0,1],"twitter"],[[14,1,0],"xmpp"],[[4,0,0],"ym"]]';
	var obj = jQuery.parseJSON(j);
	var graphByMonth = jQuery.makeArray(obj);
	
	graphByMonth = new Array(
	    [[23,0,0],'2007'],
	    [[8,0,0],'2008'],
	    [[4,1,0],'2009']
	); 
		
	$("#feedback").jqBarGraph({
		data: graphByMonth,
		width: 300,
		height: 250,
		colors: ['#122A47','#1B3E69','#72808E'],
		color: '#1A2944',
		legends: ['ads','leads','google ads'],
		legend: true,
		type:'multi',
		barSpace: 3
	});
</script>	
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/dashboard/index.zpt (edit that file instead) */; ?>