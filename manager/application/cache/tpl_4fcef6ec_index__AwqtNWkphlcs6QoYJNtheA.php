<?php 
function tpl_4fcef6ec_index__AwqtNWkphlcs6QoYJNtheA(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<?php /* tag "link" from line 2 */; ?>
<link rel="stylesheet" href="/css/calendar/ui-lightness/jquery-ui-1.8.16.custom.css" media="screen"/>
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" href="/css/dashboard/style.css" media="screen"/>
<?php /* tag "link" from line 4 */; ?>
<link rel="stylesheet" href="/css/dashboard/barchart.css" media="screen"/>
<?php /* tag "link" from line 5 */; ?>
<link rel="stylesheet" href="/css/dashboard/chart.css" media="screen"/>

<?php /* tag "script" from line 7 */; ?>
<script src="/js/calendar/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
<!--<script src="/js/dashboard/chart.js" type="text/javascript"></script>-->

<?php /* tag "script" from line 10 */; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	  // for each result row..
	  $(".extras_result").each(function() {
	    // get the width of the bar from the span html
	    var length = $(this).find("span").html();
	    length = length.replace("%","");
	    length = length / 100;
	    var lengthPx = 400 * length;
	    
	    $(this).find("p").animate({'width':lengthPx}, 2000, function() {
	      // once the bar animation has finished, fade in the results
	      $(this).find("span").fadeIn(800);
	    });
	  });
	});
</script>


<?php /* tag "script" from line 29 */; ?>
<script>
    $(function() {
        $( "#startDate" ).datepicker();
        $( "#startDate" ).change(function() {
            $( "#startDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });     
	// for extraction form
    $(function() {
        $( "#endDate" ).datepicker();
        $( "#endDate" ).change(function() {
            $( "#endDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });        
</script>		
    
    <?php /* tag "h1" from line 45 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 45 */; ?>
<span style="float:left;">Dashboard</span></h1>
	<?php /* tag "div" from line 46 */; ?>
<div id="folderTab"><?php /* tag "div" from line 46 */; ?>
<div class="folderTabText">Quick View</div></div>
	<?php /* tag "br" from line 47 */; ?>
<br/>
<!--	<div class="dashboardTabMenu">
		<span><a href="admin#/dashboard/index">Home</a></span>
		<span><a class="selected" href="#">Analytics</a></span>
		<span><a href="#">Menu 3</a></span>
		<span><a href="#">Menu 4</a></span>
	</div>
	<div class="dimension">
		Dimensions<br/>
		Start Date: <input type="text" id="startDate" name="startDate"/>&nbsp;&nbsp;&nbsp;
		End Date: <input type="text" id="endDate" name="endDate"/>&nbsp;&nbsp;&nbsp;
		<input type="button" value="Query"/>
	</div>-->
	<?php /* tag "div" from line 60 */; ?>
<div class="reportContainer">
		<?php /* tag "div" from line 61 */; ?>
<div class="tbl">
			<?php /* tag "table" from line 62 */; ?>
<table>
				<?php /* tag "tr" from line 63 */; ?>
<tr>
					<?php /* tag "td" from line 64 */; ?>
<td>
						<?php /* tag "table" from line 65 */; ?>
<table style="width:200px;">
							<?php /* tag "tr" from line 66 */; ?>
<tr><?php /* tag "td" from line 66 */; ?>
<td>&nbsp;</td></tr>
							<?php /* tag "tr" from line 67 */; ?>
<tr><?php /* tag "td" from line 67 */; ?>
<td>&nbsp;</td></tr>
							<?php /* tag "tr" from line 68 */; ?>
<tr><?php /* tag "th" from line 68 */; ?>
<th colspan="3"><?php /* tag "span" from line 68 */; ?>
<span class="tableTitle">Today Transaction</span></th></tr>
							<?php /* tag "tr" from line 69 */; ?>
<tr><?php /* tag "td" from line 69 */; ?>
<td colspan="3" align="center"><?php /* tag "span" from line 69 */; ?>
<span style="font-size:9px; font-style:Italic; font-weight:normal;"><?php /* tag "tal:block" from line 69 */; ?>
<?php echo phptal_escape($ctx->todayDate); ?>
</span></td></tr>
							<?php /* tag "tr" from line 70 */; ?>
<tr><?php /* tag "td" from line 70 */; ?>
<td>&nbsp;</td></tr>
							<?php /* tag "tr" from line 71 */; ?>
<tr>
								<?php /* tag "td" from line 72 */; ?>
<td>&nbsp;</td>
								<?php /* tag "td" from line 73 */; ?>
<td>
									<?php /* tag "table" from line 74 */; ?>
<table style="width:75px; text-align:center;">
										<?php /* tag "tr" from line 75 */; ?>
<tr>
											<?php /* tag "th" from line 76 */; ?>
<th>Hour</th><?php /* tag "th" from line 76 */; ?>
<th>Hits</th>
										</tr>
										<?php 
/* tag "tal:block" from line 78 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

											<?php 
/* tag "tal:block" from line 79 */ ;
if ($ctx->repeat->item->index <= 11):  ;
?>

											<?php /* tag "tr" from line 80 */; ?>
<tr><?php /* tag "td" from line 80 */; ?>
<td><?php /* tag "tal:block" from line 80 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 80 */; ?>
<td><?php /* tag "tal:block" from line 80 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
											<?php endif; ?>

										<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

									</table>
								</td>
								<?php /* tag "td" from line 85 */; ?>
<td valign="top">
									<?php /* tag "table" from line 86 */; ?>
<table style="width:75px;text-align:center;">
										<?php /* tag "th" from line 87 */; ?>
<th>Hour</th><?php /* tag "th" from line 87 */; ?>
<th>Hits</th>
										<?php 
/* tag "tal:block" from line 88 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

											<?php 
/* tag "tal:block" from line 89 */ ;
if ($ctx->repeat->item->index >= 12):  ;
?>

											<?php /* tag "tr" from line 90 */; ?>
<tr><?php /* tag "td" from line 90 */; ?>
<td><?php /* tag "tal:block" from line 90 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 90 */; ?>
<td><?php /* tag "tal:block" from line 90 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
											<?php endif; ?>

										<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 101 */; ?>
<div id="bargraph">
		<?php /* tag "br" from line 102 */; ?>
<br/>
			<?php /* tag "table" from line 103 */; ?>
<table style="text-align:left;">
				<?php /* tag "tr" from line 104 */; ?>
<tr>
					<?php /* tag "td" from line 105 */; ?>
<td>
						<?php /* tag "table" from line 106 */; ?>
<table class="extras_table" align="center">
							<?php /* tag "tr" from line 107 */; ?>
<tr>
								<?php /* tag "th" from line 108 */; ?>
<th colspan="2"><?php /* tag "span" from line 108 */; ?>
<span class="tableTitle">Top Followers</span></th>
							</tr>
							<?php /* tag "tr" from line 110 */; ?>
<tr><?php /* tag "td" from line 110 */; ?>
<td></td></tr>
						
							<?php 
/* tag "tal:block" from line 112 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->topFollowers)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

							<?php /* tag "tr" from line 113 */; ?>
<tr>
								<?php /* tag "th" from line 114 */; ?>
<th class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 114 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</th>
								<?php /* tag "td" from line 115 */; ?>
<td><?php /* tag "div" from line 115 */; ?>
<div class="extras_result"><?php /* tag "p" from line 115 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 115 */; ?>
<span><?php /* tag "tal:block" from line 115 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 115 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
							</tr>
							<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

							<?php /* tag "tr" from line 118 */; ?>
<tr><?php /* tag "td" from line 118 */; ?>
<td>&nbsp;</td></tr>
						</table>
						<?php /* tag "table" from line 120 */; ?>
<table class="extras_table" align="center">
							<?php /* tag "tr" from line 121 */; ?>
<tr>
								<?php /* tag "th" from line 122 */; ?>
<th colspan="2"><?php /* tag "span" from line 122 */; ?>
<span class="tableTitle">Tenant Total Hits</span></th>
							</tr>
							<?php 
/* tag "tal:block" from line 124 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->totalHitsPerTenant)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

							<?php /* tag "tr" from line 125 */; ?>
<tr>
								<?php /* tag "th" from line 126 */; ?>
<th class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 126 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</th>
								<?php /* tag "td" from line 127 */; ?>
<td><?php /* tag "div" from line 127 */; ?>
<div class="extras_result"><?php /* tag "p" from line 127 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 127 */; ?>
<span><?php /* tag "tal:block" from line 127 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 127 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
							</tr>
							<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

							<?php /* tag "tr" from line 130 */; ?>
<tr><?php /* tag "td" from line 130 */; ?>
<td>&nbsp;</td></tr>
						</table>
						<?php /* tag "table" from line 132 */; ?>
<table class="extras_table" align="center">
							<?php /* tag "tr" from line 133 */; ?>
<tr>
								<?php /* tag "th" from line 134 */; ?>
<th colspan="2"><?php /* tag "span" from line 134 */; ?>
<span class="tableTitle">Touch Point Total Hits</span></th>
							</tr>
							<?php 
/* tag "tal:block" from line 136 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->operatorTotalHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

							<?php /* tag "tr" from line 137 */; ?>
<tr>
								<?php /* tag "th" from line 138 */; ?>
<th class="extras_y-desc" scope="row">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php /* tag "tal:block" from line 138 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</th>
								<?php /* tag "td" from line 139 */; ?>
<td><?php /* tag "div" from line 139 */; ?>
<div class="extras_result"><?php /* tag "p" from line 139 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 139 */; ?>
<span><?php /* tag "tal:block" from line 139 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 139 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
							</tr>
							<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

							<?php /* tag "tr" from line 142 */; ?>
<tr><?php /* tag "td" from line 142 */; ?>
<td>&nbsp;</td></tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 148 */; ?>
<div id="monthlyReport">
			<?php /* tag "table" from line 149 */; ?>
<table>
				<?php 
/* tag "tal:block" from line 150 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->monthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

					<?php 
/* tag "tal:block" from line 151 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->h as $ctx->h): ;
?>

						<?php /* tag "tr" from line 152 */; ?>
<tr>
							<?php /* tag "td" from line 153 */; ?>
<td><?php /* tag "tal:block" from line 153 */; ?>
<?php echo phptal_escape($ctx->repeat->item->index); ?>
</td>
							<?php /* tag "td" from line 154 */; ?>
<td><?php /* tag "tal:block" from line 154 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'name')); ?>
</td>
							<?php /* tag "td" from line 155 */; ?>
<td><?php /* tag "tal:block" from line 155 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
						</tr>
					<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

				<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>
				
			</table>
		</div>
	</div>
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/dashboard/index.zpt (edit that file instead) */; ?>