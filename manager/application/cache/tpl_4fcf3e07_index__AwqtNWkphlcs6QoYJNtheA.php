<?php 
function tpl_4fcf3e07_index__AwqtNWkphlcs6QoYJNtheA(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<?php /* tag "link" from line 2 */; ?>
<link rel="stylesheet" href="/css/calendar/ui-lightness/jquery-ui-1.8.16.custom.css" media="screen"/>
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" href="/css/dashboard/style.css" media="screen"/>
<?php /* tag "link" from line 4 */; ?>
<link rel="stylesheet" href="/css/dashboard/barchart.css" media="screen"/>
<?php /* tag "link" from line 5 */; ?>
<link rel="stylesheet" href="/css/dashboard/chart.css" media="screen"/>

<?php /* tag "script" from line 7 */; ?>
<script src="/js/calendar/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
<!--<script src="/js/dashboard/chart.js" type="text/javascript"></script>-->

<?php /* tag "script" from line 10 */; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	  // for each result row..
	  $(".extras_result").each(function() {
	    // get the width of the bar from the span html
	    var length = $(this).find("span").html();
	    length = length.replace("%","");
	    length = length / 100;
	    var lengthPx = 400 * length;
	    
	    $(this).find("p").animate({'width':lengthPx}, 2000, function() {
	      // once the bar animation has finished, fade in the results
	      $(this).find("span").fadeIn(800);
	    });
	  });
	});
</script>


<?php /* tag "script" from line 29 */; ?>
<script>
    $(function() {
        $( "#startDate" ).datepicker();
        $( "#startDate" ).change(function() {
            $( "#startDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });     
	// for extraction form
    $(function() {
        $( "#endDate" ).datepicker();
        $( "#endDate" ).change(function() {
            $( "#endDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });        
</script>		
    
    <?php /* tag "h1" from line 45 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 45 */; ?>
<span style="float:left;">Dashboard</span></h1>
	<?php /* tag "div" from line 46 */; ?>
<div id="folderTab"><?php /* tag "div" from line 46 */; ?>
<div class="folderTabText">Quick View</div></div>
	<?php /* tag "br" from line 47 */; ?>
<br/>
<!--	<div class="dashboardTabMenu">
		<span><a href="admin#/dashboard/index">Home</a></span>
		<span><a class="selected" href="#">Analytics</a></span>
		<span><a href="#">Menu 3</a></span>
		<span><a href="#">Menu 4</a></span>
	</div>
	<div class="dimension">
		Dimensions<br/>
		Start Date: <input type="text" id="startDate" name="startDate"/>&nbsp;&nbsp;&nbsp;
		End Date: <input type="text" id="endDate" name="endDate"/>&nbsp;&nbsp;&nbsp;
		<input type="button" value="Query"/>
	</div>-->
	<?php /* tag "div" from line 60 */; ?>
<div class="reportContainer">
		<?php 
/* tag "tal:block" from line 61 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->detailMonthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

			<?php 
/* tag "div" from line 62 */ ;
if (null !== ($_tmp_2 = ($ctx->repeat->item->key))):  ;
$_tmp_2 = ' id="'.phptal_escape($_tmp_2).'"' ;
else:  ;
$_tmp_2 = '' ;
endif ;
?>
<div style="display:block;"<?php echo $_tmp_2 ?>
>
				<?php /* tag "table" from line 63 */; ?>
<table style="width:200px; text-align:center;">
					<?php /* tag "tr" from line 64 */; ?>
<tr>
						<?php /* tag "th" from line 65 */; ?>
<th colspan="2"><?php /* tag "span" from line 65 */; ?>
<span class="tableTitle"><?php /* tag "tal:block" from line 65 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</span></th>
					</tr>
					<?php /* tag "tr" from line 67 */; ?>
<tr>
						<?php /* tag "th" from line 68 */; ?>
<th>Name</th><?php /* tag "th" from line 68 */; ?>
<th>Hits</th>
					</tr>
					<?php 
/* tag "tal:block" from line 70 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

							<?php /* tag "tr" from line 71 */; ?>
<tr><?php /* tag "td" from line 71 */; ?>
<td><?php /* tag "tal:block" from line 71 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'name')); ?>
</td><?php /* tag "td" from line 71 */; ?>
<td><?php /* tag "tal:block" from line 71 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td></tr>
					<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

				</table>
			</div>
		<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

		<?php /* tag "div" from line 76 */; ?>
<div class="tbl">
			<?php /* tag "table" from line 77 */; ?>
<table>
				<?php /* tag "tr" from line 78 */; ?>
<tr>
					<?php /* tag "td" from line 79 */; ?>
<td>
						<?php /* tag "table" from line 80 */; ?>
<table style="width:200px;">
							<?php /* tag "tr" from line 81 */; ?>
<tr><?php /* tag "td" from line 81 */; ?>
<td>&nbsp;</td></tr>
							<?php /* tag "tr" from line 82 */; ?>
<tr><?php /* tag "td" from line 82 */; ?>
<td>&nbsp;</td></tr>
							<?php /* tag "tr" from line 83 */; ?>
<tr><?php /* tag "th" from line 83 */; ?>
<th colspan="3"><?php /* tag "span" from line 83 */; ?>
<span class="tableTitle">Today Transaction</span></th></tr>
							<?php /* tag "tr" from line 84 */; ?>
<tr><?php /* tag "td" from line 84 */; ?>
<td colspan="3" align="center"><?php /* tag "span" from line 84 */; ?>
<span style="font-size:9px; font-style:Italic; font-weight:normal;"><?php /* tag "tal:block" from line 84 */; ?>
<?php echo phptal_escape($ctx->todayDate); ?>
</span></td></tr>
							<?php /* tag "tr" from line 85 */; ?>
<tr><?php /* tag "td" from line 85 */; ?>
<td>&nbsp;</td></tr>
							<?php /* tag "tr" from line 86 */; ?>
<tr>
								<?php /* tag "td" from line 87 */; ?>
<td>&nbsp;</td>
								<?php /* tag "td" from line 88 */; ?>
<td>
									<?php /* tag "table" from line 89 */; ?>
<table style="width:75px; text-align:center;">
										<?php /* tag "tr" from line 90 */; ?>
<tr>
											<?php /* tag "th" from line 91 */; ?>
<th>Hour</th><?php /* tag "th" from line 91 */; ?>
<th>Hits</th>
										</tr>
										<?php 
/* tag "tal:block" from line 93 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

											<?php 
/* tag "tal:block" from line 94 */ ;
if ($ctx->repeat->item->index <= 11):  ;
?>

											<?php /* tag "tr" from line 95 */; ?>
<tr><?php /* tag "td" from line 95 */; ?>
<td><?php /* tag "tal:block" from line 95 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 95 */; ?>
<td><?php /* tag "tal:block" from line 95 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
											<?php endif; ?>

										<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

									</table>
								</td>
								<?php /* tag "td" from line 100 */; ?>
<td valign="top">
									<?php /* tag "table" from line 101 */; ?>
<table style="width:75px;text-align:center;">
										<?php /* tag "th" from line 102 */; ?>
<th>Hour</th><?php /* tag "th" from line 102 */; ?>
<th>Hits</th>
										<?php 
/* tag "tal:block" from line 103 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

											<?php 
/* tag "tal:block" from line 104 */ ;
if ($ctx->repeat->item->index >= 12):  ;
?>

											<?php /* tag "tr" from line 105 */; ?>
<tr><?php /* tag "td" from line 105 */; ?>
<td><?php /* tag "tal:block" from line 105 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 105 */; ?>
<td><?php /* tag "tal:block" from line 105 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
											<?php endif; ?>

										<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 116 */; ?>
<div id="bargraph">
		<?php /* tag "br" from line 117 */; ?>
<br/>
			<?php /* tag "table" from line 118 */; ?>
<table style="text-align:left;">
				<?php /* tag "tr" from line 119 */; ?>
<tr>
					<?php /* tag "td" from line 120 */; ?>
<td>
						<?php /* tag "table" from line 121 */; ?>
<table class="extras_table" align="center">
							<?php /* tag "tr" from line 122 */; ?>
<tr>
								<?php /* tag "th" from line 123 */; ?>
<th colspan="2"><?php /* tag "span" from line 123 */; ?>
<span class="tableTitle">Top Followers</span></th>
							</tr>
							<?php /* tag "tr" from line 125 */; ?>
<tr><?php /* tag "td" from line 125 */; ?>
<td></td></tr>
						
							<?php 
/* tag "tal:block" from line 127 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->topFollowers)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

							<?php /* tag "tr" from line 128 */; ?>
<tr>
								<?php /* tag "th" from line 129 */; ?>
<th class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 129 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</th>
								<?php /* tag "td" from line 130 */; ?>
<td><?php /* tag "div" from line 130 */; ?>
<div class="extras_result"><?php /* tag "p" from line 130 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 130 */; ?>
<span><?php /* tag "tal:block" from line 130 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 130 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
							</tr>
							<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

							<?php /* tag "tr" from line 133 */; ?>
<tr><?php /* tag "td" from line 133 */; ?>
<td>&nbsp;</td></tr>
						</table>
						<?php /* tag "table" from line 135 */; ?>
<table class="extras_table" align="center">
							<?php /* tag "tr" from line 136 */; ?>
<tr>
								<?php /* tag "th" from line 137 */; ?>
<th colspan="2"><?php /* tag "span" from line 137 */; ?>
<span class="tableTitle">Tenant Total Hits</span></th>
							</tr>
							<?php 
/* tag "tal:block" from line 139 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->totalHitsPerTenant)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

							<?php /* tag "tr" from line 140 */; ?>
<tr>
								<?php /* tag "th" from line 141 */; ?>
<th class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 141 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</th>
								<?php /* tag "td" from line 142 */; ?>
<td><?php /* tag "div" from line 142 */; ?>
<div class="extras_result"><?php /* tag "p" from line 142 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 142 */; ?>
<span><?php /* tag "tal:block" from line 142 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 142 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
							</tr>
							<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

							<?php /* tag "tr" from line 145 */; ?>
<tr><?php /* tag "td" from line 145 */; ?>
<td>&nbsp;</td></tr>
						</table>
						<?php /* tag "table" from line 147 */; ?>
<table class="extras_table" align="center">
							<?php /* tag "tr" from line 148 */; ?>
<tr>
								<?php /* tag "th" from line 149 */; ?>
<th colspan="2"><?php /* tag "span" from line 149 */; ?>
<span class="tableTitle">Touch Point Total Hits</span></th>
							</tr>
							<?php 
/* tag "tal:block" from line 151 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->operatorTotalHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

							<?php /* tag "tr" from line 152 */; ?>
<tr>
								<?php /* tag "th" from line 153 */; ?>
<th class="extras_y-desc" scope="row">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php /* tag "tal:block" from line 153 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</th>
								<?php /* tag "td" from line 154 */; ?>
<td><?php /* tag "div" from line 154 */; ?>
<div class="extras_result"><?php /* tag "p" from line 154 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 154 */; ?>
<span><?php /* tag "tal:block" from line 154 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 154 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
							</tr>
							<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

							<?php /* tag "tr" from line 157 */; ?>
<tr><?php /* tag "td" from line 157 */; ?>
<td>&nbsp;</td></tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 163 */; ?>
<div id="monthlyReport">
			<?php /* tag "table" from line 164 */; ?>
<table style="width:880px; margin-left:20px;">
				<?php /* tag "tr" from line 165 */; ?>
<tr><?php /* tag "td" from line 165 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 166 */; ?>
<tr><?php /* tag "th" from line 166 */; ?>
<th colspan="12" align="center"><?php /* tag "span" from line 166 */; ?>
<span class="tableTitle">Total Hits By Month</span></th></tr>
				<?php /* tag "tr" from line 167 */; ?>
<tr><?php /* tag "td" from line 167 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 168 */; ?>
<tr>
					<?php 
/* tag "tal:block" from line 169 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->monthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

						<?php /* tag "td" from line 170 */; ?>
<td valign="top">
							<?php /* tag "table" from line 171 */; ?>
<table>
								<?php /* tag "tr" from line 172 */; ?>
<tr>
									<?php /* tag "th" from line 173 */; ?>
<th colspan="2" align="center"><?php /* tag "a" from line 173 */; ?>
<a href="javascript:;" onclick="alert($(this).html());"><?php /* tag "tal:block" from line 173 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</a></th>
								</tr>
								<?php 
/* tag "tal:block" from line 175 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

									<?php /* tag "tr" from line 176 */; ?>
<tr>
										<?php /* tag "td" from line 177 */; ?>
<td><?php /* tag "tal:block" from line 177 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'total')); ?>
</td>
										<?php /* tag "td" from line 178 */; ?>
<td><?php /* tag "tal:block" from line 178 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
									</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 181 */; ?>
<tr><?php /* tag "td" from line 181 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</td>
					<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>
			
				</tr>
			</table>
		</div>
	</div>
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/dashboard/index.zpt (edit that file instead) */; ?>