<?php 
function tpl_4fceb3d1_index__AwqtNWkphlcs6QoYJNtheA(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<?php /* tag "link" from line 2 */; ?>
<link rel="stylesheet" href="/css/calendar/ui-lightness/jquery-ui-1.8.16.custom.css" media="screen"/>
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" href="/css/dashboard/style.css" media="screen"/>
<?php /* tag "link" from line 4 */; ?>
<link rel="stylesheet" href="/css/dashboard/barchart.css" media="screen"/>
<?php /* tag "link" from line 5 */; ?>
<link rel="stylesheet" href="/css/dashboard/chart.css" media="screen"/>

<?php /* tag "script" from line 7 */; ?>
<script src="/js/calendar/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
<!--<script src="/js/dashboard/chart.js" type="text/javascript"></script>-->

<?php /* tag "script" from line 10 */; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	  // for each result row..
	  $(".extras_result").each(function() {
	    // get the width of the bar from the span html
	    var length = $(this).find("span").html();
	    length = length.replace("%","");
	    length = length / 100;
	    var lengthPx = 400 * length;
	    
	    $(this).find("p").animate({'width':lengthPx}, 2000, function() {
	      // once the bar animation has finished, fade in the results
	      $(this).find("span").fadeIn(800);
	    });
	  });
	});
</script>


<?php /* tag "script" from line 29 */; ?>
<script>
    $(function() {
        $( "#startDate" ).datepicker();
        $( "#startDate" ).change(function() {
            $( "#startDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });     
	// for extraction form
    $(function() {
        $( "#endDate" ).datepicker();
        $( "#endDate" ).change(function() {
            $( "#endDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });        
</script>		
    
    <?php /* tag "h1" from line 45 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 45 */; ?>
<span style="float:left;">Dashboard</span></h1>
	<?php /* tag "div" from line 46 */; ?>
<div id="folderTab"><?php /* tag "div" from line 46 */; ?>
<div class="folderTabText">Quick View</div></div>
	<?php /* tag "br" from line 47 */; ?>
<br/>	<?php /* tag "br" from line 47 */; ?>
<br/>	<?php /* tag "br" from line 47 */; ?>
<br/>	
<!--	<div class="dashboardTabMenu">
		<span><a href="admin#/dashboard/index">Home</a></span>
		<span><a class="selected" href="#">Analytics</a></span>
		<span><a href="#">Menu 3</a></span>
		<span><a href="#">Menu 4</a></span>
	</div>
	<div class="dimension">
		Dimensions<br/>
		Start Date: <input type="text" id="startDate" name="startDate"/>&nbsp;&nbsp;&nbsp;
		End Date: <input type="text" id="endDate" name="endDate"/>&nbsp;&nbsp;&nbsp;
		<input type="button" value="Query"/>
	</div>-->
	<?php /* tag "div" from line 60 */; ?>
<div class="reportContainer">
		<?php /* tag "div" from line 61 */; ?>
<div class="tbl">
			<?php /* tag "table" from line 62 */; ?>
<table>
				<?php /* tag "tr" from line 63 */; ?>
<tr>
					<?php /* tag "td" from line 64 */; ?>
<td>
						<?php /* tag "table" from line 65 */; ?>
<table style="width:200px;">
							<?php /* tag "tr" from line 66 */; ?>
<tr><?php /* tag "td" from line 66 */; ?>
<td>&nbsp;</td></tr>
							<?php /* tag "tr" from line 67 */; ?>
<tr><?php /* tag "td" from line 67 */; ?>
<td>&nbsp;</td></tr>
							<?php /* tag "tr" from line 68 */; ?>
<tr><?php /* tag "td" from line 68 */; ?>
<td>&nbsp;</td></tr>
							<?php /* tag "tr" from line 69 */; ?>
<tr><?php /* tag "td" from line 69 */; ?>
<td>&nbsp;</td></tr>
							<?php /* tag "tr" from line 70 */; ?>
<tr>
								<?php /* tag "th" from line 71 */; ?>
<th colspan="3"><?php /* tag "span" from line 71 */; ?>
<span class="tableTitle">Today</span></th>
							</tr>
							<?php /* tag "tr" from line 73 */; ?>
<tr>
								<?php /* tag "td" from line 74 */; ?>
<td colspan="3" align="center"><?php /* tag "span" from line 74 */; ?>
<span style="font-size:9px; font-style:Italic; font-weight:normal;"><?php /* tag "tal:block" from line 74 */; ?>
<?php echo phptal_escape($ctx->todayDate); ?>
</span></td>
							</tr>
							<?php /* tag "tr" from line 76 */; ?>
<tr>
								<?php /* tag "td" from line 77 */; ?>
<td>&nbsp;</td>
								<?php /* tag "td" from line 78 */; ?>
<td>
									<?php /* tag "table" from line 79 */; ?>
<table style="width:75px; text-align:center;">
										<?php /* tag "tr" from line 80 */; ?>
<tr>
											<?php /* tag "th" from line 81 */; ?>
<th>Hour</th><?php /* tag "th" from line 81 */; ?>
<th>Hits</th>
										</tr>
										<?php 
/* tag "tal:block" from line 83 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

											<?php 
/* tag "tal:block" from line 84 */ ;
if ($ctx->repeat->item->index <= 11):  ;
?>

											<?php /* tag "tr" from line 85 */; ?>
<tr><?php /* tag "td" from line 85 */; ?>
<td><?php /* tag "tal:block" from line 85 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 85 */; ?>
<td><?php /* tag "tal:block" from line 85 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
											<?php endif; ?>

										<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

									</table>
								</td>
								<?php /* tag "td" from line 90 */; ?>
<td valign="top">
									<?php /* tag "table" from line 91 */; ?>
<table style="width:75px;text-align:center;">
										<?php /* tag "th" from line 92 */; ?>
<th>Hour</th><?php /* tag "th" from line 92 */; ?>
<th>Hits</th>
										<?php 
/* tag "tal:block" from line 93 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

											<?php 
/* tag "tal:block" from line 94 */ ;
if ($ctx->repeat->item->index >= 12):  ;
?>

											<?php /* tag "tr" from line 95 */; ?>
<tr><?php /* tag "td" from line 95 */; ?>
<td><?php /* tag "tal:block" from line 95 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 95 */; ?>
<td><?php /* tag "tal:block" from line 95 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
											<?php endif; ?>

										<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 106 */; ?>
<div id="bargraph">
		<?php /* tag "br" from line 107 */; ?>
<br/>
			<?php /* tag "table" from line 108 */; ?>
<table style="text-align:left;">
				<?php /* tag "tr" from line 109 */; ?>
<tr>
					<?php /* tag "td" from line 110 */; ?>
<td>
						<?php /* tag "table" from line 111 */; ?>
<table class="extras_table" align="center">
							<?php /* tag "tr" from line 112 */; ?>
<tr>
								<?php /* tag "th" from line 113 */; ?>
<th colspan="2"><?php /* tag "span" from line 113 */; ?>
<span class="tableTitle">Top Followers</span></th>
							</tr>
							<?php /* tag "tr" from line 115 */; ?>
<tr><?php /* tag "td" from line 115 */; ?>
<td></td></tr>
						
							<?php 
/* tag "tal:block" from line 117 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->topFollowers)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

							<?php /* tag "tr" from line 118 */; ?>
<tr>
								<?php /* tag "th" from line 119 */; ?>
<th class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 119 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</th>
								<?php /* tag "td" from line 120 */; ?>
<td><?php /* tag "div" from line 120 */; ?>
<div class="extras_result"><?php /* tag "p" from line 120 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 120 */; ?>
<span><?php /* tag "tal:block" from line 120 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 120 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
							</tr>
							<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

							<?php /* tag "tr" from line 123 */; ?>
<tr><?php /* tag "td" from line 123 */; ?>
<td>&nbsp;</td></tr>
						</table>
						<?php /* tag "table" from line 125 */; ?>
<table class="extras_table" align="center">
							<?php /* tag "tr" from line 126 */; ?>
<tr>
								<?php /* tag "th" from line 127 */; ?>
<th colspan="2"><?php /* tag "span" from line 127 */; ?>
<span class="tableTitle">Total Hits</span></th>
							</tr>
							<?php 
/* tag "tal:block" from line 129 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->totalHitsPerTenant)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

							<?php /* tag "tr" from line 130 */; ?>
<tr>
								<?php /* tag "th" from line 131 */; ?>
<th class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 131 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</th>
								<?php /* tag "td" from line 132 */; ?>
<td><?php /* tag "div" from line 132 */; ?>
<div class="extras_result"><?php /* tag "p" from line 132 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 132 */; ?>
<span><?php /* tag "tal:block" from line 132 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 132 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
							</tr>
							<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

							<?php /* tag "tr" from line 135 */; ?>
<tr><?php /* tag "td" from line 135 */; ?>
<td>&nbsp;</td></tr>
						</table>
						<?php /* tag "table" from line 137 */; ?>
<table class="extras_table" align="center">
							<?php /* tag "tr" from line 138 */; ?>
<tr>
								<?php /* tag "th" from line 139 */; ?>
<th colspan="2"><?php /* tag "span" from line 139 */; ?>
<span class="tableTitle">Touch Point Total Hits</span></th>
							</tr>
							<?php 
/* tag "tal:block" from line 141 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->operatorTotalHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

							<?php /* tag "tr" from line 142 */; ?>
<tr>
								<?php /* tag "th" from line 143 */; ?>
<th class="extras_y-desc" scope="row">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php /* tag "tal:block" from line 143 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</th>
								<?php /* tag "td" from line 144 */; ?>
<td><?php /* tag "div" from line 144 */; ?>
<div class="extras_result"><?php /* tag "p" from line 144 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 144 */; ?>
<span><?php /* tag "tal:block" from line 144 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 144 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
							</tr>
							<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

							<?php /* tag "tr" from line 147 */; ?>
<tr><?php /* tag "td" from line 147 */; ?>
<td>&nbsp;</td></tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
<!--		<div id="piegraph">
			<canvas id="chart" width="400" height="300"></canvas>
			<table id="chartData">
				<tr><th colspan="2" style="text-align:center">Pie Chart</th></tr>
				<tr><th style="text-align:center;">Touch Points</th><th style="text-align:center;">Hits</th></tr>
				<tr style="color: #72808E">
					<td>Globe</td>
					<td>500</td>
				</tr>
				<tr style="color: #000000">
					<td>XMPP</td>
					<td>300</td>
				</tr>
			</table>
			<iframe id="secretIFrame" src="" style="display:none; visibility:hidden;"></iframe>
		</div>-->
	</div>
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/dashboard/index.zpt (edit that file instead) */; ?>