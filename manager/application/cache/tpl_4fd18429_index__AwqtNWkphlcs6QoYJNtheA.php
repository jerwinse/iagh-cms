<?php 
function tpl_4fd18429_index__AwqtNWkphlcs6QoYJNtheA(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<?php /* tag "link" from line 2 */; ?>
<link rel="stylesheet" href="/css/calendar/ui-lightness/jquery-ui-1.8.16.custom.css" media="screen"/>
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" href="/css/dashboard/style.css" media="screen"/>
<?php /* tag "link" from line 4 */; ?>
<link rel="stylesheet" href="/css/dashboard/barchart.css" media="screen"/>
<?php /* tag "link" from line 5 */; ?>
<link rel="stylesheet" href="/css/dashboard/chart.css" media="screen"/>

<?php /* tag "script" from line 7 */; ?>
<script src="/js/calendar/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
<?php /* tag "script" from line 8 */; ?>
<script src="/js/dashboard/global.js" type="text/javascript"></script>
<!--<script src="/js/dashboard/chart.js" type="text/javascript"></script>-->

<?php /* tag "script" from line 11 */; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	  // for each result row..
	  $(".extras_result").each(function() {
	    // get the width of the bar from the span html
	    var length = $(this).find("span").html();
	    length = length.replace("%","");
	    length = length / 100;
	    var lengthPx = 400 * length;
	    
	    $(this).find("p").animate({'width':lengthPx}, 2000, function() {
	      // once the bar animation has finished, fade in the results
	      $(this).find("span").fadeIn(800);
	    });
	  });
	});
</script>


<?php /* tag "script" from line 30 */; ?>
<script>
    $(function() {
        $( "#startDate" ).datepicker();
        $( "#startDate" ).change(function() {
            $( "#startDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });     
	// for extraction form
    $(function() {
        $( "#endDate" ).datepicker();
        $( "#endDate" ).change(function() {
            $( "#endDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });        
</script>		
    
    <?php /* tag "h1" from line 46 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 46 */; ?>
<span style="float:left;">Dashboard</span></h1>
	<?php /* tag "div" from line 47 */; ?>
<div id="folderTab"><?php /* tag "div" from line 47 */; ?>
<div class="folderTabText">Quick View</div></div>
	<?php /* tag "br" from line 48 */; ?>
<br/>
	<?php /* tag "div" from line 49 */; ?>
<div class="dashboardTabMenu">
		<?php /* tag "span" from line 50 */; ?>
<span id="followersLink"><?php /* tag "a" from line 50 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('followers')">Top Follower</a></span>
		<?php /* tag "span" from line 51 */; ?>
<span id="tenantHitsLink"><?php /* tag "a" from line 51 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('tenantHits')">Tenant Hits</a></span>
		<?php /* tag "span" from line 52 */; ?>
<span id="touchpointHitsLink"><?php /* tag "a" from line 52 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('touchpointHits')">Touchpoint Hits</a></span>
		<?php /* tag "span" from line 53 */; ?>
<span><?php /* tag "a" from line 53 */; ?>
<a href="javascript:;">Menu 4</a></span>
	</div>
<!--	<div class="dimension">
		Dimensions<br/>
		Start Date: <input type="text" id="startDate" name="startDate"/>&nbsp;&nbsp;&nbsp;
		End Date: <input type="text" id="endDate" name="endDate"/>&nbsp;&nbsp;&nbsp;
		<input type="button" value="Query"/>
	</div>-->
	<?php /* tag "div" from line 61 */; ?>
<div class="reportContainer">
		<?php /* tag "div" from line 62 */; ?>
<div class="tbl">
			<?php 
/* tag "tal:block" from line 63 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->detailMonthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

				<?php 
/* tag "div" from line 64 */ ;
if (null !== ($_tmp_2 = ($ctx->repeat->item->key))):  ;
$_tmp_2 = ' id="'.phptal_escape($_tmp_2).'"' ;
else:  ;
$_tmp_2 = '' ;
endif ;
?>
<div style="display:none;"<?php echo $_tmp_2 ?>
>
					<?php /* tag "table" from line 65 */; ?>
<table style="width:200px; text-align:center;">
						<?php /* tag "tr" from line 66 */; ?>
<tr><?php /* tag "td" from line 66 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 66 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 67 */; ?>
<tr><?php /* tag "td" from line 67 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 67 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 68 */; ?>
<tr><?php /* tag "th" from line 68 */; ?>
<th colspan="3" align="center"><?php /* tag "span" from line 68 */; ?>
<span class="tableTitle" style="font-size:14px;"><?php /* tag "tal:block" from line 68 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</span></th></tr>
						<?php /* tag "tr" from line 69 */; ?>
<tr><?php /* tag "td" from line 69 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 69 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 70 */; ?>
<tr><?php /* tag "th" from line 70 */; ?>
<th>Name</th><?php /* tag "th" from line 70 */; ?>
<th>Hits</th></tr>
						<?php 
/* tag "tal:block" from line 71 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

							<?php /* tag "tr" from line 72 */; ?>
<tr>
								<?php /* tag "td" from line 73 */; ?>
<td width="50%"><?php /* tag "tal:block" from line 73 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'name')); ?>
</td>
								<?php /* tag "td" from line 74 */; ?>
<td width="50%"><?php /* tag "tal:block" from line 74 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
							</tr>
						<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

					</table>
				</div>
			<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

			<?php /* tag "div" from line 80 */; ?>
<div id="todayTransaction">
				<?php /* tag "table" from line 81 */; ?>
<table>
					<?php /* tag "tr" from line 82 */; ?>
<tr>
						<?php /* tag "td" from line 83 */; ?>
<td>
							<?php /* tag "table" from line 84 */; ?>
<table style="width:200px;">
								<?php /* tag "tr" from line 85 */; ?>
<tr><?php /* tag "th" from line 85 */; ?>
<th colspan="3"><?php /* tag "span" from line 85 */; ?>
<span class="tableTitle">Today Transaction</span></th></tr>
								<?php /* tag "tr" from line 86 */; ?>
<tr><?php /* tag "td" from line 86 */; ?>
<td colspan="3" align="center"><?php /* tag "span" from line 86 */; ?>
<span style="font-size:9px; font-style:Italic; font-weight:normal;"><?php /* tag "tal:block" from line 86 */; ?>
<?php echo phptal_escape($ctx->todayDate); ?>
</span></td></tr>
								<?php /* tag "tr" from line 87 */; ?>
<tr><?php /* tag "td" from line 87 */; ?>
<td>&nbsp;</td></tr>
								<?php /* tag "tr" from line 88 */; ?>
<tr>
									<?php /* tag "td" from line 89 */; ?>
<td>&nbsp;</td>
									<?php /* tag "td" from line 90 */; ?>
<td>
										<?php /* tag "table" from line 91 */; ?>
<table style="width:75px; text-align:center;">
											<?php /* tag "tr" from line 92 */; ?>
<tr>
												<?php /* tag "th" from line 93 */; ?>
<th>Hour</th><?php /* tag "th" from line 93 */; ?>
<th>Hits</th>
											</tr>
											<?php 
/* tag "tal:block" from line 95 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

												<?php 
/* tag "tal:block" from line 96 */ ;
if ($ctx->repeat->item->index <= 11):  ;
?>

												<?php /* tag "tr" from line 97 */; ?>
<tr><?php /* tag "td" from line 97 */; ?>
<td><?php /* tag "tal:block" from line 97 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 97 */; ?>
<td><?php /* tag "tal:block" from line 97 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
												<?php endif; ?>

											<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

										</table>
									</td>
									<?php /* tag "td" from line 102 */; ?>
<td valign="top">
										<?php /* tag "table" from line 103 */; ?>
<table style="width:75px;text-align:center;">
											<?php /* tag "th" from line 104 */; ?>
<th>Hour</th><?php /* tag "th" from line 104 */; ?>
<th>Hits</th>
											<?php 
/* tag "tal:block" from line 105 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

												<?php 
/* tag "tal:block" from line 106 */ ;
if ($ctx->repeat->item->index >= 12):  ;
?>

												<?php /* tag "tr" from line 107 */; ?>
<tr><?php /* tag "td" from line 107 */; ?>
<td><?php /* tag "tal:block" from line 107 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 107 */; ?>
<td><?php /* tag "tal:block" from line 107 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
												<?php endif; ?>

											<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<?php /* tag "div" from line 119 */; ?>
<div id="bargraph">
		<?php /* tag "br" from line 120 */; ?>
<br/><?php /* tag "br" from line 120 */; ?>
<br/>
			<?php /* tag "table" from line 121 */; ?>
<table style="text-align:left;">
				<?php /* tag "tr" from line 122 */; ?>
<tr>
					<?php /* tag "td" from line 123 */; ?>
<td>
						<?php /* tag "div" from line 124 */; ?>
<div id="followers">
							<?php /* tag "table" from line 125 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 126 */; ?>
<tr>
									<?php /* tag "th" from line 127 */; ?>
<th colspan="2"><?php /* tag "span" from line 127 */; ?>
<span class="tableTitle">Top Followers</span></th>
								</tr>
								<?php /* tag "tr" from line 129 */; ?>
<tr><?php /* tag "td" from line 129 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 130 */; ?>
<tr><?php /* tag "td" from line 130 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 131 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->topFollowers)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 132 */; ?>
<tr>
									<?php /* tag "td" from line 133 */; ?>
<td class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 133 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 134 */; ?>
<td><?php /* tag "div" from line 134 */; ?>
<div class="extras_result"><?php /* tag "p" from line 134 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 134 */; ?>
<span><?php /* tag "tal:block" from line 134 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 134 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 137 */; ?>
<tr><?php /* tag "td" from line 137 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 140 */; ?>
<div id="tenantHits" style="display:none">
							<?php /* tag "table" from line 141 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 142 */; ?>
<tr>
									<?php /* tag "th" from line 143 */; ?>
<th colspan="2"><?php /* tag "span" from line 143 */; ?>
<span class="tableTitle">Tenant Hits</span></th>
								</tr>
								<?php /* tag "tr" from line 145 */; ?>
<tr><?php /* tag "td" from line 145 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 146 */; ?>
<tr><?php /* tag "td" from line 146 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 147 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->totalHitsPerTenant)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 148 */; ?>
<tr>
									<?php /* tag "td" from line 149 */; ?>
<td class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 149 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 150 */; ?>
<td><?php /* tag "div" from line 150 */; ?>
<div class="extras_result"><?php /* tag "p" from line 150 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 150 */; ?>
<span><?php /* tag "tal:block" from line 150 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 150 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 153 */; ?>
<tr><?php /* tag "td" from line 153 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 156 */; ?>
<div id="touchpointHits" style="display:none">
							<?php /* tag "table" from line 157 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 158 */; ?>
<tr>
									<?php /* tag "th" from line 159 */; ?>
<th colspan="2"><?php /* tag "span" from line 159 */; ?>
<span class="tableTitle">Touch Point Hits</span></th>
								</tr>
								<?php /* tag "tr" from line 161 */; ?>
<tr><?php /* tag "td" from line 161 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 162 */; ?>
<tr><?php /* tag "td" from line 162 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 163 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->operatorTotalHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 164 */; ?>
<tr>
									<?php /* tag "td" from line 165 */; ?>
<td class="extras_y-desc" scope="row">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php /* tag "tal:block" from line 165 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 166 */; ?>
<td><?php /* tag "div" from line 166 */; ?>
<div class="extras_result"><?php /* tag "p" from line 166 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 166 */; ?>
<span><?php /* tag "tal:block" from line 166 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 166 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 169 */; ?>
<tr><?php /* tag "td" from line 169 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 176 */; ?>
<div id="monthlyReport">
			<?php /* tag "table" from line 177 */; ?>
<table style="width:880px; margin-left:20px;">
				<?php /* tag "tr" from line 178 */; ?>
<tr><?php /* tag "td" from line 178 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 179 */; ?>
<tr><?php /* tag "th" from line 179 */; ?>
<th colspan="12" align="center"><?php /* tag "span" from line 179 */; ?>
<span class="tableTitle">Total Hits By Month</span></th></tr>
				<?php /* tag "tr" from line 180 */; ?>
<tr><?php /* tag "td" from line 180 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 181 */; ?>
<tr>
					<?php 
/* tag "tal:block" from line 182 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->monthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

						<?php /* tag "td" from line 183 */; ?>
<td valign="top">
							<?php /* tag "table" from line 184 */; ?>
<table>
								<?php /* tag "tr" from line 185 */; ?>
<tr>
									<?php /* tag "th" from line 186 */; ?>
<th colspan="2" align="center"><?php /* tag "a" from line 186 */; ?>
<a href="javascript:;" onclick="global.displayTransContainer($(this).html());"><?php /* tag "tal:block" from line 186 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</a></th>
								</tr>
								<?php 
/* tag "tal:block" from line 188 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

									<?php /* tag "tr" from line 189 */; ?>
<tr>
										<?php /* tag "td" from line 190 */; ?>
<td><?php /* tag "tal:block" from line 190 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'total')); ?>
</td>
										<?php /* tag "td" from line 191 */; ?>
<td><?php /* tag "tal:block" from line 191 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
									</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 194 */; ?>
<tr><?php /* tag "td" from line 194 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</td>
					<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>
			
				</tr>
			</table>
		</div>
	</div>
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/dashboard/index.zpt (edit that file instead) */; ?>