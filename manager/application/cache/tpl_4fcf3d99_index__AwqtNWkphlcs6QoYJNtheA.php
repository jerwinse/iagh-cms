<?php 
function tpl_4fcf3d99_index__AwqtNWkphlcs6QoYJNtheA(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<?php /* tag "link" from line 2 */; ?>
<link rel="stylesheet" href="/css/calendar/ui-lightness/jquery-ui-1.8.16.custom.css" media="screen"/>
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" href="/css/dashboard/style.css" media="screen"/>
<?php /* tag "link" from line 4 */; ?>
<link rel="stylesheet" href="/css/dashboard/barchart.css" media="screen"/>
<?php /* tag "link" from line 5 */; ?>
<link rel="stylesheet" href="/css/dashboard/chart.css" media="screen"/>

<?php /* tag "script" from line 7 */; ?>
<script src="/js/calendar/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
<!--<script src="/js/dashboard/chart.js" type="text/javascript"></script>-->

<?php /* tag "script" from line 10 */; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	  // for each result row..
	  $(".extras_result").each(function() {
	    // get the width of the bar from the span html
	    var length = $(this).find("span").html();
	    length = length.replace("%","");
	    length = length / 100;
	    var lengthPx = 400 * length;
	    
	    $(this).find("p").animate({'width':lengthPx}, 2000, function() {
	      // once the bar animation has finished, fade in the results
	      $(this).find("span").fadeIn(800);
	    });
	  });
	});
</script>


<?php /* tag "script" from line 29 */; ?>
<script>
    $(function() {
        $( "#startDate" ).datepicker();
        $( "#startDate" ).change(function() {
            $( "#startDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });     
	// for extraction form
    $(function() {
        $( "#endDate" ).datepicker();
        $( "#endDate" ).change(function() {
            $( "#endDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });        
</script>		
    
    <?php /* tag "h1" from line 45 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 45 */; ?>
<span style="float:left;">Dashboard</span></h1>
	<?php /* tag "div" from line 46 */; ?>
<div id="folderTab"><?php /* tag "div" from line 46 */; ?>
<div class="folderTabText">Quick View</div></div>
	<?php /* tag "br" from line 47 */; ?>
<br/>
<!--	<div class="dashboardTabMenu">
		<span><a href="admin#/dashboard/index">Home</a></span>
		<span><a class="selected" href="#">Analytics</a></span>
		<span><a href="#">Menu 3</a></span>
		<span><a href="#">Menu 4</a></span>
	</div>
	<div class="dimension">
		Dimensions<br/>
		Start Date: <input type="text" id="startDate" name="startDate"/>&nbsp;&nbsp;&nbsp;
		End Date: <input type="text" id="endDate" name="endDate"/>&nbsp;&nbsp;&nbsp;
		<input type="button" value="Query"/>
	</div>-->
	<?php /* tag "div" from line 60 */; ?>
<div class="reportContainer">
		<?php 
/* tag "tal:block" from line 61 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->detailMonthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

			<?php 
/* tag "div" from line 62 */ ;
if (null !== ($_tmp_2 = ($ctx->repeat->item->key))):  ;
$_tmp_2 = ' id="'.phptal_escape($_tmp_2).'"' ;
else:  ;
$_tmp_2 = '' ;
endif ;
?>
<div style="display:block;"<?php echo $_tmp_2 ?>
>
				<?php /* tag "table" from line 63 */; ?>
<table>
					<?php /* tag "tr" from line 64 */; ?>
<tr>
						<?php /* tag "th" from line 65 */; ?>
<th><?php /* tag "span" from line 65 */; ?>
<span class="tableTitle"><?php /* tag "tal:block" from line 65 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</span></th>
					</tr>
					<?php 
/* tag "tal:block" from line 67 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

							<?php /* tag "tr" from line 68 */; ?>
<tr><?php /* tag "td" from line 68 */; ?>
<td><?php /* tag "tal:block" from line 68 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'name')); ?>
</td><?php /* tag "td" from line 68 */; ?>
<td><?php /* tag "tal:block" from line 68 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td></tr>
					<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

				</table>
			</div>
		<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

		<?php /* tag "div" from line 73 */; ?>
<div class="tbl">
			<?php /* tag "table" from line 74 */; ?>
<table>
				<?php /* tag "tr" from line 75 */; ?>
<tr>
					<?php /* tag "td" from line 76 */; ?>
<td>
						<?php /* tag "table" from line 77 */; ?>
<table style="width:200px;">
							<?php /* tag "tr" from line 78 */; ?>
<tr><?php /* tag "td" from line 78 */; ?>
<td>&nbsp;</td></tr>
							<?php /* tag "tr" from line 79 */; ?>
<tr><?php /* tag "td" from line 79 */; ?>
<td>&nbsp;</td></tr>
							<?php /* tag "tr" from line 80 */; ?>
<tr><?php /* tag "th" from line 80 */; ?>
<th colspan="3"><?php /* tag "span" from line 80 */; ?>
<span class="tableTitle">Today Transaction</span></th></tr>
							<?php /* tag "tr" from line 81 */; ?>
<tr><?php /* tag "td" from line 81 */; ?>
<td colspan="3" align="center"><?php /* tag "span" from line 81 */; ?>
<span style="font-size:9px; font-style:Italic; font-weight:normal;"><?php /* tag "tal:block" from line 81 */; ?>
<?php echo phptal_escape($ctx->todayDate); ?>
</span></td></tr>
							<?php /* tag "tr" from line 82 */; ?>
<tr><?php /* tag "td" from line 82 */; ?>
<td>&nbsp;</td></tr>
							<?php /* tag "tr" from line 83 */; ?>
<tr>
								<?php /* tag "td" from line 84 */; ?>
<td>&nbsp;</td>
								<?php /* tag "td" from line 85 */; ?>
<td>
									<?php /* tag "table" from line 86 */; ?>
<table style="width:75px; text-align:center;">
										<?php /* tag "tr" from line 87 */; ?>
<tr>
											<?php /* tag "th" from line 88 */; ?>
<th>Hour</th><?php /* tag "th" from line 88 */; ?>
<th>Hits</th>
										</tr>
										<?php 
/* tag "tal:block" from line 90 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

											<?php 
/* tag "tal:block" from line 91 */ ;
if ($ctx->repeat->item->index <= 11):  ;
?>

											<?php /* tag "tr" from line 92 */; ?>
<tr><?php /* tag "td" from line 92 */; ?>
<td><?php /* tag "tal:block" from line 92 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 92 */; ?>
<td><?php /* tag "tal:block" from line 92 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
											<?php endif; ?>

										<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

									</table>
								</td>
								<?php /* tag "td" from line 97 */; ?>
<td valign="top">
									<?php /* tag "table" from line 98 */; ?>
<table style="width:75px;text-align:center;">
										<?php /* tag "th" from line 99 */; ?>
<th>Hour</th><?php /* tag "th" from line 99 */; ?>
<th>Hits</th>
										<?php 
/* tag "tal:block" from line 100 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

											<?php 
/* tag "tal:block" from line 101 */ ;
if ($ctx->repeat->item->index >= 12):  ;
?>

											<?php /* tag "tr" from line 102 */; ?>
<tr><?php /* tag "td" from line 102 */; ?>
<td><?php /* tag "tal:block" from line 102 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 102 */; ?>
<td><?php /* tag "tal:block" from line 102 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
											<?php endif; ?>

										<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 113 */; ?>
<div id="bargraph">
		<?php /* tag "br" from line 114 */; ?>
<br/>
			<?php /* tag "table" from line 115 */; ?>
<table style="text-align:left;">
				<?php /* tag "tr" from line 116 */; ?>
<tr>
					<?php /* tag "td" from line 117 */; ?>
<td>
						<?php /* tag "table" from line 118 */; ?>
<table class="extras_table" align="center">
							<?php /* tag "tr" from line 119 */; ?>
<tr>
								<?php /* tag "th" from line 120 */; ?>
<th colspan="2"><?php /* tag "span" from line 120 */; ?>
<span class="tableTitle">Top Followers</span></th>
							</tr>
							<?php /* tag "tr" from line 122 */; ?>
<tr><?php /* tag "td" from line 122 */; ?>
<td></td></tr>
						
							<?php 
/* tag "tal:block" from line 124 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->topFollowers)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

							<?php /* tag "tr" from line 125 */; ?>
<tr>
								<?php /* tag "th" from line 126 */; ?>
<th class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 126 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</th>
								<?php /* tag "td" from line 127 */; ?>
<td><?php /* tag "div" from line 127 */; ?>
<div class="extras_result"><?php /* tag "p" from line 127 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 127 */; ?>
<span><?php /* tag "tal:block" from line 127 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 127 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
							</tr>
							<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

							<?php /* tag "tr" from line 130 */; ?>
<tr><?php /* tag "td" from line 130 */; ?>
<td>&nbsp;</td></tr>
						</table>
						<?php /* tag "table" from line 132 */; ?>
<table class="extras_table" align="center">
							<?php /* tag "tr" from line 133 */; ?>
<tr>
								<?php /* tag "th" from line 134 */; ?>
<th colspan="2"><?php /* tag "span" from line 134 */; ?>
<span class="tableTitle">Tenant Total Hits</span></th>
							</tr>
							<?php 
/* tag "tal:block" from line 136 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->totalHitsPerTenant)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

							<?php /* tag "tr" from line 137 */; ?>
<tr>
								<?php /* tag "th" from line 138 */; ?>
<th class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 138 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</th>
								<?php /* tag "td" from line 139 */; ?>
<td><?php /* tag "div" from line 139 */; ?>
<div class="extras_result"><?php /* tag "p" from line 139 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 139 */; ?>
<span><?php /* tag "tal:block" from line 139 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 139 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
							</tr>
							<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

							<?php /* tag "tr" from line 142 */; ?>
<tr><?php /* tag "td" from line 142 */; ?>
<td>&nbsp;</td></tr>
						</table>
						<?php /* tag "table" from line 144 */; ?>
<table class="extras_table" align="center">
							<?php /* tag "tr" from line 145 */; ?>
<tr>
								<?php /* tag "th" from line 146 */; ?>
<th colspan="2"><?php /* tag "span" from line 146 */; ?>
<span class="tableTitle">Touch Point Total Hits</span></th>
							</tr>
							<?php 
/* tag "tal:block" from line 148 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->operatorTotalHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

							<?php /* tag "tr" from line 149 */; ?>
<tr>
								<?php /* tag "th" from line 150 */; ?>
<th class="extras_y-desc" scope="row">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php /* tag "tal:block" from line 150 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</th>
								<?php /* tag "td" from line 151 */; ?>
<td><?php /* tag "div" from line 151 */; ?>
<div class="extras_result"><?php /* tag "p" from line 151 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 151 */; ?>
<span><?php /* tag "tal:block" from line 151 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 151 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
							</tr>
							<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

							<?php /* tag "tr" from line 154 */; ?>
<tr><?php /* tag "td" from line 154 */; ?>
<td>&nbsp;</td></tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 160 */; ?>
<div id="monthlyReport">
			<?php /* tag "table" from line 161 */; ?>
<table style="width:880px; margin-left:20px;">
				<?php /* tag "tr" from line 162 */; ?>
<tr><?php /* tag "td" from line 162 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 163 */; ?>
<tr><?php /* tag "th" from line 163 */; ?>
<th colspan="12" align="center"><?php /* tag "span" from line 163 */; ?>
<span class="tableTitle">Total Hits By Month</span></th></tr>
				<?php /* tag "tr" from line 164 */; ?>
<tr><?php /* tag "td" from line 164 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 165 */; ?>
<tr>
					<?php 
/* tag "tal:block" from line 166 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->monthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

						<?php /* tag "td" from line 167 */; ?>
<td valign="top">
							<?php /* tag "table" from line 168 */; ?>
<table>
								<?php /* tag "tr" from line 169 */; ?>
<tr>
									<?php /* tag "th" from line 170 */; ?>
<th colspan="2" align="center"><?php /* tag "a" from line 170 */; ?>
<a href="javascript:;" onclick="alert($(this).html());"><?php /* tag "tal:block" from line 170 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</a></th>
								</tr>
								<?php 
/* tag "tal:block" from line 172 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

									<?php /* tag "tr" from line 173 */; ?>
<tr>
										<?php /* tag "td" from line 174 */; ?>
<td><?php /* tag "tal:block" from line 174 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'total')); ?>
</td>
										<?php /* tag "td" from line 175 */; ?>
<td><?php /* tag "tal:block" from line 175 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
									</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 178 */; ?>
<tr><?php /* tag "td" from line 178 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</td>
					<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>
			
				</tr>
			</table>
		</div>
	</div>
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/dashboard/index.zpt (edit that file instead) */; ?>