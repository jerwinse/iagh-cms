<?php 
function tpl_4fc44946_create__DmUhOkC_h8MVaRMmO4fDwA(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>



<?php /* tag "script" from line 4 */; ?>
<script type="text/javascript" src="/js/jquery.itextclear.js"></script>
<?php /* tag "script" from line 5 */; ?>
<script type="text/javascript">
$(document).ready(function(){
    $('input[type=text], input[type=password], input[type=url], input[type=email], input[type=number], textarea', '.form').iTextClear(); 
    Global.onSuccess = function(){Members.create();return false;}
});

$(window).bind('drilldown', function(){
	Global.onSuccess = function(){Members.create();return false;}	
});

var Members = {
	create: function() {
		var form_data = JSON.stringify($('#form-member').serializeArray());
		
        $.ajax('/members/docreate', {
        	type: 'post',
        	data: {'data':form_data},
            dataType: "html",
            cache: false,
            beforeSend: function(jqXHR, settings) {
            	$('#message').html('');
            	$('#message').removeClass('error');
            	$('#message').removeClass('success');
            	
            	$('#message').html('<?php /* tag "h3" from line 29 */; ?>
<h3>Please wait</h3><?php /* tag "p" from line 29 */; ?>
<p>Saving record...</p>');
            	$('#message').addClass('message');
            	$('#message').addClass('info');
            	$('#message').fadeIn();
            },
            complete: function(jqXHR, textStatus) {            	
                var response = jQuery.parseJSON(jqXHR.responseText);                
                if (response.status == 'ACK')
                {
                	var html = '<?php /* tag "h3" from line 38 */; ?>
<h3>Success!</h3><?php /* tag "p" from line 38 */; ?>
<p>You have successfully created a role.</p>';
                	$('#message').html(html);
                	$('#message').removeClass('error');
                	$('#message').addClass('message');
                	$('#message').addClass('success');
                	$('#message').fadeIn().delay(10000).fadeOut('slow');
                }
                else
                {
                	var html = '<?php /* tag "h3" from line 47 */; ?>
<h3>Error!</h3><?php /* tag "p" from line 47 */; ?>
<p>There was an error saving the record.</p>';
                	$('#message').html(html);
                	$('#message').addClass('message');
                	$('#message').addClass('error');
                	$('#message').fadeIn().delay(10000).fadeOut('slow');
                }
            }
        });
	}	
};
</script>
                <?php /* tag "h1" from line 58 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 58 */; ?>
<span style="float:left;"><?php /* tag "a" from line 58 */; ?>
<a href="#/members/index" style="color:#fff;">Members</a> &raquo; Create A Member</span></h1>
                <?php /* tag "div" from line 59 */; ?>
<div class="container_12 clearfix leading">
                    <?php /* tag "div" from line 60 */; ?>
<div class="grid_12">
						<?php /* tag "div" from line 61 */; ?>
<div id="message"></div>
                    
                    	<?php /* tag "form" from line 63 */; ?>
<form id="form-member" class="form has-validation" method="post">

                            <?php /* tag "div" from line 65 */; ?>
<div class="clearfix">

                                <?php /* tag "label" from line 67 */; ?>
<label for="form-username" class="form-label">Username <?php /* tag "em" from line 67 */; ?>
<em>*</em></label>

                                <?php /* tag "div" from line 69 */; ?>
<div class="form-input"><?php /* tag "input" from line 69 */; ?>
<input type="text" id="form-username" name="username" required="required" placeholder=""/></div>

                            </div>

                            <?php /* tag "div" from line 73 */; ?>
<div class="clearfix">

                                <?php /* tag "label" from line 75 */; ?>
<label for="form-password" class="form-label">Password <?php /* tag "em" from line 75 */; ?>
<em>*</em></label>

                                <?php /* tag "div" from line 77 */; ?>
<div class="form-input"><?php /* tag "input" from line 77 */; ?>
<input type="password" id="form-password" name="password" required="required" maxlength="30" placeholder="max. 30 char."/></div>

                            </div>

                            <?php /* tag "div" from line 81 */; ?>
<div class="clearfix">

                                <?php /* tag "label" from line 83 */; ?>
<label for="form-password-check" class="form-label">Confirm Password</label>

                                <?php /* tag "div" from line 85 */; ?>
<div class="form-input"><?php /* tag "input" from line 85 */; ?>
<input type="password" id="form-password-check" name="check" required="required" data-equals="password" maxlength="30" placeholder="Re-enter your password"/></div>

                            </div>
                            
                            <?php /* tag "div" from line 89 */; ?>
<div class="clearfix">

                                <?php /* tag "label" from line 91 */; ?>
<label for="form-email" class="form-label">Email <?php /* tag "em" from line 91 */; ?>
<em>*</em></label>

                                <?php /* tag "div" from line 93 */; ?>
<div class="form-input"><?php /* tag "input" from line 93 */; ?>
<input type="email" id="form-email" name="email" required="required" placeholder="A valid email address"/></div>

                            </div>                            

                            <?php /* tag "div" from line 97 */; ?>
<div class="clearfix">

                                <?php /* tag "label" from line 99 */; ?>
<label for="form-lastname" class="form-label">Last Name <?php /* tag "em" from line 99 */; ?>
<em>*</em></label>

                                <?php /* tag "div" from line 101 */; ?>
<div class="form-input"><?php /* tag "input" from line 101 */; ?>
<input type="text" id="form-lastname" name="last_name" required="required" placeholder=""/></div>

                            </div>
                            
                            <?php /* tag "div" from line 105 */; ?>
<div class="clearfix">

                                <?php /* tag "label" from line 107 */; ?>
<label for="form-firstname" class="form-label">First Name</label>

                                <?php /* tag "div" from line 109 */; ?>
<div class="form-input"><?php /* tag "input" from line 109 */; ?>
<input type="text" id="form-firstname" name="first_name" placeholder=""/></div>

                            </div>
                            
                            <?php /* tag "div" from line 113 */; ?>
<div class="clearfix">

                                <?php /* tag "label" from line 115 */; ?>
<label for="form-role" class="form-label">Role <?php /* tag "em" from line 115 */; ?>
<em>*</em></label>

                                <?php /* tag "div" from line 117 */; ?>
<div class="form-input">
                                	<?php /* tag "select" from line 118 */; ?>
<select id="form-role" name="role" required="required">
                                		<?php /* tag "option" from line 119 */; ?>
<option></option>
                                		<?php 
/* tag "tal:block" from line 120 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->roles)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

                                			<?php 
/* tag "option" from line 121 */ ;
if (null !== ($_tmp_2 = ($ctx->path($ctx->item, 'id')))):  ;
$_tmp_2 = ' value="'.phptal_escape($_tmp_2).'"' ;
else:  ;
$_tmp_2 = '' ;
endif ;
?>
<option<?php echo $_tmp_2 ?>
><?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</option>
                                		<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

                                	</select>
                                </div>

                            </div>
                            
                            
                            <?php /* tag "div" from line 129 */; ?>
<div class="clearfix">

                                <?php /* tag "label" from line 131 */; ?>
<label for="form-tenant" class="form-label">Tenant <?php /* tag "em" from line 131 */; ?>
<em>*</em></label>

                                <?php /* tag "div" from line 133 */; ?>
<div class="form-input">
                                	<?php /* tag "select" from line 134 */; ?>
<select id="form-tenant" name="account_num" required="required">
                                		<?php /* tag "option" from line 135 */; ?>
<option></option>
                                		<?php 
/* tag "tal:block" from line 136 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->tenants)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

                                			<?php 
/* tag "option" from line 137 */ ;
if (null !== ($_tmp_1 = ($ctx->path($ctx->item, 'account_num')))):  ;
$_tmp_1 = ' value="'.phptal_escape($_tmp_1).'"' ;
else:  ;
$_tmp_1 = '' ;
endif ;
?>
<option<?php echo $_tmp_1 ?>
><?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</option>
                                		<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

                                	</select>
                                </div>

                            </div>

                            <?php /* tag "div" from line 144 */; ?>
<div class="form-action clearfix">

                                <?php /* tag "button" from line 146 */; ?>
<button class="button" type="submit">Create Member</button>

                                <?php /* tag "button" from line 148 */; ?>
<button class="button" type="reset">Clear</button>

                            </div>

                        </form>
                    </div>
                </div><?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/members/create.zpt (edit that file instead) */; ?>