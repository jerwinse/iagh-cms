<?php 
function tpl_4fdfc540_homeVideoForm__TwvVgk6ZO0L4BpLNKwzpcA(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>



<?php /* tag "script" from line 4 */; ?>
<script type="text/javascript" src="/js/jquery.itextclear.js"></script>
<?php /* tag "script" from line 5 */; ?>
<script type="text/javascript">
$(document).ready(function(){
    $('input[type=text], input[type=password], input[type=url], input[type=email], input[type=number], textarea', '.form').iTextClear(); 
    Global.onSuccess = function(){Tenants.create();return false;}
});

$(window).bind('drilldown', function(){
	Global.onSuccess = function(){Tenants.create();return false;}	
});

var Tenants = {
	create: function() {
		var form_data = JSON.stringify($('#form-tenant').serializeArray());
		
        $.ajax('/tenants/docreate', {
        	type: 'post',
        	data: {'data':form_data},
            dataType: "html",
            cache: false,
            beforeSend: function(jqXHR, settings) {
            	$('#message').html('');
            	$('#message').removeClass('error');
            	$('#message').removeClass('success');
            	
            	$('#message').html('<?php /* tag "h3" from line 29 */; ?>
<h3>Please wait</h3><?php /* tag "p" from line 29 */; ?>
<p>Saving record...</p>');
            	$('#message').addClass('message');
            	$('#message').addClass('info');
            	$('#message').fadeIn();
            },
            complete: function(jqXHR, textStatus) {            	
                var response = jQuery.parseJSON(jqXHR.responseText);                
                if (response.status == 'ACK')
                {
                	var html = '<?php /* tag "h3" from line 38 */; ?>
<h3>Success!</h3><?php /* tag "p" from line 38 */; ?>
<p>You have successfully created a tenant.</p>';
                	$('#message').html(html);
                	$('#message').removeClass('error');
                	$('#message').addClass('message');
                	$('#message').addClass('success');
                	$('#message').fadeIn().delay(10000).fadeOut('slow');
                }
                else
                {
                	var html = '<?php /* tag "h3" from line 47 */; ?>
<h3>Error!</h3><?php /* tag "p" from line 47 */; ?>
<p>There was an error saving the record.</p>';
                	$('#message').html(html);
                	$('#message').addClass('message');
                	$('#message').addClass('error');
                	$('#message').fadeIn().delay(10000).fadeOut('slow');
                }
            }
        });
	}	
};
</script>
	<?php 
/* tag "tal:block" from line 58 */ ;
if ($ctx->add):  ;
/* add video form */ ;
?>

                <?php /* tag "h1" from line 59 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 59 */; ?>
<span style="float:left;"><?php /* tag "a" from line 59 */; ?>
<a href="#/core/home" style="color:#fff;">Videos</a> &raquo; Add Video</span></h1>
                <?php /* tag "div" from line 60 */; ?>
<div class="container_12 clearfix leading">
                    <?php /* tag "div" from line 61 */; ?>
<div class="grid_12">
						<?php /* tag "div" from line 62 */; ?>
<div id="message"></div>
                    
                    	<?php /* tag "form" from line 64 */; ?>
<form id="form-video" class="form has-validation" method="post">

                            <?php /* tag "div" from line 66 */; ?>
<div class="clearfix">

                                <?php /* tag "label" from line 68 */; ?>
<label for="form-name" class="form-label">Video Title<?php /* tag "em" from line 68 */; ?>
<em>*</em></label>

                                <?php /* tag "div" from line 70 */; ?>
<div class="form-input"><?php /* tag "input" from line 70 */; ?>
<input type="text" id="form-title" name="title" required="required" placeholder=""/></div>

                            </div>

                            <?php /* tag "div" from line 74 */; ?>
<div class="clearfix">

                                <?php /* tag "label" from line 76 */; ?>
<label for="form-keyword" class="form-label">Video Description <?php /* tag "em" from line 76 */; ?>
<em>*</em></label>

                                <?php /* tag "div" from line 78 */; ?>
<div class="form-input"><?php /* tag "input" from line 78 */; ?>
<input type="text" id="form-description" name="videoDescription" required="required" placeholder=""/></div>

                            </div>
                            
                            <?php /* tag "div" from line 82 */; ?>
<div class="clearfix">

                                <?php /* tag "label" from line 84 */; ?>
<label for="form-keyword" class="form-label">Video URL <?php /* tag "em" from line 84 */; ?>
<em>*</em></label>

                                <?php /* tag "div" from line 86 */; ?>
<div class="form-input"><?php /* tag "input" from line 86 */; ?>
<input type="text" id="form-url" name="videoURL" required="required" placeholder=""/></div>

                            </div>

                            <?php /* tag "div" from line 90 */; ?>
<div class="form-action clearfix">

                                <?php /* tag "button" from line 92 */; ?>
<button class="button" type="submit">Save</button>

                                <?php /* tag "button" from line 94 */; ?>
<button class="button" type="reset">Clear</button>

                            </div>

                        </form>
                    </div>
                </div>
	<?php endif; ?>
	
	<?php 
/* tag "tal:block" from line 102 */ ;
if (!($ctx->add)):  ;
/* edit video form */ ;
?>

                <?php /* tag "h1" from line 103 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 103 */; ?>
<span style="float:left;"><?php /* tag "a" from line 103 */; ?>
<a href="#/core/home" style="color:#fff;">Videos</a> &raquo; Edit Video</span></h1>
                <?php /* tag "div" from line 104 */; ?>
<div class="container_12 clearfix leading">
                    <?php /* tag "div" from line 105 */; ?>
<div class="grid_12">
						<?php /* tag "div" from line 106 */; ?>
<div id="message"></div>
                    
                    	<?php /* tag "form" from line 108 */; ?>
<form id="form-video" class="form has-validation" method="post">

                            <?php /* tag "div" from line 110 */; ?>
<div class="clearfix">

                                <?php /* tag "label" from line 112 */; ?>
<label for="form-name" class="form-label">Video Title<?php /* tag "em" from line 112 */; ?>
<em>*</em></label>

                                <?php /* tag "div" from line 114 */; ?>
<div class="form-input"><?php /* tag "input" from line 114 */; ?>
<input type="text" id="form-title" name="title" required="required" placeholder=""/></div>

                            </div>

                            <?php /* tag "div" from line 118 */; ?>
<div class="clearfix">

                                <?php /* tag "label" from line 120 */; ?>
<label for="form-keyword" class="form-label">Video Description <?php /* tag "em" from line 120 */; ?>
<em>*</em></label>

                                <?php /* tag "div" from line 122 */; ?>
<div class="form-input"><?php /* tag "input" from line 122 */; ?>
<input type="text" id="form-description" name="videoDescription" required="required" placeholder=""/></div>

                            </div>
                            
                            <?php /* tag "div" from line 126 */; ?>
<div class="clearfix">

                                <?php /* tag "label" from line 128 */; ?>
<label for="form-keyword" class="form-label">Video URL <?php /* tag "em" from line 128 */; ?>
<em>*</em></label>

                                <?php /* tag "div" from line 130 */; ?>
<div class="form-input"><?php /* tag "input" from line 130 */; ?>
<input type="text" id="form-url" name="videoURL" required="required" placeholder=""/></div>

                            </div>

                            <?php /* tag "div" from line 134 */; ?>
<div class="form-action clearfix">

                                <?php /* tag "button" from line 136 */; ?>
<button class="button" type="submit">Save</button>

                                <?php /* tag "button" from line 138 */; ?>
<button class="button" type="reset">Clear</button>

                            </div>

                        </form>
                    </div>
                </div>
	<?php endif; ?>

<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/iagh/manager/application/views/core/homeVideoForm.zpt (edit that file instead) */; ?>