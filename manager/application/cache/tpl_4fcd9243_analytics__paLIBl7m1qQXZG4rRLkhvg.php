<?php 
function tpl_4fcd9243_analytics__paLIBl7m1qQXZG4rRLkhvg(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<?php /* tag "link" from line 2 */; ?>
<link rel="stylesheet" href="/css/dashboard/style.css" media="screen"/>
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" href="/css/dashboard/barchart.css" media="screen"/>
<?php /* tag "link" from line 4 */; ?>
<link rel="stylesheet" href="/css/calendar/ui-lightness/jquery-ui-1.8.16.custom.css" media="screen"/>
<?php /* tag "script" from line 5 */; ?>
<script src="/js/calendar/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>

<?php /* tag "script" from line 7 */; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	  // for each result row..
	  $(".extras_result").each(function() {
	    // get the width of the bar from the span html
	    var length = $(this).find("span").html();
	    length = length.replace("%","");
	    length = length / 100;
	    var lengthPx = 450 * length;
	    
	    $(this).find("p").animate({'width':lengthPx}, 2000, function() {
	      // once the bar animation has finished, fade in the results
	      $(this).find("span").fadeIn(800);
	    });
	  });
	});
</script>


<?php /* tag "script" from line 26 */; ?>
<script>
    $(function() {
        $( "#startDate" ).datepicker();
        $( "#startDate" ).change(function() {
            $( "#startDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });     
	// for extraction form
    $(function() {
        $( "#endDate" ).datepicker();
        $( "#endDate" ).change(function() {
            $( "#endDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });        
</script>		
    
    <?php /* tag "h1" from line 42 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 42 */; ?>
<span style="float:left;">Dashboard</span></h1>
	<?php /* tag "div" from line 43 */; ?>
<div id="folderTab"><?php /* tag "div" from line 43 */; ?>
<div class="folderTabText">Analytics</div></div>
	<?php /* tag "br" from line 44 */; ?>
<br/>	<?php /* tag "br" from line 44 */; ?>
<br/>	<?php /* tag "br" from line 44 */; ?>
<br/>	
	<?php /* tag "div" from line 45 */; ?>
<div class="dashboardTabMenu">
		<?php /* tag "span" from line 46 */; ?>
<span><?php /* tag "a" from line 46 */; ?>
<a href="admin#/dashboard/index">Home</a></span>
		<?php /* tag "span" from line 47 */; ?>
<span><?php /* tag "a" from line 47 */; ?>
<a class="selected" href="#">Analytics</a></span>
		<?php /* tag "span" from line 48 */; ?>
<span><?php /* tag "a" from line 48 */; ?>
<a href="#">Menu 3</a></span>
		<?php /* tag "span" from line 49 */; ?>
<span><?php /* tag "a" from line 49 */; ?>
<a href="#">Menu 4</a></span>
	</div>
	<?php /* tag "div" from line 51 */; ?>
<div class="dimension">
		Dimensions<?php /* tag "br" from line 52 */; ?>
<br/>
		Start Date: <?php /* tag "input" from line 53 */; ?>
<input type="text" id="startDate" name="startDate"/>&nbsp;&nbsp;&nbsp;
		End Date: <?php /* tag "input" from line 54 */; ?>
<input type="text" id="endDate" name="endDate"/>&nbsp;&nbsp;&nbsp;
		<?php /* tag "input" from line 55 */; ?>
<input type="button" value="Query"/>
	</div>
	<?php /* tag "div" from line 57 */; ?>
<div class="reportContainer">
		<?php /* tag "div" from line 58 */; ?>
<div class="tbl">
			<?php /* tag "table" from line 59 */; ?>
<table>
				<?php /* tag "tr" from line 60 */; ?>
<tr>
					<?php /* tag "td" from line 61 */; ?>
<td>
						<?php /* tag "table" from line 62 */; ?>
<table style="width:200px;">
							<?php /* tag "tr" from line 63 */; ?>
<tr>
								<?php /* tag "th" from line 64 */; ?>
<th colspan="3">Today</th>
							</tr>
							<?php /* tag "tr" from line 66 */; ?>
<tr>
								<?php /* tag "td" from line 67 */; ?>
<td colspan="3" align="center"><?php /* tag "span" from line 67 */; ?>
<span style="font-size:9px; font-style:Italic; font-weight:normal;"><?php /* tag "tal:block" from line 67 */; ?>
<?php echo phptal_escape($ctx->todayDate); ?>
</span></td>
							</tr>
							<?php /* tag "tr" from line 69 */; ?>
<tr>
								<?php /* tag "td" from line 70 */; ?>
<td>&nbsp;</td>
								<?php /* tag "td" from line 71 */; ?>
<td>
									<?php /* tag "table" from line 72 */; ?>
<table style="width:75px; text-align:center;">
										<?php /* tag "tr" from line 73 */; ?>
<tr>
											<?php /* tag "th" from line 74 */; ?>
<th>Hour</th><?php /* tag "th" from line 74 */; ?>
<th>Hits</th>
										</tr>
										<?php 
/* tag "tal:block" from line 76 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

											<?php 
/* tag "tal:block" from line 77 */ ;
if ($ctx->repeat->item->index <= 11):  ;
?>

											<?php /* tag "tr" from line 78 */; ?>
<tr><?php /* tag "td" from line 78 */; ?>
<td><?php /* tag "tal:block" from line 78 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 78 */; ?>
<td><?php /* tag "tal:block" from line 78 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
											<?php endif; ?>

										<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

									</table>
								</td>
								<?php /* tag "td" from line 83 */; ?>
<td valign="top">
									<?php /* tag "table" from line 84 */; ?>
<table style="width:75px;text-align:center;">
										<?php /* tag "th" from line 85 */; ?>
<th>Hour</th><?php /* tag "th" from line 85 */; ?>
<th>Hits</th>
										<?php 
/* tag "tal:block" from line 86 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

											<?php 
/* tag "tal:block" from line 87 */ ;
if ($ctx->repeat->item->index >= 12):  ;
?>

											<?php /* tag "tr" from line 88 */; ?>
<tr><?php /* tag "td" from line 88 */; ?>
<td><?php /* tag "tal:block" from line 88 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 88 */; ?>
<td><?php /* tag "tal:block" from line 88 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
											<?php endif; ?>

										<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 99 */; ?>
<div id="bargraph">
			<?php /* tag "br" from line 100 */; ?>
<br/>
			<?php /* tag "table" from line 101 */; ?>
<table class="extras_table" align="center">
			<?php /* tag "tr" from line 102 */; ?>
<tr>
				<?php /* tag "th" from line 103 */; ?>
<th colspan="2">Top Follows</th>
			</tr>
			<?php /* tag "tr" from line 105 */; ?>
<tr><?php /* tag "td" from line 105 */; ?>
<td colspan="2">&nbsp;</td></tr>
				<?php
					$a[0]=1000;
					$a[1]=100;
					$a[2]=200;
					$a[3]=120;
					$a[4]=110;
					$a[5]=170;
					$t = array_sum($a);
					
					for($i=0; $i<count($a);$i++){
						$p = $a[$i]/$t*100;
						$p = number_format($p);
						echo '<tr>
								<th  class="extras_y-desc" scope="row">user '.$i.'</th>
								<td><div class="extras_result"><p class="extras_p">&nbsp;<span>'.$p.'%</span></p>'.number_format($a[$i]).'</div></td>
							</tr>
						';
					}
				?>
			</table>
		</div>
	</div>
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/dashboard/analytics.zpt (edit that file instead) */; ?>