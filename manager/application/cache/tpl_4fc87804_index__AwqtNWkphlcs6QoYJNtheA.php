<?php 
function tpl_4fc87804_index__AwqtNWkphlcs6QoYJNtheA(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<?php /* tag "link" from line 2 */; ?>
<link rel="stylesheet" href="/css/dashboard/style.css" media="screen"/>
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" href="/css/dashboard/barchart.css" media="screen"/>
<?php /* tag "script" from line 4 */; ?>
<script language="javascript" src="/js/jquery-ui-1.8.16.custom.min.js"></script>
<?php /* tag "script" from line 5 */; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	  // for each result row..
	  $(".extras_result").each(function() {
	    // get the width of the bar from the span html
	    var length = $(this).find("span").html();
	    length = '100%';
	    $(this).find("p").animate({'width':length}, 2000, function() {
	      // once the bar animation has finished, fade in the results
	      $(this).find("span").fadeIn(800);
	    });
	  });
	});
</script>
	<?php /* tag "h1" from line 19 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 19 */; ?>
<span style="float:left;">Dashboard</span></h1>
	<?php /* tag "div" from line 20 */; ?>
<div id="folderTab"><?php /* tag "div" from line 20 */; ?>
<div class="folderTabText">Hello World</div></div>
	<?php /* tag "div" from line 21 */; ?>
<div class="folderContainer">
		<?php /* tag "div" from line 22 */; ?>
<div class="tbl">
			<?php /* tag "table" from line 23 */; ?>
<table>
				<?php /* tag "tr" from line 24 */; ?>
<tr>
					<?php /* tag "td" from line 25 */; ?>
<td>
						<?php /* tag "table" from line 26 */; ?>
<table style="width:200px;">
							<?php /* tag "tr" from line 27 */; ?>
<tr>
								<?php /* tag "th" from line 28 */; ?>
<th colspan="2">By Hour</th>
							</tr>
							<?php /* tag "tr" from line 30 */; ?>
<tr>
								<?php /* tag "td" from line 31 */; ?>
<td>
									<?php /* tag "table" from line 32 */; ?>
<table style="width:75px;">
										<?php /* tag "tr" from line 33 */; ?>
<tr><?php /* tag "td" from line 33 */; ?>
<td>12 am</td><?php /* tag "td" from line 33 */; ?>
<td>141</td></tr>
										<?php /* tag "tr" from line 34 */; ?>
<tr><?php /* tag "td" from line 34 */; ?>
<td>01 am</td><?php /* tag "td" from line 34 */; ?>
<td>142</td></tr>
										<?php /* tag "tr" from line 35 */; ?>
<tr><?php /* tag "td" from line 35 */; ?>
<td>02 am</td><?php /* tag "td" from line 35 */; ?>
<td>141</td></tr>
										<?php /* tag "tr" from line 36 */; ?>
<tr><?php /* tag "td" from line 36 */; ?>
<td>03 am</td><?php /* tag "td" from line 36 */; ?>
<td>144</td></tr>
										<?php /* tag "tr" from line 37 */; ?>
<tr><?php /* tag "td" from line 37 */; ?>
<td>04 am</td><?php /* tag "td" from line 37 */; ?>
<td>145</td></tr>
										<?php /* tag "tr" from line 38 */; ?>
<tr><?php /* tag "td" from line 38 */; ?>
<td>05 am</td><?php /* tag "td" from line 38 */; ?>
<td>142</td></tr>
										<?php /* tag "tr" from line 39 */; ?>
<tr><?php /* tag "td" from line 39 */; ?>
<td>06 am</td><?php /* tag "td" from line 39 */; ?>
<td>147</td></tr>
										<?php /* tag "tr" from line 40 */; ?>
<tr><?php /* tag "td" from line 40 */; ?>
<td>07 am</td><?php /* tag "td" from line 40 */; ?>
<td>145</td></tr>
										<?php /* tag "tr" from line 41 */; ?>
<tr><?php /* tag "td" from line 41 */; ?>
<td>08 am</td><?php /* tag "td" from line 41 */; ?>
<td>143</td></tr>
										<?php /* tag "tr" from line 42 */; ?>
<tr><?php /* tag "td" from line 42 */; ?>
<td>09 am</td><?php /* tag "td" from line 42 */; ?>
<td>143</td></tr>
										<?php /* tag "tr" from line 43 */; ?>
<tr><?php /* tag "td" from line 43 */; ?>
<td>10 am</td><?php /* tag "td" from line 43 */; ?>
<td>144</td></tr>
										<?php /* tag "tr" from line 44 */; ?>
<tr><?php /* tag "td" from line 44 */; ?>
<td>11 am</td><?php /* tag "td" from line 44 */; ?>
<td>145</td></tr>
									</table>
								</td>
								<?php /* tag "td" from line 47 */; ?>
<td>
									<?php /* tag "table" from line 48 */; ?>
<table style="width:75px;">
										<?php /* tag "tr" from line 49 */; ?>
<tr><?php /* tag "td" from line 49 */; ?>
<td>12 pm</td><?php /* tag "td" from line 49 */; ?>
<td>144</td></tr>
										<?php /* tag "tr" from line 50 */; ?>
<tr><?php /* tag "td" from line 50 */; ?>
<td>01 pm</td><?php /* tag "td" from line 50 */; ?>
<td>147</td></tr>
										<?php /* tag "tr" from line 51 */; ?>
<tr><?php /* tag "td" from line 51 */; ?>
<td>02 pm</td><?php /* tag "td" from line 51 */; ?>
<td>149</td></tr>
										<?php /* tag "tr" from line 52 */; ?>
<tr><?php /* tag "td" from line 52 */; ?>
<td>03 pm</td><?php /* tag "td" from line 52 */; ?>
<td>124</td></tr>
										<?php /* tag "tr" from line 53 */; ?>
<tr><?php /* tag "td" from line 53 */; ?>
<td>04 pm</td><?php /* tag "td" from line 53 */; ?>
<td>114</td></tr>
										<?php /* tag "tr" from line 54 */; ?>
<tr><?php /* tag "td" from line 54 */; ?>
<td>05 pm</td><?php /* tag "td" from line 54 */; ?>
<td>134</td></tr>
										<?php /* tag "tr" from line 55 */; ?>
<tr><?php /* tag "td" from line 55 */; ?>
<td>06 pm</td><?php /* tag "td" from line 55 */; ?>
<td>124</td></tr>
										<?php /* tag "tr" from line 56 */; ?>
<tr><?php /* tag "td" from line 56 */; ?>
<td>07 pm</td><?php /* tag "td" from line 56 */; ?>
<td>144</td></tr>
										<?php /* tag "tr" from line 57 */; ?>
<tr><?php /* tag "td" from line 57 */; ?>
<td>08 pm</td><?php /* tag "td" from line 57 */; ?>
<td>134</td></tr>
										<?php /* tag "tr" from line 58 */; ?>
<tr><?php /* tag "td" from line 58 */; ?>
<td>09 pm</td><?php /* tag "td" from line 58 */; ?>
<td>114</td></tr>
										<?php /* tag "tr" from line 59 */; ?>
<tr><?php /* tag "td" from line 59 */; ?>
<td>10 pm</td><?php /* tag "td" from line 59 */; ?>
<td>164</td></tr>
										<?php /* tag "tr" from line 60 */; ?>
<tr><?php /* tag "td" from line 60 */; ?>
<td>11 pm</td><?php /* tag "td" from line 60 */; ?>
<td>184</td></tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 69 */; ?>
<div id="bargraph">
			<?php /* tag "table" from line 70 */; ?>
<table cellpadding="0" cellspacing="0" class="extras_table">
				<?php
					$a[0]=100;
					$a[1]=100;
					$a[2]=200;
					$a[3]=100;
					$t = array_sum($a);
					
					for($i=0; $i<count($a);$i++){
						$p = $a[$i]/$t*100;
						echo '<tr>
								<th  class="extras_y-desc" scope="row">2012-05-01</th>
								<td><div class="extras_result"><p class="extras_p">&nbsp;<span>'.$p.'%</span></p>2033</div></td>
							</tr>
						';
					}
				?>
				<?php /* tag "tr" from line 87 */; ?>
<tr>
					<?php /* tag "th" from line 88 */; ?>
<th class="extras_y-desc" scope="row">2012-05-01</th>
					<?php /* tag "td" from line 89 */; ?>
<td><?php /* tag "div" from line 89 */; ?>
<div class="extras_result"><?php /* tag "p" from line 89 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 89 */; ?>
<span>90%</span></p>2033</div></td>
				</tr>
				<?php /* tag "tr" from line 91 */; ?>
<tr>
					<?php /* tag "th" from line 92 */; ?>
<th class="extras_y-desc" scope="row">2012-05-02</th>
					<?php /* tag "td" from line 93 */; ?>
<td><?php /* tag "div" from line 93 */; ?>
<div class="extras_result"><?php /* tag "p" from line 93 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 93 */; ?>
<span>20%</span></p>23</div></td>
				</tr>
				<?php /* tag "tr" from line 95 */; ?>
<tr>
					<?php /* tag "th" from line 96 */; ?>
<th class="extras_y-desc" scope="row">2012-05-03</th>
					<?php /* tag "td" from line 97 */; ?>
<td><?php /* tag "div" from line 97 */; ?>
<div class="extras_result"><?php /* tag "p" from line 97 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 97 */; ?>
<span>90%</span></p>2033</div></td>
				</tr>
				<?php /* tag "tr" from line 99 */; ?>
<tr>
					<?php /* tag "th" from line 100 */; ?>
<th class="extras_y-desc" scope="row">2012-05-04</th>
					<?php /* tag "td" from line 101 */; ?>
<td><?php /* tag "div" from line 101 */; ?>
<div class="extras_result"><?php /* tag "p" from line 101 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 101 */; ?>
<span>20%</span></p>23</div></td>
				</tr>
			</table>
		</div>
	</div>
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/dashboard/index.zpt (edit that file instead) */; ?>