<?php 
function tpl_4fcd9dfa_analytics__paLIBl7m1qQXZG4rRLkhvg(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<?php /* tag "link" from line 2 */; ?>
<link rel="stylesheet" href="/css/dashboard/style.css" media="screen"/>
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" href="/css/dashboard/barchart.css" media="screen"/>
<?php /* tag "link" from line 4 */; ?>
<link rel="stylesheet" href="/css/dashboard/chart.css" media="screen"/>
<?php /* tag "link" from line 5 */; ?>
<link rel="stylesheet" href="/css/calendar/ui-lightness/jquery-ui-1.8.16.custom.css" media="screen"/>
<?php /* tag "script" from line 6 */; ?>
<script src="/js/calendar/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>

<?php /* tag "script" from line 8 */; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	  // for each result row..
	  $(".extras_result").each(function() {
	    // get the width of the bar from the span html
	    var length = $(this).find("span").html();
	    length = length.replace("%","");
	    length = length / 100;
	    var lengthPx = 450 * length;
	    
	    $(this).find("p").animate({'width':lengthPx}, 2000, function() {
	      // once the bar animation has finished, fade in the results
	      $(this).find("span").fadeIn(800);
	    });
	  });
	});
</script>


<?php /* tag "script" from line 27 */; ?>
<script>
    $(function() {
        $( "#startDate" ).datepicker();
        $( "#startDate" ).change(function() {
            $( "#startDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });     
	// for extraction form
    $(function() {
        $( "#endDate" ).datepicker();
        $( "#endDate" ).change(function() {
            $( "#endDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });        
</script>		
    
    <?php /* tag "h1" from line 43 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 43 */; ?>
<span style="float:left;">Dashboard</span></h1>
	<?php /* tag "div" from line 44 */; ?>
<div id="folderTab"><?php /* tag "div" from line 44 */; ?>
<div class="folderTabText">Analytics</div></div>
	<?php /* tag "br" from line 45 */; ?>
<br/>	<?php /* tag "br" from line 45 */; ?>
<br/>	<?php /* tag "br" from line 45 */; ?>
<br/>	
	<?php /* tag "div" from line 46 */; ?>
<div class="dashboardTabMenu">
		<?php /* tag "span" from line 47 */; ?>
<span><?php /* tag "a" from line 47 */; ?>
<a href="admin#/dashboard/index">Home</a></span>
		<?php /* tag "span" from line 48 */; ?>
<span><?php /* tag "a" from line 48 */; ?>
<a class="selected" href="#">Analytics</a></span>
		<?php /* tag "span" from line 49 */; ?>
<span><?php /* tag "a" from line 49 */; ?>
<a href="#">Menu 3</a></span>
		<?php /* tag "span" from line 50 */; ?>
<span><?php /* tag "a" from line 50 */; ?>
<a href="#">Menu 4</a></span>
	</div>
	<?php /* tag "div" from line 52 */; ?>
<div class="dimension">
		Dimensions<?php /* tag "br" from line 53 */; ?>
<br/>
		Start Date: <?php /* tag "input" from line 54 */; ?>
<input type="text" id="startDate" name="startDate"/>&nbsp;&nbsp;&nbsp;
		End Date: <?php /* tag "input" from line 55 */; ?>
<input type="text" id="endDate" name="endDate"/>&nbsp;&nbsp;&nbsp;
		<?php /* tag "input" from line 56 */; ?>
<input type="button" value="Query"/>
	</div>
	<?php /* tag "div" from line 58 */; ?>
<div class="reportContainer">
		<?php /* tag "div" from line 59 */; ?>
<div class="tbl">
			<?php /* tag "table" from line 60 */; ?>
<table>
				<?php /* tag "tr" from line 61 */; ?>
<tr>
					<?php /* tag "td" from line 62 */; ?>
<td>
						<?php /* tag "table" from line 63 */; ?>
<table style="width:200px;">
							<?php /* tag "tr" from line 64 */; ?>
<tr>
								<?php /* tag "th" from line 65 */; ?>
<th colspan="3">Today</th>
							</tr>
							<?php /* tag "tr" from line 67 */; ?>
<tr>
								<?php /* tag "td" from line 68 */; ?>
<td colspan="3" align="center"><?php /* tag "span" from line 68 */; ?>
<span style="font-size:9px; font-style:Italic; font-weight:normal;"><?php /* tag "tal:block" from line 68 */; ?>
<?php echo phptal_escape($ctx->todayDate); ?>
</span></td>
							</tr>
							<?php /* tag "tr" from line 70 */; ?>
<tr>
								<?php /* tag "td" from line 71 */; ?>
<td>&nbsp;</td>
								<?php /* tag "td" from line 72 */; ?>
<td>
									<?php /* tag "table" from line 73 */; ?>
<table style="width:75px; text-align:center;">
										<?php /* tag "tr" from line 74 */; ?>
<tr>
											<?php /* tag "th" from line 75 */; ?>
<th>Hour</th><?php /* tag "th" from line 75 */; ?>
<th>Hits</th>
										</tr>
										<?php 
/* tag "tal:block" from line 77 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

											<?php 
/* tag "tal:block" from line 78 */ ;
if ($ctx->repeat->item->index <= 11):  ;
?>

											<?php /* tag "tr" from line 79 */; ?>
<tr><?php /* tag "td" from line 79 */; ?>
<td><?php /* tag "tal:block" from line 79 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 79 */; ?>
<td><?php /* tag "tal:block" from line 79 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
											<?php endif; ?>

										<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

									</table>
								</td>
								<?php /* tag "td" from line 84 */; ?>
<td valign="top">
									<?php /* tag "table" from line 85 */; ?>
<table style="width:75px;text-align:center;">
										<?php /* tag "th" from line 86 */; ?>
<th>Hour</th><?php /* tag "th" from line 86 */; ?>
<th>Hits</th>
										<?php 
/* tag "tal:block" from line 87 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

											<?php 
/* tag "tal:block" from line 88 */ ;
if ($ctx->repeat->item->index >= 12):  ;
?>

											<?php /* tag "tr" from line 89 */; ?>
<tr><?php /* tag "td" from line 89 */; ?>
<td><?php /* tag "tal:block" from line 89 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 89 */; ?>
<td><?php /* tag "tal:block" from line 89 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
											<?php endif; ?>

										<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 100 */; ?>
<div id="bargraph">
			<?php /* tag "br" from line 101 */; ?>
<br/>
			<?php /* tag "table" from line 102 */; ?>
<table class="extras_table" align="center">
			<?php /* tag "tr" from line 103 */; ?>
<tr>
				<?php /* tag "th" from line 104 */; ?>
<th colspan="2">Top Follows</th>
			</tr>
			<?php /* tag "tr" from line 106 */; ?>
<tr><?php /* tag "td" from line 106 */; ?>
<td colspan="2">&nbsp;</td></tr>
				<?php
					$a[0]=1000;
					$a[1]=100;
					$a[2]=200;
					$a[3]=120;
					$a[4]=110;
					$a[5]=170;
					$t = array_sum($a);
					
					for($i=0; $i<count($a);$i++){
						$p = $a[$i]/$t*100;
						$p = number_format($p);
						echo '<tr>
								<th  class="extras_y-desc" scope="row">user '.$i.'</th>
								<td><div class="extras_result"><p class="extras_p">&nbsp;<span>'.$p.'%</span></p>'.number_format($a[$i]).'</div></td>
							</tr>
						';
					}
				?>
			</table>
		</div>
		<?php /* tag "div" from line 128 */; ?>
<div id="piegraph">
			<?php /* tag "canvas" from line 129 */; ?>
<canvas id="chart" width="400" height="200"></canvas>
			<?php /* tag "table" from line 130 */; ?>
<table id="chartData">
				<?php /* tag "tr" from line 131 */; ?>
<tr><?php /* tag "th" from line 131 */; ?>
<th colspan="2" style="text-align:center">PIE</th></tr>
				<?php /* tag "tr" from line 132 */; ?>
<tr><?php /* tag "th" from line 132 */; ?>
<th>Date</th><?php /* tag "th" from line 132 */; ?>
<th>Hits</th></tr>
				<?php /* tag "tr" from line 133 */; ?>
<tr style="color: #72808E">
					<?php /* tag "td" from line 134 */; ?>
<td>2012-01-02</td>
					<?php /* tag "td" from line 135 */; ?>
<td>500</td>
				</tr>
				<?php /* tag "tr" from line 137 */; ?>
<tr style="color: #000000">
					<?php /* tag "td" from line 138 */; ?>
<td>2012-01-03</td>
					<?php /* tag "td" from line 139 */; ?>
<td>300</td>
				</tr>
			</table>
			<?php /* tag "iframe" from line 142 */; ?>
<iframe id="secretIFrame" src="" style="display:none; visibility:hidden;"></iframe>
		</div>
	</div>
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/dashboard/analytics.zpt (edit that file instead) */; ?>