<?php 
function tpl_4fcc7fdd_index__AwqtNWkphlcs6QoYJNtheA(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<?php /* tag "link" from line 2 */; ?>
<link rel="stylesheet" href="/css/dashboard/style.css" media="screen"/>
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" href="/css/dashboard/barchart.css" media="screen"/>
<?php /* tag "script" from line 4 */; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	  // for each result row..
	  $(".extras_result").each(function() {
	    // get the width of the bar from the span html
	    var length = $(this).find("span").html();
	    length = length.replace("%","");
	    length = length / 100;
	    var lengthPx = 450 * length;
	    
	    $(this).find("p").animate({'width':lengthPx}, 2000, function() {
	      // once the bar animation has finished, fade in the results
	      $(this).find("span").fadeIn(800);
	    });
	  });
	});
</script>
	<?php /* tag "h1" from line 21 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 21 */; ?>
<span style="float:left;">Dashboard</span></h1>
	<?php /* tag "div" from line 22 */; ?>
<div id="folderTab"><?php /* tag "div" from line 22 */; ?>
<div class="folderTabText">Analytics</div></div>
	<?php /* tag "div" from line 23 */; ?>
<div class="folderContainer">
		<?php /* tag "div" from line 24 */; ?>
<div class="tbl">
		<?php /* tag "br" from line 25 */; ?>
<br/>
			<?php /* tag "table" from line 26 */; ?>
<table>
				<?php /* tag "tr" from line 27 */; ?>
<tr>
					<?php /* tag "td" from line 28 */; ?>
<td>
						<?php /* tag "table" from line 29 */; ?>
<table style="width:200px;">
							<?php /* tag "tr" from line 30 */; ?>
<tr>
								<?php /* tag "th" from line 31 */; ?>
<th colspan="3">By Hour <?php /* tag "tal:block" from line 31 */; ?>
<?php echo phptal_escape($ctx->timeToday); ?>
</th>
							</tr>
							<?php /* tag "tr" from line 33 */; ?>
<tr>
								<?php /* tag "td" from line 34 */; ?>
<td>&nbsp;</td>
								<?php /* tag "td" from line 35 */; ?>
<td>
									<?php /* tag "table" from line 36 */; ?>
<table style="width:75px;">
										<?php 
/* tag "tal:block" from line 37 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->date = new PHPTAL_RepeatController($ctx->time)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->date as $ctx->date): ;
?>

											<?php /* tag "tr" from line 38 */; ?>
<tr>
												<?php /* tag "td" from line 39 */; ?>
<td><?php /* tag "tal:block" from line 39 */; ?>
<?php echo phptal_escape($ctx->path($ctx->date, 'time')); ?>
</td>
											</tr>
										<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

<!--										<tr><td>12 am</td><td>141</td></tr>
										<tr><td>01 am</td><td>142</td></tr>
										<tr><td>02 am</td><td>141</td></tr>
										<tr><td>03 am</td><td>144</td></tr>
										<tr><td>04 am</td><td>145</td></tr>
										<tr><td>05 am</td><td>142</td></tr>
										<tr><td>06 am</td><td>147</td></tr>
										<tr><td>07 am</td><td>145</td></tr>
										<tr><td>08 am</td><td>143</td></tr>
										<tr><td>09 am</td><td>143</td></tr>
										<tr><td>10 am</td><td>144</td></tr>
										<tr><td>11 am</td><td>145</td></tr>-->
									</table>
								</td>
								<?php /* tag "td" from line 56 */; ?>
<td>
									<?php /* tag "table" from line 57 */; ?>
<table style="width:75px;">
<!--										<tr><td>12 pm</td><td>144</td></tr>
										<tr><td>01 pm</td><td>147</td></tr>
										<tr><td>02 pm</td><td>149</td></tr>
										<tr><td>03 pm</td><td>124</td></tr>
										<tr><td>04 pm</td><td>114</td></tr>
										<tr><td>05 pm</td><td>134</td></tr>
										<tr><td>06 pm</td><td>124</td></tr>
										<tr><td>07 pm</td><td>144</td></tr>
										<tr><td>08 pm</td><td>134</td></tr>
										<tr><td>09 pm</td><td>114</td></tr>
										<tr><td>10 pm</td><td>164</td></tr>
										<tr><td>11 pm</td><td>184</td></tr>-->
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 78 */; ?>
<div id="bargraph">
			<?php /* tag "br" from line 79 */; ?>
<br/>
			<?php /* tag "table" from line 80 */; ?>
<table class="extras_table" align="center">
			<?php /* tag "tr" from line 81 */; ?>
<tr>
				<?php /* tag "th" from line 82 */; ?>
<th colspan="2">Top Follows</th>
			</tr>
				<?php
					$a[0]=1000;
					$a[1]=100;
					$a[2]=200;
					$a[3]=120;
					$a[4]=110;
					$a[5]=170;
					$t = array_sum($a);
					
					for($i=0; $i<count($a);$i++){
						$p = $a[$i]/$t*100;
						$p = number_format($p);
						echo '<tr>
								<th  class="extras_y-desc" scope="row">user '.$i.'</th>
								<td><div class="extras_result"><p class="extras_p">&nbsp;<span>'.$p.'%</span></p>'.number_format($a[$i]).'</div></td>
							</tr>
						';
					}
				?>
			</table>
		</div>
	</div>
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/dashboard/index.zpt (edit that file instead) */; ?>