<?php 
function tpl_4fd92391_base__BN2bS1DUavycj9v7hRtcUQ_admin(PHPTAL $_thistpl, PHPTAL $tpl) {
$tpl = clone $tpl ;
$ctx = $tpl->getContext() ;
$_translator = $tpl->getTranslator() ;
?>

<?php $ctx->setDocType('<!DOCTYPE html>',false); ?>

<!--[if IE 7 ]>   <html lang="en" class="ie7 lte8"> <![endif]--> 
<!--[if IE 8 ]>   <html lang="en" class="ie8 lte8"> <![endif]--> 
<!--[if IE 9 ]>   <html lang="en" class="ie9"> <![endif]--> 
<!--[if gt IE 9]> <html lang="en"> <![endif]-->
<!--[if !IE]><!--> <?php /* tag "html" from line 7 */; ?>
<html lang="en"> <!--<![endif]-->
<?php /* tag "head" from line 8 */; ?>
<head><!--[if lte IE 9 ]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->

<!-- iPad Settings -->
<?php /* tag "meta" from line 11 */; ?>
<meta name="apple-mobile-web-app-capable" content="yes"/>
<?php /* tag "meta" from line 12 */; ?>
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/> 
<?php /* tag "meta" from line 13 */; ?>
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0"/>
<!-- Adding "maximum-scale=1" fixes the Mobile Safari auto-zoom bug: http://filamentgroup.com/examples/iosScaleBug/ -->
<!-- iPad Settings End -->

<?php /* tag "title" from line 17 */; ?>
<title><?php echo phptal_escape($ctx->title) ?>
</title>

<?php /* tag "link" from line 19 */; ?>
<link rel="shortcut icon" href="favicon.ico"/>

<!-- iOS ICONS -->
<?php /* tag "link" from line 22 */; ?>
<link rel="apple-touch-icon" href="touch-icon-iphone.png"/>
<?php /* tag "link" from line 23 */; ?>
<link rel="apple-touch-icon" sizes="72x72" href="touch-icon-ipad.png"/>
<?php /* tag "link" from line 24 */; ?>
<link rel="apple-touch-icon" sizes="114x114" href="touch-icon-iphone4.png"/>
<?php /* tag "link" from line 25 */; ?>
<link rel="apple-touch-startup-image" href="touch-startup-image.png"/>
<!-- iOS ICONS END -->

<!-- STYLESHEETS -->

<?php /* tag "link" from line 30 */; ?>
<link rel="stylesheet" href="/css/reset.css" media="screen"/>
<?php /* tag "link" from line 31 */; ?>
<link rel="stylesheet" href="/css/grids.css" media="screen"/>
<?php /* tag "link" from line 32 */; ?>
<link rel="stylesheet" href="/css/ui.css" media="screen"/>
<?php /* tag "link" from line 33 */; ?>
<link rel="stylesheet" href="/css/forms.css" media="screen"/>
<?php /* tag "link" from line 34 */; ?>
<link rel="stylesheet" href="/css/device/general.css" media="screen"/>
<!--[if !IE]><!-->
<?php /* tag "link" from line 36 */; ?>
<link rel="stylesheet" href="/css/device/tablet.css" media="only screen and (min-width: 768px) and (max-width: 991px)"/>
<?php /* tag "link" from line 37 */; ?>
<link rel="stylesheet" href="/css/device/mobile.css" media="only screen and (max-width: 767px)"/>
<?php /* tag "link" from line 38 */; ?>
<link rel="stylesheet" href="/css/device/wide-mobile.css" media="only screen and (min-width: 480px) and (max-width: 767px)"/>
<!--<![endif]-->
<?php /* tag "link" from line 40 */; ?>
<link rel="stylesheet" href="/css/jquery.uniform.css" media="screen"/>
<?php /* tag "link" from line 41 */; ?>
<link rel="stylesheet" href="/css/jquery.popover.css" media="screen"/>
<?php /* tag "link" from line 42 */; ?>
<link rel="stylesheet" href="/css/jquery.itextsuggest.css" media="screen"/>
<?php /* tag "link" from line 43 */; ?>
<link rel="stylesheet" href="/css/themes/lightblue/style.css" media="screen"/>



<?php /* tag "style" from line 47 */; ?>
<style type="text/css">
    #loading-container {position: absolute; top:50%; left:50%;}
    #loading-content {width:800px; text-align:center; margin-left: -400px; height:50px; margin-top:-25px; line-height: 50px;}
    #loading-content {font-family: "Helvetica", "Arial", sans-serif; font-size: 18px; color: black; text-shadow: 0px 1px 0px white; }
    #loading-graphic {margin-right: 0.2em; margin-bottom:-2px;}
    #loading {background-color:#abc4ff; background-image: -moz-radial-gradient(50% 50%, ellipse closest-side, #abc4ff, #87a7ff 100%); background-image: -webkit-radial-gradient(50% 50%, ellipse closest-side, #abc4ff, #87a7ff 100%); background-image: -o-radial-gradient(50% 50%, ellipse closest-side, #abc4ff, #87a7ff 100%); background-image: -ms-radial-gradient(50% 50%, ellipse closest-side, #abc4ff, #87a7ff 100%); background-image: radial-gradient(50% 50%, ellipse closest-side, #abc4ff, #87a7ff 100%); height:100%; width:100%; overflow:hidden; position: absolute; left: 0; top: 0; z-index: 99999;}
</style>

<!-- STYLESHEETS END -->

<!--[if lt IE 9]>
<script src="/js/html5.js"></script>
<script type="text/javascript" src="/js/selectivizr.js"></script>
<![endif]-->

</head>
<?php /* tag "body" from line 63 */; ?>
<body style="overflow: hidden;">
    <?php /* tag "div" from line 64 */; ?>
<div id="loading"> 

        <?php /* tag "script" from line 66 */; ?>
<script type="text/javascript"> 
            document.write('<?php /* tag "div" from line 67 */; ?>
<div id="loading-container"><?php /* tag "p" from line 67 */; ?>
<p id="loading-content">' +
                           '<?php /* tag "img" from line 68 */; ?>
<img id="loading-graphic" width="16" height="16" src="/images/ajax-loader-abc4ff.gif"/> ' +
                           'Loading...</p></div>');
        </script> 

    </div> 

    <?php /* tag "div" from line 74 */; ?>
<div id="wrapper">
        <?php /* tag "header" from line 75 */; ?>
<header>
            <?php /* tag "h1" from line 76 */; ?>
<h1><?php /* tag "a" from line 76 */; ?>
<a href="#">CloudMsngr</a></h1>
            <?php /* tag "nav" from line 77 */; ?>
<nav>
                <?php /* tag "div" from line 78 */; ?>
<div class="container_12">
                    <?php /* tag "div" from line 79 */; ?>
<div class="grid_12">
                        <?php /* tag "ul" from line 80 */; ?>
<ul class="toolbar clearfix fl">
                            <?php /* tag "li" from line 81 */; ?>
<li>
                                <?php /* tag "a" from line 82 */; ?>
<a href="#" title="Activity" class="icon-only" id="activity-button">
                                    <?php /* tag "img" from line 83 */; ?>
<img src="/images/navicons-small/77.png" alt=""/>
                                    <?php /* tag "span" from line 84 */; ?>
<span class="message-count">1</span>
                                </a>
                            </li>
                            <?php /* tag "li" from line 87 */; ?>
<li>
                                <?php /* tag "a" from line 88 */; ?>
<a href="#" title="Notifications" class="icon-only" id="notifications-button">
                                    <?php /* tag "img" from line 89 */; ?>
<img src="/images/navicons-small/08.png" alt=""/>
                                    <?php /* tag "span" from line 90 */; ?>
<span class="message-count">3</span>
                                </a>
                            </li>
                            <?php /* tag "li" from line 93 */; ?>
<li>
                                <?php /* tag "a" from line 94 */; ?>
<a href="#" title="Settings" class="icon-only" id="settings-button">
                                    <?php /* tag "img" from line 95 */; ?>
<img src="/images/navicons-small/19.png" alt=""/>
                                </a>
                            </li>
                        </ul>
                        <?php /* tag "a" from line 99 */; ?>
<a href="/auth/logout" title="Logout" class="button icon-with-text fr"><?php /* tag "img" from line 99 */; ?>
<img src="/images/navicons-small/129.png" alt=""/>Logout</a>
                        <?php /* tag "div" from line 100 */; ?>
<div class="user-info fr">
                            Logged in as <?php /* tag "a" from line 101 */; ?>
<a href="#"><?php echo phptal_escape($ctx->path($ctx->user, 'fullname')); ?>
</a>
                        </div>
                    </div>
                </div>
            </nav>
        </header>
        
        <?php /* tag "section" from line 108 */; ?>
<section>
            <!-- Sidebar -->

            <?php /* tag "aside" from line 111 */; ?>
<aside>
                <?php /* tag "nav" from line 112 */; ?>
<nav class="drilldownMenu">
                    <?php /* tag "h1" from line 113 */; ?>
<h1>
                        <?php /* tag "span" from line 114 */; ?>
<span class="title">Main Menu</span>
                        <?php /* tag "button" from line 115 */; ?>
<button title="Go Back" class="back">Back</button>
                    </h1>
                    <?php /* tag "div" from line 117 */; ?>
<div class="clearfix" id="searchform">
                        <?php /* tag "div" from line 118 */; ?>
<div class="searchcontainer">
                            <?php /* tag "div" from line 119 */; ?>
<div class="searchbox" onclick="$(this).find('input').focus();">
                                <?php /* tag "input" from line 120 */; ?>
<input type="text" name="q" id="q" autocomplete="off" placeholder="Search..."/>
                            </div>
                            <?php /* tag "input" from line 122 */; ?>
<input type="button" value="Cancel"/>
                        </div>
                        <?php /* tag "div" from line 124 */; ?>
<div class="search_results"></div>
                    </div>                        
                    <?php /* tag "ul" from line 126 */; ?>
<ul class="tlm">
                    	<?php /* tag "li" from line 127 */; ?>
<li class="current"><?php /* tag "a" from line 127 */; ?>
<a href="#/dashboard/index" title="Dashboard"><?php /* tag "img" from line 127 */; ?>
<img src="/images/navicons/81.png" alt=""/><?php /* tag "span" from line 127 */; ?>
<span>Dashboard</span></a></li>
						<?php /* tag "li" from line 128 */; ?>
<li><?php /* tag "a" from line 128 */; ?>
<a href="#/core/index" title="Core"><?php /* tag "img" from line 128 */; ?>
<img src="/images/navicons/131.png" alt=""/><?php /* tag "span" from line 128 */; ?>
<span>Core</span></a>
							<?php /* tag "ul" from line 129 */; ?>
<ul>
								<?php /* tag "li" from line 130 */; ?>
<li><?php /* tag "a" from line 130 */; ?>
<a href="#/roles/index" title="Roles"><?php /* tag "img" from line 130 */; ?>
<img src="/images/navicons/23.png" alt=""/><?php /* tag "span" from line 130 */; ?>
<span>Roles</span></a></li>
								<?php /* tag "li" from line 131 */; ?>
<li><?php /* tag "a" from line 131 */; ?>
<a href="#/permissions/index" title="Permissions"><?php /* tag "img" from line 131 */; ?>
<img src="/images/navicons/25.png" alt=""/><?php /* tag "span" from line 131 */; ?>
<span>Permissions</span></a></li>
							</ul>
						</li>
						
						<?php /* tag "li" from line 135 */; ?>
<li><?php /* tag "a" from line 135 */; ?>
<a href="#/members/index" title="Members"><?php /* tag "img" from line 135 */; ?>
<img src="/images/navicons/112.png" alt=""/><?php /* tag "span" from line 135 */; ?>
<span>Members</span></a></li>
						<?php /* tag "li" from line 136 */; ?>
<li class="hasul"><?php /* tag "a" from line 136 */; ?>
<a href="#" title="ACL"><?php /* tag "img" from line 136 */; ?>
<img src="/images/navicons/119.png" alt=""/><?php /* tag "span" from line 136 */; ?>
<span>ACL</span></a>
<!--							<ul>
								<li><a href="#/roles/index" title="Roles"><img src="/images/navicons/23.png" alt=""/><span>Roles</span></a></li>
								<li><a href="#/permissions/index" title="Permissions"><img src="/images/navicons/25.png" alt=""/><span>Permissions</span></a></li>
							</ul>-->
						</li>
						<?php /* tag "li" from line 142 */; ?>
<li class="hasul"><?php /* tag "a" from line 142 */; ?>
<a href="#" title="Monitoring"><?php /* tag "img" from line 142 */; ?>
<img src="/images/navicons/77.png" alt=""/><?php /* tag "span" from line 142 */; ?>
<span>Monitoring</span></a>
							<?php /* tag "ul" from line 143 */; ?>
<ul>
								<?php /* tag "li" from line 144 */; ?>
<li><?php /* tag "a" from line 144 */; ?>
<a href="#/monitoring/server" title="Server"><?php /* tag "img" from line 144 */; ?>
<img src="/images/navicons/69.png" alt=""/><?php /* tag "span" from line 144 */; ?>
<span>Server</span></a></li>
								<?php /* tag "li" from line 145 */; ?>
<li><?php /* tag "a" from line 145 */; ?>
<a href="#/monitoring/gateway" title="Gateway"><?php /* tag "img" from line 145 */; ?>
<img src="/images/navicons/113.png" alt=""/><?php /* tag "span" from line 145 */; ?>
<span>Gateway</span></a></li>
								<?php /* tag "li" from line 146 */; ?>
<li><?php /* tag "a" from line 146 */; ?>
<a href="#/monitoring/queues" title="Queues"><?php /* tag "img" from line 146 */; ?>
<img src="/images/navicons/104.png" alt=""/><?php /* tag "span" from line 146 */; ?>
<span>Queues</span></a></li>
								<?php /* tag "li" from line 147 */; ?>
<li><?php /* tag "a" from line 147 */; ?>
<a href="#/monitoring/connection" title="Connection"><?php /* tag "img" from line 147 */; ?>
<img src="/images/navicons/55.png" alt=""/><?php /* tag "span" from line 147 */; ?>
<span>Connection</span></a></li>
							</ul>
						</li>
						<?php /* tag "li" from line 150 */; ?>
<li class="hasul"><?php /* tag "a" from line 150 */; ?>
<a href="#" title="Report"><?php /* tag "img" from line 150 */; ?>
<img src="/images/navicons/137.png" alt=""/><?php /* tag "span" from line 150 */; ?>
<span>Report</span></a>
							<?php /* tag "ul" from line 151 */; ?>
<ul>
								<?php /* tag "li" from line 152 */; ?>
<li><?php /* tag "a" from line 152 */; ?>
<a href="#/report/mtr" title="MTR"><?php /* tag "img" from line 152 */; ?>
<img src="/images/navicons/29.png" alt=""/><?php /* tag "span" from line 152 */; ?>
<span>MTR</span></a></li>
								<?php /* tag "li" from line 153 */; ?>
<li><?php /* tag "a" from line 153 */; ?>
<a href="#/report/mts" title="MTS"><?php /* tag "img" from line 153 */; ?>
<img src="/images/navicons/29.png" alt=""/><?php /* tag "span" from line 153 */; ?>
<span>MTS</span></a></li>
							</ul>
						</li>
                    </ul>
                </nav>
            </aside>

            <!-- Sidebar End -->

            <?php /* tag "section" from line 162 */; ?>
<section>
                <?php /* tag "header" from line 163 */; ?>
<header>
                    <?php /* tag "div" from line 164 */; ?>
<div class="container_12 clearfix">
                        <?php /* tag "a" from line 165 */; ?>
<a href="#menu" class="showmenu button">Menu</a>
                        <?php /* tag "h1" from line 166 */; ?>
<h1 class="grid_12">Dashboard</h1>
                    </div>
                </header>
                <?php /* tag "section" from line 169 */; ?>
<section id="main-content" class="clearfix">
                </section>
                <?php /* tag "footer" from line 171 */; ?>
<footer class="clearfix">
                    <?php /* tag "div" from line 172 */; ?>
<div class="container_12">
                        <?php /* tag "div" from line 173 */; ?>
<div class="grid_12">
                            Copyright &copy; 2012. Solucient, Inc.  
                        </div>
                    </div>
                </footer>
            </section>

            <!-- Main Section End -->
        </section>
    </div>
    
    <!-- MAIN JAVASCRIPTS -->
    <!-- <script src="//code.jquery.com/jquery-1.7.min.js"></script> -->
    <!-- <script>window.jQuery || document.write("<script src='/js/jquery.min.js'>\x3C/script>")</script> -->
    <?php /* tag "script" from line 187 */; ?>
<script type="text/javascript" src="/js/jquery.min.js"></script>
    <?php /* tag "script" from line 188 */; ?>
<script type="text/javascript" src="/js/jquery.tools.min.js"></script>
    <?php /* tag "script" from line 189 */; ?>
<script type="text/javascript" src="/js/jquery.uniform.min.js"></script>
    <?php /* tag "script" from line 190 */; ?>
<script type="text/javascript" src="/js/jquery.easing.js"></script>
    <?php /* tag "script" from line 191 */; ?>
<script type="text/javascript" src="/js/jquery.ui.totop.js"></script>
    <?php /* tag "script" from line 192 */; ?>
<script type="text/javascript" src="/js/jquery.itextsuggest.js"></script>
    <?php /* tag "script" from line 193 */; ?>
<script type="text/javascript" src="/js/jquery.itextclear.js"></script>
    <?php /* tag "script" from line 194 */; ?>
<script type="text/javascript" src="/js/jquery.hashchange.min.js"></script>
    <?php /* tag "script" from line 195 */; ?>
<script type="text/javascript" src="/js/jquery.drilldownmenu.js"></script>
    <?php /* tag "script" from line 196 */; ?>
<script type="text/javascript" src="/js/jquery.popover.js"></script>
    
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/js/PIE.js"></script>
    <script type="text/javascript" src="/js/ie.js"></script>
    <![endif]-->

    <?php /* tag "script" from line 203 */; ?>
<script type="text/javascript" src="/js/global.js"></script>
    <!-- MAIN JAVASCRIPTS END -->

    <!-- LOADING SCRIPT -->
    <?php /* tag "script" from line 207 */; ?>
<script>
    $(window).load(function(){
        $("#loading").fadeOut(function(){
            $(this).remove();
            $('body').removeAttr('style');
        });
    });
    </script>
    <!-- LOADING SCRIPT -->
    
    <!-- POPOVERS SETUP-->
    <?php /* tag "div" from line 218 */; ?>
<div id="activity-popover" class="popover">
        <?php /* tag "header" from line 219 */; ?>
<header>
            Activity
        </header>
        <?php /* tag "section" from line 222 */; ?>
<section>
            <?php /* tag "div" from line 223 */; ?>
<div class="content">
                <?php /* tag "nav" from line 224 */; ?>
<nav>
                    <?php /* tag "ul" from line 225 */; ?>
<ul>
                        <?php /* tag "li" from line 226 */; ?>
<li class="new"><?php /* tag "a" from line 226 */; ?>
<a><?php /* tag "span" from line 226 */; ?>
<span class="avatar"></span>John Doe created a new project</a></li>
                        <?php /* tag "li" from line 227 */; ?>
<li class="read"><?php /* tag "a" from line 227 */; ?>
<a><?php /* tag "span" from line 227 */; ?>
<span class="avatar"></span>John Doe created a new project</a></li>
                        <?php /* tag "li" from line 228 */; ?>
<li class="read"><?php /* tag "a" from line 228 */; ?>
<a><?php /* tag "span" from line 228 */; ?>
<span class="avatar"></span>Jane Doe updated a project</a></li>
                        <?php /* tag "li" from line 229 */; ?>
<li class="read"><?php /* tag "a" from line 229 */; ?>
<a><?php /* tag "span" from line 229 */; ?>
<span class="avatar"></span>John Doe uploaded a document</a></li>
                        <?php /* tag "li" from line 230 */; ?>
<li class="read"><?php /* tag "a" from line 230 */; ?>
<a><?php /* tag "span" from line 230 */; ?>
<span class="avatar"></span>John Doe deleted a project</a></li>
                        <?php /* tag "li" from line 231 */; ?>
<li class="read"><?php /* tag "a" from line 231 */; ?>
<a><?php /* tag "span" from line 231 */; ?>
<span class="avatar"></span>John Doe marked a project as done</a></li>
                    </ul>
                </nav>
            </div>
        </section>
    </div>
    <?php /* tag "div" from line 237 */; ?>
<div id="notifications-popover" class="popover">
        <?php /* tag "header" from line 238 */; ?>
<header>
            Notifications
        </header>
        <?php /* tag "section" from line 241 */; ?>
<section>
            <?php /* tag "div" from line 242 */; ?>
<div class="content">
                <?php /* tag "nav" from line 243 */; ?>
<nav>
                    <?php /* tag "ul" from line 244 */; ?>
<ul>
                        <?php /* tag "li" from line 245 */; ?>
<li class="new"><?php /* tag "a" from line 245 */; ?>
<a><?php /* tag "span" from line 245 */; ?>
<span class="avatar"></span>John Doe created a new project</a></li>
                        <?php /* tag "li" from line 246 */; ?>
<li class="new"><?php /* tag "a" from line 246 */; ?>
<a><?php /* tag "span" from line 246 */; ?>
<span class="avatar"></span>John Doe created a new project</a></li>
                        <?php /* tag "li" from line 247 */; ?>
<li class="new"><?php /* tag "a" from line 247 */; ?>
<a><?php /* tag "span" from line 247 */; ?>
<span class="avatar"></span>Jane Doe updated a project</a></li>
                        <?php /* tag "li" from line 248 */; ?>
<li class="read"><?php /* tag "a" from line 248 */; ?>
<a><?php /* tag "span" from line 248 */; ?>
<span class="avatar"></span>John Doe uploaded a document</a></li>
                        <?php /* tag "li" from line 249 */; ?>
<li class="read"><?php /* tag "a" from line 249 */; ?>
<a><?php /* tag "span" from line 249 */; ?>
<span class="avatar"></span>John Doe deleted a project</a></li>
                        <?php /* tag "li" from line 250 */; ?>
<li class="read"><?php /* tag "a" from line 250 */; ?>
<a><?php /* tag "span" from line 250 */; ?>
<span class="avatar"></span>John Doe marked a project as done</a></li>
                        <?php /* tag "li" from line 251 */; ?>
<li><?php /* tag "a" from line 251 */; ?>
<a href="#notifications.html" title="Notifications">See notification styles and growl like messages...</a></li>
                    </ul>
                </nav>
            </div>
        </section>
    </div>
    <?php /* tag "div" from line 257 */; ?>
<div id="settings-popover" class="popover">
        <?php /* tag "header" from line 258 */; ?>
<header>
            Settings
        </header>
        <?php /* tag "section" from line 261 */; ?>
<section>
            <?php /* tag "div" from line 262 */; ?>
<div class="content">
                <?php /* tag "nav" from line 263 */; ?>
<nav>
                    <?php /* tag "ul" from line 264 */; ?>
<ul>
                        <?php /* tag "li" from line 265 */; ?>
<li><?php /* tag "a" from line 265 */; ?>
<a>Project Settings</a></li>
                        <?php /* tag "li" from line 266 */; ?>
<li><?php /* tag "a" from line 266 */; ?>
<a>Account Settings</a></li>
                    </ul>
                </nav>
            </div>
        </section>
    </div>
    <?php /* tag "script" from line 272 */; ?>
<script>/*<![CDATA[*/
        $(document).ready(function() {
            $('#activity-button').popover('#activity-popover', {preventRight: true});
            $('#notifications-button').popover('#notifications-popover', {preventRight: true});
            $('#settings-button').popover('#settings-popover', {preventRight: true});

            /**
             * setup search
             */
            function googleSearch(q){
                $('#searchform .searchbox a').fadeOut()
                $.ajax({
                    url: 'php/google_search_results.php',
                    data: 'q='+encodeURIComponent(q),
                    cache: false,
                    success: function(response){
                        $('.search_results').html(response);
                    }
                });
            }

            // Set iTextSuggest
            $('#searchform .searchbox').length && $('#searchform .searchbox').find('input[type=text]').iTextClear().iTextSuggest({
                url: 'php/google_suggestions_results.php',
                onKeydown: function(query){
                    googleSearch(query);
                },
                onChange: function(query){
                    googleSearch(query);
                },
                onSelect: function(query){
                    googleSearch(query);
                },
                onSubmit: function(query){
                    googleSearch(query);
                },
                onEmpty: function(){
                    $('.search_results').html('');
                }
            }).focus(function(){
                $('#wrapper > section > aside > nav > ul').fadeOut(function(){
                    $('#searchform .search_results').show();
                });
                $(this).parents('#searchform .searchbox').animate({marginRight: 70}).next().fadeIn();
            });
            
            $('#searchform .searchcontainer').find('input[type=button]').click(function(){
                $('#searchform .search_results').hide();
                $('#searchform .searchbox').find('input[type=text]').val('');
                $('#searchform .search_results').html('');
                $('#wrapper > section > aside > nav > ul').fadeIn();
                $('.searchbox', $(this).parent()).animate({marginRight: 0}).next().fadeOut();
            });
        });
    /*]]>*/</script>
    <!-- POPOVERS SETUP END-->

</body>
</html>
<?php 
}

 ?>
<?php 
function tpl_4fd92391_base__BN2bS1DUavycj9v7hRtcUQ(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/iagh/manager/application/views/common/base.zpt (edit that file instead) */; ?>