<?php 
function tpl_4fcda61f_analytics__paLIBl7m1qQXZG4rRLkhvg(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<?php /* tag "link" from line 2 */; ?>
<link rel="stylesheet" href="/css/calendar/ui-lightness/jquery-ui-1.8.16.custom.css" media="screen"/>
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" href="/css/dashboard/style.css" media="screen"/>
<?php /* tag "link" from line 4 */; ?>
<link rel="stylesheet" href="/css/dashboard/barchart.css" media="screen"/>
<?php /* tag "link" from line 5 */; ?>
<link rel="stylesheet" href="/css/dashboard/chart.css" media="screen"/>

<?php /* tag "script" from line 7 */; ?>
<script src="/js/calendar/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
<?php /* tag "script" from line 8 */; ?>
<script src="/js/dashboard/chart.js" type="text/javascript"></script>

<?php /* tag "script" from line 10 */; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	  // for each result row..
	  $(".extras_result").each(function() {
	    // get the width of the bar from the span html
	    var length = $(this).find("span").html();
	    length = length.replace("%","");
	    length = length / 100;
	    var lengthPx = 450 * length;
	    
	    $(this).find("p").animate({'width':lengthPx}, 2000, function() {
	      // once the bar animation has finished, fade in the results
	      $(this).find("span").fadeIn(800);
	    });
	  });
	});
</script>


<?php /* tag "script" from line 29 */; ?>
<script>
    $(function() {
        $( "#startDate" ).datepicker();
        $( "#startDate" ).change(function() {
            $( "#startDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });     
	// for extraction form
    $(function() {
        $( "#endDate" ).datepicker();
        $( "#endDate" ).change(function() {
            $( "#endDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });        
</script>		

<?php /* tag "script" from line 45 */; ?>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<?php /* tag "script" from line 46 */; ?>
<script type="text/javascript">
  google.load("visualization", "1", {packages:["corechart"]});
  google.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Task', 'Hours per Day'],
      ['Work',     11],
      ['Eat',      2],
      ['Commute',  2],
      ['Watch TV', 2],
      ['Sleep',    7]
    ]);

    var options = {
      title: 'My Daily Activities'
    };

    var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
    chart.draw(data, options);
  }
</script>
    
    <?php /* tag "h1" from line 68 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 68 */; ?>
<span style="float:left;">Dashboard</span></h1>
	<?php /* tag "div" from line 69 */; ?>
<div id="folderTab"><?php /* tag "div" from line 69 */; ?>
<div class="folderTabText">Analytics</div></div>
	<?php /* tag "br" from line 70 */; ?>
<br/>	<?php /* tag "br" from line 70 */; ?>
<br/>	<?php /* tag "br" from line 70 */; ?>
<br/>	
	<?php /* tag "div" from line 71 */; ?>
<div class="dashboardTabMenu">
		<?php /* tag "span" from line 72 */; ?>
<span><?php /* tag "a" from line 72 */; ?>
<a href="admin#/dashboard/index">Home</a></span>
		<?php /* tag "span" from line 73 */; ?>
<span><?php /* tag "a" from line 73 */; ?>
<a class="selected" href="#">Analytics</a></span>
		<?php /* tag "span" from line 74 */; ?>
<span><?php /* tag "a" from line 74 */; ?>
<a href="#">Menu 3</a></span>
		<?php /* tag "span" from line 75 */; ?>
<span><?php /* tag "a" from line 75 */; ?>
<a href="#">Menu 4</a></span>
	</div>
	<?php /* tag "div" from line 77 */; ?>
<div class="dimension">
		Dimensions<?php /* tag "br" from line 78 */; ?>
<br/>
		Start Date: <?php /* tag "input" from line 79 */; ?>
<input type="text" id="startDate" name="startDate"/>&nbsp;&nbsp;&nbsp;
		End Date: <?php /* tag "input" from line 80 */; ?>
<input type="text" id="endDate" name="endDate"/>&nbsp;&nbsp;&nbsp;
		<?php /* tag "input" from line 81 */; ?>
<input type="button" value="Query"/>
	</div>
	<?php /* tag "div" from line 83 */; ?>
<div class="reportContainer">
		<?php /* tag "div" from line 84 */; ?>
<div class="tbl">
			<?php /* tag "table" from line 85 */; ?>
<table>
				<?php /* tag "tr" from line 86 */; ?>
<tr>
					<?php /* tag "td" from line 87 */; ?>
<td>
						<?php /* tag "table" from line 88 */; ?>
<table style="width:200px;">
							<?php /* tag "tr" from line 89 */; ?>
<tr>
								<?php /* tag "th" from line 90 */; ?>
<th colspan="3">Today</th>
							</tr>
							<?php /* tag "tr" from line 92 */; ?>
<tr>
								<?php /* tag "td" from line 93 */; ?>
<td colspan="3" align="center"><?php /* tag "span" from line 93 */; ?>
<span style="font-size:9px; font-style:Italic; font-weight:normal;"><?php /* tag "tal:block" from line 93 */; ?>
<?php echo phptal_escape($ctx->todayDate); ?>
</span></td>
							</tr>
							<?php /* tag "tr" from line 95 */; ?>
<tr>
								<?php /* tag "td" from line 96 */; ?>
<td>&nbsp;</td>
								<?php /* tag "td" from line 97 */; ?>
<td>
									<?php /* tag "table" from line 98 */; ?>
<table style="width:75px; text-align:center;">
										<?php /* tag "tr" from line 99 */; ?>
<tr>
											<?php /* tag "th" from line 100 */; ?>
<th>Hour</th><?php /* tag "th" from line 100 */; ?>
<th>Hits</th>
										</tr>
										<?php 
/* tag "tal:block" from line 102 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

											<?php 
/* tag "tal:block" from line 103 */ ;
if ($ctx->repeat->item->index <= 11):  ;
?>

											<?php /* tag "tr" from line 104 */; ?>
<tr><?php /* tag "td" from line 104 */; ?>
<td><?php /* tag "tal:block" from line 104 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 104 */; ?>
<td><?php /* tag "tal:block" from line 104 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
											<?php endif; ?>

										<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

									</table>
								</td>
								<?php /* tag "td" from line 109 */; ?>
<td valign="top">
									<?php /* tag "table" from line 110 */; ?>
<table style="width:75px;text-align:center;">
										<?php /* tag "th" from line 111 */; ?>
<th>Hour</th><?php /* tag "th" from line 111 */; ?>
<th>Hits</th>
										<?php 
/* tag "tal:block" from line 112 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

											<?php 
/* tag "tal:block" from line 113 */ ;
if ($ctx->repeat->item->index >= 12):  ;
?>

											<?php /* tag "tr" from line 114 */; ?>
<tr><?php /* tag "td" from line 114 */; ?>
<td><?php /* tag "tal:block" from line 114 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 114 */; ?>
<td><?php /* tag "tal:block" from line 114 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
											<?php endif; ?>

										<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 125 */; ?>
<div id="bargraph">
			<?php /* tag "br" from line 126 */; ?>
<br/>
			<?php /* tag "table" from line 127 */; ?>
<table class="extras_table" align="center">
			<?php /* tag "tr" from line 128 */; ?>
<tr>
				<?php /* tag "th" from line 129 */; ?>
<th colspan="2">Top Follows</th>
			</tr>
			<?php /* tag "tr" from line 131 */; ?>
<tr><?php /* tag "td" from line 131 */; ?>
<td colspan="2">&nbsp;</td></tr>
				<?php
					$a[0]=1000;
					$a[1]=100;
					$a[2]=200;
					$a[3]=120;
					$a[4]=110;
					$a[5]=170;
					$t = array_sum($a);
					
					for($i=0; $i<count($a);$i++){
						$p = $a[$i]/$t*100;
						$p = number_format($p);
						echo '<tr>
								<th  class="extras_y-desc" scope="row">user '.$i.'</th>
								<td><div class="extras_result"><p class="extras_p">&nbsp;<span>'.$p.'%</span></p>'.number_format($a[$i]).'</div></td>
							</tr>
						';
					}
				?>
			</table>
		</div>
		<?php /* tag "div" from line 153 */; ?>
<div id="piegraph">
			<?php /* tag "div" from line 154 */; ?>
<div id="chart_div" style="width: 900px; height: 500px;"></div>

		</div>
	</div>
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/dashboard/analytics.zpt (edit that file instead) */; ?>