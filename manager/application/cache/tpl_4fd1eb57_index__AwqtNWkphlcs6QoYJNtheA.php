<?php 
function tpl_4fd1eb57_index__AwqtNWkphlcs6QoYJNtheA(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<?php /* tag "link" from line 2 */; ?>
<link rel="stylesheet" href="/css/calendar/ui-lightness/jquery-ui-1.8.16.custom.css" media="screen"/>
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" href="/css/dashboard/style.css" media="screen"/>
<?php /* tag "link" from line 4 */; ?>
<link rel="stylesheet" href="/css/dashboard/barchart.css" media="screen"/>
<?php /* tag "link" from line 5 */; ?>
<link rel="stylesheet" href="/css/dashboard/chart.css" media="screen"/>

<?php /* tag "script" from line 7 */; ?>
<script src="/js/calendar/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
<!--<script src="/js/calendar/jquery.js" type="text/javascript"></script>-->
<?php /* tag "script" from line 9 */; ?>
<script src="/js/dashboard/global.js" type="text/javascript"></script>

<!--<script src="/js/dashboard/chart.js" type="text/javascript"></script>-->

<?php /* tag "script" from line 13 */; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	  // for each result row..
	  $(".extras_result").each(function() {
	    // get the width of the bar from the span html
	    var length = $(this).find("span").html();
	    length = length.replace("%","");
	    length = length / 100;
	    var lengthPx = 400 * length;
	    
	    $(this).find("p").animate({'width':lengthPx}, 2000, function() {
	      // once the bar animation has finished, fade in the results
	      $(this).find("span").fadeIn(800);
	    });
	  });
	});
</script>


<?php /* tag "script" from line 32 */; ?>
<script>
    $(function() {
        $( "#startDate" ).datepicker();
        $( "#startDate" ).change(function() {
            $( "#startDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });     
	// for extraction form
    $(function() {
        $( "#endDate" ).datepicker();
        $( "#endDate" ).change(function() {
            $( "#endDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });        
    
	var j = '[[[2,0,0],"globe"],[[5,0,1],"twitter"],[[14,1,0],"xmpp"],[[4,0,0],"ym"]]';
	var obj = jQuery.parseJSON(j);
	var graphByMonth = jQuery.makeArray(obj);
    
</script>
    
    <?php /* tag "h1" from line 53 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 53 */; ?>
<span style="float:left;">Dashboard</span></h1>
	<?php /* tag "div" from line 54 */; ?>
<div id="folderTab"><?php /* tag "div" from line 54 */; ?>
<div class="folderTabText">Quick View</div></div>
	<?php /* tag "br" from line 55 */; ?>
<br/>
	<?php /* tag "div" from line 56 */; ?>
<div class="dashboardTabMenu">
		<?php /* tag "span" from line 57 */; ?>
<span id="followersLink"><?php /* tag "a" from line 57 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('followers')">Top Follower</a></span>
		<?php /* tag "span" from line 58 */; ?>
<span id="tenantHitsLink"><?php /* tag "a" from line 58 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('tenantHits')">Tenant Hits</a></span>
		<?php /* tag "span" from line 59 */; ?>
<span id="touchpointHitsLink"><?php /* tag "a" from line 59 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('touchpointHits')">Touchpoint Hits</a></span>
		<?php /* tag "span" from line 60 */; ?>
<span id="feedbackLink"><?php /* tag "a" from line 60 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('feedback')">Feedback</a></span>
	</div>
<!--	<div class="dimension">
		Dimensions<br/>
		Start Date: <input type="text" id="startDate" name="startDate"/>&nbsp;&nbsp;&nbsp;
		End Date: <input type="text" id="endDate" name="endDate"/>&nbsp;&nbsp;&nbsp;
		<input type="button" value="Query"/>
	</div>-->
	<?php /* tag "tal:block" from line 68 */; ?>

		<?php 
/* tag "input" from line 69 */ ;
if (null !== ($_tmp_1 = ($ctx->severityHits))):  ;
$_tmp_1 = ' value="'.phptal_escape($_tmp_1).'"' ;
else:  ;
$_tmp_1 = '' ;
endif ;
?>
<input type="hidden" name="severityData" id="severityData"<?php echo $_tmp_1 ?>
/>
	
	<?php /* tag "div" from line 71 */; ?>
<div class="reportContainer">
		<?php /* tag "div" from line 72 */; ?>
<div class="tbl">
			<?php 
/* tag "tal:block" from line 73 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->detailMonthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

				<?php 
/* tag "div" from line 74 */ ;
if (null !== ($_tmp_2 = ($ctx->repeat->item->key))):  ;
$_tmp_2 = ' id="'.phptal_escape($_tmp_2).'"' ;
else:  ;
$_tmp_2 = '' ;
endif ;
?>
<div style="display:none;"<?php echo $_tmp_2 ?>
>
					<?php /* tag "table" from line 75 */; ?>
<table style="width:200px; text-align:center;">
						<?php /* tag "tr" from line 76 */; ?>
<tr><?php /* tag "td" from line 76 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 76 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 77 */; ?>
<tr><?php /* tag "td" from line 77 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 77 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 78 */; ?>
<tr><?php /* tag "th" from line 78 */; ?>
<th colspan="3" align="center"><?php /* tag "span" from line 78 */; ?>
<span class="tableTitle" style="font-size:14px;"><?php /* tag "tal:block" from line 78 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</span></th></tr>
						<?php /* tag "tr" from line 79 */; ?>
<tr><?php /* tag "td" from line 79 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 79 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 80 */; ?>
<tr><?php /* tag "th" from line 80 */; ?>
<th>Name</th><?php /* tag "th" from line 80 */; ?>
<th>Hits</th></tr>
						<?php 
/* tag "tal:block" from line 81 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

							<?php /* tag "tr" from line 82 */; ?>
<tr>
								<?php /* tag "td" from line 83 */; ?>
<td width="50%"><?php /* tag "tal:block" from line 83 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'name')); ?>
</td>
								<?php /* tag "td" from line 84 */; ?>
<td width="50%"><?php /* tag "tal:block" from line 84 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
							</tr>
						<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

					</table>
				</div>
			<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

			<?php /* tag "div" from line 90 */; ?>
<div id="todayTransaction">
				<?php /* tag "table" from line 91 */; ?>
<table>
					<?php /* tag "tr" from line 92 */; ?>
<tr>
						<?php /* tag "td" from line 93 */; ?>
<td>
							<?php /* tag "table" from line 94 */; ?>
<table style="width:200px;">
								<?php /* tag "tr" from line 95 */; ?>
<tr><?php /* tag "th" from line 95 */; ?>
<th colspan="3"><?php /* tag "span" from line 95 */; ?>
<span class="tableTitle">Today Transaction</span></th></tr>
								<?php /* tag "tr" from line 96 */; ?>
<tr><?php /* tag "td" from line 96 */; ?>
<td colspan="3" align="center"><?php /* tag "span" from line 96 */; ?>
<span style="font-size:9px; font-style:Italic; font-weight:normal;"><?php /* tag "tal:block" from line 96 */; ?>
<?php echo phptal_escape($ctx->todayDate); ?>
</span></td></tr>
								<?php /* tag "tr" from line 97 */; ?>
<tr><?php /* tag "td" from line 97 */; ?>
<td>&nbsp;</td></tr>
								<?php /* tag "tr" from line 98 */; ?>
<tr>
									<?php /* tag "td" from line 99 */; ?>
<td>&nbsp;</td>
									<?php /* tag "td" from line 100 */; ?>
<td>
										<?php /* tag "table" from line 101 */; ?>
<table style="width:75px; text-align:center;">
											<?php /* tag "tr" from line 102 */; ?>
<tr>
												<?php /* tag "th" from line 103 */; ?>
<th>Hour</th><?php /* tag "th" from line 103 */; ?>
<th>Hits</th>
											</tr>
											<?php 
/* tag "tal:block" from line 105 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

												<?php 
/* tag "tal:block" from line 106 */ ;
if ($ctx->repeat->item->index <= 11):  ;
?>

												<?php /* tag "tr" from line 107 */; ?>
<tr><?php /* tag "td" from line 107 */; ?>
<td><?php /* tag "tal:block" from line 107 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 107 */; ?>
<td><?php /* tag "tal:block" from line 107 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
												<?php endif; ?>

											<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

										</table>
									</td>
									<?php /* tag "td" from line 112 */; ?>
<td valign="top">
										<?php /* tag "table" from line 113 */; ?>
<table style="width:75px;text-align:center;">
											<?php /* tag "th" from line 114 */; ?>
<th>Hour</th><?php /* tag "th" from line 114 */; ?>
<th>Hits</th>
											<?php 
/* tag "tal:block" from line 115 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

												<?php 
/* tag "tal:block" from line 116 */ ;
if ($ctx->repeat->item->index >= 12):  ;
?>

												<?php /* tag "tr" from line 117 */; ?>
<tr><?php /* tag "td" from line 117 */; ?>
<td><?php /* tag "tal:block" from line 117 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 117 */; ?>
<td><?php /* tag "tal:block" from line 117 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
												<?php endif; ?>

											<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<?php /* tag "div" from line 129 */; ?>
<div id="bargraph">
		<?php /* tag "br" from line 130 */; ?>
<br/><?php /* tag "br" from line 130 */; ?>
<br/>
			<?php /* tag "table" from line 131 */; ?>
<table style="text-align:left;">
				<?php /* tag "tr" from line 132 */; ?>
<tr>
					<?php /* tag "td" from line 133 */; ?>
<td>
						<?php /* tag "div" from line 134 */; ?>
<div id="followers">
							<?php /* tag "table" from line 135 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 136 */; ?>
<tr>
									<?php /* tag "th" from line 137 */; ?>
<th colspan="2"><?php /* tag "span" from line 137 */; ?>
<span class="tableTitle">Top Followers</span></th>
								</tr>
								<?php /* tag "tr" from line 139 */; ?>
<tr><?php /* tag "td" from line 139 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 140 */; ?>
<tr><?php /* tag "td" from line 140 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 141 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->topFollowers)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 142 */; ?>
<tr>
									<?php /* tag "td" from line 143 */; ?>
<td class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 143 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 144 */; ?>
<td><?php /* tag "div" from line 144 */; ?>
<div class="extras_result"><?php /* tag "p" from line 144 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 144 */; ?>
<span><?php /* tag "tal:block" from line 144 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 144 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 147 */; ?>
<tr><?php /* tag "td" from line 147 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 150 */; ?>
<div id="tenantHits" style="display:none">
							<?php /* tag "table" from line 151 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 152 */; ?>
<tr>
									<?php /* tag "th" from line 153 */; ?>
<th colspan="2"><?php /* tag "span" from line 153 */; ?>
<span class="tableTitle">Tenant Hits</span></th>
								</tr>
								<?php /* tag "tr" from line 155 */; ?>
<tr><?php /* tag "td" from line 155 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 156 */; ?>
<tr><?php /* tag "td" from line 156 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 157 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->totalHitsPerTenant)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 158 */; ?>
<tr>
									<?php /* tag "td" from line 159 */; ?>
<td class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 159 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 160 */; ?>
<td><?php /* tag "div" from line 160 */; ?>
<div class="extras_result"><?php /* tag "p" from line 160 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 160 */; ?>
<span><?php /* tag "tal:block" from line 160 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 160 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 163 */; ?>
<tr><?php /* tag "td" from line 163 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 166 */; ?>
<div id="touchpointHits" style="display:none">
							<?php /* tag "table" from line 167 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 168 */; ?>
<tr>
									<?php /* tag "th" from line 169 */; ?>
<th colspan="2"><?php /* tag "span" from line 169 */; ?>
<span class="tableTitle">Touch Point Hits</span></th>
								</tr>
								<?php /* tag "tr" from line 171 */; ?>
<tr><?php /* tag "td" from line 171 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 172 */; ?>
<tr><?php /* tag "td" from line 172 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 173 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->operatorTotalHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 174 */; ?>
<tr>
									<?php /* tag "td" from line 175 */; ?>
<td class="extras_y-desc" scope="row">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php /* tag "tal:block" from line 175 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 176 */; ?>
<td><?php /* tag "div" from line 176 */; ?>
<div class="extras_result"><?php /* tag "p" from line 176 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 176 */; ?>
<span><?php /* tag "tal:block" from line 176 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 176 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 179 */; ?>
<tr><?php /* tag "td" from line 179 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 182 */; ?>
<div id="feedback" style="display:none">
							<?php /* tag "br" from line 183 */; ?>
<br/><?php /* tag "br" from line 183 */; ?>
<br/><?php /* tag "br" from line 183 */; ?>
<br/>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 189 */; ?>
<div id="monthlyReport">
			<?php /* tag "table" from line 190 */; ?>
<table style="width:880px; margin-left:20px;">
				<?php /* tag "tr" from line 191 */; ?>
<tr><?php /* tag "td" from line 191 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 192 */; ?>
<tr><?php /* tag "th" from line 192 */; ?>
<th colspan="12" align="center"><?php /* tag "span" from line 192 */; ?>
<span class="tableTitle">Total Hits By Month</span></th></tr>
				<?php /* tag "tr" from line 193 */; ?>
<tr><?php /* tag "td" from line 193 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 194 */; ?>
<tr>
					<?php 
/* tag "tal:block" from line 195 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->monthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

						<?php /* tag "td" from line 196 */; ?>
<td valign="top">
							<?php /* tag "table" from line 197 */; ?>
<table>
								<?php /* tag "tr" from line 198 */; ?>
<tr>
									<?php /* tag "th" from line 199 */; ?>
<th colspan="2" align="center"><?php /* tag "a" from line 199 */; ?>
<a href="javascript:;" onclick="global.displayTransContainer($(this).html());"><?php /* tag "tal:block" from line 199 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</a></th>
								</tr>
								<?php 
/* tag "tal:block" from line 201 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

									<?php /* tag "tr" from line 202 */; ?>
<tr>
										<?php /* tag "td" from line 203 */; ?>
<td><?php /* tag "tal:block" from line 203 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'total')); ?>
</td>
										<?php /* tag "td" from line 204 */; ?>
<td><?php /* tag "tal:block" from line 204 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
									</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 207 */; ?>
<tr><?php /* tag "td" from line 207 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</td>
					<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>
			
				</tr>
			</table>
		</div>
	</div>
<?php /* tag "script" from line 215 */; ?>
<script src="/js/dashboard/jquery.js" type="text/javascript"></script>
<?php /* tag "script" from line 216 */; ?>
<script src="/js/dashboard/jqBarGraph.1.1.js" type="text/javascript"></script>
<?php /* tag "script" from line 217 */; ?>
<script>

//	var jData = $("#severityData").val();	
	graphByMonth = new Array(
	    [[23,0,0],'2007'],
	    [[8,0,0],'2008'],
	    [[4,1,0],'2009']
	); 
		
	$("#feedback").jqBarGraph({
		data: graphByMonth,
		width: 300,
		height: 250,
		colors: ['#122A47','#1B3E69','#72808E'],
		color: '#1A2944',
		legends: ['ads','leads','google ads'],
		legend: true,
		type:'multi',
		barSpace: 3
	});
</script>	
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/dashboard/index.zpt (edit that file instead) */; ?>