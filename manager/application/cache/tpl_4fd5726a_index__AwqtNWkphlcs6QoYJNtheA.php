<?php 
function tpl_4fd5726a_index__AwqtNWkphlcs6QoYJNtheA(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<?php /* tag "link" from line 2 */; ?>
<link rel="stylesheet" href="/css/calendar/ui-lightness/jquery-ui-1.8.16.custom.css" media="screen"/>
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" href="/css/dashboard/style.css" media="screen"/>
<?php /* tag "link" from line 4 */; ?>
<link rel="stylesheet" href="/css/dashboard/barchart.css" media="screen"/>
<?php /* tag "link" from line 5 */; ?>
<link rel="stylesheet" href="/css/dashboard/chart.css" media="screen"/>

<?php /* tag "script" from line 7 */; ?>
<script src="/js/calendar/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
<!--<script src="/js/calendar/jquery.js" type="text/javascript"></script>-->
<?php /* tag "script" from line 9 */; ?>
<script src="/js/dashboard/global.js" type="text/javascript"></script>

<!--<script src="/js/dashboard/chart.js" type="text/javascript"></script>-->

<?php /* tag "script" from line 13 */; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	  // for each result row..
	  $(".extras_result").each(function() {
	    // get the width of the bar from the span html
	    var length = $(this).find("span").html();
	    length = length.replace("%","");
	    length = length / 100;
	    var lengthPx = 400 * length;
	    
	    $(this).find("p").animate({'width':lengthPx}, 2000, function() {
	      // once the bar animation has finished, fade in the results
	      $(this).find("span").fadeIn(800);
	    });
	  });
	});
</script>


<?php /* tag "script" from line 32 */; ?>
<script>
    $(function() {
        $( "#startDate" ).datepicker();
        $( "#startDate" ).change(function() {
            $( "#startDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });     
	// for extraction form
    $(function() {
        $( "#endDate" ).datepicker();
        $( "#endDate" ).change(function() {
            $( "#endDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });        
    
	var sOperator = $("#severityOperator").val();
	var sObj = jQuery.parseJSON(sOperator);
	var sArr = jQuery.makeArray(sObj);
	
	var jData = $("#severityData").val();
	var obj = jQuery.parseJSON(jData);
	var graphBySeverity = jQuery.makeArray(obj);
    
	var statusData = $("#statusData").val();
	var statusObj = jQuery.parseJSON(statusData);
	var graphByStatus = jQuery.makeArray(statusObj);
    
</script>
    
    <?php /* tag "h1" from line 61 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 61 */; ?>
<span style="float:left;">Dashboard</span></h1>
	<?php /* tag "div" from line 62 */; ?>
<div id="folderTab"><?php /* tag "div" from line 62 */; ?>
<div class="folderTabText">Quick View</div></div>
	<?php /* tag "br" from line 63 */; ?>
<br/>
	<?php /* tag "div" from line 64 */; ?>
<div class="dashboardTabMenu">
		<?php /* tag "span" from line 65 */; ?>
<span id="followersLink"><?php /* tag "a" from line 65 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('followers')">Top Follower</a></span>
		<?php /* tag "span" from line 66 */; ?>
<span id="tenantHitsLink"><?php /* tag "a" from line 66 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('tenantHits')">Tenant Hits</a></span>
		<?php /* tag "span" from line 67 */; ?>
<span id="touchpointHitsLink"><?php /* tag "a" from line 67 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('touchpointHits')">Touchpoint Hits</a></span>
		<?php /* tag "span" from line 68 */; ?>
<span id="feedbackLink"><?php /* tag "a" from line 68 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('feedbackSeverity')">Feedback</a></span>
	</div>
<!--	<div class="dimension">
		Dimensions<br/>
		Start Date: <input type="text" id="startDate" name="startDate"/>&nbsp;&nbsp;&nbsp;
		End Date: <input type="text" id="endDate" name="endDate"/>&nbsp;&nbsp;&nbsp;
		<input type="button" value="Query"/>
	</div>-->
	<?php /* tag "tal:block" from line 76 */; ?>

		<?php 
/* tag "input" from line 77 */ ;
if (null !== ($_tmp_1 = ($ctx->statusHits))):  ;
$_tmp_1 = ' value="'.phptal_escape($_tmp_1).'"' ;
else:  ;
$_tmp_1 = '' ;
endif ;
?>
<input type="hidden" name="statusData" id="statusData"<?php echo $_tmp_1 ?>
/>
		<?php 
/* tag "input" from line 78 */ ;
if (null !== ($_tmp_1 = ($ctx->severityHits))):  ;
$_tmp_1 = ' value="'.phptal_escape($_tmp_1).'"' ;
else:  ;
$_tmp_1 = '' ;
endif ;
?>
<input type="hidden" name="severityData" id="severityData"<?php echo $_tmp_1 ?>
/>
		<?php 
/* tag "input" from line 79 */ ;
if (null !== ($_tmp_1 = ($ctx->severityOperators))):  ;
$_tmp_1 = ' value="'.phptal_escape($_tmp_1).'"' ;
else:  ;
$_tmp_1 = '' ;
endif ;
?>
<input type="hidden" name="severityOperator" id="severityOperator"<?php echo $_tmp_1 ?>
/>
	
	<?php /* tag "div" from line 81 */; ?>
<div class="reportContainer">
		<?php /* tag "div" from line 82 */; ?>
<div class="tbl">
			<?php 
/* tag "tal:block" from line 83 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->detailMonthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

				<?php 
/* tag "div" from line 84 */ ;
if (null !== ($_tmp_2 = ($ctx->repeat->item->key))):  ;
$_tmp_2 = ' id="'.phptal_escape($_tmp_2).'"' ;
else:  ;
$_tmp_2 = '' ;
endif ;
?>
<div style="display:none;"<?php echo $_tmp_2 ?>
>
					<?php /* tag "table" from line 85 */; ?>
<table style="width:200px; text-align:center;">
						<?php /* tag "tr" from line 86 */; ?>
<tr><?php /* tag "td" from line 86 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 86 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 87 */; ?>
<tr><?php /* tag "td" from line 87 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 87 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 88 */; ?>
<tr><?php /* tag "th" from line 88 */; ?>
<th colspan="3" align="center"><?php /* tag "span" from line 88 */; ?>
<span class="tableTitle" style="font-size:14px;"><?php /* tag "tal:block" from line 88 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</span></th></tr>
						<?php /* tag "tr" from line 89 */; ?>
<tr><?php /* tag "td" from line 89 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 89 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 90 */; ?>
<tr><?php /* tag "th" from line 90 */; ?>
<th>Name</th><?php /* tag "th" from line 90 */; ?>
<th>Hits</th></tr>
						<?php 
/* tag "tal:block" from line 91 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

							<?php /* tag "tr" from line 92 */; ?>
<tr>
								<?php /* tag "td" from line 93 */; ?>
<td width="50%"><?php /* tag "tal:block" from line 93 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'name')); ?>
</td>
								<?php /* tag "td" from line 94 */; ?>
<td width="50%"><?php /* tag "tal:block" from line 94 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
							</tr>
						<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

					</table>
				</div>
			<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

			<?php /* tag "div" from line 100 */; ?>
<div id="todayTransaction">
				<?php /* tag "table" from line 101 */; ?>
<table>
					<?php /* tag "tr" from line 102 */; ?>
<tr>
						<?php /* tag "td" from line 103 */; ?>
<td>
							<?php /* tag "table" from line 104 */; ?>
<table style="width:200px;">
								<?php /* tag "tr" from line 105 */; ?>
<tr><?php /* tag "th" from line 105 */; ?>
<th colspan="3"><?php /* tag "span" from line 105 */; ?>
<span class="tableTitle">Today Transaction</span></th></tr>
								<?php /* tag "tr" from line 106 */; ?>
<tr><?php /* tag "td" from line 106 */; ?>
<td colspan="3" align="center"><?php /* tag "span" from line 106 */; ?>
<span style="font-size:9px; font-style:Italic; font-weight:normal;"><?php /* tag "tal:block" from line 106 */; ?>
<?php echo phptal_escape($ctx->todayDate); ?>
</span></td></tr>
								<?php /* tag "tr" from line 107 */; ?>
<tr><?php /* tag "td" from line 107 */; ?>
<td>&nbsp;</td></tr>
								<?php /* tag "tr" from line 108 */; ?>
<tr>
									<?php /* tag "td" from line 109 */; ?>
<td>&nbsp;</td>
									<?php /* tag "td" from line 110 */; ?>
<td>
										<?php /* tag "table" from line 111 */; ?>
<table style="width:75px; text-align:center;">
											<?php /* tag "tr" from line 112 */; ?>
<tr>
												<?php /* tag "th" from line 113 */; ?>
<th>Hour</th><?php /* tag "th" from line 113 */; ?>
<th>Hits</th>
											</tr>
											<?php 
/* tag "tal:block" from line 115 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

												<?php 
/* tag "tal:block" from line 116 */ ;
if ($ctx->repeat->item->index <= 11):  ;
?>

												<?php /* tag "tr" from line 117 */; ?>
<tr><?php /* tag "td" from line 117 */; ?>
<td><?php /* tag "tal:block" from line 117 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 117 */; ?>
<td><?php /* tag "tal:block" from line 117 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
												<?php endif; ?>

											<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

										</table>
									</td>
									<?php /* tag "td" from line 122 */; ?>
<td valign="top">
										<?php /* tag "table" from line 123 */; ?>
<table style="width:75px;text-align:center;">
											<?php /* tag "th" from line 124 */; ?>
<th>Hour</th><?php /* tag "th" from line 124 */; ?>
<th>Hits</th>
											<?php 
/* tag "tal:block" from line 125 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

												<?php 
/* tag "tal:block" from line 126 */ ;
if ($ctx->repeat->item->index >= 12):  ;
?>

												<?php /* tag "tr" from line 127 */; ?>
<tr><?php /* tag "td" from line 127 */; ?>
<td><?php /* tag "tal:block" from line 127 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 127 */; ?>
<td><?php /* tag "tal:block" from line 127 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
												<?php endif; ?>

											<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<?php /* tag "div" from line 139 */; ?>
<div id="bargraph">
		<?php /* tag "br" from line 140 */; ?>
<br/><?php /* tag "br" from line 140 */; ?>
<br/>
			<?php /* tag "table" from line 141 */; ?>
<table style="text-align:left;">
				<?php /* tag "tr" from line 142 */; ?>
<tr>
					<?php /* tag "td" from line 143 */; ?>
<td>
						<?php /* tag "div" from line 144 */; ?>
<div id="followers">
							<?php /* tag "table" from line 145 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 146 */; ?>
<tr>
									<?php /* tag "th" from line 147 */; ?>
<th colspan="2"><?php /* tag "span" from line 147 */; ?>
<span class="tableTitle">Top Followers</span></th>
								</tr>
								<?php /* tag "tr" from line 149 */; ?>
<tr><?php /* tag "td" from line 149 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 150 */; ?>
<tr><?php /* tag "td" from line 150 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 151 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->topFollowers)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 152 */; ?>
<tr>
									<?php /* tag "td" from line 153 */; ?>
<td class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 153 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 154 */; ?>
<td><?php /* tag "div" from line 154 */; ?>
<div class="extras_result"><?php /* tag "p" from line 154 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 154 */; ?>
<span><?php /* tag "tal:block" from line 154 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 154 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 157 */; ?>
<tr><?php /* tag "td" from line 157 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 160 */; ?>
<div id="tenantHits" style="display:none">
							<?php /* tag "table" from line 161 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 162 */; ?>
<tr>
									<?php /* tag "th" from line 163 */; ?>
<th colspan="2"><?php /* tag "span" from line 163 */; ?>
<span class="tableTitle">Tenant Hits</span></th>
								</tr>
								<?php /* tag "tr" from line 165 */; ?>
<tr><?php /* tag "td" from line 165 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 166 */; ?>
<tr><?php /* tag "td" from line 166 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 167 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->totalHitsPerTenant)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 168 */; ?>
<tr>
									<?php /* tag "td" from line 169 */; ?>
<td class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 169 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 170 */; ?>
<td><?php /* tag "div" from line 170 */; ?>
<div class="extras_result"><?php /* tag "p" from line 170 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 170 */; ?>
<span><?php /* tag "tal:block" from line 170 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 170 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 173 */; ?>
<tr><?php /* tag "td" from line 173 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 176 */; ?>
<div id="touchpointHits" style="display:none">
							<?php /* tag "table" from line 177 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 178 */; ?>
<tr>
									<?php /* tag "th" from line 179 */; ?>
<th colspan="2"><?php /* tag "span" from line 179 */; ?>
<span class="tableTitle">Touch Point Hits</span></th>
								</tr>
								<?php /* tag "tr" from line 181 */; ?>
<tr><?php /* tag "td" from line 181 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 182 */; ?>
<tr><?php /* tag "td" from line 182 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 183 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->operatorTotalHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 184 */; ?>
<tr>
									<?php /* tag "td" from line 185 */; ?>
<td class="extras_y-desc" scope="row">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php /* tag "tal:block" from line 185 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 186 */; ?>
<td><?php /* tag "div" from line 186 */; ?>
<div class="extras_result"><?php /* tag "p" from line 186 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 186 */; ?>
<span><?php /* tag "tal:block" from line 186 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 186 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 189 */; ?>
<tr><?php /* tag "td" from line 189 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 192 */; ?>
<div id="feedbackSeverity" style="display:none"></div>
						<?php /* tag "div" from line 193 */; ?>
<div id="feedbackStatus" style="display:none"></div>						
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 198 */; ?>
<div id="monthlyReport">
			<?php /* tag "table" from line 199 */; ?>
<table style="width:880px; margin-left:20px;">
				<?php /* tag "tr" from line 200 */; ?>
<tr><?php /* tag "td" from line 200 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 201 */; ?>
<tr><?php /* tag "th" from line 201 */; ?>
<th colspan="12" align="center"><?php /* tag "span" from line 201 */; ?>
<span class="tableTitle">Total Hits By Month</span></th></tr>
				<?php /* tag "tr" from line 202 */; ?>
<tr><?php /* tag "td" from line 202 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 203 */; ?>
<tr>
					<?php 
/* tag "tal:block" from line 204 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->monthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

						<?php /* tag "td" from line 205 */; ?>
<td valign="top">
							<?php /* tag "table" from line 206 */; ?>
<table>
								<?php /* tag "tr" from line 207 */; ?>
<tr>
									<?php /* tag "th" from line 208 */; ?>
<th colspan="2" align="center"><?php /* tag "a" from line 208 */; ?>
<a href="javascript:;" onclick="global.displayTransContainer($(this).html());"><?php /* tag "tal:block" from line 208 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</a></th>
								</tr>
								<?php 
/* tag "tal:block" from line 210 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

									<?php /* tag "tr" from line 211 */; ?>
<tr>
										<?php /* tag "td" from line 212 */; ?>
<td><?php /* tag "tal:block" from line 212 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'total')); ?>
</td>
										<?php /* tag "td" from line 213 */; ?>
<td><?php /* tag "tal:block" from line 213 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
									</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 216 */; ?>
<tr><?php /* tag "td" from line 216 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</td>
					<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>
			
				</tr>
			</table>
		</div>
	</div>
<?php /* tag "script" from line 224 */; ?>
<script src="/js/dashboard/jquery.js" type="text/javascript"></script>
<?php /* tag "script" from line 225 */; ?>
<script src="/js/dashboard/jqBarGraph.1.1.js" type="text/javascript"></script>
<?php /* tag "script" from line 226 */; ?>
<script>
			
//	$("#feedbackSeverity").jqBarGraph({
//		data: graphBySeverity,
//		width: 550,
//		height: 250,
//		colors: ['#122A47','#185aac','#5fa6fd'],
//		color: '#000000',
//		legends: ['low','medium','high'],
//		legend: true,
//		type:'multi'
//	});
	
	$("#feedbackStatus").jqBarGraph({
		data: graphByStatus,
		width: 550,
		height: 250,
		colors: ['#122A47','#185aac'],
		color: '#000000',
		legends: ['open','closed'],
		legend: true,
		type:'multi'
	});
</script>	
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/dashboard/index.zpt (edit that file instead) */; ?>