<?php 
function tpl_4fd1ebdc_index__AwqtNWkphlcs6QoYJNtheA(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<?php /* tag "link" from line 2 */; ?>
<link rel="stylesheet" href="/css/calendar/ui-lightness/jquery-ui-1.8.16.custom.css" media="screen"/>
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" href="/css/dashboard/style.css" media="screen"/>
<?php /* tag "link" from line 4 */; ?>
<link rel="stylesheet" href="/css/dashboard/barchart.css" media="screen"/>
<?php /* tag "link" from line 5 */; ?>
<link rel="stylesheet" href="/css/dashboard/chart.css" media="screen"/>

<?php /* tag "script" from line 7 */; ?>
<script src="/js/calendar/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
<!--<script src="/js/calendar/jquery.js" type="text/javascript"></script>-->
<?php /* tag "script" from line 9 */; ?>
<script src="/js/dashboard/global.js" type="text/javascript"></script>

<!--<script src="/js/dashboard/chart.js" type="text/javascript"></script>-->

<?php /* tag "script" from line 13 */; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	  // for each result row..
	  $(".extras_result").each(function() {
	    // get the width of the bar from the span html
	    var length = $(this).find("span").html();
	    length = length.replace("%","");
	    length = length / 100;
	    var lengthPx = 400 * length;
	    
	    $(this).find("p").animate({'width':lengthPx}, 2000, function() {
	      // once the bar animation has finished, fade in the results
	      $(this).find("span").fadeIn(800);
	    });
	  });
	});
</script>


<?php /* tag "script" from line 32 */; ?>
<script>
    $(function() {
        $( "#startDate" ).datepicker();
        $( "#startDate" ).change(function() {
            $( "#startDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });     
	// for extraction form
    $(function() {
        $( "#endDate" ).datepicker();
        $( "#endDate" ).change(function() {
            $( "#endDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });        
    
	var jData = $("#severityData").val();	
//	var j = '[[[2,0,0],"globe"],[[5,0,1],"twitter"],[[14,1,0],"xmpp"],[[4,0,0],"ym"]]';
	var obj = jQuery.parseJSON(jData);
	var graphByMonth = jQuery.makeArray(obj);
    
</script>
    
    <?php /* tag "h1" from line 54 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 54 */; ?>
<span style="float:left;">Dashboard</span></h1>
	<?php /* tag "div" from line 55 */; ?>
<div id="folderTab"><?php /* tag "div" from line 55 */; ?>
<div class="folderTabText">Quick View</div></div>
	<?php /* tag "br" from line 56 */; ?>
<br/>
	<?php /* tag "div" from line 57 */; ?>
<div class="dashboardTabMenu">
		<?php /* tag "span" from line 58 */; ?>
<span id="followersLink"><?php /* tag "a" from line 58 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('followers')">Top Follower</a></span>
		<?php /* tag "span" from line 59 */; ?>
<span id="tenantHitsLink"><?php /* tag "a" from line 59 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('tenantHits')">Tenant Hits</a></span>
		<?php /* tag "span" from line 60 */; ?>
<span id="touchpointHitsLink"><?php /* tag "a" from line 60 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('touchpointHits')">Touchpoint Hits</a></span>
		<?php /* tag "span" from line 61 */; ?>
<span id="feedbackLink"><?php /* tag "a" from line 61 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('feedback')">Feedback</a></span>
	</div>
<!--	<div class="dimension">
		Dimensions<br/>
		Start Date: <input type="text" id="startDate" name="startDate"/>&nbsp;&nbsp;&nbsp;
		End Date: <input type="text" id="endDate" name="endDate"/>&nbsp;&nbsp;&nbsp;
		<input type="button" value="Query"/>
	</div>-->
	<?php /* tag "tal:block" from line 69 */; ?>

		<?php 
/* tag "input" from line 70 */ ;
if (null !== ($_tmp_1 = ($ctx->severityHits))):  ;
$_tmp_1 = ' value="'.phptal_escape($_tmp_1).'"' ;
else:  ;
$_tmp_1 = '' ;
endif ;
?>
<input type="hidden" name="severityData" id="severityData"<?php echo $_tmp_1 ?>
/>
	
	<?php /* tag "div" from line 72 */; ?>
<div class="reportContainer">
		<?php /* tag "div" from line 73 */; ?>
<div class="tbl">
			<?php 
/* tag "tal:block" from line 74 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->detailMonthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

				<?php 
/* tag "div" from line 75 */ ;
if (null !== ($_tmp_2 = ($ctx->repeat->item->key))):  ;
$_tmp_2 = ' id="'.phptal_escape($_tmp_2).'"' ;
else:  ;
$_tmp_2 = '' ;
endif ;
?>
<div style="display:none;"<?php echo $_tmp_2 ?>
>
					<?php /* tag "table" from line 76 */; ?>
<table style="width:200px; text-align:center;">
						<?php /* tag "tr" from line 77 */; ?>
<tr><?php /* tag "td" from line 77 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 77 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 78 */; ?>
<tr><?php /* tag "td" from line 78 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 78 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 79 */; ?>
<tr><?php /* tag "th" from line 79 */; ?>
<th colspan="3" align="center"><?php /* tag "span" from line 79 */; ?>
<span class="tableTitle" style="font-size:14px;"><?php /* tag "tal:block" from line 79 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</span></th></tr>
						<?php /* tag "tr" from line 80 */; ?>
<tr><?php /* tag "td" from line 80 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 80 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 81 */; ?>
<tr><?php /* tag "th" from line 81 */; ?>
<th>Name</th><?php /* tag "th" from line 81 */; ?>
<th>Hits</th></tr>
						<?php 
/* tag "tal:block" from line 82 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

							<?php /* tag "tr" from line 83 */; ?>
<tr>
								<?php /* tag "td" from line 84 */; ?>
<td width="50%"><?php /* tag "tal:block" from line 84 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'name')); ?>
</td>
								<?php /* tag "td" from line 85 */; ?>
<td width="50%"><?php /* tag "tal:block" from line 85 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
							</tr>
						<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

					</table>
				</div>
			<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

			<?php /* tag "div" from line 91 */; ?>
<div id="todayTransaction">
				<?php /* tag "table" from line 92 */; ?>
<table>
					<?php /* tag "tr" from line 93 */; ?>
<tr>
						<?php /* tag "td" from line 94 */; ?>
<td>
							<?php /* tag "table" from line 95 */; ?>
<table style="width:200px;">
								<?php /* tag "tr" from line 96 */; ?>
<tr><?php /* tag "th" from line 96 */; ?>
<th colspan="3"><?php /* tag "span" from line 96 */; ?>
<span class="tableTitle">Today Transaction</span></th></tr>
								<?php /* tag "tr" from line 97 */; ?>
<tr><?php /* tag "td" from line 97 */; ?>
<td colspan="3" align="center"><?php /* tag "span" from line 97 */; ?>
<span style="font-size:9px; font-style:Italic; font-weight:normal;"><?php /* tag "tal:block" from line 97 */; ?>
<?php echo phptal_escape($ctx->todayDate); ?>
</span></td></tr>
								<?php /* tag "tr" from line 98 */; ?>
<tr><?php /* tag "td" from line 98 */; ?>
<td>&nbsp;</td></tr>
								<?php /* tag "tr" from line 99 */; ?>
<tr>
									<?php /* tag "td" from line 100 */; ?>
<td>&nbsp;</td>
									<?php /* tag "td" from line 101 */; ?>
<td>
										<?php /* tag "table" from line 102 */; ?>
<table style="width:75px; text-align:center;">
											<?php /* tag "tr" from line 103 */; ?>
<tr>
												<?php /* tag "th" from line 104 */; ?>
<th>Hour</th><?php /* tag "th" from line 104 */; ?>
<th>Hits</th>
											</tr>
											<?php 
/* tag "tal:block" from line 106 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

												<?php 
/* tag "tal:block" from line 107 */ ;
if ($ctx->repeat->item->index <= 11):  ;
?>

												<?php /* tag "tr" from line 108 */; ?>
<tr><?php /* tag "td" from line 108 */; ?>
<td><?php /* tag "tal:block" from line 108 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 108 */; ?>
<td><?php /* tag "tal:block" from line 108 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
												<?php endif; ?>

											<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

										</table>
									</td>
									<?php /* tag "td" from line 113 */; ?>
<td valign="top">
										<?php /* tag "table" from line 114 */; ?>
<table style="width:75px;text-align:center;">
											<?php /* tag "th" from line 115 */; ?>
<th>Hour</th><?php /* tag "th" from line 115 */; ?>
<th>Hits</th>
											<?php 
/* tag "tal:block" from line 116 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

												<?php 
/* tag "tal:block" from line 117 */ ;
if ($ctx->repeat->item->index >= 12):  ;
?>

												<?php /* tag "tr" from line 118 */; ?>
<tr><?php /* tag "td" from line 118 */; ?>
<td><?php /* tag "tal:block" from line 118 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 118 */; ?>
<td><?php /* tag "tal:block" from line 118 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
												<?php endif; ?>

											<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<?php /* tag "div" from line 130 */; ?>
<div id="bargraph">
		<?php /* tag "br" from line 131 */; ?>
<br/><?php /* tag "br" from line 131 */; ?>
<br/>
			<?php /* tag "table" from line 132 */; ?>
<table style="text-align:left;">
				<?php /* tag "tr" from line 133 */; ?>
<tr>
					<?php /* tag "td" from line 134 */; ?>
<td>
						<?php /* tag "div" from line 135 */; ?>
<div id="followers">
							<?php /* tag "table" from line 136 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 137 */; ?>
<tr>
									<?php /* tag "th" from line 138 */; ?>
<th colspan="2"><?php /* tag "span" from line 138 */; ?>
<span class="tableTitle">Top Followers</span></th>
								</tr>
								<?php /* tag "tr" from line 140 */; ?>
<tr><?php /* tag "td" from line 140 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 141 */; ?>
<tr><?php /* tag "td" from line 141 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 142 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->topFollowers)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 143 */; ?>
<tr>
									<?php /* tag "td" from line 144 */; ?>
<td class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 144 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 145 */; ?>
<td><?php /* tag "div" from line 145 */; ?>
<div class="extras_result"><?php /* tag "p" from line 145 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 145 */; ?>
<span><?php /* tag "tal:block" from line 145 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 145 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 148 */; ?>
<tr><?php /* tag "td" from line 148 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 151 */; ?>
<div id="tenantHits" style="display:none">
							<?php /* tag "table" from line 152 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 153 */; ?>
<tr>
									<?php /* tag "th" from line 154 */; ?>
<th colspan="2"><?php /* tag "span" from line 154 */; ?>
<span class="tableTitle">Tenant Hits</span></th>
								</tr>
								<?php /* tag "tr" from line 156 */; ?>
<tr><?php /* tag "td" from line 156 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 157 */; ?>
<tr><?php /* tag "td" from line 157 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 158 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->totalHitsPerTenant)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 159 */; ?>
<tr>
									<?php /* tag "td" from line 160 */; ?>
<td class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 160 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 161 */; ?>
<td><?php /* tag "div" from line 161 */; ?>
<div class="extras_result"><?php /* tag "p" from line 161 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 161 */; ?>
<span><?php /* tag "tal:block" from line 161 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 161 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 164 */; ?>
<tr><?php /* tag "td" from line 164 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 167 */; ?>
<div id="touchpointHits" style="display:none">
							<?php /* tag "table" from line 168 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 169 */; ?>
<tr>
									<?php /* tag "th" from line 170 */; ?>
<th colspan="2"><?php /* tag "span" from line 170 */; ?>
<span class="tableTitle">Touch Point Hits</span></th>
								</tr>
								<?php /* tag "tr" from line 172 */; ?>
<tr><?php /* tag "td" from line 172 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 173 */; ?>
<tr><?php /* tag "td" from line 173 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 174 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->operatorTotalHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 175 */; ?>
<tr>
									<?php /* tag "td" from line 176 */; ?>
<td class="extras_y-desc" scope="row">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php /* tag "tal:block" from line 176 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 177 */; ?>
<td><?php /* tag "div" from line 177 */; ?>
<div class="extras_result"><?php /* tag "p" from line 177 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 177 */; ?>
<span><?php /* tag "tal:block" from line 177 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 177 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 180 */; ?>
<tr><?php /* tag "td" from line 180 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 183 */; ?>
<div id="feedback" style="display:none">
							<?php /* tag "br" from line 184 */; ?>
<br/><?php /* tag "br" from line 184 */; ?>
<br/><?php /* tag "br" from line 184 */; ?>
<br/>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 190 */; ?>
<div id="monthlyReport">
			<?php /* tag "table" from line 191 */; ?>
<table style="width:880px; margin-left:20px;">
				<?php /* tag "tr" from line 192 */; ?>
<tr><?php /* tag "td" from line 192 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 193 */; ?>
<tr><?php /* tag "th" from line 193 */; ?>
<th colspan="12" align="center"><?php /* tag "span" from line 193 */; ?>
<span class="tableTitle">Total Hits By Month</span></th></tr>
				<?php /* tag "tr" from line 194 */; ?>
<tr><?php /* tag "td" from line 194 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 195 */; ?>
<tr>
					<?php 
/* tag "tal:block" from line 196 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->monthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

						<?php /* tag "td" from line 197 */; ?>
<td valign="top">
							<?php /* tag "table" from line 198 */; ?>
<table>
								<?php /* tag "tr" from line 199 */; ?>
<tr>
									<?php /* tag "th" from line 200 */; ?>
<th colspan="2" align="center"><?php /* tag "a" from line 200 */; ?>
<a href="javascript:;" onclick="global.displayTransContainer($(this).html());"><?php /* tag "tal:block" from line 200 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</a></th>
								</tr>
								<?php 
/* tag "tal:block" from line 202 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

									<?php /* tag "tr" from line 203 */; ?>
<tr>
										<?php /* tag "td" from line 204 */; ?>
<td><?php /* tag "tal:block" from line 204 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'total')); ?>
</td>
										<?php /* tag "td" from line 205 */; ?>
<td><?php /* tag "tal:block" from line 205 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
									</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 208 */; ?>
<tr><?php /* tag "td" from line 208 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</td>
					<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>
			
				</tr>
			</table>
		</div>
	</div>
<?php /* tag "script" from line 216 */; ?>
<script src="/js/dashboard/jquery.js" type="text/javascript"></script>
<?php /* tag "script" from line 217 */; ?>
<script src="/js/dashboard/jqBarGraph.1.1.js" type="text/javascript"></script>
<?php /* tag "script" from line 218 */; ?>
<script>

//	graphByMonth = new Array(
//	    [[23,0,0],'2007'],
//	    [[8,0,0],'2008'],
//	    [[4,1,0],'2009']
//	); 
			
	$("#feedback").jqBarGraph({
		data: graphByMonth,
		width: 350,
		height: 250,
		colors: ['#122A47','#1B3E69','#72808E'],
		color: '#1A2944',
		legends: ['ads','leads','google ads'],
		legend: true,
		type:'multi',
		barSpace: 3
	});
</script>	
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/dashboard/index.zpt (edit that file instead) */; ?>