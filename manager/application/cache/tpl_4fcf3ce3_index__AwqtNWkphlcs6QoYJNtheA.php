<?php 
function tpl_4fcf3ce3_index__AwqtNWkphlcs6QoYJNtheA(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<?php /* tag "link" from line 2 */; ?>
<link rel="stylesheet" href="/css/calendar/ui-lightness/jquery-ui-1.8.16.custom.css" media="screen"/>
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" href="/css/dashboard/style.css" media="screen"/>
<?php /* tag "link" from line 4 */; ?>
<link rel="stylesheet" href="/css/dashboard/barchart.css" media="screen"/>
<?php /* tag "link" from line 5 */; ?>
<link rel="stylesheet" href="/css/dashboard/chart.css" media="screen"/>

<?php /* tag "script" from line 7 */; ?>
<script src="/js/calendar/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
<!--<script src="/js/dashboard/chart.js" type="text/javascript"></script>-->

<?php /* tag "script" from line 10 */; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	  // for each result row..
	  $(".extras_result").each(function() {
	    // get the width of the bar from the span html
	    var length = $(this).find("span").html();
	    length = length.replace("%","");
	    length = length / 100;
	    var lengthPx = 400 * length;
	    
	    $(this).find("p").animate({'width':lengthPx}, 2000, function() {
	      // once the bar animation has finished, fade in the results
	      $(this).find("span").fadeIn(800);
	    });
	  });
	});
</script>


<?php /* tag "script" from line 29 */; ?>
<script>
    $(function() {
        $( "#startDate" ).datepicker();
        $( "#startDate" ).change(function() {
            $( "#startDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });     
	// for extraction form
    $(function() {
        $( "#endDate" ).datepicker();
        $( "#endDate" ).change(function() {
            $( "#endDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });        
</script>		
    
    <?php /* tag "h1" from line 45 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 45 */; ?>
<span style="float:left;">Dashboard</span></h1>
	<?php /* tag "div" from line 46 */; ?>
<div id="folderTab"><?php /* tag "div" from line 46 */; ?>
<div class="folderTabText">Quick View</div></div>
	<?php /* tag "br" from line 47 */; ?>
<br/>
<!--	<div class="dashboardTabMenu">
		<span><a href="admin#/dashboard/index">Home</a></span>
		<span><a class="selected" href="#">Analytics</a></span>
		<span><a href="#">Menu 3</a></span>
		<span><a href="#">Menu 4</a></span>
	</div>
	<div class="dimension">
		Dimensions<br/>
		Start Date: <input type="text" id="startDate" name="startDate"/>&nbsp;&nbsp;&nbsp;
		End Date: <input type="text" id="endDate" name="endDate"/>&nbsp;&nbsp;&nbsp;
		<input type="button" value="Query"/>
	</div>-->
	<?php /* tag "div" from line 60 */; ?>
<div class="reportContainer">
		<?php 
/* tag "tal:block" from line 61 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->detailMonthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

			<?php 
/* tag "div" from line 62 */ ;
if (null !== ($_tmp_2 = ($ctx->repeat->item->key))):  ;
$_tmp_2 = ' id="'.phptal_escape($_tmp_2).'"' ;
else:  ;
$_tmp_2 = '' ;
endif ;
?>
<div style="display:block;"<?php echo $_tmp_2 ?>
>
				<?php 
/* tag "tal:block" from line 63 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

					<?php /* tag "table" from line 64 */; ?>
<table>
						<?php /* tag "tr" from line 65 */; ?>
<tr><?php /* tag "td" from line 65 */; ?>
<td><?php /* tag "tal:block" from line 65 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'name')); ?>
</td><?php /* tag "td" from line 65 */; ?>
<td><?php /* tag "tal:block" from line 65 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td></tr>
					</table>
				<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

			</div>
		<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

		<?php /* tag "div" from line 70 */; ?>
<div class="tbl">
			<?php /* tag "table" from line 71 */; ?>
<table>
				<?php /* tag "tr" from line 72 */; ?>
<tr>
					<?php /* tag "td" from line 73 */; ?>
<td>
						<?php /* tag "table" from line 74 */; ?>
<table style="width:200px;">
							<?php /* tag "tr" from line 75 */; ?>
<tr><?php /* tag "td" from line 75 */; ?>
<td>&nbsp;</td></tr>
							<?php /* tag "tr" from line 76 */; ?>
<tr><?php /* tag "td" from line 76 */; ?>
<td>&nbsp;</td></tr>
							<?php /* tag "tr" from line 77 */; ?>
<tr><?php /* tag "th" from line 77 */; ?>
<th colspan="3"><?php /* tag "span" from line 77 */; ?>
<span class="tableTitle">Today Transaction</span></th></tr>
							<?php /* tag "tr" from line 78 */; ?>
<tr><?php /* tag "td" from line 78 */; ?>
<td colspan="3" align="center"><?php /* tag "span" from line 78 */; ?>
<span style="font-size:9px; font-style:Italic; font-weight:normal;"><?php /* tag "tal:block" from line 78 */; ?>
<?php echo phptal_escape($ctx->todayDate); ?>
</span></td></tr>
							<?php /* tag "tr" from line 79 */; ?>
<tr><?php /* tag "td" from line 79 */; ?>
<td>&nbsp;</td></tr>
							<?php /* tag "tr" from line 80 */; ?>
<tr>
								<?php /* tag "td" from line 81 */; ?>
<td>&nbsp;</td>
								<?php /* tag "td" from line 82 */; ?>
<td>
									<?php /* tag "table" from line 83 */; ?>
<table style="width:75px; text-align:center;">
										<?php /* tag "tr" from line 84 */; ?>
<tr>
											<?php /* tag "th" from line 85 */; ?>
<th>Hour</th><?php /* tag "th" from line 85 */; ?>
<th>Hits</th>
										</tr>
										<?php 
/* tag "tal:block" from line 87 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

											<?php 
/* tag "tal:block" from line 88 */ ;
if ($ctx->repeat->item->index <= 11):  ;
?>

											<?php /* tag "tr" from line 89 */; ?>
<tr><?php /* tag "td" from line 89 */; ?>
<td><?php /* tag "tal:block" from line 89 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 89 */; ?>
<td><?php /* tag "tal:block" from line 89 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
											<?php endif; ?>

										<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

									</table>
								</td>
								<?php /* tag "td" from line 94 */; ?>
<td valign="top">
									<?php /* tag "table" from line 95 */; ?>
<table style="width:75px;text-align:center;">
										<?php /* tag "th" from line 96 */; ?>
<th>Hour</th><?php /* tag "th" from line 96 */; ?>
<th>Hits</th>
										<?php 
/* tag "tal:block" from line 97 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

											<?php 
/* tag "tal:block" from line 98 */ ;
if ($ctx->repeat->item->index >= 12):  ;
?>

											<?php /* tag "tr" from line 99 */; ?>
<tr><?php /* tag "td" from line 99 */; ?>
<td><?php /* tag "tal:block" from line 99 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 99 */; ?>
<td><?php /* tag "tal:block" from line 99 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
											<?php endif; ?>

										<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 110 */; ?>
<div id="bargraph">
		<?php /* tag "br" from line 111 */; ?>
<br/>
			<?php /* tag "table" from line 112 */; ?>
<table style="text-align:left;">
				<?php /* tag "tr" from line 113 */; ?>
<tr>
					<?php /* tag "td" from line 114 */; ?>
<td>
						<?php /* tag "table" from line 115 */; ?>
<table class="extras_table" align="center">
							<?php /* tag "tr" from line 116 */; ?>
<tr>
								<?php /* tag "th" from line 117 */; ?>
<th colspan="2"><?php /* tag "span" from line 117 */; ?>
<span class="tableTitle">Top Followers</span></th>
							</tr>
							<?php /* tag "tr" from line 119 */; ?>
<tr><?php /* tag "td" from line 119 */; ?>
<td></td></tr>
						
							<?php 
/* tag "tal:block" from line 121 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->topFollowers)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

							<?php /* tag "tr" from line 122 */; ?>
<tr>
								<?php /* tag "th" from line 123 */; ?>
<th class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 123 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</th>
								<?php /* tag "td" from line 124 */; ?>
<td><?php /* tag "div" from line 124 */; ?>
<div class="extras_result"><?php /* tag "p" from line 124 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 124 */; ?>
<span><?php /* tag "tal:block" from line 124 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 124 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
							</tr>
							<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

							<?php /* tag "tr" from line 127 */; ?>
<tr><?php /* tag "td" from line 127 */; ?>
<td>&nbsp;</td></tr>
						</table>
						<?php /* tag "table" from line 129 */; ?>
<table class="extras_table" align="center">
							<?php /* tag "tr" from line 130 */; ?>
<tr>
								<?php /* tag "th" from line 131 */; ?>
<th colspan="2"><?php /* tag "span" from line 131 */; ?>
<span class="tableTitle">Tenant Total Hits</span></th>
							</tr>
							<?php 
/* tag "tal:block" from line 133 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->totalHitsPerTenant)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

							<?php /* tag "tr" from line 134 */; ?>
<tr>
								<?php /* tag "th" from line 135 */; ?>
<th class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 135 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</th>
								<?php /* tag "td" from line 136 */; ?>
<td><?php /* tag "div" from line 136 */; ?>
<div class="extras_result"><?php /* tag "p" from line 136 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 136 */; ?>
<span><?php /* tag "tal:block" from line 136 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 136 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
							</tr>
							<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

							<?php /* tag "tr" from line 139 */; ?>
<tr><?php /* tag "td" from line 139 */; ?>
<td>&nbsp;</td></tr>
						</table>
						<?php /* tag "table" from line 141 */; ?>
<table class="extras_table" align="center">
							<?php /* tag "tr" from line 142 */; ?>
<tr>
								<?php /* tag "th" from line 143 */; ?>
<th colspan="2"><?php /* tag "span" from line 143 */; ?>
<span class="tableTitle">Touch Point Total Hits</span></th>
							</tr>
							<?php 
/* tag "tal:block" from line 145 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->operatorTotalHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

							<?php /* tag "tr" from line 146 */; ?>
<tr>
								<?php /* tag "th" from line 147 */; ?>
<th class="extras_y-desc" scope="row">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php /* tag "tal:block" from line 147 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</th>
								<?php /* tag "td" from line 148 */; ?>
<td><?php /* tag "div" from line 148 */; ?>
<div class="extras_result"><?php /* tag "p" from line 148 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 148 */; ?>
<span><?php /* tag "tal:block" from line 148 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 148 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
							</tr>
							<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

							<?php /* tag "tr" from line 151 */; ?>
<tr><?php /* tag "td" from line 151 */; ?>
<td>&nbsp;</td></tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 157 */; ?>
<div id="monthlyReport">
			<?php /* tag "table" from line 158 */; ?>
<table style="width:880px; margin-left:20px;">
				<?php /* tag "tr" from line 159 */; ?>
<tr><?php /* tag "td" from line 159 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 160 */; ?>
<tr><?php /* tag "th" from line 160 */; ?>
<th colspan="12" align="center"><?php /* tag "span" from line 160 */; ?>
<span class="tableTitle">Total Hits By Month</span></th></tr>
				<?php /* tag "tr" from line 161 */; ?>
<tr><?php /* tag "td" from line 161 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 162 */; ?>
<tr>
					<?php 
/* tag "tal:block" from line 163 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->monthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

						<?php /* tag "td" from line 164 */; ?>
<td valign="top">
							<?php /* tag "table" from line 165 */; ?>
<table>
								<?php /* tag "tr" from line 166 */; ?>
<tr>
									<?php /* tag "th" from line 167 */; ?>
<th colspan="2" align="center"><?php /* tag "a" from line 167 */; ?>
<a href="javascript:;" onclick="alert($(this).html());"><?php /* tag "tal:block" from line 167 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</a></th>
								</tr>
								<?php 
/* tag "tal:block" from line 169 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

									<?php /* tag "tr" from line 170 */; ?>
<tr>
										<?php /* tag "td" from line 171 */; ?>
<td><?php /* tag "tal:block" from line 171 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'total')); ?>
</td>
										<?php /* tag "td" from line 172 */; ?>
<td><?php /* tag "tal:block" from line 172 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
									</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 175 */; ?>
<tr><?php /* tag "td" from line 175 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</td>
					<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>
			
				</tr>
			</table>
		</div>
	</div>
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/dashboard/index.zpt (edit that file instead) */; ?>