<?php 
function tpl_4fd19f78_index__AwqtNWkphlcs6QoYJNtheA(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<?php /* tag "link" from line 2 */; ?>
<link rel="stylesheet" href="/css/calendar/ui-lightness/jquery-ui-1.8.16.custom.css" media="screen"/>
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" href="/css/dashboard/style.css" media="screen"/>
<?php /* tag "link" from line 4 */; ?>
<link rel="stylesheet" href="/css/dashboard/barchart.css" media="screen"/>
<?php /* tag "link" from line 5 */; ?>
<link rel="stylesheet" href="/css/dashboard/chart.css" media="screen"/>

<?php /* tag "script" from line 7 */; ?>
<script src="/js/calendar/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
<?php /* tag "script" from line 8 */; ?>
<script src="/js/dashboard/jqBarGraph.1.1.js" type="text/javascript"></script>
<?php /* tag "script" from line 9 */; ?>
<script src="/js/dashboard/global.js" type="text/javascript"></script>
<!--<script src="/js/dashboard/chart.js" type="text/javascript"></script>-->

<?php /* tag "script" from line 12 */; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	  // for each result row..
	  $(".extras_result").each(function() {
	    // get the width of the bar from the span html
	    var length = $(this).find("span").html();
	    length = length.replace("%","");
	    length = length / 100;
	    var lengthPx = 400 * length;
	    
	    $(this).find("p").animate({'width':lengthPx}, 2000, function() {
	      // once the bar animation has finished, fade in the results
	      $(this).find("span").fadeIn(800);
	    });
	  });
	});
</script>


<?php /* tag "script" from line 31 */; ?>
<script>
    $(function() {
        $( "#startDate" ).datepicker();
        $( "#startDate" ).change(function() {
            $( "#startDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });     
	// for extraction form
    $(function() {
        $( "#endDate" ).datepicker();
        $( "#endDate" ).change(function() {
            $( "#endDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });        
</script>

<?php /* tag "script" from line 47 */; ?>
<script>
	var arrayOfData = new Array(
	    [[14,54,26],'2007'],
	    [[8,48,38],'2008'],
	    [[4,36,57],'2009']
	); 

	$('#severityHits').jqBarGraph({ data: arrayOfData }); 
	
//	data: arrayOfData, // array of data for your graph
//	title: false, // title of your graph, accept HTML
//	barSpace: 10, // this is default space between bars in pixels
//	width: 400, // default width of your graph
//	height: 200, //default height of your graph
//	color: '#000000', // if you don't send colors for your data this will be default bars color
//	colors: false, // array of colors that will be used for your bars and legends
//	lbl: '', // if there is no label in your array
//	sort: false, // sort your data before displaying graph, you can sort as 'asc' or 'desc'
//	position: 'bottom', // position of your bars, can be 'bottom' or 'top'. 'top' doesn't work for multi type
//	prefix: '', // text that will be shown before every label
//	postfix: '', // text that will be shown after every label
//	animate: true, // if you don't need animated appearance change to false
//	speed: 2, // speed of animation in seconds
//	legendWidth: 100, // width of your legend box
//	legend: false, // if you want legend change to true
//	legends: false, // array for legend. for simple graph type legend will be extracted from labels if you don't set this
//	type: false, // for multi array data default graph type is stacked, you can change to 'multi' for multi bar type
//	showValues: true, // you can use this for multi and stacked type and it will show values of every bar part
//	showValuesColor: '#fff' // color of font for values
	
</script>
    
    <?php /* tag "h1" from line 79 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 79 */; ?>
<span style="float:left;">Dashboard</span></h1>
	<?php /* tag "div" from line 80 */; ?>
<div id="folderTab"><?php /* tag "div" from line 80 */; ?>
<div class="folderTabText">Quick View</div></div>
	<?php /* tag "br" from line 81 */; ?>
<br/>
	<?php /* tag "div" from line 82 */; ?>
<div class="dashboardTabMenu">
		<?php /* tag "span" from line 83 */; ?>
<span id="followersLink"><?php /* tag "a" from line 83 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('followers')">Top Follower</a></span>
		<?php /* tag "span" from line 84 */; ?>
<span id="tenantHitsLink"><?php /* tag "a" from line 84 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('tenantHits')">Tenant Hits</a></span>
		<?php /* tag "span" from line 85 */; ?>
<span id="touchpointHitsLink"><?php /* tag "a" from line 85 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('touchpointHits')">Touchpoint Hits</a></span>
		<?php /* tag "span" from line 86 */; ?>
<span id="feedbackLink"><?php /* tag "a" from line 86 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('feedback')">Feedback</a></span>
	</div>
<!--	<div class="dimension">
		Dimensions<br/>
		Start Date: <input type="text" id="startDate" name="startDate"/>&nbsp;&nbsp;&nbsp;
		End Date: <input type="text" id="endDate" name="endDate"/>&nbsp;&nbsp;&nbsp;
		<input type="button" value="Query"/>
	</div>-->
	<?php /* tag "div" from line 94 */; ?>
<div class="reportContainer">
		<?php /* tag "div" from line 95 */; ?>
<div class="tbl">
			<?php 
/* tag "tal:block" from line 96 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->detailMonthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

				<?php 
/* tag "div" from line 97 */ ;
if (null !== ($_tmp_2 = ($ctx->repeat->item->key))):  ;
$_tmp_2 = ' id="'.phptal_escape($_tmp_2).'"' ;
else:  ;
$_tmp_2 = '' ;
endif ;
?>
<div style="display:none;"<?php echo $_tmp_2 ?>
>
					<?php /* tag "table" from line 98 */; ?>
<table style="width:200px; text-align:center;">
						<?php /* tag "tr" from line 99 */; ?>
<tr><?php /* tag "td" from line 99 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 99 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 100 */; ?>
<tr><?php /* tag "td" from line 100 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 100 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 101 */; ?>
<tr><?php /* tag "th" from line 101 */; ?>
<th colspan="3" align="center"><?php /* tag "span" from line 101 */; ?>
<span class="tableTitle" style="font-size:14px;"><?php /* tag "tal:block" from line 101 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</span></th></tr>
						<?php /* tag "tr" from line 102 */; ?>
<tr><?php /* tag "td" from line 102 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 102 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 103 */; ?>
<tr><?php /* tag "th" from line 103 */; ?>
<th>Name</th><?php /* tag "th" from line 103 */; ?>
<th>Hits</th></tr>
						<?php 
/* tag "tal:block" from line 104 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

							<?php /* tag "tr" from line 105 */; ?>
<tr>
								<?php /* tag "td" from line 106 */; ?>
<td width="50%"><?php /* tag "tal:block" from line 106 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'name')); ?>
</td>
								<?php /* tag "td" from line 107 */; ?>
<td width="50%"><?php /* tag "tal:block" from line 107 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
							</tr>
						<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

					</table>
				</div>
			<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

			<?php /* tag "div" from line 113 */; ?>
<div id="todayTransaction">
				<?php /* tag "table" from line 114 */; ?>
<table>
					<?php /* tag "tr" from line 115 */; ?>
<tr>
						<?php /* tag "td" from line 116 */; ?>
<td>
							<?php /* tag "table" from line 117 */; ?>
<table style="width:200px;">
								<?php /* tag "tr" from line 118 */; ?>
<tr><?php /* tag "th" from line 118 */; ?>
<th colspan="3"><?php /* tag "span" from line 118 */; ?>
<span class="tableTitle">Today Transaction</span></th></tr>
								<?php /* tag "tr" from line 119 */; ?>
<tr><?php /* tag "td" from line 119 */; ?>
<td colspan="3" align="center"><?php /* tag "span" from line 119 */; ?>
<span style="font-size:9px; font-style:Italic; font-weight:normal;"><?php /* tag "tal:block" from line 119 */; ?>
<?php echo phptal_escape($ctx->todayDate); ?>
</span></td></tr>
								<?php /* tag "tr" from line 120 */; ?>
<tr><?php /* tag "td" from line 120 */; ?>
<td>&nbsp;</td></tr>
								<?php /* tag "tr" from line 121 */; ?>
<tr>
									<?php /* tag "td" from line 122 */; ?>
<td>&nbsp;</td>
									<?php /* tag "td" from line 123 */; ?>
<td>
										<?php /* tag "table" from line 124 */; ?>
<table style="width:75px; text-align:center;">
											<?php /* tag "tr" from line 125 */; ?>
<tr>
												<?php /* tag "th" from line 126 */; ?>
<th>Hour</th><?php /* tag "th" from line 126 */; ?>
<th>Hits</th>
											</tr>
											<?php 
/* tag "tal:block" from line 128 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

												<?php 
/* tag "tal:block" from line 129 */ ;
if ($ctx->repeat->item->index <= 11):  ;
?>

												<?php /* tag "tr" from line 130 */; ?>
<tr><?php /* tag "td" from line 130 */; ?>
<td><?php /* tag "tal:block" from line 130 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 130 */; ?>
<td><?php /* tag "tal:block" from line 130 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
												<?php endif; ?>

											<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

										</table>
									</td>
									<?php /* tag "td" from line 135 */; ?>
<td valign="top">
										<?php /* tag "table" from line 136 */; ?>
<table style="width:75px;text-align:center;">
											<?php /* tag "th" from line 137 */; ?>
<th>Hour</th><?php /* tag "th" from line 137 */; ?>
<th>Hits</th>
											<?php 
/* tag "tal:block" from line 138 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

												<?php 
/* tag "tal:block" from line 139 */ ;
if ($ctx->repeat->item->index >= 12):  ;
?>

												<?php /* tag "tr" from line 140 */; ?>
<tr><?php /* tag "td" from line 140 */; ?>
<td><?php /* tag "tal:block" from line 140 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 140 */; ?>
<td><?php /* tag "tal:block" from line 140 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
												<?php endif; ?>

											<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<?php /* tag "div" from line 152 */; ?>
<div id="bargraph">
		<?php /* tag "br" from line 153 */; ?>
<br/><?php /* tag "br" from line 153 */; ?>
<br/>
			<?php /* tag "table" from line 154 */; ?>
<table style="text-align:left;">
				<?php /* tag "tr" from line 155 */; ?>
<tr>
					<?php /* tag "td" from line 156 */; ?>
<td>
						<?php /* tag "div" from line 157 */; ?>
<div id="followers">
							<?php /* tag "table" from line 158 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 159 */; ?>
<tr>
									<?php /* tag "th" from line 160 */; ?>
<th colspan="2"><?php /* tag "span" from line 160 */; ?>
<span class="tableTitle">Top Followers</span></th>
								</tr>
								<?php /* tag "tr" from line 162 */; ?>
<tr><?php /* tag "td" from line 162 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 163 */; ?>
<tr><?php /* tag "td" from line 163 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 164 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->topFollowers)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 165 */; ?>
<tr>
									<?php /* tag "td" from line 166 */; ?>
<td class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 166 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 167 */; ?>
<td><?php /* tag "div" from line 167 */; ?>
<div class="extras_result"><?php /* tag "p" from line 167 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 167 */; ?>
<span><?php /* tag "tal:block" from line 167 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 167 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 170 */; ?>
<tr><?php /* tag "td" from line 170 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 173 */; ?>
<div id="tenantHits" style="display:none">
							<?php /* tag "table" from line 174 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 175 */; ?>
<tr>
									<?php /* tag "th" from line 176 */; ?>
<th colspan="2"><?php /* tag "span" from line 176 */; ?>
<span class="tableTitle">Tenant Hits</span></th>
								</tr>
								<?php /* tag "tr" from line 178 */; ?>
<tr><?php /* tag "td" from line 178 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 179 */; ?>
<tr><?php /* tag "td" from line 179 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 180 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->totalHitsPerTenant)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 181 */; ?>
<tr>
									<?php /* tag "td" from line 182 */; ?>
<td class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 182 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 183 */; ?>
<td><?php /* tag "div" from line 183 */; ?>
<div class="extras_result"><?php /* tag "p" from line 183 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 183 */; ?>
<span><?php /* tag "tal:block" from line 183 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 183 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 186 */; ?>
<tr><?php /* tag "td" from line 186 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 189 */; ?>
<div id="touchpointHits" style="display:none">
							<?php /* tag "table" from line 190 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 191 */; ?>
<tr>
									<?php /* tag "th" from line 192 */; ?>
<th colspan="2"><?php /* tag "span" from line 192 */; ?>
<span class="tableTitle">Touch Point Hits</span></th>
								</tr>
								<?php /* tag "tr" from line 194 */; ?>
<tr><?php /* tag "td" from line 194 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 195 */; ?>
<tr><?php /* tag "td" from line 195 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 196 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->operatorTotalHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 197 */; ?>
<tr>
									<?php /* tag "td" from line 198 */; ?>
<td class="extras_y-desc" scope="row">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php /* tag "tal:block" from line 198 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 199 */; ?>
<td><?php /* tag "div" from line 199 */; ?>
<div class="extras_result"><?php /* tag "p" from line 199 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 199 */; ?>
<span><?php /* tag "tal:block" from line 199 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 199 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 202 */; ?>
<tr><?php /* tag "td" from line 202 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 205 */; ?>
<div id="feedback" style="display:none">
							<?php /* tag "table" from line 206 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 207 */; ?>
<tr>
									<?php /* tag "th" from line 208 */; ?>
<th><?php /* tag "span" from line 208 */; ?>
<span class="tableTitle" style="text-decoration:underline;"><?php /* tag "a" from line 208 */; ?>
<a href="javascript:;">Severity</a></span></th>
									<?php /* tag "th" from line 209 */; ?>
<th>&nbsp;/&nbsp;</th>
																		<?php /* tag "th" from line 210 */; ?>
<th><?php /* tag "span" from line 210 */; ?>
<span class="tableTitle" style="text-decoration:underline;"><?php /* tag "a" from line 210 */; ?>
<a href="javascript:;">Status</a></span></th>
								</tr>
							</table>
							<?php /* tag "div" from line 213 */; ?>
<div id="severityHits">
								<?php /* tag "table" from line 214 */; ?>
<table>
									<?php /* tag "tr" from line 215 */; ?>
<tr>
										<?php /* tag "td" from line 216 */; ?>
<td></td>
									</tr>
								</table>
							</div>
							<?php /* tag "div" from line 220 */; ?>
<div id="status">
								<?php /* tag "table" from line 221 */; ?>
<table>
									<?php /* tag "tr" from line 222 */; ?>
<tr>
										<?php /* tag "td" from line 223 */; ?>
<td></td>
									</tr>
								</table>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 232 */; ?>
<div id="monthlyReport">
			<?php /* tag "table" from line 233 */; ?>
<table style="width:880px; margin-left:20px;">
				<?php /* tag "tr" from line 234 */; ?>
<tr><?php /* tag "td" from line 234 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 235 */; ?>
<tr><?php /* tag "th" from line 235 */; ?>
<th colspan="12" align="center"><?php /* tag "span" from line 235 */; ?>
<span class="tableTitle">Total Hits By Month</span></th></tr>
				<?php /* tag "tr" from line 236 */; ?>
<tr><?php /* tag "td" from line 236 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 237 */; ?>
<tr>
					<?php 
/* tag "tal:block" from line 238 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->monthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

						<?php /* tag "td" from line 239 */; ?>
<td valign="top">
							<?php /* tag "table" from line 240 */; ?>
<table>
								<?php /* tag "tr" from line 241 */; ?>
<tr>
									<?php /* tag "th" from line 242 */; ?>
<th colspan="2" align="center"><?php /* tag "a" from line 242 */; ?>
<a href="javascript:;" onclick="global.displayTransContainer($(this).html());"><?php /* tag "tal:block" from line 242 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</a></th>
								</tr>
								<?php 
/* tag "tal:block" from line 244 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

									<?php /* tag "tr" from line 245 */; ?>
<tr>
										<?php /* tag "td" from line 246 */; ?>
<td><?php /* tag "tal:block" from line 246 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'total')); ?>
</td>
										<?php /* tag "td" from line 247 */; ?>
<td><?php /* tag "tal:block" from line 247 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
									</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 250 */; ?>
<tr><?php /* tag "td" from line 250 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</td>
					<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>
			
				</tr>
			</table>
		</div>
	</div>
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/dashboard/index.zpt (edit that file instead) */; ?>