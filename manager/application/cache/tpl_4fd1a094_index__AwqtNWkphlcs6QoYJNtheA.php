<?php 
function tpl_4fd1a094_index__AwqtNWkphlcs6QoYJNtheA(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<?php /* tag "link" from line 2 */; ?>
<link rel="stylesheet" href="/css/calendar/ui-lightness/jquery-ui-1.8.16.custom.css" media="screen"/>
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" href="/css/dashboard/style.css" media="screen"/>
<?php /* tag "link" from line 4 */; ?>
<link rel="stylesheet" href="/css/dashboard/barchart.css" media="screen"/>
<?php /* tag "link" from line 5 */; ?>
<link rel="stylesheet" href="/css/dashboard/chart.css" media="screen"/>

<?php /* tag "script" from line 7 */; ?>
<script src="/js/calendar/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
<?php /* tag "script" from line 8 */; ?>
<script src="/js/dashboard/jqBarGraph.1.1.js" type="text/javascript"></script>
<?php /* tag "script" from line 9 */; ?>
<script src="/js/dashboard/global.js" type="text/javascript"></script>
<!--<script src="/js/dashboard/chart.js" type="text/javascript"></script>-->

<?php /* tag "script" from line 12 */; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	  // for each result row..
	  $(".extras_result").each(function() {
	    // get the width of the bar from the span html
	    var length = $(this).find("span").html();
	    length = length.replace("%","");
	    length = length / 100;
	    var lengthPx = 400 * length;
	    
	    $(this).find("p").animate({'width':lengthPx}, 2000, function() {
	      // once the bar animation has finished, fade in the results
	      $(this).find("span").fadeIn(800);
	    });
	  });
	});
</script>


<?php /* tag "script" from line 31 */; ?>
<script>
    $(function() {
        $( "#startDate" ).datepicker();
        $( "#startDate" ).change(function() {
            $( "#startDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });     
	// for extraction form
    $(function() {
        $( "#endDate" ).datepicker();
        $( "#endDate" ).change(function() {
            $( "#endDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });        
</script>

<?php /* tag "script" from line 47 */; ?>
<script>
	graphByMonth = new Array(
	    [[14,54,26],'2007'],
	    [[8,48,38],'2008'],
	    [[4,36,57],'2009']
	); 

//	$('#severityHits').jqBarGraph({ data: arrayOfData }); 
	
	$("#severityHits").jqBarGraph({
		data: graphByMonth,
		width: 500,
		colors: ['#122A47','#1B3E69'],
		color: '#1A2944',
		barSpace: 5,
		title: '<?php /* tag "h3" from line 62 */; ?>
<h3>Number of visitors per month<?php /* tag "br" from line 62 */; ?>
<br/><?php /* tag "small" from line 62 */; ?>
<small>simple bar graph</small></h3>'
	});
	
	
//	data: arrayOfData, // array of data for your graph
//	title: false, // title of your graph, accept HTML
//	barSpace: 10, // this is default space between bars in pixels
//	width: 400, // default width of your graph
//	height: 200, //default height of your graph
//	color: '#000000', // if you don't send colors for your data this will be default bars color
//	colors: false, // array of colors that will be used for your bars and legends
//	lbl: '', // if there is no label in your array
//	sort: false, // sort your data before displaying graph, you can sort as 'asc' or 'desc'
//	position: 'bottom', // position of your bars, can be 'bottom' or 'top'. 'top' doesn't work for multi type
//	prefix: '', // text that will be shown before every label
//	postfix: '', // text that will be shown after every label
//	animate: true, // if you don't need animated appearance change to false
//	speed: 2, // speed of animation in seconds
//	legendWidth: 100, // width of your legend box
//	legend: false, // if you want legend change to true
//	legends: false, // array for legend. for simple graph type legend will be extracted from labels if you don't set this
//	type: false, // for multi array data default graph type is stacked, you can change to 'multi' for multi bar type
//	showValues: true, // you can use this for multi and stacked type and it will show values of every bar part
//	showValuesColor: '#fff' // color of font for values
	
</script>
    
    <?php /* tag "h1" from line 89 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 89 */; ?>
<span style="float:left;">Dashboard</span></h1>
	<?php /* tag "div" from line 90 */; ?>
<div id="folderTab"><?php /* tag "div" from line 90 */; ?>
<div class="folderTabText">Quick View</div></div>
	<?php /* tag "br" from line 91 */; ?>
<br/>
	<?php /* tag "div" from line 92 */; ?>
<div class="dashboardTabMenu">
		<?php /* tag "span" from line 93 */; ?>
<span id="followersLink"><?php /* tag "a" from line 93 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('followers')">Top Follower</a></span>
		<?php /* tag "span" from line 94 */; ?>
<span id="tenantHitsLink"><?php /* tag "a" from line 94 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('tenantHits')">Tenant Hits</a></span>
		<?php /* tag "span" from line 95 */; ?>
<span id="touchpointHitsLink"><?php /* tag "a" from line 95 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('touchpointHits')">Touchpoint Hits</a></span>
		<?php /* tag "span" from line 96 */; ?>
<span id="feedbackLink"><?php /* tag "a" from line 96 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('feedback')">Feedback</a></span>
	</div>
<!--	<div class="dimension">
		Dimensions<br/>
		Start Date: <input type="text" id="startDate" name="startDate"/>&nbsp;&nbsp;&nbsp;
		End Date: <input type="text" id="endDate" name="endDate"/>&nbsp;&nbsp;&nbsp;
		<input type="button" value="Query"/>
	</div>-->
	<?php /* tag "div" from line 104 */; ?>
<div class="reportContainer">
		<?php /* tag "div" from line 105 */; ?>
<div class="tbl">
			<?php 
/* tag "tal:block" from line 106 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->detailMonthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

				<?php 
/* tag "div" from line 107 */ ;
if (null !== ($_tmp_2 = ($ctx->repeat->item->key))):  ;
$_tmp_2 = ' id="'.phptal_escape($_tmp_2).'"' ;
else:  ;
$_tmp_2 = '' ;
endif ;
?>
<div style="display:none;"<?php echo $_tmp_2 ?>
>
					<?php /* tag "table" from line 108 */; ?>
<table style="width:200px; text-align:center;">
						<?php /* tag "tr" from line 109 */; ?>
<tr><?php /* tag "td" from line 109 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 109 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 110 */; ?>
<tr><?php /* tag "td" from line 110 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 110 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 111 */; ?>
<tr><?php /* tag "th" from line 111 */; ?>
<th colspan="3" align="center"><?php /* tag "span" from line 111 */; ?>
<span class="tableTitle" style="font-size:14px;"><?php /* tag "tal:block" from line 111 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</span></th></tr>
						<?php /* tag "tr" from line 112 */; ?>
<tr><?php /* tag "td" from line 112 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 112 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 113 */; ?>
<tr><?php /* tag "th" from line 113 */; ?>
<th>Name</th><?php /* tag "th" from line 113 */; ?>
<th>Hits</th></tr>
						<?php 
/* tag "tal:block" from line 114 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

							<?php /* tag "tr" from line 115 */; ?>
<tr>
								<?php /* tag "td" from line 116 */; ?>
<td width="50%"><?php /* tag "tal:block" from line 116 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'name')); ?>
</td>
								<?php /* tag "td" from line 117 */; ?>
<td width="50%"><?php /* tag "tal:block" from line 117 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
							</tr>
						<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

					</table>
				</div>
			<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

			<?php /* tag "div" from line 123 */; ?>
<div id="todayTransaction">
				<?php /* tag "table" from line 124 */; ?>
<table>
					<?php /* tag "tr" from line 125 */; ?>
<tr>
						<?php /* tag "td" from line 126 */; ?>
<td>
							<?php /* tag "table" from line 127 */; ?>
<table style="width:200px;">
								<?php /* tag "tr" from line 128 */; ?>
<tr><?php /* tag "th" from line 128 */; ?>
<th colspan="3"><?php /* tag "span" from line 128 */; ?>
<span class="tableTitle">Today Transaction</span></th></tr>
								<?php /* tag "tr" from line 129 */; ?>
<tr><?php /* tag "td" from line 129 */; ?>
<td colspan="3" align="center"><?php /* tag "span" from line 129 */; ?>
<span style="font-size:9px; font-style:Italic; font-weight:normal;"><?php /* tag "tal:block" from line 129 */; ?>
<?php echo phptal_escape($ctx->todayDate); ?>
</span></td></tr>
								<?php /* tag "tr" from line 130 */; ?>
<tr><?php /* tag "td" from line 130 */; ?>
<td>&nbsp;</td></tr>
								<?php /* tag "tr" from line 131 */; ?>
<tr>
									<?php /* tag "td" from line 132 */; ?>
<td>&nbsp;</td>
									<?php /* tag "td" from line 133 */; ?>
<td>
										<?php /* tag "table" from line 134 */; ?>
<table style="width:75px; text-align:center;">
											<?php /* tag "tr" from line 135 */; ?>
<tr>
												<?php /* tag "th" from line 136 */; ?>
<th>Hour</th><?php /* tag "th" from line 136 */; ?>
<th>Hits</th>
											</tr>
											<?php 
/* tag "tal:block" from line 138 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

												<?php 
/* tag "tal:block" from line 139 */ ;
if ($ctx->repeat->item->index <= 11):  ;
?>

												<?php /* tag "tr" from line 140 */; ?>
<tr><?php /* tag "td" from line 140 */; ?>
<td><?php /* tag "tal:block" from line 140 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 140 */; ?>
<td><?php /* tag "tal:block" from line 140 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
												<?php endif; ?>

											<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

										</table>
									</td>
									<?php /* tag "td" from line 145 */; ?>
<td valign="top">
										<?php /* tag "table" from line 146 */; ?>
<table style="width:75px;text-align:center;">
											<?php /* tag "th" from line 147 */; ?>
<th>Hour</th><?php /* tag "th" from line 147 */; ?>
<th>Hits</th>
											<?php 
/* tag "tal:block" from line 148 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

												<?php 
/* tag "tal:block" from line 149 */ ;
if ($ctx->repeat->item->index >= 12):  ;
?>

												<?php /* tag "tr" from line 150 */; ?>
<tr><?php /* tag "td" from line 150 */; ?>
<td><?php /* tag "tal:block" from line 150 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 150 */; ?>
<td><?php /* tag "tal:block" from line 150 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
												<?php endif; ?>

											<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<?php /* tag "div" from line 162 */; ?>
<div id="bargraph">
		<?php /* tag "br" from line 163 */; ?>
<br/><?php /* tag "br" from line 163 */; ?>
<br/>
			<?php /* tag "table" from line 164 */; ?>
<table style="text-align:left;">
				<?php /* tag "tr" from line 165 */; ?>
<tr>
					<?php /* tag "td" from line 166 */; ?>
<td>
						<?php /* tag "div" from line 167 */; ?>
<div id="followers">
							<?php /* tag "table" from line 168 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 169 */; ?>
<tr>
									<?php /* tag "th" from line 170 */; ?>
<th colspan="2"><?php /* tag "span" from line 170 */; ?>
<span class="tableTitle">Top Followers</span></th>
								</tr>
								<?php /* tag "tr" from line 172 */; ?>
<tr><?php /* tag "td" from line 172 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 173 */; ?>
<tr><?php /* tag "td" from line 173 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 174 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->topFollowers)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 175 */; ?>
<tr>
									<?php /* tag "td" from line 176 */; ?>
<td class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 176 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 177 */; ?>
<td><?php /* tag "div" from line 177 */; ?>
<div class="extras_result"><?php /* tag "p" from line 177 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 177 */; ?>
<span><?php /* tag "tal:block" from line 177 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 177 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 180 */; ?>
<tr><?php /* tag "td" from line 180 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 183 */; ?>
<div id="tenantHits" style="display:none">
							<?php /* tag "table" from line 184 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 185 */; ?>
<tr>
									<?php /* tag "th" from line 186 */; ?>
<th colspan="2"><?php /* tag "span" from line 186 */; ?>
<span class="tableTitle">Tenant Hits</span></th>
								</tr>
								<?php /* tag "tr" from line 188 */; ?>
<tr><?php /* tag "td" from line 188 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 189 */; ?>
<tr><?php /* tag "td" from line 189 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 190 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->totalHitsPerTenant)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 191 */; ?>
<tr>
									<?php /* tag "td" from line 192 */; ?>
<td class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 192 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 193 */; ?>
<td><?php /* tag "div" from line 193 */; ?>
<div class="extras_result"><?php /* tag "p" from line 193 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 193 */; ?>
<span><?php /* tag "tal:block" from line 193 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 193 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 196 */; ?>
<tr><?php /* tag "td" from line 196 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 199 */; ?>
<div id="touchpointHits" style="display:none">
							<?php /* tag "table" from line 200 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 201 */; ?>
<tr>
									<?php /* tag "th" from line 202 */; ?>
<th colspan="2"><?php /* tag "span" from line 202 */; ?>
<span class="tableTitle">Touch Point Hits</span></th>
								</tr>
								<?php /* tag "tr" from line 204 */; ?>
<tr><?php /* tag "td" from line 204 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 205 */; ?>
<tr><?php /* tag "td" from line 205 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 206 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->operatorTotalHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 207 */; ?>
<tr>
									<?php /* tag "td" from line 208 */; ?>
<td class="extras_y-desc" scope="row">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php /* tag "tal:block" from line 208 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 209 */; ?>
<td><?php /* tag "div" from line 209 */; ?>
<div class="extras_result"><?php /* tag "p" from line 209 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 209 */; ?>
<span><?php /* tag "tal:block" from line 209 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 209 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 212 */; ?>
<tr><?php /* tag "td" from line 212 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 215 */; ?>
<div id="feedback" style="display:none">
							<?php /* tag "table" from line 216 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 217 */; ?>
<tr>
									<?php /* tag "th" from line 218 */; ?>
<th><?php /* tag "span" from line 218 */; ?>
<span class="tableTitle" style="text-decoration:underline;"><?php /* tag "a" from line 218 */; ?>
<a href="javascript:;">Severity</a></span></th>
									<?php /* tag "th" from line 219 */; ?>
<th>&nbsp;/&nbsp;</th>
																		<?php /* tag "th" from line 220 */; ?>
<th><?php /* tag "span" from line 220 */; ?>
<span class="tableTitle" style="text-decoration:underline;"><?php /* tag "a" from line 220 */; ?>
<a href="javascript:;">Status</a></span></th>
								</tr>
							</table>
							<?php /* tag "div" from line 223 */; ?>
<div id="severityHits">
								<?php /* tag "table" from line 224 */; ?>
<table>
									<?php /* tag "tr" from line 225 */; ?>
<tr>
										<?php /* tag "td" from line 226 */; ?>
<td></td>
									</tr>
								</table>
							</div>
							<?php /* tag "div" from line 230 */; ?>
<div id="status">
								<?php /* tag "table" from line 231 */; ?>
<table>
									<?php /* tag "tr" from line 232 */; ?>
<tr>
										<?php /* tag "td" from line 233 */; ?>
<td></td>
									</tr>
								</table>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 242 */; ?>
<div id="monthlyReport">
			<?php /* tag "table" from line 243 */; ?>
<table style="width:880px; margin-left:20px;">
				<?php /* tag "tr" from line 244 */; ?>
<tr><?php /* tag "td" from line 244 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 245 */; ?>
<tr><?php /* tag "th" from line 245 */; ?>
<th colspan="12" align="center"><?php /* tag "span" from line 245 */; ?>
<span class="tableTitle">Total Hits By Month</span></th></tr>
				<?php /* tag "tr" from line 246 */; ?>
<tr><?php /* tag "td" from line 246 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 247 */; ?>
<tr>
					<?php 
/* tag "tal:block" from line 248 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->monthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

						<?php /* tag "td" from line 249 */; ?>
<td valign="top">
							<?php /* tag "table" from line 250 */; ?>
<table>
								<?php /* tag "tr" from line 251 */; ?>
<tr>
									<?php /* tag "th" from line 252 */; ?>
<th colspan="2" align="center"><?php /* tag "a" from line 252 */; ?>
<a href="javascript:;" onclick="global.displayTransContainer($(this).html());"><?php /* tag "tal:block" from line 252 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</a></th>
								</tr>
								<?php 
/* tag "tal:block" from line 254 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

									<?php /* tag "tr" from line 255 */; ?>
<tr>
										<?php /* tag "td" from line 256 */; ?>
<td><?php /* tag "tal:block" from line 256 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'total')); ?>
</td>
										<?php /* tag "td" from line 257 */; ?>
<td><?php /* tag "tal:block" from line 257 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
									</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 260 */; ?>
<tr><?php /* tag "td" from line 260 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</td>
					<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>
			
				</tr>
			</table>
		</div>
	</div>
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/dashboard/index.zpt (edit that file instead) */; ?>