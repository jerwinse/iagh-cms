<?php 
function tpl_4fcf4437_index__AwqtNWkphlcs6QoYJNtheA(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<?php /* tag "link" from line 2 */; ?>
<link rel="stylesheet" href="/css/calendar/ui-lightness/jquery-ui-1.8.16.custom.css" media="screen"/>
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" href="/css/dashboard/style.css" media="screen"/>
<?php /* tag "link" from line 4 */; ?>
<link rel="stylesheet" href="/css/dashboard/barchart.css" media="screen"/>
<?php /* tag "link" from line 5 */; ?>
<link rel="stylesheet" href="/css/dashboard/chart.css" media="screen"/>

<?php /* tag "script" from line 7 */; ?>
<script src="/js/calendar/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
<?php /* tag "script" from line 8 */; ?>
<script src="/js/dashboard/global.js" type="text/javascript"></script>
<!--<script src="/js/dashboard/chart.js" type="text/javascript"></script>-->

<?php /* tag "script" from line 11 */; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	  // for each result row..
	  $(".extras_result").each(function() {
	    // get the width of the bar from the span html
	    var length = $(this).find("span").html();
	    length = length.replace("%","");
	    length = length / 100;
	    var lengthPx = 400 * length;
	    
	    $(this).find("p").animate({'width':lengthPx}, 2000, function() {
	      // once the bar animation has finished, fade in the results
	      $(this).find("span").fadeIn(800);
	    });
	  });
	});
</script>


<?php /* tag "script" from line 30 */; ?>
<script>
    $(function() {
        $( "#startDate" ).datepicker();
        $( "#startDate" ).change(function() {
            $( "#startDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });     
	// for extraction form
    $(function() {
        $( "#endDate" ).datepicker();
        $( "#endDate" ).change(function() {
            $( "#endDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });        
</script>		
    
    <?php /* tag "h1" from line 46 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 46 */; ?>
<span style="float:left;">Dashboard</span></h1>
	<?php /* tag "div" from line 47 */; ?>
<div id="folderTab"><?php /* tag "div" from line 47 */; ?>
<div class="folderTabText">Quick View</div></div>
	<?php /* tag "br" from line 48 */; ?>
<br/>
<!--	<div class="dashboardTabMenu">
		<span><a href="admin#/dashboard/index">Home</a></span>
		<span><a class="selected" href="#">Analytics</a></span>
		<span><a href="#">Menu 3</a></span>
		<span><a href="#">Menu 4</a></span>
	</div>
	<div class="dimension">
		Dimensions<br/>
		Start Date: <input type="text" id="startDate" name="startDate"/>&nbsp;&nbsp;&nbsp;
		End Date: <input type="text" id="endDate" name="endDate"/>&nbsp;&nbsp;&nbsp;
		<input type="button" value="Query"/>
	</div>-->
	<?php /* tag "div" from line 61 */; ?>
<div class="reportContainer">
		<?php /* tag "div" from line 62 */; ?>
<div class="tbl">
			<?php 
/* tag "tal:block" from line 63 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->detailMonthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

				<?php 
/* tag "div" from line 64 */ ;
if (null !== ($_tmp_2 = ($ctx->repeat->item->key))):  ;
$_tmp_2 = ' id="'.phptal_escape($_tmp_2).'"' ;
else:  ;
$_tmp_2 = '' ;
endif ;
?>
<div style="display:none;"<?php echo $_tmp_2 ?>
>
					<?php /* tag "table" from line 65 */; ?>
<table style="width:200px; text-align:center;">
						<?php /* tag "tr" from line 66 */; ?>
<tr><?php /* tag "td" from line 66 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 66 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 67 */; ?>
<tr><?php /* tag "td" from line 67 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 67 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 68 */; ?>
<tr><?php /* tag "th" from line 68 */; ?>
<th colspan="2"><?php /* tag "span" from line 68 */; ?>
<span class="tableTitle"><?php /* tag "tal:block" from line 68 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</span></th></tr>
						<?php /* tag "tr" from line 69 */; ?>
<tr><?php /* tag "td" from line 69 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 69 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 70 */; ?>
<tr><?php /* tag "th" from line 70 */; ?>
<th>Name</th><?php /* tag "th" from line 70 */; ?>
<th>Hits</th></tr>
						<?php 
/* tag "tal:block" from line 71 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

								<?php /* tag "tr" from line 72 */; ?>
<tr><?php /* tag "td" from line 72 */; ?>
<td><?php /* tag "tal:block" from line 72 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'name')); ?>
</td><?php /* tag "td" from line 72 */; ?>
<td><?php /* tag "tal:block" from line 72 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td></tr>
						<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

					</table>
				</div>
			<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

			<?php /* tag "div" from line 77 */; ?>
<div id="todayTransaction">
				<?php /* tag "table" from line 78 */; ?>
<table>
					<?php /* tag "tr" from line 79 */; ?>
<tr>
						<?php /* tag "td" from line 80 */; ?>
<td>
							<?php /* tag "table" from line 81 */; ?>
<table style="width:200px;">
								<?php /* tag "tr" from line 82 */; ?>
<tr><?php /* tag "td" from line 82 */; ?>
<td>&nbsp;</td></tr>
								<?php /* tag "tr" from line 83 */; ?>
<tr><?php /* tag "td" from line 83 */; ?>
<td>&nbsp;</td></tr>
								<?php /* tag "tr" from line 84 */; ?>
<tr><?php /* tag "th" from line 84 */; ?>
<th colspan="3"><?php /* tag "span" from line 84 */; ?>
<span class="tableTitle">Today Transaction</span></th></tr>
								<?php /* tag "tr" from line 85 */; ?>
<tr><?php /* tag "td" from line 85 */; ?>
<td colspan="3" align="center"><?php /* tag "span" from line 85 */; ?>
<span style="font-size:9px; font-style:Italic; font-weight:normal;"><?php /* tag "tal:block" from line 85 */; ?>
<?php echo phptal_escape($ctx->todayDate); ?>
</span></td></tr>
								<?php /* tag "tr" from line 86 */; ?>
<tr><?php /* tag "td" from line 86 */; ?>
<td>&nbsp;</td></tr>
								<?php /* tag "tr" from line 87 */; ?>
<tr>
									<?php /* tag "td" from line 88 */; ?>
<td>&nbsp;</td>
									<?php /* tag "td" from line 89 */; ?>
<td>
										<?php /* tag "table" from line 90 */; ?>
<table style="width:75px; text-align:center;">
											<?php /* tag "tr" from line 91 */; ?>
<tr>
												<?php /* tag "th" from line 92 */; ?>
<th>Hour</th><?php /* tag "th" from line 92 */; ?>
<th>Hits</th>
											</tr>
											<?php 
/* tag "tal:block" from line 94 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

												<?php 
/* tag "tal:block" from line 95 */ ;
if ($ctx->repeat->item->index <= 11):  ;
?>

												<?php /* tag "tr" from line 96 */; ?>
<tr><?php /* tag "td" from line 96 */; ?>
<td><?php /* tag "tal:block" from line 96 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 96 */; ?>
<td><?php /* tag "tal:block" from line 96 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
												<?php endif; ?>

											<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

										</table>
									</td>
									<?php /* tag "td" from line 101 */; ?>
<td valign="top">
										<?php /* tag "table" from line 102 */; ?>
<table style="width:75px;text-align:center;">
											<?php /* tag "th" from line 103 */; ?>
<th>Hour</th><?php /* tag "th" from line 103 */; ?>
<th>Hits</th>
											<?php 
/* tag "tal:block" from line 104 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

												<?php 
/* tag "tal:block" from line 105 */ ;
if ($ctx->repeat->item->index >= 12):  ;
?>

												<?php /* tag "tr" from line 106 */; ?>
<tr><?php /* tag "td" from line 106 */; ?>
<td><?php /* tag "tal:block" from line 106 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 106 */; ?>
<td><?php /* tag "tal:block" from line 106 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
												<?php endif; ?>

											<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<?php /* tag "div" from line 118 */; ?>
<div id="bargraph">
		<?php /* tag "br" from line 119 */; ?>
<br/>
			<?php /* tag "table" from line 120 */; ?>
<table style="text-align:left;">
				<?php /* tag "tr" from line 121 */; ?>
<tr>
					<?php /* tag "td" from line 122 */; ?>
<td>
						<?php /* tag "table" from line 123 */; ?>
<table class="extras_table" align="center">
							<?php /* tag "tr" from line 124 */; ?>
<tr>
								<?php /* tag "th" from line 125 */; ?>
<th colspan="2"><?php /* tag "span" from line 125 */; ?>
<span class="tableTitle">Top Followers</span></th>
							</tr>
							<?php /* tag "tr" from line 127 */; ?>
<tr><?php /* tag "td" from line 127 */; ?>
<td></td></tr>
						
							<?php 
/* tag "tal:block" from line 129 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->topFollowers)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

							<?php /* tag "tr" from line 130 */; ?>
<tr>
								<?php /* tag "th" from line 131 */; ?>
<th class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 131 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</th>
								<?php /* tag "td" from line 132 */; ?>
<td><?php /* tag "div" from line 132 */; ?>
<div class="extras_result"><?php /* tag "p" from line 132 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 132 */; ?>
<span><?php /* tag "tal:block" from line 132 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 132 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
							</tr>
							<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

							<?php /* tag "tr" from line 135 */; ?>
<tr><?php /* tag "td" from line 135 */; ?>
<td>&nbsp;</td></tr>
						</table>
						<?php /* tag "table" from line 137 */; ?>
<table class="extras_table" align="center">
							<?php /* tag "tr" from line 138 */; ?>
<tr>
								<?php /* tag "th" from line 139 */; ?>
<th colspan="2"><?php /* tag "span" from line 139 */; ?>
<span class="tableTitle">Tenant Total Hits</span></th>
							</tr>
							<?php 
/* tag "tal:block" from line 141 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->totalHitsPerTenant)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

							<?php /* tag "tr" from line 142 */; ?>
<tr>
								<?php /* tag "th" from line 143 */; ?>
<th class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 143 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</th>
								<?php /* tag "td" from line 144 */; ?>
<td><?php /* tag "div" from line 144 */; ?>
<div class="extras_result"><?php /* tag "p" from line 144 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 144 */; ?>
<span><?php /* tag "tal:block" from line 144 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 144 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
							</tr>
							<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

							<?php /* tag "tr" from line 147 */; ?>
<tr><?php /* tag "td" from line 147 */; ?>
<td>&nbsp;</td></tr>
						</table>
						<?php /* tag "table" from line 149 */; ?>
<table class="extras_table" align="center">
							<?php /* tag "tr" from line 150 */; ?>
<tr>
								<?php /* tag "th" from line 151 */; ?>
<th colspan="2"><?php /* tag "span" from line 151 */; ?>
<span class="tableTitle">Touch Point Total Hits</span></th>
							</tr>
							<?php 
/* tag "tal:block" from line 153 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->operatorTotalHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

							<?php /* tag "tr" from line 154 */; ?>
<tr>
								<?php /* tag "th" from line 155 */; ?>
<th class="extras_y-desc" scope="row">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php /* tag "tal:block" from line 155 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</th>
								<?php /* tag "td" from line 156 */; ?>
<td><?php /* tag "div" from line 156 */; ?>
<div class="extras_result"><?php /* tag "p" from line 156 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 156 */; ?>
<span><?php /* tag "tal:block" from line 156 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 156 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
							</tr>
							<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

							<?php /* tag "tr" from line 159 */; ?>
<tr><?php /* tag "td" from line 159 */; ?>
<td>&nbsp;</td></tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 165 */; ?>
<div id="monthlyReport">
			<?php /* tag "table" from line 166 */; ?>
<table style="width:880px; margin-left:20px;">
				<?php /* tag "tr" from line 167 */; ?>
<tr><?php /* tag "td" from line 167 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 168 */; ?>
<tr><?php /* tag "th" from line 168 */; ?>
<th colspan="12" align="center"><?php /* tag "span" from line 168 */; ?>
<span class="tableTitle">Total Hits By Month</span></th></tr>
				<?php /* tag "tr" from line 169 */; ?>
<tr><?php /* tag "td" from line 169 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 170 */; ?>
<tr>
					<?php 
/* tag "tal:block" from line 171 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->monthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

						<?php /* tag "td" from line 172 */; ?>
<td valign="top">
							<?php /* tag "table" from line 173 */; ?>
<table>
								<?php /* tag "tr" from line 174 */; ?>
<tr>
									<?php /* tag "th" from line 175 */; ?>
<th colspan="2" align="center"><?php /* tag "a" from line 175 */; ?>
<a href="javascript:;" onclick="global.displayTransContainer($(this).html());"><?php /* tag "tal:block" from line 175 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</a></th>
								</tr>
								<?php 
/* tag "tal:block" from line 177 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

									<?php /* tag "tr" from line 178 */; ?>
<tr>
										<?php /* tag "td" from line 179 */; ?>
<td><?php /* tag "tal:block" from line 179 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'total')); ?>
</td>
										<?php /* tag "td" from line 180 */; ?>
<td><?php /* tag "tal:block" from line 180 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
									</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 183 */; ?>
<tr><?php /* tag "td" from line 183 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</td>
					<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>
			
				</tr>
			</table>
		</div>
	</div>
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/dashboard/index.zpt (edit that file instead) */; ?>