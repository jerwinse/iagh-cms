<?php 
function tpl_4fcf390b_index__AwqtNWkphlcs6QoYJNtheA(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<?php /* tag "link" from line 2 */; ?>
<link rel="stylesheet" href="/css/calendar/ui-lightness/jquery-ui-1.8.16.custom.css" media="screen"/>
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" href="/css/dashboard/style.css" media="screen"/>
<?php /* tag "link" from line 4 */; ?>
<link rel="stylesheet" href="/css/dashboard/barchart.css" media="screen"/>
<?php /* tag "link" from line 5 */; ?>
<link rel="stylesheet" href="/css/dashboard/chart.css" media="screen"/>

<?php /* tag "script" from line 7 */; ?>
<script src="/js/calendar/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
<!--<script src="/js/dashboard/chart.js" type="text/javascript"></script>-->

<?php /* tag "script" from line 10 */; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	  // for each result row..
	  $(".extras_result").each(function() {
	    // get the width of the bar from the span html
	    var length = $(this).find("span").html();
	    length = length.replace("%","");
	    length = length / 100;
	    var lengthPx = 400 * length;
	    
	    $(this).find("p").animate({'width':lengthPx}, 2000, function() {
	      // once the bar animation has finished, fade in the results
	      $(this).find("span").fadeIn(800);
	    });
	  });
	});
</script>


<?php /* tag "script" from line 29 */; ?>
<script>
    $(function() {
        $( "#startDate" ).datepicker();
        $( "#startDate" ).change(function() {
            $( "#startDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });     
	// for extraction form
    $(function() {
        $( "#endDate" ).datepicker();
        $( "#endDate" ).change(function() {
            $( "#endDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });        
</script>		

					<?php /* tag "script" from line 45 */; ?>
<script>
					function test(this){
						alert($(this).html());
					}
					</script>	
    
    <?php /* tag "h1" from line 51 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 51 */; ?>
<span style="float:left;">Dashboard</span></h1>
	<?php /* tag "div" from line 52 */; ?>
<div id="folderTab"><?php /* tag "div" from line 52 */; ?>
<div class="folderTabText">Quick View</div></div>
	<?php /* tag "br" from line 53 */; ?>
<br/>
<!--	<div class="dashboardTabMenu">
		<span><a href="admin#/dashboard/index">Home</a></span>
		<span><a class="selected" href="#">Analytics</a></span>
		<span><a href="#">Menu 3</a></span>
		<span><a href="#">Menu 4</a></span>
	</div>
	<div class="dimension">
		Dimensions<br/>
		Start Date: <input type="text" id="startDate" name="startDate"/>&nbsp;&nbsp;&nbsp;
		End Date: <input type="text" id="endDate" name="endDate"/>&nbsp;&nbsp;&nbsp;
		<input type="button" value="Query"/>
	</div>-->
	<?php /* tag "div" from line 66 */; ?>
<div class="reportContainer">
		<?php /* tag "div" from line 67 */; ?>
<div class="tbl">
			<?php /* tag "table" from line 68 */; ?>
<table>
				<?php /* tag "tr" from line 69 */; ?>
<tr>
					<?php /* tag "td" from line 70 */; ?>
<td>
						<?php /* tag "table" from line 71 */; ?>
<table style="width:200px;">
							<?php /* tag "tr" from line 72 */; ?>
<tr><?php /* tag "td" from line 72 */; ?>
<td>&nbsp;</td></tr>
							<?php /* tag "tr" from line 73 */; ?>
<tr><?php /* tag "td" from line 73 */; ?>
<td>&nbsp;</td></tr>
							<?php /* tag "tr" from line 74 */; ?>
<tr><?php /* tag "th" from line 74 */; ?>
<th colspan="3"><?php /* tag "span" from line 74 */; ?>
<span class="tableTitle">Today Transaction</span></th></tr>
							<?php /* tag "tr" from line 75 */; ?>
<tr><?php /* tag "td" from line 75 */; ?>
<td colspan="3" align="center"><?php /* tag "span" from line 75 */; ?>
<span style="font-size:9px; font-style:Italic; font-weight:normal;"><?php /* tag "tal:block" from line 75 */; ?>
<?php echo phptal_escape($ctx->todayDate); ?>
</span></td></tr>
							<?php /* tag "tr" from line 76 */; ?>
<tr><?php /* tag "td" from line 76 */; ?>
<td>&nbsp;</td></tr>
							<?php /* tag "tr" from line 77 */; ?>
<tr>
								<?php /* tag "td" from line 78 */; ?>
<td>&nbsp;</td>
								<?php /* tag "td" from line 79 */; ?>
<td>
									<?php /* tag "table" from line 80 */; ?>
<table style="width:75px; text-align:center;">
										<?php /* tag "tr" from line 81 */; ?>
<tr>
											<?php /* tag "th" from line 82 */; ?>
<th>Hour</th><?php /* tag "th" from line 82 */; ?>
<th>Hits</th>
										</tr>
										<?php 
/* tag "tal:block" from line 84 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

											<?php 
/* tag "tal:block" from line 85 */ ;
if ($ctx->repeat->item->index <= 11):  ;
?>

											<?php /* tag "tr" from line 86 */; ?>
<tr><?php /* tag "td" from line 86 */; ?>
<td><?php /* tag "tal:block" from line 86 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 86 */; ?>
<td><?php /* tag "tal:block" from line 86 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
											<?php endif; ?>

										<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

									</table>
								</td>
								<?php /* tag "td" from line 91 */; ?>
<td valign="top">
									<?php /* tag "table" from line 92 */; ?>
<table style="width:75px;text-align:center;">
										<?php /* tag "th" from line 93 */; ?>
<th>Hour</th><?php /* tag "th" from line 93 */; ?>
<th>Hits</th>
										<?php 
/* tag "tal:block" from line 94 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

											<?php 
/* tag "tal:block" from line 95 */ ;
if ($ctx->repeat->item->index >= 12):  ;
?>

											<?php /* tag "tr" from line 96 */; ?>
<tr><?php /* tag "td" from line 96 */; ?>
<td><?php /* tag "tal:block" from line 96 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 96 */; ?>
<td><?php /* tag "tal:block" from line 96 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
											<?php endif; ?>

										<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 107 */; ?>
<div id="bargraph">
		<?php /* tag "br" from line 108 */; ?>
<br/>
			<?php /* tag "table" from line 109 */; ?>
<table style="text-align:left;">
				<?php /* tag "tr" from line 110 */; ?>
<tr>
					<?php /* tag "td" from line 111 */; ?>
<td>
						<?php /* tag "table" from line 112 */; ?>
<table class="extras_table" align="center">
							<?php /* tag "tr" from line 113 */; ?>
<tr>
								<?php /* tag "th" from line 114 */; ?>
<th colspan="2"><?php /* tag "span" from line 114 */; ?>
<span class="tableTitle">Top Followers</span></th>
							</tr>
							<?php /* tag "tr" from line 116 */; ?>
<tr><?php /* tag "td" from line 116 */; ?>
<td></td></tr>
						
							<?php 
/* tag "tal:block" from line 118 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->topFollowers)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

							<?php /* tag "tr" from line 119 */; ?>
<tr>
								<?php /* tag "th" from line 120 */; ?>
<th class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 120 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</th>
								<?php /* tag "td" from line 121 */; ?>
<td><?php /* tag "div" from line 121 */; ?>
<div class="extras_result"><?php /* tag "p" from line 121 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 121 */; ?>
<span><?php /* tag "tal:block" from line 121 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 121 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
							</tr>
							<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

							<?php /* tag "tr" from line 124 */; ?>
<tr><?php /* tag "td" from line 124 */; ?>
<td>&nbsp;</td></tr>
						</table>
						<?php /* tag "table" from line 126 */; ?>
<table class="extras_table" align="center">
							<?php /* tag "tr" from line 127 */; ?>
<tr>
								<?php /* tag "th" from line 128 */; ?>
<th colspan="2"><?php /* tag "span" from line 128 */; ?>
<span class="tableTitle">Tenant Total Hits</span></th>
							</tr>
							<?php 
/* tag "tal:block" from line 130 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->totalHitsPerTenant)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

							<?php /* tag "tr" from line 131 */; ?>
<tr>
								<?php /* tag "th" from line 132 */; ?>
<th class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 132 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</th>
								<?php /* tag "td" from line 133 */; ?>
<td><?php /* tag "div" from line 133 */; ?>
<div class="extras_result"><?php /* tag "p" from line 133 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 133 */; ?>
<span><?php /* tag "tal:block" from line 133 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 133 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
							</tr>
							<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

							<?php /* tag "tr" from line 136 */; ?>
<tr><?php /* tag "td" from line 136 */; ?>
<td>&nbsp;</td></tr>
						</table>
						<?php /* tag "table" from line 138 */; ?>
<table class="extras_table" align="center">
							<?php /* tag "tr" from line 139 */; ?>
<tr>
								<?php /* tag "th" from line 140 */; ?>
<th colspan="2"><?php /* tag "span" from line 140 */; ?>
<span class="tableTitle">Touch Point Total Hits</span></th>
							</tr>
							<?php 
/* tag "tal:block" from line 142 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->operatorTotalHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

							<?php /* tag "tr" from line 143 */; ?>
<tr>
								<?php /* tag "th" from line 144 */; ?>
<th class="extras_y-desc" scope="row">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php /* tag "tal:block" from line 144 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</th>
								<?php /* tag "td" from line 145 */; ?>
<td><?php /* tag "div" from line 145 */; ?>
<div class="extras_result"><?php /* tag "p" from line 145 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 145 */; ?>
<span><?php /* tag "tal:block" from line 145 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 145 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
							</tr>
							<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

							<?php /* tag "tr" from line 148 */; ?>
<tr><?php /* tag "td" from line 148 */; ?>
<td>&nbsp;</td></tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 154 */; ?>
<div id="monthlyReport">
			<?php /* tag "table" from line 155 */; ?>
<table style="width:880px; margin-left:20px;">
				<?php /* tag "tr" from line 156 */; ?>
<tr><?php /* tag "td" from line 156 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 157 */; ?>
<tr><?php /* tag "th" from line 157 */; ?>
<th colspan="12" align="center"><?php /* tag "span" from line 157 */; ?>
<span class="tableTitle">Total Hits By Month</span></th></tr>
				<?php /* tag "tr" from line 158 */; ?>
<tr><?php /* tag "td" from line 158 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 159 */; ?>
<tr>
					<?php 
/* tag "tal:block" from line 160 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->monthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

						<?php /* tag "td" from line 161 */; ?>
<td valign="top">
							<?php /* tag "table" from line 162 */; ?>
<table>
								<?php /* tag "tr" from line 163 */; ?>
<tr>
									<?php /* tag "th" from line 164 */; ?>
<th colspan="2" align="center"><?php /* tag "a" from line 164 */; ?>
<a href="javascript:;" onclick="test(this);"><?php /* tag "tal:block" from line 164 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</a></th>
								</tr>
								<?php 
/* tag "tal:block" from line 166 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->h as $ctx->h): ;
?>

									<?php /* tag "tr" from line 167 */; ?>
<tr>
										<?php /* tag "td" from line 168 */; ?>
<td><?php /* tag "tal:block" from line 168 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'total')); ?>
</td>
										<?php /* tag "td" from line 169 */; ?>
<td><?php /* tag "tal:block" from line 169 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
									</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 172 */; ?>
<tr><?php /* tag "td" from line 172 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</td>
					<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>
			
				</tr>
			</table>
		</div>
	</div>
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/dashboard/index.zpt (edit that file instead) */; ?>