<?php 
function tpl_4fd1a22c_index__AwqtNWkphlcs6QoYJNtheA(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<?php /* tag "link" from line 2 */; ?>
<link rel="stylesheet" href="/css/calendar/ui-lightness/jquery-ui-1.8.16.custom.css" media="screen"/>
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" href="/css/dashboard/style.css" media="screen"/>
<?php /* tag "link" from line 4 */; ?>
<link rel="stylesheet" href="/css/dashboard/barchart.css" media="screen"/>
<?php /* tag "link" from line 5 */; ?>
<link rel="stylesheet" href="/css/dashboard/chart.css" media="screen"/>

<?php /* tag "script" from line 7 */; ?>
<script src="/js/calendar/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
<?php /* tag "script" from line 8 */; ?>
<script src="/js/calendar/jquery.js" type="text/javascript"></script>
<?php /* tag "script" from line 9 */; ?>
<script src="/js/dashboard/jqBarGraph.1.1.js" type="text/javascript"></script>
<?php /* tag "script" from line 10 */; ?>
<script src="/js/dashboard/global.js" type="text/javascript"></script>
<!--<script src="/js/dashboard/chart.js" type="text/javascript"></script>-->

<?php /* tag "script" from line 13 */; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	  // for each result row..
	  $(".extras_result").each(function() {
	    // get the width of the bar from the span html
	    var length = $(this).find("span").html();
	    length = length.replace("%","");
	    length = length / 100;
	    var lengthPx = 400 * length;
	    
	    $(this).find("p").animate({'width':lengthPx}, 2000, function() {
	      // once the bar animation has finished, fade in the results
	      $(this).find("span").fadeIn(800);
	    });
	  });
	});
</script>


<?php /* tag "script" from line 32 */; ?>
<script>
    $(function() {
        $( "#startDate" ).datepicker();
        $( "#startDate" ).change(function() {
            $( "#startDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });     
	// for extraction form
    $(function() {
        $( "#endDate" ).datepicker();
        $( "#endDate" ).change(function() {
            $( "#endDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });        
</script>

<?php /* tag "script" from line 48 */; ?>
<script>
	graphByMonth = new Array(
	    [[14,54,26],'2007'],
	    [[8,48,38],'2008'],
	    [[4,36,57],'2009']
	); 

//	$('#severityHits').jqBarGraph({ data: arrayOfData }); 
	
	$("#severityHits").jqBarGraph({
		data: graphByMonth,
		width: 500,
		colors: ['#122A47','#1B3E69'],
		color: '#1A2944',
		barSpace: 5,
		title: '<?php /* tag "h3" from line 63 */; ?>
<h3>Number of visitors per month<?php /* tag "br" from line 63 */; ?>
<br/><?php /* tag "small" from line 63 */; ?>
<small>simple bar graph</small></h3>'
	});
	
	
//	data: arrayOfData, // array of data for your graph
//	title: false, // title of your graph, accept HTML
//	barSpace: 10, // this is default space between bars in pixels
//	width: 400, // default width of your graph
//	height: 200, //default height of your graph
//	color: '#000000', // if you don't send colors for your data this will be default bars color
//	colors: false, // array of colors that will be used for your bars and legends
//	lbl: '', // if there is no label in your array
//	sort: false, // sort your data before displaying graph, you can sort as 'asc' or 'desc'
//	position: 'bottom', // position of your bars, can be 'bottom' or 'top'. 'top' doesn't work for multi type
//	prefix: '', // text that will be shown before every label
//	postfix: '', // text that will be shown after every label
//	animate: true, // if you don't need animated appearance change to false
//	speed: 2, // speed of animation in seconds
//	legendWidth: 100, // width of your legend box
//	legend: false, // if you want legend change to true
//	legends: false, // array for legend. for simple graph type legend will be extracted from labels if you don't set this
//	type: false, // for multi array data default graph type is stacked, you can change to 'multi' for multi bar type
//	showValues: true, // you can use this for multi and stacked type and it will show values of every bar part
//	showValuesColor: '#fff' // color of font for values
	
</script>
    
    <?php /* tag "h1" from line 90 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 90 */; ?>
<span style="float:left;">Dashboard</span></h1>
	<?php /* tag "div" from line 91 */; ?>
<div id="folderTab"><?php /* tag "div" from line 91 */; ?>
<div class="folderTabText">Quick View</div></div>
	<?php /* tag "br" from line 92 */; ?>
<br/>
	<?php /* tag "div" from line 93 */; ?>
<div class="dashboardTabMenu">
		<?php /* tag "span" from line 94 */; ?>
<span id="followersLink"><?php /* tag "a" from line 94 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('followers')">Top Follower</a></span>
		<?php /* tag "span" from line 95 */; ?>
<span id="tenantHitsLink"><?php /* tag "a" from line 95 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('tenantHits')">Tenant Hits</a></span>
		<?php /* tag "span" from line 96 */; ?>
<span id="touchpointHitsLink"><?php /* tag "a" from line 96 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('touchpointHits')">Touchpoint Hits</a></span>
		<?php /* tag "span" from line 97 */; ?>
<span id="feedbackLink"><?php /* tag "a" from line 97 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('feedback')">Feedback</a></span>
	</div>
<!--	<div class="dimension">
		Dimensions<br/>
		Start Date: <input type="text" id="startDate" name="startDate"/>&nbsp;&nbsp;&nbsp;
		End Date: <input type="text" id="endDate" name="endDate"/>&nbsp;&nbsp;&nbsp;
		<input type="button" value="Query"/>
	</div>-->
	<?php /* tag "div" from line 105 */; ?>
<div class="reportContainer">
		<?php /* tag "div" from line 106 */; ?>
<div class="tbl">
			<?php 
/* tag "tal:block" from line 107 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->detailMonthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

				<?php 
/* tag "div" from line 108 */ ;
if (null !== ($_tmp_2 = ($ctx->repeat->item->key))):  ;
$_tmp_2 = ' id="'.phptal_escape($_tmp_2).'"' ;
else:  ;
$_tmp_2 = '' ;
endif ;
?>
<div style="display:none;"<?php echo $_tmp_2 ?>
>
					<?php /* tag "table" from line 109 */; ?>
<table style="width:200px; text-align:center;">
						<?php /* tag "tr" from line 110 */; ?>
<tr><?php /* tag "td" from line 110 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 110 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 111 */; ?>
<tr><?php /* tag "td" from line 111 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 111 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 112 */; ?>
<tr><?php /* tag "th" from line 112 */; ?>
<th colspan="3" align="center"><?php /* tag "span" from line 112 */; ?>
<span class="tableTitle" style="font-size:14px;"><?php /* tag "tal:block" from line 112 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</span></th></tr>
						<?php /* tag "tr" from line 113 */; ?>
<tr><?php /* tag "td" from line 113 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 113 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 114 */; ?>
<tr><?php /* tag "th" from line 114 */; ?>
<th>Name</th><?php /* tag "th" from line 114 */; ?>
<th>Hits</th></tr>
						<?php 
/* tag "tal:block" from line 115 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

							<?php /* tag "tr" from line 116 */; ?>
<tr>
								<?php /* tag "td" from line 117 */; ?>
<td width="50%"><?php /* tag "tal:block" from line 117 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'name')); ?>
</td>
								<?php /* tag "td" from line 118 */; ?>
<td width="50%"><?php /* tag "tal:block" from line 118 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
							</tr>
						<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

					</table>
				</div>
			<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

			<?php /* tag "div" from line 124 */; ?>
<div id="todayTransaction">
				<?php /* tag "table" from line 125 */; ?>
<table>
					<?php /* tag "tr" from line 126 */; ?>
<tr>
						<?php /* tag "td" from line 127 */; ?>
<td>
							<?php /* tag "table" from line 128 */; ?>
<table style="width:200px;">
								<?php /* tag "tr" from line 129 */; ?>
<tr><?php /* tag "th" from line 129 */; ?>
<th colspan="3"><?php /* tag "span" from line 129 */; ?>
<span class="tableTitle">Today Transaction</span></th></tr>
								<?php /* tag "tr" from line 130 */; ?>
<tr><?php /* tag "td" from line 130 */; ?>
<td colspan="3" align="center"><?php /* tag "span" from line 130 */; ?>
<span style="font-size:9px; font-style:Italic; font-weight:normal;"><?php /* tag "tal:block" from line 130 */; ?>
<?php echo phptal_escape($ctx->todayDate); ?>
</span></td></tr>
								<?php /* tag "tr" from line 131 */; ?>
<tr><?php /* tag "td" from line 131 */; ?>
<td>&nbsp;</td></tr>
								<?php /* tag "tr" from line 132 */; ?>
<tr>
									<?php /* tag "td" from line 133 */; ?>
<td>&nbsp;</td>
									<?php /* tag "td" from line 134 */; ?>
<td>
										<?php /* tag "table" from line 135 */; ?>
<table style="width:75px; text-align:center;">
											<?php /* tag "tr" from line 136 */; ?>
<tr>
												<?php /* tag "th" from line 137 */; ?>
<th>Hour</th><?php /* tag "th" from line 137 */; ?>
<th>Hits</th>
											</tr>
											<?php 
/* tag "tal:block" from line 139 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

												<?php 
/* tag "tal:block" from line 140 */ ;
if ($ctx->repeat->item->index <= 11):  ;
?>

												<?php /* tag "tr" from line 141 */; ?>
<tr><?php /* tag "td" from line 141 */; ?>
<td><?php /* tag "tal:block" from line 141 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 141 */; ?>
<td><?php /* tag "tal:block" from line 141 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
												<?php endif; ?>

											<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

										</table>
									</td>
									<?php /* tag "td" from line 146 */; ?>
<td valign="top">
										<?php /* tag "table" from line 147 */; ?>
<table style="width:75px;text-align:center;">
											<?php /* tag "th" from line 148 */; ?>
<th>Hour</th><?php /* tag "th" from line 148 */; ?>
<th>Hits</th>
											<?php 
/* tag "tal:block" from line 149 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

												<?php 
/* tag "tal:block" from line 150 */ ;
if ($ctx->repeat->item->index >= 12):  ;
?>

												<?php /* tag "tr" from line 151 */; ?>
<tr><?php /* tag "td" from line 151 */; ?>
<td><?php /* tag "tal:block" from line 151 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 151 */; ?>
<td><?php /* tag "tal:block" from line 151 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
												<?php endif; ?>

											<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<?php /* tag "div" from line 163 */; ?>
<div id="bargraph">
		<?php /* tag "br" from line 164 */; ?>
<br/><?php /* tag "br" from line 164 */; ?>
<br/>
			<?php /* tag "table" from line 165 */; ?>
<table style="text-align:left;">
				<?php /* tag "tr" from line 166 */; ?>
<tr>
					<?php /* tag "td" from line 167 */; ?>
<td>
						<?php /* tag "div" from line 168 */; ?>
<div id="followers">
							<?php /* tag "table" from line 169 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 170 */; ?>
<tr>
									<?php /* tag "th" from line 171 */; ?>
<th colspan="2"><?php /* tag "span" from line 171 */; ?>
<span class="tableTitle">Top Followers</span></th>
								</tr>
								<?php /* tag "tr" from line 173 */; ?>
<tr><?php /* tag "td" from line 173 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 174 */; ?>
<tr><?php /* tag "td" from line 174 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 175 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->topFollowers)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 176 */; ?>
<tr>
									<?php /* tag "td" from line 177 */; ?>
<td class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 177 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 178 */; ?>
<td><?php /* tag "div" from line 178 */; ?>
<div class="extras_result"><?php /* tag "p" from line 178 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 178 */; ?>
<span><?php /* tag "tal:block" from line 178 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 178 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 181 */; ?>
<tr><?php /* tag "td" from line 181 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 184 */; ?>
<div id="tenantHits" style="display:none">
							<?php /* tag "table" from line 185 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 186 */; ?>
<tr>
									<?php /* tag "th" from line 187 */; ?>
<th colspan="2"><?php /* tag "span" from line 187 */; ?>
<span class="tableTitle">Tenant Hits</span></th>
								</tr>
								<?php /* tag "tr" from line 189 */; ?>
<tr><?php /* tag "td" from line 189 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 190 */; ?>
<tr><?php /* tag "td" from line 190 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 191 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->totalHitsPerTenant)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 192 */; ?>
<tr>
									<?php /* tag "td" from line 193 */; ?>
<td class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 193 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 194 */; ?>
<td><?php /* tag "div" from line 194 */; ?>
<div class="extras_result"><?php /* tag "p" from line 194 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 194 */; ?>
<span><?php /* tag "tal:block" from line 194 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 194 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 197 */; ?>
<tr><?php /* tag "td" from line 197 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 200 */; ?>
<div id="touchpointHits" style="display:none">
							<?php /* tag "table" from line 201 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 202 */; ?>
<tr>
									<?php /* tag "th" from line 203 */; ?>
<th colspan="2"><?php /* tag "span" from line 203 */; ?>
<span class="tableTitle">Touch Point Hits</span></th>
								</tr>
								<?php /* tag "tr" from line 205 */; ?>
<tr><?php /* tag "td" from line 205 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 206 */; ?>
<tr><?php /* tag "td" from line 206 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 207 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->operatorTotalHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 208 */; ?>
<tr>
									<?php /* tag "td" from line 209 */; ?>
<td class="extras_y-desc" scope="row">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php /* tag "tal:block" from line 209 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 210 */; ?>
<td><?php /* tag "div" from line 210 */; ?>
<div class="extras_result"><?php /* tag "p" from line 210 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 210 */; ?>
<span><?php /* tag "tal:block" from line 210 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 210 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 213 */; ?>
<tr><?php /* tag "td" from line 213 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 216 */; ?>
<div id="feedback" style="display:none">
							<?php /* tag "table" from line 217 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 218 */; ?>
<tr>
									<?php /* tag "th" from line 219 */; ?>
<th><?php /* tag "span" from line 219 */; ?>
<span class="tableTitle" style="text-decoration:underline;"><?php /* tag "a" from line 219 */; ?>
<a href="javascript:;">Severity</a></span></th>
									<?php /* tag "th" from line 220 */; ?>
<th>&nbsp;/&nbsp;</th>
																		<?php /* tag "th" from line 221 */; ?>
<th><?php /* tag "span" from line 221 */; ?>
<span class="tableTitle" style="text-decoration:underline;"><?php /* tag "a" from line 221 */; ?>
<a href="javascript:;">Status</a></span></th>
								</tr>
							</table>
							<?php /* tag "div" from line 224 */; ?>
<div id="severityHits">
								<?php /* tag "table" from line 225 */; ?>
<table>
									<?php /* tag "tr" from line 226 */; ?>
<tr>
										<?php /* tag "td" from line 227 */; ?>
<td></td>
									</tr>
								</table>
							</div>
							<?php /* tag "div" from line 231 */; ?>
<div id="status">
								<?php /* tag "table" from line 232 */; ?>
<table>
									<?php /* tag "tr" from line 233 */; ?>
<tr>
										<?php /* tag "td" from line 234 */; ?>
<td></td>
									</tr>
								</table>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 243 */; ?>
<div id="monthlyReport">
			<?php /* tag "table" from line 244 */; ?>
<table style="width:880px; margin-left:20px;">
				<?php /* tag "tr" from line 245 */; ?>
<tr><?php /* tag "td" from line 245 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 246 */; ?>
<tr><?php /* tag "th" from line 246 */; ?>
<th colspan="12" align="center"><?php /* tag "span" from line 246 */; ?>
<span class="tableTitle">Total Hits By Month</span></th></tr>
				<?php /* tag "tr" from line 247 */; ?>
<tr><?php /* tag "td" from line 247 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 248 */; ?>
<tr>
					<?php 
/* tag "tal:block" from line 249 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->monthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

						<?php /* tag "td" from line 250 */; ?>
<td valign="top">
							<?php /* tag "table" from line 251 */; ?>
<table>
								<?php /* tag "tr" from line 252 */; ?>
<tr>
									<?php /* tag "th" from line 253 */; ?>
<th colspan="2" align="center"><?php /* tag "a" from line 253 */; ?>
<a href="javascript:;" onclick="global.displayTransContainer($(this).html());"><?php /* tag "tal:block" from line 253 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</a></th>
								</tr>
								<?php 
/* tag "tal:block" from line 255 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

									<?php /* tag "tr" from line 256 */; ?>
<tr>
										<?php /* tag "td" from line 257 */; ?>
<td><?php /* tag "tal:block" from line 257 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'total')); ?>
</td>
										<?php /* tag "td" from line 258 */; ?>
<td><?php /* tag "tal:block" from line 258 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
									</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 261 */; ?>
<tr><?php /* tag "td" from line 261 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</td>
					<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>
			
				</tr>
			</table>
		</div>
	</div>
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/dashboard/index.zpt (edit that file instead) */; ?>