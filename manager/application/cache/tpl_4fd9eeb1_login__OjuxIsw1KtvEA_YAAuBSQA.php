<?php 
function tpl_4fd9eeb1_login__OjuxIsw1KtvEA_YAAuBSQA(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<!-- DATATABLES CSS -->
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" media="screen" href="/lib/datatables/css/vpad.css"/>
<?php /* tag "script" from line 4 */; ?>
<script type="text/javascript" src="/lib/datatables/js/jquery.dataTables.js"></script> 
<?php /* tag "script" from line 5 */; ?>
<script type="text/javascript"> 
    $(document).ready(function() {
        $('#videos').dataTable( {
            "sPaginationType": "full_numbers"
        } );
    } );
</script> 
<!-- DATATABLES CSS END -->

                <?php /* tag "h1" from line 14 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 14 */; ?>
<span style="float:left;">Videos</span></h1>
                <?php /* tag "div" from line 15 */; ?>
<div class="container_12 clearfix leading">
	                <?php /* tag "span" from line 16 */; ?>
<span>
	                	<?php /* tag "a" from line 17 */; ?>
<a href="#/core/addVideo/?page=login" title="Create A Tenant" class="button icon-with-text"><?php /* tag "img" from line 17 */; ?>
<img src="/images/navicons-small/10.png" alt=""/>Add Video</a>&nbsp;
	                	<?php /* tag "a" from line 18 */; ?>
<a href="#" onclick="core.publishVideo()" title="Publish Selected" class="button icon-with-text"><?php /* tag "img" from line 18 */; ?>
<img src="/images/navicons-small/115.png" alt=""/>Publish Selected</a>
	                	<?php /* tag "a" from line 19 */; ?>
<a href="#" onclick="core.deleteVideo()" title="Delete Selected" class="button icon-with-text"><?php /* tag "img" from line 19 */; ?>
<img src="/images/navicons-small/135.png" alt=""/>Delete Selected</a>
	                </span>
                	<?php /* tag "br" from line 21 */; ?>
<br/><?php /* tag "br" from line 21 */; ?>
<br/>
                	<?php /* tag "a" from line 22 */; ?>
<a href="javascript:;" class="confirm" id="alert">Normal test</a>
                    <?php /* tag "div" from line 23 */; ?>
<div class="grid_12">
                        <?php /* tag "div" from line 24 */; ?>
<div id="demo" class="clearfix"> 
                            <?php /* tag "table" from line 25 */; ?>
<table class="display" id="videos"> 
                                <?php /* tag "thead" from line 26 */; ?>
<thead> 
                                    <?php /* tag "tr" from line 27 */; ?>
<tr>
                                        <?php /* tag "th" from line 28 */; ?>
<th>Title</th>
                                        <?php /* tag "th" from line 29 */; ?>
<th>Description</th> 
                                    </tr> 
                                </thead> 
                                <?php /* tag "tbody" from line 32 */; ?>
<tbody> 
                                	<?php 
/* tag "tal:block" from line 33 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->data)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

                                		<?php /* tag "tr" from line 34 */; ?>
<tr>
                                			<?php /* tag "td" from line 35 */; ?>
<td>
                                				<?php 
/* tag "input" from line 36 */ ;
if (null !== ($_tmp_2 = ($ctx->path($ctx->item, 'id')))):  ;
$_tmp_2 = ' value="'.phptal_escape($_tmp_2).'"' ;
else:  ;
$_tmp_2 = '' ;
endif ;
?>
<input type="checkbox" name="video[]" id="video[]"<?php echo $_tmp_2 ?>
/>
                                				<?php 
/* tag "tal:block" from line 37 */ ;
$ctx = $tpl->pushContext() ;
$ctx->videoId = $ctx->path($ctx->item, 'id') ;
/* tag "a" from line 37 */ ;
if (null !== ($_tmp_2 = ('admin#/core/editVideo/?id='.($ctx->videoId)))):  ;
$_tmp_2 = ' href="'.phptal_escape($_tmp_2).'"' ;
else:  ;
$_tmp_2 = '' ;
endif ;
?>
<a<?php echo $_tmp_2 ?>
><?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</a><?php $ctx = $tpl->popContext(); ?>

                                			</td>
                                			<?php /* tag "td" from line 39 */; ?>
<td>
                                				<?php /* tag "tal:block" from line 40 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'desc')); ?>

                                			</td>
                                		</tr>
                                	<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

                                </tbody> 
                            </table> 
                        </div>                            
                    </div>
                </div>
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/iagh/manager/application/views/core/login.zpt (edit that file instead) */; ?>