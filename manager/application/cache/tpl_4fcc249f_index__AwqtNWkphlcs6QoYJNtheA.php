<?php 
function tpl_4fcc249f_index__AwqtNWkphlcs6QoYJNtheA(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<?php /* tag "link" from line 2 */; ?>
<link rel="stylesheet" href="/css/dashboard/style.css" media="screen"/>
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" href="/css/dashboard/barchart.css" media="screen"/>
<?php /* tag "script" from line 4 */; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	  // for each result row..
	  $(".extras_result").each(function() {
	    // get the width of the bar from the span html
	    var length = $(this).find("span").html();
	    length = length.replace("%","");
	    length = length / 100;
	    var lengthPx = 450 * length;
	    
	    $(this).find("p").animate({'width':lengthPx}, 2000, function() {
	      // once the bar animation has finished, fade in the results
	      $(this).find("span").fadeIn(800);
	    });
	  });
	});
</script>
	<?php /* tag "h1" from line 21 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 21 */; ?>
<span style="float:left;">Dashboard</span></h1>
	<?php /* tag "div" from line 22 */; ?>
<div id="folderTab"><?php /* tag "div" from line 22 */; ?>
<div class="folderTabText">Analytics</div></div>
	<?php /* tag "div" from line 23 */; ?>
<div class="folderContainer">
		<?php /* tag "div" from line 24 */; ?>
<div class="tbl">
			<?php /* tag "table" from line 25 */; ?>
<table>
				<?php /* tag "tr" from line 26 */; ?>
<tr>
					<?php /* tag "td" from line 27 */; ?>
<td>
						<?php /* tag "table" from line 28 */; ?>
<table style="width:200px;">
							<?php /* tag "tr" from line 29 */; ?>
<tr>
								<?php /* tag "th" from line 30 */; ?>
<th colspan="3">By Hour</th>
							</tr>
							<?php /* tag "tr" from line 32 */; ?>
<tr>
								<?php /* tag "td" from line 33 */; ?>
<td>&nbsp;</td>
								<?php /* tag "td" from line 34 */; ?>
<td>
									<?php /* tag "table" from line 35 */; ?>
<table style="width:75px;">
										<?php /* tag "tr" from line 36 */; ?>
<tr><?php /* tag "td" from line 36 */; ?>
<td>12 am</td><?php /* tag "td" from line 36 */; ?>
<td>141</td></tr>
										<?php /* tag "tr" from line 37 */; ?>
<tr><?php /* tag "td" from line 37 */; ?>
<td>01 am</td><?php /* tag "td" from line 37 */; ?>
<td>142</td></tr>
										<?php /* tag "tr" from line 38 */; ?>
<tr><?php /* tag "td" from line 38 */; ?>
<td>02 am</td><?php /* tag "td" from line 38 */; ?>
<td>141</td></tr>
										<?php /* tag "tr" from line 39 */; ?>
<tr><?php /* tag "td" from line 39 */; ?>
<td>03 am</td><?php /* tag "td" from line 39 */; ?>
<td>144</td></tr>
										<?php /* tag "tr" from line 40 */; ?>
<tr><?php /* tag "td" from line 40 */; ?>
<td>04 am</td><?php /* tag "td" from line 40 */; ?>
<td>145</td></tr>
										<?php /* tag "tr" from line 41 */; ?>
<tr><?php /* tag "td" from line 41 */; ?>
<td>05 am</td><?php /* tag "td" from line 41 */; ?>
<td>142</td></tr>
										<?php /* tag "tr" from line 42 */; ?>
<tr><?php /* tag "td" from line 42 */; ?>
<td>06 am</td><?php /* tag "td" from line 42 */; ?>
<td>147</td></tr>
										<?php /* tag "tr" from line 43 */; ?>
<tr><?php /* tag "td" from line 43 */; ?>
<td>07 am</td><?php /* tag "td" from line 43 */; ?>
<td>145</td></tr>
										<?php /* tag "tr" from line 44 */; ?>
<tr><?php /* tag "td" from line 44 */; ?>
<td>08 am</td><?php /* tag "td" from line 44 */; ?>
<td>143</td></tr>
										<?php /* tag "tr" from line 45 */; ?>
<tr><?php /* tag "td" from line 45 */; ?>
<td>09 am</td><?php /* tag "td" from line 45 */; ?>
<td>143</td></tr>
										<?php /* tag "tr" from line 46 */; ?>
<tr><?php /* tag "td" from line 46 */; ?>
<td>10 am</td><?php /* tag "td" from line 46 */; ?>
<td>144</td></tr>
										<?php /* tag "tr" from line 47 */; ?>
<tr><?php /* tag "td" from line 47 */; ?>
<td>11 am</td><?php /* tag "td" from line 47 */; ?>
<td>145</td></tr>
									</table>
								</td>
								<?php /* tag "td" from line 50 */; ?>
<td>
									<?php /* tag "table" from line 51 */; ?>
<table style="width:75px;">
										<?php /* tag "tr" from line 52 */; ?>
<tr><?php /* tag "td" from line 52 */; ?>
<td>12 pm</td><?php /* tag "td" from line 52 */; ?>
<td>144</td></tr>
										<?php /* tag "tr" from line 53 */; ?>
<tr><?php /* tag "td" from line 53 */; ?>
<td>01 pm</td><?php /* tag "td" from line 53 */; ?>
<td>147</td></tr>
										<?php /* tag "tr" from line 54 */; ?>
<tr><?php /* tag "td" from line 54 */; ?>
<td>02 pm</td><?php /* tag "td" from line 54 */; ?>
<td>149</td></tr>
										<?php /* tag "tr" from line 55 */; ?>
<tr><?php /* tag "td" from line 55 */; ?>
<td>03 pm</td><?php /* tag "td" from line 55 */; ?>
<td>124</td></tr>
										<?php /* tag "tr" from line 56 */; ?>
<tr><?php /* tag "td" from line 56 */; ?>
<td>04 pm</td><?php /* tag "td" from line 56 */; ?>
<td>114</td></tr>
										<?php /* tag "tr" from line 57 */; ?>
<tr><?php /* tag "td" from line 57 */; ?>
<td>05 pm</td><?php /* tag "td" from line 57 */; ?>
<td>134</td></tr>
										<?php /* tag "tr" from line 58 */; ?>
<tr><?php /* tag "td" from line 58 */; ?>
<td>06 pm</td><?php /* tag "td" from line 58 */; ?>
<td>124</td></tr>
										<?php /* tag "tr" from line 59 */; ?>
<tr><?php /* tag "td" from line 59 */; ?>
<td>07 pm</td><?php /* tag "td" from line 59 */; ?>
<td>144</td></tr>
										<?php /* tag "tr" from line 60 */; ?>
<tr><?php /* tag "td" from line 60 */; ?>
<td>08 pm</td><?php /* tag "td" from line 60 */; ?>
<td>134</td></tr>
										<?php /* tag "tr" from line 61 */; ?>
<tr><?php /* tag "td" from line 61 */; ?>
<td>09 pm</td><?php /* tag "td" from line 61 */; ?>
<td>114</td></tr>
										<?php /* tag "tr" from line 62 */; ?>
<tr><?php /* tag "td" from line 62 */; ?>
<td>10 pm</td><?php /* tag "td" from line 62 */; ?>
<td>164</td></tr>
										<?php /* tag "tr" from line 63 */; ?>
<tr><?php /* tag "td" from line 63 */; ?>
<td>11 pm</td><?php /* tag "td" from line 63 */; ?>
<td>184</td></tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 72 */; ?>
<div id="bargraph">
			<?php /* tag "table" from line 73 */; ?>
<table class="extras_table">
			<?php /* tag "tr" from line 74 */; ?>
<tr>
				<?php /* tag "th" from line 75 */; ?>
<th colspan="2">Top Follows</th>
			</tr>
				<?php
					$a[0]=1000;
//					$a[1]=100;
//					$a[2]=200;
//					$a[3]=120;
//					$a[4]=110;
//					$a[5]=170;
					$t = array_sum($a);
					
					for($i=0; $i<count($a);$i++){
						$p = $a[$i]/$t*100;
						$p = number_format($p);
						echo '<tr>
								<th  class="extras_y-desc" scope="row">user '.$i.'</th>
								<td><div class="extras_result"><p class="extras_p">&nbsp;'.$a[$i].'<span>'.$p.'%</span></p></div></td>
							</tr>
						';
					}
				?>
			</table>
		</div>
	</div>
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/dashboard/index.zpt (edit that file instead) */; ?>