<?php 
function tpl_4fd1a249_index__AwqtNWkphlcs6QoYJNtheA(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<?php /* tag "link" from line 2 */; ?>
<link rel="stylesheet" href="/css/calendar/ui-lightness/jquery-ui-1.8.16.custom.css" media="screen"/>
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" href="/css/dashboard/style.css" media="screen"/>
<?php /* tag "link" from line 4 */; ?>
<link rel="stylesheet" href="/css/dashboard/barchart.css" media="screen"/>
<?php /* tag "link" from line 5 */; ?>
<link rel="stylesheet" href="/css/dashboard/chart.css" media="screen"/>

<?php /* tag "script" from line 7 */; ?>
<script src="/js/calendar/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
<?php /* tag "script" from line 8 */; ?>
<script src="/js/calendar/jquery.js" type="text/javascript"></script>
<?php /* tag "script" from line 9 */; ?>
<script src="/js/dashboard/jqBarGraph.1.1.js" type="text/javascript"></script>
<?php /* tag "script" from line 10 */; ?>
<script src="/js/dashboard/global.js" type="text/javascript"></script>
<!--<script src="/js/dashboard/chart.js" type="text/javascript"></script>-->

<?php /* tag "script" from line 13 */; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	  // for each result row..
	  $(".extras_result").each(function() {
	    // get the width of the bar from the span html
	    var length = $(this).find("span").html();
	    length = length.replace("%","");
	    length = length / 100;
	    var lengthPx = 400 * length;
	    
	    $(this).find("p").animate({'width':lengthPx}, 2000, function() {
	      // once the bar animation has finished, fade in the results
	      $(this).find("span").fadeIn(800);
	    });
	  });
	});
</script>


<?php /* tag "script" from line 32 */; ?>
<script>
    $(function() {
        $( "#startDate" ).datepicker();
        $( "#startDate" ).change(function() {
            $( "#startDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });     
	// for extraction form
    $(function() {
        $( "#endDate" ).datepicker();
        $( "#endDate" ).change(function() {
            $( "#endDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });        
</script>

<?php /* tag "script" from line 48 */; ?>
<script>
	graphByMonth = new Array(
	    [[14,54,26],'2007'],
	    [[8,48,38],'2008'],
	    [[4,36,57],'2009']
	); 

	$("#severityHits").jqBarGraph({
		data: graphByMonth,
		width: 500,
		colors: ['#122A47','#1B3E69'],
		color: '#1A2944',
		barSpace: 5,
		title: '<?php /* tag "h3" from line 61 */; ?>
<h3>Number of visitors per month<?php /* tag "br" from line 61 */; ?>
<br/><?php /* tag "small" from line 61 */; ?>
<small>simple bar graph</small></h3>'
	});
</script>
    
    <?php /* tag "h1" from line 65 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 65 */; ?>
<span style="float:left;">Dashboard</span></h1>
	<?php /* tag "div" from line 66 */; ?>
<div id="folderTab"><?php /* tag "div" from line 66 */; ?>
<div class="folderTabText">Quick View</div></div>
	<?php /* tag "br" from line 67 */; ?>
<br/>
	<?php /* tag "div" from line 68 */; ?>
<div class="dashboardTabMenu">
		<?php /* tag "span" from line 69 */; ?>
<span id="followersLink"><?php /* tag "a" from line 69 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('followers')">Top Follower</a></span>
		<?php /* tag "span" from line 70 */; ?>
<span id="tenantHitsLink"><?php /* tag "a" from line 70 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('tenantHits')">Tenant Hits</a></span>
		<?php /* tag "span" from line 71 */; ?>
<span id="touchpointHitsLink"><?php /* tag "a" from line 71 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('touchpointHits')">Touchpoint Hits</a></span>
		<?php /* tag "span" from line 72 */; ?>
<span id="feedbackLink"><?php /* tag "a" from line 72 */; ?>
<a href="javascript:;" onclick="global.displayTabMenuReports('feedback')">Feedback</a></span>
	</div>
<!--	<div class="dimension">
		Dimensions<br/>
		Start Date: <input type="text" id="startDate" name="startDate"/>&nbsp;&nbsp;&nbsp;
		End Date: <input type="text" id="endDate" name="endDate"/>&nbsp;&nbsp;&nbsp;
		<input type="button" value="Query"/>
	</div>-->
	<?php /* tag "div" from line 80 */; ?>
<div class="reportContainer">
		<?php /* tag "div" from line 81 */; ?>
<div class="tbl">
			<?php 
/* tag "tal:block" from line 82 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->detailMonthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

				<?php 
/* tag "div" from line 83 */ ;
if (null !== ($_tmp_2 = ($ctx->repeat->item->key))):  ;
$_tmp_2 = ' id="'.phptal_escape($_tmp_2).'"' ;
else:  ;
$_tmp_2 = '' ;
endif ;
?>
<div style="display:none;"<?php echo $_tmp_2 ?>
>
					<?php /* tag "table" from line 84 */; ?>
<table style="width:200px; text-align:center;">
						<?php /* tag "tr" from line 85 */; ?>
<tr><?php /* tag "td" from line 85 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 85 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 86 */; ?>
<tr><?php /* tag "td" from line 86 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 86 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 87 */; ?>
<tr><?php /* tag "th" from line 87 */; ?>
<th colspan="3" align="center"><?php /* tag "span" from line 87 */; ?>
<span class="tableTitle" style="font-size:14px;"><?php /* tag "tal:block" from line 87 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</span></th></tr>
						<?php /* tag "tr" from line 88 */; ?>
<tr><?php /* tag "td" from line 88 */; ?>
<td>&nbsp;</td><?php /* tag "td" from line 88 */; ?>
<td>&nbsp;</td></tr>
						<?php /* tag "tr" from line 89 */; ?>
<tr><?php /* tag "th" from line 89 */; ?>
<th>Name</th><?php /* tag "th" from line 89 */; ?>
<th>Hits</th></tr>
						<?php 
/* tag "tal:block" from line 90 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

							<?php /* tag "tr" from line 91 */; ?>
<tr>
								<?php /* tag "td" from line 92 */; ?>
<td width="50%"><?php /* tag "tal:block" from line 92 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'name')); ?>
</td>
								<?php /* tag "td" from line 93 */; ?>
<td width="50%"><?php /* tag "tal:block" from line 93 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
							</tr>
						<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

					</table>
				</div>
			<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

			<?php /* tag "div" from line 99 */; ?>
<div id="todayTransaction">
				<?php /* tag "table" from line 100 */; ?>
<table>
					<?php /* tag "tr" from line 101 */; ?>
<tr>
						<?php /* tag "td" from line 102 */; ?>
<td>
							<?php /* tag "table" from line 103 */; ?>
<table style="width:200px;">
								<?php /* tag "tr" from line 104 */; ?>
<tr><?php /* tag "th" from line 104 */; ?>
<th colspan="3"><?php /* tag "span" from line 104 */; ?>
<span class="tableTitle">Today Transaction</span></th></tr>
								<?php /* tag "tr" from line 105 */; ?>
<tr><?php /* tag "td" from line 105 */; ?>
<td colspan="3" align="center"><?php /* tag "span" from line 105 */; ?>
<span style="font-size:9px; font-style:Italic; font-weight:normal;"><?php /* tag "tal:block" from line 105 */; ?>
<?php echo phptal_escape($ctx->todayDate); ?>
</span></td></tr>
								<?php /* tag "tr" from line 106 */; ?>
<tr><?php /* tag "td" from line 106 */; ?>
<td>&nbsp;</td></tr>
								<?php /* tag "tr" from line 107 */; ?>
<tr>
									<?php /* tag "td" from line 108 */; ?>
<td>&nbsp;</td>
									<?php /* tag "td" from line 109 */; ?>
<td>
										<?php /* tag "table" from line 110 */; ?>
<table style="width:75px; text-align:center;">
											<?php /* tag "tr" from line 111 */; ?>
<tr>
												<?php /* tag "th" from line 112 */; ?>
<th>Hour</th><?php /* tag "th" from line 112 */; ?>
<th>Hits</th>
											</tr>
											<?php 
/* tag "tal:block" from line 114 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

												<?php 
/* tag "tal:block" from line 115 */ ;
if ($ctx->repeat->item->index <= 11):  ;
?>

												<?php /* tag "tr" from line 116 */; ?>
<tr><?php /* tag "td" from line 116 */; ?>
<td><?php /* tag "tal:block" from line 116 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 116 */; ?>
<td><?php /* tag "tal:block" from line 116 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
												<?php endif; ?>

											<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

										</table>
									</td>
									<?php /* tag "td" from line 121 */; ?>
<td valign="top">
										<?php /* tag "table" from line 122 */; ?>
<table style="width:75px;text-align:center;">
											<?php /* tag "th" from line 123 */; ?>
<th>Hour</th><?php /* tag "th" from line 123 */; ?>
<th>Hits</th>
											<?php 
/* tag "tal:block" from line 124 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

												<?php 
/* tag "tal:block" from line 125 */ ;
if ($ctx->repeat->item->index >= 12):  ;
?>

												<?php /* tag "tr" from line 126 */; ?>
<tr><?php /* tag "td" from line 126 */; ?>
<td><?php /* tag "tal:block" from line 126 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 126 */; ?>
<td><?php /* tag "tal:block" from line 126 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
												<?php endif; ?>

											<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<?php /* tag "div" from line 138 */; ?>
<div id="bargraph">
		<?php /* tag "br" from line 139 */; ?>
<br/><?php /* tag "br" from line 139 */; ?>
<br/>
			<?php /* tag "table" from line 140 */; ?>
<table style="text-align:left;">
				<?php /* tag "tr" from line 141 */; ?>
<tr>
					<?php /* tag "td" from line 142 */; ?>
<td>
						<?php /* tag "div" from line 143 */; ?>
<div id="followers">
							<?php /* tag "table" from line 144 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 145 */; ?>
<tr>
									<?php /* tag "th" from line 146 */; ?>
<th colspan="2"><?php /* tag "span" from line 146 */; ?>
<span class="tableTitle">Top Followers</span></th>
								</tr>
								<?php /* tag "tr" from line 148 */; ?>
<tr><?php /* tag "td" from line 148 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 149 */; ?>
<tr><?php /* tag "td" from line 149 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 150 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->topFollowers)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 151 */; ?>
<tr>
									<?php /* tag "td" from line 152 */; ?>
<td class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 152 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 153 */; ?>
<td><?php /* tag "div" from line 153 */; ?>
<div class="extras_result"><?php /* tag "p" from line 153 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 153 */; ?>
<span><?php /* tag "tal:block" from line 153 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 153 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 156 */; ?>
<tr><?php /* tag "td" from line 156 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 159 */; ?>
<div id="tenantHits" style="display:none">
							<?php /* tag "table" from line 160 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 161 */; ?>
<tr>
									<?php /* tag "th" from line 162 */; ?>
<th colspan="2"><?php /* tag "span" from line 162 */; ?>
<span class="tableTitle">Tenant Hits</span></th>
								</tr>
								<?php /* tag "tr" from line 164 */; ?>
<tr><?php /* tag "td" from line 164 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 165 */; ?>
<tr><?php /* tag "td" from line 165 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 166 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->item = new PHPTAL_RepeatController($ctx->totalHitsPerTenant)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 167 */; ?>
<tr>
									<?php /* tag "td" from line 168 */; ?>
<td class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 168 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 169 */; ?>
<td><?php /* tag "div" from line 169 */; ?>
<div class="extras_result"><?php /* tag "p" from line 169 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 169 */; ?>
<span><?php /* tag "tal:block" from line 169 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 169 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 172 */; ?>
<tr><?php /* tag "td" from line 172 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 175 */; ?>
<div id="touchpointHits" style="display:none">
							<?php /* tag "table" from line 176 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 177 */; ?>
<tr>
									<?php /* tag "th" from line 178 */; ?>
<th colspan="2"><?php /* tag "span" from line 178 */; ?>
<span class="tableTitle">Touch Point Hits</span></th>
								</tr>
								<?php /* tag "tr" from line 180 */; ?>
<tr><?php /* tag "td" from line 180 */; ?>
<td>&nbsp;</td></tr>							
								<?php /* tag "tr" from line 181 */; ?>
<tr><?php /* tag "td" from line 181 */; ?>
<td>&nbsp;</td></tr>							
								<?php 
/* tag "tal:block" from line 182 */ ;
$_tmp_2 = $ctx->repeat ;
$_tmp_2->item = new PHPTAL_RepeatController($ctx->operatorTotalHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_2->item as $ctx->item): ;
?>

								<?php /* tag "tr" from line 183 */; ?>
<tr>
									<?php /* tag "td" from line 184 */; ?>
<td class="extras_y-desc" scope="row">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php /* tag "tal:block" from line 184 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
									<?php /* tag "td" from line 185 */; ?>
<td><?php /* tag "div" from line 185 */; ?>
<div class="extras_result"><?php /* tag "p" from line 185 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 185 */; ?>
<span><?php /* tag "tal:block" from line 185 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 185 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
								</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 188 */; ?>
<tr><?php /* tag "td" from line 188 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</div>
						<?php /* tag "div" from line 191 */; ?>
<div id="feedback" style="display:none">
							<?php /* tag "table" from line 192 */; ?>
<table class="extras_table" align="center">
								<?php /* tag "tr" from line 193 */; ?>
<tr>
									<?php /* tag "th" from line 194 */; ?>
<th><?php /* tag "span" from line 194 */; ?>
<span class="tableTitle" style="text-decoration:underline;"><?php /* tag "a" from line 194 */; ?>
<a href="javascript:;">Severity</a></span></th>
									<?php /* tag "th" from line 195 */; ?>
<th>&nbsp;/&nbsp;</th>
																		<?php /* tag "th" from line 196 */; ?>
<th><?php /* tag "span" from line 196 */; ?>
<span class="tableTitle" style="text-decoration:underline;"><?php /* tag "a" from line 196 */; ?>
<a href="javascript:;">Status</a></span></th>
								</tr>
							</table>
							<?php /* tag "div" from line 199 */; ?>
<div id="severityHits">
								<?php /* tag "table" from line 200 */; ?>
<table>
									<?php /* tag "tr" from line 201 */; ?>
<tr>
										<?php /* tag "td" from line 202 */; ?>
<td></td>
									</tr>
								</table>
							</div>
							<?php /* tag "div" from line 206 */; ?>
<div id="status">
								<?php /* tag "table" from line 207 */; ?>
<table>
									<?php /* tag "tr" from line 208 */; ?>
<tr>
										<?php /* tag "td" from line 209 */; ?>
<td></td>
									</tr>
								</table>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 218 */; ?>
<div id="monthlyReport">
			<?php /* tag "table" from line 219 */; ?>
<table style="width:880px; margin-left:20px;">
				<?php /* tag "tr" from line 220 */; ?>
<tr><?php /* tag "td" from line 220 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 221 */; ?>
<tr><?php /* tag "th" from line 221 */; ?>
<th colspan="12" align="center"><?php /* tag "span" from line 221 */; ?>
<span class="tableTitle">Total Hits By Month</span></th></tr>
				<?php /* tag "tr" from line 222 */; ?>
<tr><?php /* tag "td" from line 222 */; ?>
<td>&nbsp;</td></tr>
				<?php /* tag "tr" from line 223 */; ?>
<tr>
					<?php 
/* tag "tal:block" from line 224 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->monthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

						<?php /* tag "td" from line 225 */; ?>
<td valign="top">
							<?php /* tag "table" from line 226 */; ?>
<table>
								<?php /* tag "tr" from line 227 */; ?>
<tr>
									<?php /* tag "th" from line 228 */; ?>
<th colspan="2" align="center"><?php /* tag "a" from line 228 */; ?>
<a href="javascript:;" onclick="global.displayTransContainer($(this).html());"><?php /* tag "tal:block" from line 228 */; ?>
<?php echo phptal_escape($ctx->repeat->item->key); ?>
</a></th>
								</tr>
								<?php 
/* tag "tal:block" from line 230 */ ;
$_tmp_3 = $ctx->repeat ;
$_tmp_3->h = new PHPTAL_RepeatController($ctx->item)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_3->h as $ctx->h): ;
?>

									<?php /* tag "tr" from line 231 */; ?>
<tr>
										<?php /* tag "td" from line 232 */; ?>
<td><?php /* tag "tal:block" from line 232 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'total')); ?>
</td>
										<?php /* tag "td" from line 233 */; ?>
<td><?php /* tag "tal:block" from line 233 */; ?>
<?php echo phptal_escape($ctx->path($ctx->h, 'hits')); ?>
</td>
									</tr>
								<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

								<?php /* tag "tr" from line 236 */; ?>
<tr><?php /* tag "td" from line 236 */; ?>
<td>&nbsp;</td></tr>
							</table>
						</td>
					<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>
			
				</tr>
			</table>
		</div>
	</div>
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/dashboard/index.zpt (edit that file instead) */; ?>