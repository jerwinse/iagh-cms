<?php 
function tpl_4fc44946_create__ZHJflLmEMRgWdQDhnR19vA(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>



<?php /* tag "script" from line 4 */; ?>
<script type="text/javascript" src="/js/jquery.itextclear.js"></script>
<?php /* tag "script" from line 5 */; ?>
<script type="text/javascript">
$(document).ready(function(){
    $('input[type=text], input[type=password], input[type=url], input[type=email], input[type=number], textarea', '.form').iTextClear(); 
    Global.onSuccess = function(){Permissions.create();return false;}
});

$(window).bind('drilldown', function(){
	Global.onSuccess = function(){Permissions.create();return false;}	
});

var Permissions = {
	create: function() {
		var form_data = JSON.stringify($('#form-permission').serializeArray());
		
        $.ajax('/permissions/docreate', {
        	type: 'post',
        	data: {'data':form_data},
            dataType: "html",
            cache: false,
            beforeSend: function(jqXHR, settings) {
            	$('#message').html('');
            	$('#message').removeClass('error');
            	$('#message').removeClass('success');
            	
            	$('#message').html('<?php /* tag "h3" from line 29 */; ?>
<h3>Please wait</h3><?php /* tag "p" from line 29 */; ?>
<p>Saving record...</p>');
            	$('#message').addClass('message');
            	$('#message').addClass('info');
            	$('#message').fadeIn();
            },
            complete: function(jqXHR, textStatus) {            	
                var response = jQuery.parseJSON(jqXHR.responseText);                
                if (response.status == 'ACK')
                {
                	var html = '<?php /* tag "h3" from line 38 */; ?>
<h3>Success!</h3><?php /* tag "p" from line 38 */; ?>
<p>You have successfully created a role.</p>';
                	$('#message').html(html);
                	$('#message').removeClass('error');
                	$('#message').addClass('message');
                	$('#message').addClass('success');
                	$('#message').fadeIn().delay(10000).fadeOut('slow');
                }
                else
                {
                	var html = '<?php /* tag "h3" from line 47 */; ?>
<h3>Error!</h3><?php /* tag "p" from line 47 */; ?>
<p>There was an error saving the record.</p>';
                	$('#message').html(html);
                	$('#message').addClass('message');
                	$('#message').addClass('error');
                	$('#message').fadeIn().delay(10000).fadeOut('slow');
                }
            }
        });
	}	
};
</script>
                <?php /* tag "h1" from line 58 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 58 */; ?>
<span style="float:left;"><?php /* tag "a" from line 58 */; ?>
<a href="#/permissions/index" style="color:#fff;">ACL Permissions</a> &raquo; Create A Permission</span></h1>
                <?php /* tag "div" from line 59 */; ?>
<div class="container_12 clearfix leading">
                    <?php /* tag "div" from line 60 */; ?>
<div class="grid_12">
						<?php /* tag "div" from line 61 */; ?>
<div id="message"></div>
                    
                    	<?php /* tag "form" from line 63 */; ?>
<form id="form-permission" class="form has-validation" method="post">

                            <?php /* tag "div" from line 65 */; ?>
<div class="clearfix">

                                <?php /* tag "label" from line 67 */; ?>
<label for="form-key" class="form-label">Key <?php /* tag "em" from line 67 */; ?>
<em>*</em></label>

                                <?php /* tag "div" from line 69 */; ?>
<div class="form-input"><?php /* tag "input" from line 69 */; ?>
<input type="text" id="form-key" name="permKey" required="required" maxlength="30" placeholder="Alphanumeric (max 30 char.)"/></div>

                            </div>

                            <?php /* tag "div" from line 73 */; ?>
<div class="clearfix">

                                <?php /* tag "label" from line 75 */; ?>
<label for="form-name" class="form-label">Name <?php /* tag "em" from line 75 */; ?>
<em>*</em></label>

                                <?php /* tag "div" from line 77 */; ?>
<div class="form-input"><?php /* tag "input" from line 77 */; ?>
<input type="text" id="form-name" name="permName" required="required" placeholder=""/></div>

                            </div>

                            <?php /* tag "div" from line 81 */; ?>
<div class="form-action clearfix">

                                <?php /* tag "button" from line 83 */; ?>
<button class="button" type="submit">Create Permission</button>

                                <?php /* tag "button" from line 85 */; ?>
<button class="button" type="reset">Clear</button>

                            </div>

                        </form>
                    </div>
                </div><?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/permissions/create.zpt (edit that file instead) */; ?>