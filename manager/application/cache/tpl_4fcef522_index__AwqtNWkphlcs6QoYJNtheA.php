<?php 
function tpl_4fcef522_index__AwqtNWkphlcs6QoYJNtheA(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<?php /* tag "link" from line 2 */; ?>
<link rel="stylesheet" href="/css/calendar/ui-lightness/jquery-ui-1.8.16.custom.css" media="screen"/>
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" href="/css/dashboard/style.css" media="screen"/>
<?php /* tag "link" from line 4 */; ?>
<link rel="stylesheet" href="/css/dashboard/barchart.css" media="screen"/>
<?php /* tag "link" from line 5 */; ?>
<link rel="stylesheet" href="/css/dashboard/chart.css" media="screen"/>

<?php /* tag "script" from line 7 */; ?>
<script src="/js/calendar/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
<!--<script src="/js/dashboard/chart.js" type="text/javascript"></script>-->

<?php /* tag "script" from line 10 */; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	  // for each result row..
	  $(".extras_result").each(function() {
	    // get the width of the bar from the span html
	    var length = $(this).find("span").html();
	    length = length.replace("%","");
	    length = length / 100;
	    var lengthPx = 400 * length;
	    
	    $(this).find("p").animate({'width':lengthPx}, 2000, function() {
	      // once the bar animation has finished, fade in the results
	      $(this).find("span").fadeIn(800);
	    });
	  });
	});
</script>


<?php /* tag "script" from line 29 */; ?>
<script>
    $(function() {
        $( "#startDate" ).datepicker();
        $( "#startDate" ).change(function() {
            $( "#startDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });     
	// for extraction form
    $(function() {
        $( "#endDate" ).datepicker();
        $( "#endDate" ).change(function() {
            $( "#endDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });        
</script>		
    
    <?php /* tag "h1" from line 45 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 45 */; ?>
<span style="float:left;">Dashboard</span></h1>
	<?php /* tag "div" from line 46 */; ?>
<div id="folderTab"><?php /* tag "div" from line 46 */; ?>
<div class="folderTabText">Quick View</div></div>
	<?php /* tag "br" from line 47 */; ?>
<br/>
<!--	<div class="dashboardTabMenu">
		<span><a href="admin#/dashboard/index">Home</a></span>
		<span><a class="selected" href="#">Analytics</a></span>
		<span><a href="#">Menu 3</a></span>
		<span><a href="#">Menu 4</a></span>
	</div>
	<div class="dimension">
		Dimensions<br/>
		Start Date: <input type="text" id="startDate" name="startDate"/>&nbsp;&nbsp;&nbsp;
		End Date: <input type="text" id="endDate" name="endDate"/>&nbsp;&nbsp;&nbsp;
		<input type="button" value="Query"/>
	</div>-->
	<?php /* tag "div" from line 60 */; ?>
<div class="reportContainer">
		<?php /* tag "tal:block" from line 61 */; ?>
<?php echo phptal_escape($ctx->monthlyHits); ?>

		<?php /* tag "div" from line 62 */; ?>
<div class="tbl">
			<?php /* tag "table" from line 63 */; ?>
<table>
				<?php /* tag "tr" from line 64 */; ?>
<tr>
					<?php /* tag "td" from line 65 */; ?>
<td>
						<?php /* tag "table" from line 66 */; ?>
<table style="width:200px;">
							<?php /* tag "tr" from line 67 */; ?>
<tr><?php /* tag "td" from line 67 */; ?>
<td>&nbsp;</td></tr>
							<?php /* tag "tr" from line 68 */; ?>
<tr><?php /* tag "td" from line 68 */; ?>
<td>&nbsp;</td></tr>
							<?php /* tag "tr" from line 69 */; ?>
<tr><?php /* tag "th" from line 69 */; ?>
<th colspan="3"><?php /* tag "span" from line 69 */; ?>
<span class="tableTitle">Today Transaction</span></th></tr>
							<?php /* tag "tr" from line 70 */; ?>
<tr><?php /* tag "td" from line 70 */; ?>
<td colspan="3" align="center"><?php /* tag "span" from line 70 */; ?>
<span style="font-size:9px; font-style:Italic; font-weight:normal;"><?php /* tag "tal:block" from line 70 */; ?>
<?php echo phptal_escape($ctx->todayDate); ?>
</span></td></tr>
							<?php /* tag "tr" from line 71 */; ?>
<tr><?php /* tag "td" from line 71 */; ?>
<td>&nbsp;</td></tr>
							<?php /* tag "tr" from line 72 */; ?>
<tr>
								<?php /* tag "td" from line 73 */; ?>
<td>&nbsp;</td>
								<?php /* tag "td" from line 74 */; ?>
<td>
									<?php /* tag "table" from line 75 */; ?>
<table style="width:75px; text-align:center;">
										<?php /* tag "tr" from line 76 */; ?>
<tr>
											<?php /* tag "th" from line 77 */; ?>
<th>Hour</th><?php /* tag "th" from line 77 */; ?>
<th>Hits</th>
										</tr>
										<?php 
/* tag "tal:block" from line 79 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

											<?php 
/* tag "tal:block" from line 80 */ ;
if ($ctx->repeat->item->index <= 11):  ;
?>

											<?php /* tag "tr" from line 81 */; ?>
<tr><?php /* tag "td" from line 81 */; ?>
<td><?php /* tag "tal:block" from line 81 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 81 */; ?>
<td><?php /* tag "tal:block" from line 81 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
											<?php endif; ?>

										<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

									</table>
								</td>
								<?php /* tag "td" from line 86 */; ?>
<td valign="top">
									<?php /* tag "table" from line 87 */; ?>
<table style="width:75px;text-align:center;">
										<?php /* tag "th" from line 88 */; ?>
<th>Hour</th><?php /* tag "th" from line 88 */; ?>
<th>Hits</th>
										<?php 
/* tag "tal:block" from line 89 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

											<?php 
/* tag "tal:block" from line 90 */ ;
if ($ctx->repeat->item->index >= 12):  ;
?>

											<?php /* tag "tr" from line 91 */; ?>
<tr><?php /* tag "td" from line 91 */; ?>
<td><?php /* tag "tal:block" from line 91 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 91 */; ?>
<td><?php /* tag "tal:block" from line 91 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
											<?php endif; ?>

										<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 102 */; ?>
<div id="bargraph">
		<?php /* tag "br" from line 103 */; ?>
<br/>
			<?php /* tag "table" from line 104 */; ?>
<table style="text-align:left;">
				<?php /* tag "tr" from line 105 */; ?>
<tr>
					<?php /* tag "td" from line 106 */; ?>
<td>
						<?php /* tag "table" from line 107 */; ?>
<table class="extras_table" align="center">
							<?php /* tag "tr" from line 108 */; ?>
<tr>
								<?php /* tag "th" from line 109 */; ?>
<th colspan="2"><?php /* tag "span" from line 109 */; ?>
<span class="tableTitle">Top Followers</span></th>
							</tr>
							<?php /* tag "tr" from line 111 */; ?>
<tr><?php /* tag "td" from line 111 */; ?>
<td></td></tr>
						
							<?php 
/* tag "tal:block" from line 113 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->topFollowers)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

							<?php /* tag "tr" from line 114 */; ?>
<tr>
								<?php /* tag "th" from line 115 */; ?>
<th class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 115 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</th>
								<?php /* tag "td" from line 116 */; ?>
<td><?php /* tag "div" from line 116 */; ?>
<div class="extras_result"><?php /* tag "p" from line 116 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 116 */; ?>
<span><?php /* tag "tal:block" from line 116 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 116 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
							</tr>
							<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

							<?php /* tag "tr" from line 119 */; ?>
<tr><?php /* tag "td" from line 119 */; ?>
<td>&nbsp;</td></tr>
						</table>
						<?php /* tag "table" from line 121 */; ?>
<table class="extras_table" align="center">
							<?php /* tag "tr" from line 122 */; ?>
<tr>
								<?php /* tag "th" from line 123 */; ?>
<th colspan="2"><?php /* tag "span" from line 123 */; ?>
<span class="tableTitle">Tenant Total Hits</span></th>
							</tr>
							<?php 
/* tag "tal:block" from line 125 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->totalHitsPerTenant)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

							<?php /* tag "tr" from line 126 */; ?>
<tr>
								<?php /* tag "th" from line 127 */; ?>
<th class="extras_y-desc" scope="row"><?php /* tag "tal:block" from line 127 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</th>
								<?php /* tag "td" from line 128 */; ?>
<td><?php /* tag "div" from line 128 */; ?>
<div class="extras_result"><?php /* tag "p" from line 128 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 128 */; ?>
<span><?php /* tag "tal:block" from line 128 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 128 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
							</tr>
							<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

							<?php /* tag "tr" from line 131 */; ?>
<tr><?php /* tag "td" from line 131 */; ?>
<td>&nbsp;</td></tr>
						</table>
						<?php /* tag "table" from line 133 */; ?>
<table class="extras_table" align="center">
							<?php /* tag "tr" from line 134 */; ?>
<tr>
								<?php /* tag "th" from line 135 */; ?>
<th colspan="2"><?php /* tag "span" from line 135 */; ?>
<span class="tableTitle">Touch Point Total Hits</span></th>
							</tr>
							<?php 
/* tag "tal:block" from line 137 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->operatorTotalHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

							<?php /* tag "tr" from line 138 */; ?>
<tr>
								<?php /* tag "th" from line 139 */; ?>
<th class="extras_y-desc" scope="row">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php /* tag "tal:block" from line 139 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</th>
								<?php /* tag "td" from line 140 */; ?>
<td><?php /* tag "div" from line 140 */; ?>
<div class="extras_result"><?php /* tag "p" from line 140 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 140 */; ?>
<span><?php /* tag "tal:block" from line 140 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'percent')); ?>
</span></p><?php /* tag "tal:block" from line 140 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'hits')); ?>
</div></td>
							</tr>
							<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

							<?php /* tag "tr" from line 143 */; ?>
<tr><?php /* tag "td" from line 143 */; ?>
<td>&nbsp;</td></tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 149 */; ?>
<div id="monthlyReport">
			<?php /* tag "table" from line 150 */; ?>
<table>
				<?php 
/* tag "tal:block" from line 151 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->monthlyHits)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

				<?php /* tag "tr" from line 152 */; ?>
<tr>
					<?php /* tag "td" from line 153 */; ?>
<td><?php /* tag "tal:block" from line 153 */; ?>
<?php echo phptal_escape($ctx->item); ?>
</td>
					<?php /* tag "td" from line 154 */; ?>
<td><?php /* tag "tal:block" from line 154 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'name')); ?>
</td>
				</tr>
				<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>
				
			</table>
		</div>
	</div>
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/dashboard/index.zpt (edit that file instead) */; ?>