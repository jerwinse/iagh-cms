<?php 
function tpl_4fce23a6_analytics__paLIBl7m1qQXZG4rRLkhvg(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<?php /* tag "link" from line 2 */; ?>
<link rel="stylesheet" href="/css/calendar/ui-lightness/jquery-ui-1.8.16.custom.css" media="screen"/>
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" href="/css/dashboard/style.css" media="screen"/>
<?php /* tag "link" from line 4 */; ?>
<link rel="stylesheet" href="/css/dashboard/barchart.css" media="screen"/>
<?php /* tag "link" from line 5 */; ?>
<link rel="stylesheet" href="/css/dashboard/chart.css" media="screen"/>

<?php /* tag "script" from line 7 */; ?>
<script src="/js/calendar/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
<?php /* tag "script" from line 8 */; ?>
<script src="/js/dashboard/chart.js" type="text/javascript"></script>

<?php /* tag "script" from line 10 */; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	  // for each result row..
	  $(".extras_result").each(function() {
	    // get the width of the bar from the span html
	    var length = $(this).find("span").html();
	    length = length.replace("%","");
	    length = length / 100;
	    var lengthPx = 450 * length;
	    
	    $(this).find("p").animate({'width':lengthPx}, 2000, function() {
	      // once the bar animation has finished, fade in the results
	      $(this).find("span").fadeIn(800);
	    });
	  });
	});
</script>


<?php /* tag "script" from line 29 */; ?>
<script>
    $(function() {
        $( "#startDate" ).datepicker();
        $( "#startDate" ).change(function() {
            $( "#startDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });     
	// for extraction form
    $(function() {
        $( "#endDate" ).datepicker();
        $( "#endDate" ).change(function() {
            $( "#endDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
        });
    });        
</script>		
    
    <?php /* tag "h1" from line 45 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 45 */; ?>
<span style="float:left;">Dashboard</span></h1>
	<?php /* tag "div" from line 46 */; ?>
<div id="folderTab"><?php /* tag "div" from line 46 */; ?>
<div class="folderTabText">Analytics</div></div>
	<?php /* tag "br" from line 47 */; ?>
<br/>	<?php /* tag "br" from line 47 */; ?>
<br/>	<?php /* tag "br" from line 47 */; ?>
<br/>	
	<?php /* tag "div" from line 48 */; ?>
<div class="dashboardTabMenu">
		<?php /* tag "span" from line 49 */; ?>
<span><?php /* tag "a" from line 49 */; ?>
<a href="admin#/dashboard/index">Home</a></span>
		<?php /* tag "span" from line 50 */; ?>
<span><?php /* tag "a" from line 50 */; ?>
<a class="selected" href="#">Analytics</a></span>
		<?php /* tag "span" from line 51 */; ?>
<span><?php /* tag "a" from line 51 */; ?>
<a href="#">Menu 3</a></span>
		<?php /* tag "span" from line 52 */; ?>
<span><?php /* tag "a" from line 52 */; ?>
<a href="#">Menu 4</a></span>
	</div>
	<?php /* tag "div" from line 54 */; ?>
<div class="dimension">
		Dimensions<?php /* tag "br" from line 55 */; ?>
<br/>
		Start Date: <?php /* tag "input" from line 56 */; ?>
<input type="text" id="startDate" name="startDate"/>&nbsp;&nbsp;&nbsp;
		End Date: <?php /* tag "input" from line 57 */; ?>
<input type="text" id="endDate" name="endDate"/>&nbsp;&nbsp;&nbsp;
		<?php /* tag "input" from line 58 */; ?>
<input type="button" value="Query"/>
	</div>
	<?php /* tag "div" from line 60 */; ?>
<div class="reportContainer">
	<?php /* tag "tal:block" from line 61 */; ?>
<?php echo phptal_escape($ctx->topFollowers); ?>

		<?php /* tag "div" from line 62 */; ?>
<div class="tbl">
			<?php /* tag "table" from line 63 */; ?>
<table>
				<?php /* tag "tr" from line 64 */; ?>
<tr>
					<?php /* tag "td" from line 65 */; ?>
<td>
						<?php /* tag "table" from line 66 */; ?>
<table style="width:200px;">
							<?php /* tag "tr" from line 67 */; ?>
<tr>
								<?php /* tag "th" from line 68 */; ?>
<th colspan="3">Today</th>
							</tr>
							<?php /* tag "tr" from line 70 */; ?>
<tr>
								<?php /* tag "td" from line 71 */; ?>
<td colspan="3" align="center"><?php /* tag "span" from line 71 */; ?>
<span style="font-size:9px; font-style:Italic; font-weight:normal;"><?php /* tag "tal:block" from line 71 */; ?>
<?php echo phptal_escape($ctx->todayDate); ?>
</span></td>
							</tr>
							<?php /* tag "tr" from line 73 */; ?>
<tr>
								<?php /* tag "td" from line 74 */; ?>
<td>&nbsp;</td>
								<?php /* tag "td" from line 75 */; ?>
<td>
									<?php /* tag "table" from line 76 */; ?>
<table style="width:75px; text-align:center;">
										<?php /* tag "tr" from line 77 */; ?>
<tr>
											<?php /* tag "th" from line 78 */; ?>
<th>Hour</th><?php /* tag "th" from line 78 */; ?>
<th>Hits</th>
										</tr>
										<?php 
/* tag "tal:block" from line 80 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

											<?php 
/* tag "tal:block" from line 81 */ ;
if ($ctx->repeat->item->index <= 11):  ;
?>

											<?php /* tag "tr" from line 82 */; ?>
<tr><?php /* tag "td" from line 82 */; ?>
<td><?php /* tag "tal:block" from line 82 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 82 */; ?>
<td><?php /* tag "tal:block" from line 82 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
											<?php endif; ?>

										<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

									</table>
								</td>
								<?php /* tag "td" from line 87 */; ?>
<td valign="top">
									<?php /* tag "table" from line 88 */; ?>
<table style="width:75px;text-align:center;">
										<?php /* tag "th" from line 89 */; ?>
<th>Hour</th><?php /* tag "th" from line 89 */; ?>
<th>Hits</th>
										<?php 
/* tag "tal:block" from line 90 */ ;
$_tmp_1 = $ctx->repeat ;
$_tmp_1->item = new PHPTAL_RepeatController($ctx->timeToday)
 ;
$ctx = $tpl->pushContext() ;
foreach ($_tmp_1->item as $ctx->item): ;
?>

											<?php 
/* tag "tal:block" from line 91 */ ;
if ($ctx->repeat->item->index >= 12):  ;
?>

											<?php /* tag "tr" from line 92 */; ?>
<tr><?php /* tag "td" from line 92 */; ?>
<td><?php /* tag "tal:block" from line 92 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'id')); ?>
</td><?php /* tag "td" from line 92 */; ?>
<td><?php /* tag "tal:block" from line 92 */; ?>
<?php echo phptal_escape($ctx->path($ctx->item, 'value')); ?>
</td></tr>
											<?php endif; ?>

										<?php 
endforeach ;
$ctx = $tpl->popContext() ;
?>

									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 103 */; ?>
<div id="bargraph">
			<?php /* tag "br" from line 104 */; ?>
<br/>
			<?php /* tag "table" from line 105 */; ?>
<table class="extras_table" align="center">
			<?php /* tag "tr" from line 106 */; ?>
<tr>
				<?php /* tag "th" from line 107 */; ?>
<th colspan="2">Top Follows</th>
			</tr>
			<?php /* tag "tr" from line 109 */; ?>
<tr><?php /* tag "td" from line 109 */; ?>
<td colspan="2">&nbsp;</td></tr>
				<?php
					$a[0]=1000;
					$a[1]=100;
					$a[2]=200;
					$a[3]=120;
					$a[4]=110;
					$a[5]=170;
					$t = array_sum($a);
					
					for($i=0; $i<count($a);$i++){
						$p = $a[$i]/$t*100;
						$p = number_format($p);
						echo '<tr>
								<th  class="extras_y-desc" scope="row">user '.$i.'</th>
								<td><div class="extras_result"><p class="extras_p">&nbsp;<span>'.$p.'%</span></p>'.number_format($a[$i]).'</div></td>
							</tr>
						';
					}
				?>
			</table>
		</div>
		<?php /* tag "div" from line 131 */; ?>
<div id="piegraph">
			<?php /* tag "canvas" from line 132 */; ?>
<canvas id="chart" width="400" height="300"></canvas>
			<?php /* tag "table" from line 133 */; ?>
<table id="chartData">
				<?php /* tag "tr" from line 134 */; ?>
<tr><?php /* tag "th" from line 134 */; ?>
<th colspan="2" style="text-align:center">Pie Chart</th></tr>
				<?php /* tag "tr" from line 135 */; ?>
<tr><?php /* tag "th" from line 135 */; ?>
<th style="text-align:center;">Touch Points</th><?php /* tag "th" from line 135 */; ?>
<th style="text-align:center;">Hits</th></tr>
				<?php /* tag "tr" from line 136 */; ?>
<tr style="color: #72808E">
					<?php /* tag "td" from line 137 */; ?>
<td>Globe</td>
					<?php /* tag "td" from line 138 */; ?>
<td>500</td>
				</tr>
				<?php /* tag "tr" from line 140 */; ?>
<tr style="color: #000000">
					<?php /* tag "td" from line 141 */; ?>
<td>XMPP</td>
					<?php /* tag "td" from line 142 */; ?>
<td>300</td>
				</tr>
			</table>
			<?php /* tag "iframe" from line 145 */; ?>
<iframe id="secretIFrame" src="" style="display:none; visibility:hidden;"></iframe>
		</div>
	</div>
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/dashboard/analytics.zpt (edit that file instead) */; ?>