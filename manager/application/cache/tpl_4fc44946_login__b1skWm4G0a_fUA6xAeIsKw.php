<?php 
function tpl_4fc44946_login__b1skWm4G0a_fUA6xAeIsKw(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
$ctx->setDocType('<!doctype html>',false) ;
?>

<!--[if IE 7 ]>   <html lang="en" class="ie7 lte8"> <![endif]--> 
<!--[if IE 8 ]>   <html lang="en" class="ie8 lte8"> <![endif]--> 
<!--[if IE 9 ]>   <html lang="en" class="ie9"> <![endif]--> 
<!--[if gt IE 9]> <html lang="en"> <![endif]-->
<!--[if !IE]><!--> <?php /* tag "html" from line 6 */; ?>
<html lang="en"> <!--<![endif]-->
<?php /* tag "head" from line 7 */; ?>
<head>
<?php /* tag "meta" from line 8 */; ?>
<meta charset="utf-8"/>
<!--[if lte IE 9 ]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->

<!-- iPad Settings -->
<?php /* tag "meta" from line 12 */; ?>
<meta name="apple-mobile-web-app-capable" content="yes"/>
<?php /* tag "meta" from line 13 */; ?>
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/> 
<?php /* tag "meta" from line 14 */; ?>
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0"/>
<!-- Adding "maximum-scale=1" fixes the Mobile Safari auto-zoom bug: http://filamentgroup.com/examples/iosScaleBug/ -->
<!-- iPad End -->

<?php /* tag "title" from line 18 */; ?>
<title><?php echo phptal_escape($ctx->title) ?>
</title>

<!-- iOS ICONS -->
<?php /* tag "link" from line 21 */; ?>
<link rel="apple-touch-icon" href="touch-icon-iphone.png"/>
<?php /* tag "link" from line 22 */; ?>
<link rel="apple-touch-icon" sizes="72x72" href="touch-icon-ipad.png"/>
<?php /* tag "link" from line 23 */; ?>
<link rel="apple-touch-icon" sizes="114x114" href="touch-icon-iphone4.png"/>
<?php /* tag "link" from line 24 */; ?>
<link rel="apple-touch-startup-image" href="touch-startup-image.png"/>
<!-- iOS ICONS END -->

<!-- STYLESHEETS -->

<?php /* tag "link" from line 29 */; ?>
<link rel="stylesheet" media="screen" href="/css/reset.css"/>
<?php /* tag "link" from line 30 */; ?>
<link rel="stylesheet" media="screen" href="/css/grids.css"/>
<?php /* tag "link" from line 31 */; ?>
<link rel="stylesheet" media="screen" href="/css/style.css"/>
<?php /* tag "link" from line 32 */; ?>
<link rel="stylesheet" media="screen" href="/css/ui.css"/>
<?php /* tag "link" from line 33 */; ?>
<link rel="stylesheet" media="screen" href="/css/jquery.uniform.css"/>
<?php /* tag "link" from line 34 */; ?>
<link rel="stylesheet" media="screen" href="/css/forms.css"/>
<?php /* tag "link" from line 35 */; ?>
<link rel="stylesheet" media="screen" href="/css/themes/lightblue/style.css"/>

<?php /* tag "style" from line 37 */; ?>
<style type="text/css">
    #loading-container {position: absolute; top:50%; left:50%;}
    #loading-content {width:800px; text-align:center; margin-left: -400px; height:50px; margin-top:-25px; line-height: 50px;}
    #loading-content {font-family: "Helvetica", "Arial", sans-serif; font-size: 18px; color: black; text-shadow: 0px 1px 0px white; }
    #loading-graphic {margin-right: 0.2em; margin-bottom:-2px;}
    #loading {background-color:#abc4ff; background-image: -moz-radial-gradient(50% 50%, ellipse closest-side, #abc4ff, #87a7ff 100%); background-image: -webkit-radial-gradient(50% 50%, ellipse closest-side, #abc4ff, #87a7ff 100%); background-image: -o-radial-gradient(50% 50%, ellipse closest-side, #abc4ff, #87a7ff 100%); background-image: -ms-radial-gradient(50% 50%, ellipse closest-side, #abc4ff, #87a7ff 100%); background-image: radial-gradient(50% 50%, ellipse closest-side, #abc4ff, #87a7ff 100%); height:100%; width:100%; overflow:hidden; position: absolute; left: 0; top: 0; z-index: 99999;}
</style>

<!-- STYLESHEETS END -->

<!--[if lt IE 9]>
<script src="/js/html5.js"></script>
<script type="text/javascript" src="/js/selectivizr.js"></script>
<script type="text/javascript" src="/js/respond.min.js"></script>
<![endif]-->

</head>
<?php /* tag "body" from line 54 */; ?>
<body class="login" style="overflow: hidden;">
    <?php /* tag "div" from line 55 */; ?>
<div id="loading"> 

        <?php /* tag "script" from line 57 */; ?>
<script type="text/javascript"> 
            document.write('<?php /* tag "div" from line 58 */; ?>
<div id="loading-container"><?php /* tag "p" from line 58 */; ?>
<p id="loading-content">' +
                           '<?php /* tag "img" from line 59 */; ?>
<img id="loading-graphic" width="16" height="16" src="/images/ajax-loader-abc4ff.gif"/> ' +
                           'Loading...</p></div>');
        </script> 

    </div> 

    <?php /* tag "div" from line 65 */; ?>
<div class="login-box">
    	<?php /* tag "section" from line 66 */; ?>
<section class="login-box-top">
            <?php /* tag "header" from line 67 */; ?>
<header>
                <?php /* tag "h2" from line 68 */; ?>
<h2 class="logo ac">vPad Login</h2>
            </header>
            <?php /* tag "section" from line 70 */; ?>
<section>
                <?php /* tag "form" from line 71 */; ?>
<form id="form" class="has-validation" action="/auth/login" method="post" style="margin-top: 30px">
                	<?php 
/* tag "tal:block" from line 72 */ ;
if ($ctx->error):  ;
/* tag "div" from line 72 */ ;
?>
<div style="padding:0 15px 10px 15px;color:red;"><?php /* tag "span" from line 72 */; ?>
<span>Incorrect login. Please check CloudMsngr account, email/username, or password.</span></div><?php endif; ?>

                    <?php /* tag "div" from line 73 */; ?>
<div class="user-pass">
                        <?php /* tag "input" from line 74 */; ?>
<input type="text" id="account_num" class="full" value="" name="account_num" required="required" placeholder="CloudMsngr Account" autocomplete="off"/>
                        <?php /* tag "input" from line 75 */; ?>
<input type="text" id="username" class="full" value="" name="username" required="required" placeholder="Email/Username" autocomplete="off"/>
                        <?php /* tag "input" from line 76 */; ?>
<input type="password" id="password" class="full" value="" name="password" required="required" placeholder="Password" autocomplete="off"/>
                    </div>
                    <?php /* tag "p" from line 78 */; ?>
<p class="clearfix">
                        <?php /* tag "span" from line 79 */; ?>
<span class="fl" style="line-height: 23px;">
                            <?php /* tag "label" from line 80 */; ?>
<label class="choice" for="remember">
                                <?php /* tag "input" from line 81 */; ?>
<input type="checkbox" id="remember" class="" value="1" name="remember"/>
                                Keep me logged in
                            </label>
                        </span>

                        <?php /* tag "button" from line 86 */; ?>
<button class="fr" type="submit">Login</button>
                    </p>
                </form>
            </section>
    	</section>
	</div>
    
    <!-- MAIN JAVASCRIPTS -->
    <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.js"></script> -->
    <!-- <script>window.jQuery || document.write("<script src='/js/jquery.min.js'>\x3C/script>")</script> -->
    <?php /* tag "script" from line 96 */; ?>
<script type="text/javascript" src="/js/jquery.min.js"></script>
    <?php /* tag "script" from line 97 */; ?>
<script type="text/javascript" src="/js/jquery.tools.min.js"></script>
    <?php /* tag "script" from line 98 */; ?>
<script type="text/javascript" src="/js/jquery.uniform.min.js"></script>
    <?php /* tag "script" from line 99 */; ?>
<script type="text/javascript" src="/js/jquery.easing.js"></script>
    <?php /* tag "script" from line 100 */; ?>
<script type="text/javascript" src="/js/jquery.ui.totop.js"></script>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/js/PIE.js"></script>
    <script type="text/javascript" src="/js/ie.js"></script>
    <![endif]-->

    <?php /* tag "script" from line 106 */; ?>
<script type="text/javascript" src="/js/global.js"></script>
    <!-- MAIN JAVASCRIPTS END -->

    <!-- LOADING SCRIPT -->
    <?php /* tag "script" from line 110 */; ?>
<script>
    $(window).load(function(){
        $("#loading").fadeOut(function(){
            $(this).remove();
            $('body').removeAttr('style');
        });
    });
    </script>
    <!-- LOADING SCRIPT -->

</body>
</html><?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/common/login.zpt (edit that file instead) */; ?>