<?php 
function tpl_4fc87811_index__AwqtNWkphlcs6QoYJNtheA(PHPTAL $tpl, PHPTAL_Context $ctx) {
$_thistpl = $tpl ;
$_translator = $tpl->getTranslator() ;
/* tag "documentElement" from line 1 */ ;
/* tag "tal:block" from line 1 */ ;
?>

<?php /* tag "link" from line 2 */; ?>
<link rel="stylesheet" href="/css/dashboard/style.css" media="screen"/>
<?php /* tag "link" from line 3 */; ?>
<link rel="stylesheet" href="/css/dashboard/barchart.css" media="screen"/>
<?php /* tag "script" from line 4 */; ?>
<script language="javascript" src="/js/jquery-ui-1.8.16.custom.min.js"></script>
<?php /* tag "script" from line 5 */; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	  // for each result row..
	  $(".extras_result").each(function() {
	    // get the width of the bar from the span html
	    var length = $(this).find("span").html();
	    $(this).find("p").animate({'width':length}, 2000, function() {
	      // once the bar animation has finished, fade in the results
	      $(this).find("span").fadeIn(800);
	    });
	  });
	});
</script>
	<?php /* tag "h1" from line 18 */; ?>
<h1 class="page-title"><?php /* tag "span" from line 18 */; ?>
<span style="float:left;">Dashboard</span></h1>
	<?php /* tag "div" from line 19 */; ?>
<div id="folderTab"><?php /* tag "div" from line 19 */; ?>
<div class="folderTabText">Hello World</div></div>
	<?php /* tag "div" from line 20 */; ?>
<div class="folderContainer">
		<?php /* tag "div" from line 21 */; ?>
<div class="tbl">
			<?php /* tag "table" from line 22 */; ?>
<table>
				<?php /* tag "tr" from line 23 */; ?>
<tr>
					<?php /* tag "td" from line 24 */; ?>
<td>
						<?php /* tag "table" from line 25 */; ?>
<table style="width:200px;">
							<?php /* tag "tr" from line 26 */; ?>
<tr>
								<?php /* tag "th" from line 27 */; ?>
<th colspan="2">By Hour</th>
							</tr>
							<?php /* tag "tr" from line 29 */; ?>
<tr>
								<?php /* tag "td" from line 30 */; ?>
<td>
									<?php /* tag "table" from line 31 */; ?>
<table style="width:75px;">
										<?php /* tag "tr" from line 32 */; ?>
<tr><?php /* tag "td" from line 32 */; ?>
<td>12 am</td><?php /* tag "td" from line 32 */; ?>
<td>141</td></tr>
										<?php /* tag "tr" from line 33 */; ?>
<tr><?php /* tag "td" from line 33 */; ?>
<td>01 am</td><?php /* tag "td" from line 33 */; ?>
<td>142</td></tr>
										<?php /* tag "tr" from line 34 */; ?>
<tr><?php /* tag "td" from line 34 */; ?>
<td>02 am</td><?php /* tag "td" from line 34 */; ?>
<td>141</td></tr>
										<?php /* tag "tr" from line 35 */; ?>
<tr><?php /* tag "td" from line 35 */; ?>
<td>03 am</td><?php /* tag "td" from line 35 */; ?>
<td>144</td></tr>
										<?php /* tag "tr" from line 36 */; ?>
<tr><?php /* tag "td" from line 36 */; ?>
<td>04 am</td><?php /* tag "td" from line 36 */; ?>
<td>145</td></tr>
										<?php /* tag "tr" from line 37 */; ?>
<tr><?php /* tag "td" from line 37 */; ?>
<td>05 am</td><?php /* tag "td" from line 37 */; ?>
<td>142</td></tr>
										<?php /* tag "tr" from line 38 */; ?>
<tr><?php /* tag "td" from line 38 */; ?>
<td>06 am</td><?php /* tag "td" from line 38 */; ?>
<td>147</td></tr>
										<?php /* tag "tr" from line 39 */; ?>
<tr><?php /* tag "td" from line 39 */; ?>
<td>07 am</td><?php /* tag "td" from line 39 */; ?>
<td>145</td></tr>
										<?php /* tag "tr" from line 40 */; ?>
<tr><?php /* tag "td" from line 40 */; ?>
<td>08 am</td><?php /* tag "td" from line 40 */; ?>
<td>143</td></tr>
										<?php /* tag "tr" from line 41 */; ?>
<tr><?php /* tag "td" from line 41 */; ?>
<td>09 am</td><?php /* tag "td" from line 41 */; ?>
<td>143</td></tr>
										<?php /* tag "tr" from line 42 */; ?>
<tr><?php /* tag "td" from line 42 */; ?>
<td>10 am</td><?php /* tag "td" from line 42 */; ?>
<td>144</td></tr>
										<?php /* tag "tr" from line 43 */; ?>
<tr><?php /* tag "td" from line 43 */; ?>
<td>11 am</td><?php /* tag "td" from line 43 */; ?>
<td>145</td></tr>
									</table>
								</td>
								<?php /* tag "td" from line 46 */; ?>
<td>
									<?php /* tag "table" from line 47 */; ?>
<table style="width:75px;">
										<?php /* tag "tr" from line 48 */; ?>
<tr><?php /* tag "td" from line 48 */; ?>
<td>12 pm</td><?php /* tag "td" from line 48 */; ?>
<td>144</td></tr>
										<?php /* tag "tr" from line 49 */; ?>
<tr><?php /* tag "td" from line 49 */; ?>
<td>01 pm</td><?php /* tag "td" from line 49 */; ?>
<td>147</td></tr>
										<?php /* tag "tr" from line 50 */; ?>
<tr><?php /* tag "td" from line 50 */; ?>
<td>02 pm</td><?php /* tag "td" from line 50 */; ?>
<td>149</td></tr>
										<?php /* tag "tr" from line 51 */; ?>
<tr><?php /* tag "td" from line 51 */; ?>
<td>03 pm</td><?php /* tag "td" from line 51 */; ?>
<td>124</td></tr>
										<?php /* tag "tr" from line 52 */; ?>
<tr><?php /* tag "td" from line 52 */; ?>
<td>04 pm</td><?php /* tag "td" from line 52 */; ?>
<td>114</td></tr>
										<?php /* tag "tr" from line 53 */; ?>
<tr><?php /* tag "td" from line 53 */; ?>
<td>05 pm</td><?php /* tag "td" from line 53 */; ?>
<td>134</td></tr>
										<?php /* tag "tr" from line 54 */; ?>
<tr><?php /* tag "td" from line 54 */; ?>
<td>06 pm</td><?php /* tag "td" from line 54 */; ?>
<td>124</td></tr>
										<?php /* tag "tr" from line 55 */; ?>
<tr><?php /* tag "td" from line 55 */; ?>
<td>07 pm</td><?php /* tag "td" from line 55 */; ?>
<td>144</td></tr>
										<?php /* tag "tr" from line 56 */; ?>
<tr><?php /* tag "td" from line 56 */; ?>
<td>08 pm</td><?php /* tag "td" from line 56 */; ?>
<td>134</td></tr>
										<?php /* tag "tr" from line 57 */; ?>
<tr><?php /* tag "td" from line 57 */; ?>
<td>09 pm</td><?php /* tag "td" from line 57 */; ?>
<td>114</td></tr>
										<?php /* tag "tr" from line 58 */; ?>
<tr><?php /* tag "td" from line 58 */; ?>
<td>10 pm</td><?php /* tag "td" from line 58 */; ?>
<td>164</td></tr>
										<?php /* tag "tr" from line 59 */; ?>
<tr><?php /* tag "td" from line 59 */; ?>
<td>11 pm</td><?php /* tag "td" from line 59 */; ?>
<td>184</td></tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<?php /* tag "div" from line 68 */; ?>
<div id="bargraph">
			<?php /* tag "table" from line 69 */; ?>
<table cellpadding="0" cellspacing="0" class="extras_table">
				<?php
					$a[0]=100;
					$a[1]=100;
					$a[2]=200;
					$a[3]=100;
					$t = array_sum($a);
					
					for($i=0; $i<count($a);$i++){
						$p = $a[$i]/$t*100;
						echo '<tr>
								<th  class="extras_y-desc" scope="row">2012-05-01</th>
								<td><div class="extras_result"><p class="extras_p">&nbsp;<span>'.$p.'%</span></p>2033</div></td>
							</tr>
						';
					}
				?>
				<?php /* tag "tr" from line 86 */; ?>
<tr>
					<?php /* tag "th" from line 87 */; ?>
<th class="extras_y-desc" scope="row">2012-05-01</th>
					<?php /* tag "td" from line 88 */; ?>
<td><?php /* tag "div" from line 88 */; ?>
<div class="extras_result"><?php /* tag "p" from line 88 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 88 */; ?>
<span>90%</span></p>2033</div></td>
				</tr>
				<?php /* tag "tr" from line 90 */; ?>
<tr>
					<?php /* tag "th" from line 91 */; ?>
<th class="extras_y-desc" scope="row">2012-05-02</th>
					<?php /* tag "td" from line 92 */; ?>
<td><?php /* tag "div" from line 92 */; ?>
<div class="extras_result"><?php /* tag "p" from line 92 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 92 */; ?>
<span>20%</span></p>23</div></td>
				</tr>
				<?php /* tag "tr" from line 94 */; ?>
<tr>
					<?php /* tag "th" from line 95 */; ?>
<th class="extras_y-desc" scope="row">2012-05-03</th>
					<?php /* tag "td" from line 96 */; ?>
<td><?php /* tag "div" from line 96 */; ?>
<div class="extras_result"><?php /* tag "p" from line 96 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 96 */; ?>
<span>90%</span></p>2033</div></td>
				</tr>
				<?php /* tag "tr" from line 98 */; ?>
<tr>
					<?php /* tag "th" from line 99 */; ?>
<th class="extras_y-desc" scope="row">2012-05-04</th>
					<?php /* tag "td" from line 100 */; ?>
<td><?php /* tag "div" from line 100 */; ?>
<div class="extras_result"><?php /* tag "p" from line 100 */; ?>
<p class="extras_p">&nbsp;<?php /* tag "span" from line 100 */; ?>
<span>20%</span></p>23</div></td>
				</tr>
			</table>
		</div>
	</div>
<?php 
/* end */ ;

}

?>
<?php /* 
*** DO NOT EDIT THIS FILE ***

Generated by PHPTAL from /home/jerwin/web/cloudmsngrSite/cloudmsngr/manager/application/views/dashboard/index.zpt (edit that file instead) */; ?>