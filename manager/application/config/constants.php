<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define('SERVICES', 'services');
define('CONTENTS', 'contents');
define('KEYWORDS', 'keywords');
define('SERVICE_ROUTES', 'service_routes');
define('ACTIONS', 'actions');
define('ACCESS_NUMBERS', 'access_number');
define('SCENARIOS', 'scenarios');
define('ACTION_CONFIGURATIONS', 'action_configurations');
define('ACTION_DOCUMENTATIONS', 'action_documentations');
define('ACTION_GROUPS', 'action_groups');
define('SCHEDULED_CONTENTS', 'scheduled_contents');
define('CONTENT_GROUPS', 'content_groups');
define('SCENARIO_CATEGORIES', 'scenario_categories');
define('KEYWORD_OPERATORS', 'keyword_operators');
define('SERVICE_TYPES', 'service_types');
define('SERVICE_GROUPS', 'service_groups');
define('TAGGING', 'tagging');
define('BILLING_INFO', 'billing_info');
define('BILLING_SERVICE_NUM', 'billing_service_num');
define('BILLING_REVSHARE', 'billing_revshare');
define('ERROR_CODES', 'error_codes');
define('OPERATORS', 'operators');
define('CONTENT_TYPES', 'content_types');
define('CONTENT_SUBTYPES', 'content_sub_types');
define('CONTENT_CATEGORIES', 'content_categories');
define('CONTENT_METADATA', 'content_metadata');

define('TAG_SERVICE_GROUPS', 'tag_service_groups');
define('TAG_SERVICE_SUB_GROUPS', 'tag_service_sub_groups');
define('TAG_SERVICES', 'tag_services');
define('TAG_KEYWORDS', 'tag_keywords');
define('TAG_SUB_KEYWORDS', 'tag_sub_keywords');
define('TAG_TRANSACTION_TYPES', 'tag_transaction_types');

define('TABLE_MANAGER', 'table_manager');
define('TABLE_TYPES', 'user_input_types');
define('TABLE_FIELDS', 'user_input_fields');
define('TABLES', 'user_input_tables');
define('TABLE_INPUT_FIELDS', 'user_input_table_fields');

define('TARIFF_RATES', 'tariff_rates');

define('MSISDN_INFO', 'msisdn_info');
define('USER_INPUT_TABLES', 'user_input_tables');
define('USER_INPUT_TYPES', 'user_input_types');

define('KEYWORD_ALIASES', 'keyword_aliases');

define('SHOWS', 'shows');
define('SHOW_ALIASES', 'show_aliases');
define('USER_SHOWS', 'user_shows');
define('USER_CONTENTS', 'user_contents');

define('USER_BBVOTING_HANDLER', 'user_bbvoting_handler');
define('BBVOTING_HANDLER', 'bbvoting_handler');
define('BBVOTING_TABLE_DEFINITION', 'bbvoting_table_definition');
define('BUILDR_INSTANCE', 'buildr_instances');

/* End of file constants.php */
/* Location: ./application/config/constants.php */