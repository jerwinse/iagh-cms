<?php
$config['itemperpage'] = 5;

$config['access_numbers'] = array('mobile_access_num', 'twitter_access_num', 'ym_access_num', 'xmpp_access_num', 'facebook_access_num');
$config['operators'] = array('mobile_access_num' => '2910', 'twitter_access_num' => 'twitter', 'ym_access_num' => 'ym', 'xmpp_access_num' => 'xmpp', 'facebook_access_num' => 'facebook');

// Services default variables
$config['services']['service_group_id'] = 0;
$config['services']['alias'] = '';
$config['services']['service_type_id'] = '';
$config['services']['active'] = 1;
$config['services']['params'] = serialize(array());
$config['services']['create_by'] = 'CloudMsngr';
$config['services']['create_date'] = date('Y-m-d H:i:s');

$config['kg_a_keywords']['eval_rule'] = 1;
$config['kg_a_keywords']['service_num_suffix'] = '';
$config['kg_a_keywords']['priority'] = 10;
$config['kg_a_keywords']['validator'] = '';
$config['kg_a_keywords']['is_active'] = 1;
$config['kg_a_keywords']['is_main'] = 0;
$config['kg_a_keywords']['request_type_id'] = 0;
$config['kg_a_keywords']['whitelist'] = '';
$config['kg_a_keywords']['blacklist'] = '';
$config['kg_a_keywords']['sourcetype'] = 's';

$config['kg_b_keywords']['eval_rule'] = 1;
$config['kg_b_keywords']['service_num_suffix'] = '';
$config['kg_b_keywords']['priority'] = 10;
$config['kg_b_keywords']['validator'] = '';
$config['kg_b_keywords']['is_active'] = 1;
$config['kg_b_keywords']['is_main'] = 0;
$config['kg_b_keywords']['request_type_id'] = 0;
$config['kg_b_keywords']['whitelist'] = '';
$config['kg_b_keywords']['blacklist'] = '';
$config['kg_b_keywords']['sourcetype'] = 's';

$config['kg_c_keywords']['eval_rule'] = 1;
$config['kg_c_keywords']['service_num_suffix'] = '';
$config['kg_c_keywords']['priority'] = 10;
$config['kg_c_keywords']['validator'] = '';
$config['kg_c_keywords']['is_active'] = 1;
$config['kg_c_keywords']['is_main'] = 0;
$config['kg_c_keywords']['request_type_id'] = 0;
$config['kg_c_keywords']['whitelist'] = '';
$config['kg_c_keywords']['blacklist'] = '';
$config['kg_c_keywords']['sourcetype'] = 's';

$config['kg_d_keywords']['eval_rule'] = 1;
$config['kg_d_keywords']['service_num_suffix'] = '';
$config['kg_d_keywords']['priority'] = 10;
$config['kg_d_keywords']['validator'] = '';
$config['kg_d_keywords']['is_active'] = 1;
$config['kg_d_keywords']['is_main'] = 0;
$config['kg_d_keywords']['request_type_id'] = 0;
$config['kg_d_keywords']['whitelist'] = '';
$config['kg_d_keywords']['blacklist'] = '';
$config['kg_d_keywords']['sourcetype'] = 's';

$config['kg_e_keywords']['eval_rule'] = 3;
$config['kg_e_keywords']['service_num_suffix'] = '';
$config['kg_e_keywords']['priority'] = 10;
$config['kg_e_keywords']['validator'] = '';
$config['kg_e_keywords']['is_active'] = 1;
$config['kg_e_keywords']['is_main'] = 0;
$config['kg_e_keywords']['request_type_id'] = 0;
$config['kg_e_keywords']['whitelist'] = '';
$config['kg_e_keywords']['blacklist'] = '';
$config['kg_e_keywords']['sourcetype'] = 's';

$config['kg_f_keywords']['eval_rule'] = 0;
$config['kg_f_keywords']['service_num_suffix'] = '';
$config['kg_f_keywords']['priority'] = 10;
$config['kg_f_keywords']['validator'] = '';
$config['kg_f_keywords']['is_active'] = 1;
$config['kg_f_keywords']['is_main'] = 0;
$config['kg_f_keywords']['request_type_id'] = 0;
$config['kg_f_keywords']['whitelist'] = '';
$config['kg_f_keywords']['blacklist'] = '';
$config['kg_f_keywords']['sourcetype'] = 's';

$config['kg_g_keywords']['eval_rule'] = 1;
$config['kg_g_keywords']['service_num_suffix'] = '';
$config['kg_g_keywords']['priority'] = 10;
$config['kg_g_keywords']['validator'] = '';
$config['kg_g_keywords']['is_active'] = 1;
$config['kg_g_keywords']['is_main'] = 0;
$config['kg_g_keywords']['request_type_id'] = 0;
$config['kg_g_keywords']['whitelist'] = 'CLOUDBOT';
$config['kg_g_keywords']['blacklist'] = '';
$config['kg_g_keywords']['sourcetype'] = 's';

$config['kg_h_keywords']['eval_rule'] = 1;
$config['kg_h_keywords']['service_num_suffix'] = '';
$config['kg_h_keywords']['priority'] = 10;
$config['kg_h_keywords']['validator'] = '';
$config['kg_h_keywords']['is_active'] = 1;
$config['kg_h_keywords']['is_main'] = 0;
$config['kg_h_keywords']['request_type_id'] = 0;
$config['kg_h_keywords']['whitelist'] = 'CLOUDBOT';
$config['kg_h_keywords']['blacklist'] = '';
$config['kg_h_keywords']['sourcetype'] = 's';

$config['kg_i_keywords']['eval_rule'] = 1;
$config['kg_i_keywords']['service_num_suffix'] = '';
$config['kg_i_keywords']['priority'] = 10;
$config['kg_i_keywords']['validator'] = '';
$config['kg_i_keywords']['is_active'] = 1;
$config['kg_i_keywords']['is_main'] = 0;
$config['kg_i_keywords']['request_type_id'] = 0;
$config['kg_i_keywords']['whitelist'] = '';
$config['kg_i_keywords']['blacklist'] = '';
$config['kg_i_keywords']['sourcetype'] = 's';

$config['kg_j_keywords']['eval_rule'] = 1;
$config['kg_j_keywords']['service_num_suffix'] = '';
$config['kg_j_keywords']['priority'] = 10;
$config['kg_j_keywords']['validator'] = '';
$config['kg_j_keywords']['is_active'] = 1;
$config['kg_j_keywords']['is_main'] = 0;
$config['kg_j_keywords']['request_type_id'] = 0;
$config['kg_j_keywords']['whitelist'] = '';
$config['kg_j_keywords']['blacklist'] = '';
$config['kg_j_keywords']['sourcetype'] = 's';

$config['kg_k_keywords']['eval_rule'] = 1;
$config['kg_k_keywords']['service_num_suffix'] = '';
$config['kg_k_keywords']['priority'] = 10;
$config['kg_k_keywords']['validator'] = '';
$config['kg_k_keywords']['is_active'] = 1;
$config['kg_k_keywords']['is_main'] = 0;
$config['kg_k_keywords']['request_type_id'] = 0;
$config['kg_k_keywords']['whitelist'] = '';
$config['kg_k_keywords']['blacklist'] = '';
$config['kg_k_keywords']['sourcetype'] = 's';

$config['scenarios']['scenario_category_id'] = 0;
$config['scenarios']['on_eval_scenario'] =  0;
$config['scenarios']['on_mo'] =  0;
$config['scenarios']['on_dlr'] =  0;
$config['scenarios']['on_dlr_nack'] = 0;
$config['scenarios']['is_cacheable'] =  0;
$config['scenarios']['is_sync'] =  0;

$config['action_group']['description'] = '';
$config['action_group']['exec_method'] = 0;
$config['action_group']['event'] = 0;