<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|				 NOTE: For MySQL and MySQLi databases, this setting is only used
| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
|				 (and in table creation queries made with DB Forge).
| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
| 				 can make your site vulnerable to SQL injection if you are using a
| 				 multi-byte character set and are running versions lower than these.
| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group = 'acl';
$active_record = TRUE;

$db['acl']['hostname'] = '10.145.150.19';
$db['acl']['username'] = 'jerwin';
$db['acl']['password'] = 'warlord';
$db['acl']['database'] = 'iagh_cms_acl';
$db['acl']['dbdriver'] = 'mysql';
$db['acl']['dbprefix'] = '';
$db['acl']['pconnect'] = TRUE;
$db['acl']['db_debug'] = FALSE;
$db['acl']['cache_on'] = FALSE;
$db['acl']['cachedir'] = '';
$db['acl']['char_set'] = 'utf8';
$db['acl']['dbcollat'] = 'utf8_general_ci';
$db['acl']['swap_pre'] = '';
$db['acl']['autoinit'] = TRUE;
$db['acl']['stricton'] = FALSE;

$db['system']['hostname'] = '10.145.150.19';
$db['system']['username'] = 'jerwin';
$db['system']['password'] = 'warlord';
$db['system']['database'] = 'iagh_cms_system';
$db['system']['dbdriver'] = 'mysql';
$db['system']['dbprefix'] = '';
$db['system']['pconnect'] = TRUE;
$db['system']['db_debug'] = FALSE;
$db['system']['cache_on'] = FALSE;
$db['system']['cachedir'] = '';
$db['system']['char_set'] = 'utf8';
$db['system']['dbcollat'] = 'utf8_general_ci';
$db['system']['swap_pre'] = '';
$db['system']['autoinit'] = TRUE;
$db['system']['stricton'] = FALSE;

//$db['trans']['hostname'] = '127.0.0.1';
//$db['trans']['username'] = 'jerwin';
//$db['trans']['password'] = 'warlord';
//$db['trans']['database'] = 'transaction_db_DEV';
//$db['trans']['dbdriver'] = 'mysql';
//$db['trans']['dbprefix'] = '';
//$db['trans']['pconnect'] = TRUE;
//$db['trans']['db_debug'] = FALSE;
//$db['trans']['cache_on'] = FALSE;
//$db['trans']['cachedir'] = '';
//$db['trans']['char_set'] = 'utf8';
//$db['trans']['dbcollat'] = 'utf8_general_ci';
//$db['trans']['swap_pre'] = '';
//$db['trans']['autoinit'] = TRUE;
//$db['trans']['stricton'] = FALSE;
//
//$db['report']['hostname'] = '127.0.0.1';
//$db['report']['username'] = 'jerwin';
//$db['report']['password'] = 'warlord';
//$db['report']['database'] = 'cloudmsngr_report_db';
//$db['report']['dbdriver'] = 'mysql';
//$db['report']['dbprefix'] = '';
//$db['report']['pconnect'] = TRUE;
//$db['report']['db_debug'] = FALSE;
//$db['report']['cache_on'] = FALSE;
//$db['report']['cachedir'] = '';
//$db['report']['char_set'] = 'utf8';
//$db['report']['dbcollat'] = 'utf8_general_ci';
//$db['report']['swap_pre'] = '';
//$db['report']['autoinit'] = TRUE;
//$db['report']['stricton'] = FALSE;


/* End of file database.php */
/* Location: ./application/config/database.php */