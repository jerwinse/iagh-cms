<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['filter']['services'] = array(
	"id",
	"name",
	"service_group_id",
	"alias",
	"service_type_id",
	"end_date",
	"active",
	"params"
);

$config['filter']['service_routes'] = array(
	"id",
	"service_id",
	"description",
	"v_order"
);

$config['filter']['scenarios'] = array(
	"id",
	"service_route_id",
	"scenario_category_id",
	"description",
	"v_order",
	"on_eval_scenario",
	"on_mo",
	"on_dlr",
	"on_dlr_nack",
	"is_cacheable",
	"is_sync"
);

$config['filter']['keywords'] = array(
	"id",
	"keyword",
	"eval_rule",
	"operator",
	"service_num",
	"service_num_suffix",
	"priority",
	"is_active",
	"is_main",
	"request_type_id",
	"service_route_id",
	"timestamp",
	"whitelist",
	"blacklist",
	"sourcetype"
);

$config['filter']['action_configurations'] = array(
	"id",
	"action_group_id",
	"action_id",
	"content_id",
	"bill_info",
	"event_trap",
	"params",
	"v_order"
);

$config['filter']['actions'] = array(
	"id",
	"description",
	"input_params",
	"type",
	"class_name",
	"filename",
	"name"
);

$config['filter']['billing_info'] = array(
	"id",
	"code",
	"operator",
	"service_num",
	"tariffid_a",
	"tariffid_b",
	"tariff",
	"revshare"
);
/*
$config['filter']['content_groups'] = array(
	"id",
	"description"
);
*/

$config['filter']['contents'] = array(
	"id",
	"description",
	"product_code",
	"content",
	"type",
	"sub_type"
);

$config['filter']['error_codes'] = array(
	"id",
	"operator",
	"smsc",
	"code",
	"description",
	"retry_rule",
	"error_class"
);

$config['filter']['keyword_operators'] = array(
	"id",
	"keyword_id",
	"operator"
);

$config['filter']['operators'] = array(
	"id",
	"operator"
);

$config['filter']['scheduled_contents'] = array(
	"id",
	"content_group_id",
	"product_code",
	"start_date",
	"end_date",
	"timestamp",
	"priority"
);

$config['filter']['tagging'] = array(
	"id",
	"service_id",
	"scenario_id",
	"description",
	"tagging",
	"criteria",
	"chunk"
);
