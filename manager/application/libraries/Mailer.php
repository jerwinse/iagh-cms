<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 * @author        	Jerwin Espiritu
 * @license         Solucient Inc.
 * @link			http://solucientinc.com
 */
class Mailer {
	
	private $message = "";
	private $subject = "";
	private $from = "";
	private $to = "";
	private $cc = "";
	private $name = "";
	private $replyTo = "";
	public $output = 0;
	
	public function __construct(){
		
	}
	
	public function mailName($param=""){
		$this->name = $param;
	}
	
	public function setMessage($param){
		if(is_array($param)){
			$this->message = implode("\n", $param);
		}
		else{
			$this->message = $param;
		}
	}
	
	public function setSubject($param=""){
			$this->subject = $param;
	}
	
	public function setFrom($param){
		if(is_array($param)){
			$this->from = implode(",", $param);
		}
		else{
			$this->from = $param;
		}
		
	}
	
	public function setTo($param){
		if(is_array($param)){
			$this->to = implode(",", $param);
		}
		else{
			$this->to = $param;
		}
		
	}
	
	public function setReplyTo($param){
		if(is_array($param)){
			$this->replyTo = implode(",", $param);
		}
		else{
			$this->replyTo = $param;
		}
		
	}
	
	public function setCC($param){
		if(is_array($param)){
			$this->cc = implode(",", $param);
		}
		else{
			$this->cc = $param;
		}
		
	}
	
	public function sendEmail(){
		$header = array();
	    $now = date('l jS \of F Y h:i:s A');
	    
	    $header[] = "From: ". $this->name . " <" . $this->from . ">";
	    if(!empty($this->cc)){
	    	$header[] = "Cc: ". " <" . $this->cc . ">\r\n";
	    }
	    if(!empty($this->replyTo)){
	    	$header[] = "Reply-To: ". " <" . $this->replyTo . ">\r\n";
	    }
	    $header[] = "X-Mailer: PHP/". phpversion();
	    
	    $header = implode("\r\n", $header);
	    
	    if(mail($this->to, $this->subject, $this->message, $header)){
	    	$this->output =  1;
	    }
	    else{
	    	$this->output = 0;
	    }
	}
}
