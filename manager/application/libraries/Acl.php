<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class acl
{
	var $perms = array();		//Array : Stores the permissions for the user
	var $rolePerms = array();		//Array : Stores the permissions for the user
	var $userPerms = array();		//Array : Stores the permissions for the user
	var $userID;			//Integer : Stores the ID of the current user
	var $userRoles = array();	//Array : Stores the roles of the current user
	var $ci;
	function __construct($config=array()) {
		$this->ci = &get_instance();

		$this->userID = floatval($config['userID']);
		$this->userRoles = $this->getUserRoles();
		$this->buildACL();
	}

	function buildACL() {
		//first, get the rules for the user's role
		if (count($this->userRoles) > 0)
		{
			$this->perms = array_merge($this->perms,$this->getRolePerms($this->userRoles));
			$this->rolePerms = $this->getRolePerms($this->userRoles);
		}
		//then, get the individual user permissions
		$this->perms = array_merge($this->perms,$this->getUserPerms($this->userID));
		$this->userPerms = $this->getUserPerms($this->userID);
	}

	function getPermKeyFromID($permID) {
		//$strSQL = "SELECT `permKey` FROM `".DB_PREFIX."permissions` WHERE `ID` = " . floatval($permID) . " LIMIT 1";
		$this->ci->db->select('permKey');
		$this->ci->db->where('id',floatval($permID));
		//$sql = $this->ci->db->get('perm_data',1);
		$sql = $this->ci->db->get('acl_permissions',1);
		$data = $sql->result();
		return $data[0]->permKey;
	}

	function getPermNameFromID($permID) {
		//$strSQL = "SELECT `permName` FROM `".DB_PREFIX."permissions` WHERE `ID` = " . floatval($permID) . " LIMIT 1";
		$this->ci->db->select('permName');
		$this->ci->db->where('id',floatval($permID));
		//$sql = $this->ci->db->get('perm_data',1);
		$sql = $this->ci->db->get('acl_permissions',1);
		$data = $sql->result();
		return $data[0]->permName;
	}

	function getRoleNameFromID($roleID) {
		//$strSQL = "SELECT `roleName` FROM `".DB_PREFIX."roles` WHERE `ID` = " . floatval($roleID) . " LIMIT 1";
		$this->ci->db->select('roleName');
		$this->ci->db->where('id',floatval($roleID),1);
		//$sql = $this->ci->db->get('role_data');
		$sql = $this->ci->db->get('acl_roles');
		$data = $sql->result();
		return $data[0]->roleName;
	}

	function getUserRoles($id = 0) {
		//$strSQL = "SELECT * FROM `".DB_PREFIX."user_roles` WHERE `userID` = " . floatval($this->userID) . " ORDER BY `addDate` ASC";

		if ($id == 0)
			$this->ci->db->where(array('userID'=>floatval($this->userID)));
		else 
			$this->ci->db->where(array('userID'=>floatval($id)));
		$this->ci->db->order_by('addDate','asc');
		$sql = $this->ci->db->get('acl_user_roles');
		$data = $sql->result();

		$resp = array();
		foreach( $data as $row )
		{
			$resp[] = $row->roleID;
		}
		return $resp;
	}
	
	function getRoleMembers($roleId=0){
		if($roleId>0){
			$this->ci->db->where("roleID", $roleId);
			$sql = $this->ci->db->get('acl_user_roles');
			if($sql->num_rows()>0){
				$data = $sql->result();
			}
			else {
				$data = array();
			}
			return $data;
		}
	}

	# added by jerwin espiritu
	function getAllRoles($format='ids') {
		$format = strtolower($format);
		//$strSQL = "SELECT * FROM `".DB_PREFIX."roles` ORDER BY `roleName` ASC";
		$this->ci->db->order_by('roleName','asc');
		//$sql = $this->ci->db->get('role_data');
		$sql = $this->ci->db->get('acl_roles');
		$data = $sql->result();

		$resp = array();
		foreach( $data as $row )
		{
			if ($format == 'full')
			{
				$resp[] = array("id" => $row->ID,"name" => $row->roleName);
			} else {
				$resp[] = $row->ID;
			}
		}
		return $resp;
	}
	
	function updateRolePermission($roleId = 0, $permissionId = 0){
		if($roleId>0 && $permissionId>0){
			$this->ci->db->where("roleID", $roleId);
			$this->ci->db->update('acl_role_perms', array('permID'=>$permissionId));
		}		
	}
	
	function removeRole($roleId){
		if($roleId>0){
			$this->ci->db->delete('acl_roles', array('ID'=>$roleId));
		}
	}
	
	function removeRolePermission($roleId){
		if($roleId>0){
			$this->ci->db->delete('acl_roles_perms', array('ID'=>$roleId));
		}		
	}
	#  ---------------- 

	function getAllPerms($format='ids') {
		$format = strtolower($format);
		//$strSQL = "SELECT * FROM `".DB_PREFIX."permissions` ORDER BY `permKey` ASC";

		$this->ci->db->order_by('id','asc');
		//$sql = $this->ci->db->get('perm_data');
		$sql = $this->ci->db->get('acl_permissions');
		$data = $sql->result();

		$resp = array();
		foreach( $data as $row )
		{
			if ($format == 'full')
			{
				$resp[$row->permKey] = array('id' => $row->ID, 'name' => $row->permName, 'key' => $row->permKey);
			} else {
				$resp[] = $row->ID;
			}
		}
		return $resp;
	}

	function getRolePerms($role) {
		if (is_array($role))
		{
			//$roleSQL = "SELECT * FROM `".DB_PREFIX."role_perms` WHERE `roleID` IN (" . implode(",",$role) . ") ORDER BY `ID` ASC";
			$this->ci->db->where_in('roleID',$role);
		} else {
			//$roleSQL = "SELECT * FROM `".DB_PREFIX."role_perms` WHERE `roleID` = " . floatval($role) . " ORDER BY `ID` ASC";
			$this->ci->db->where(array('roleID'=>floatval($role)));

		}
		$this->ci->db->order_by('id','asc');
		$sql = $this->ci->db->get('acl_role_perms'); //$this->db->select($roleSQL);
		$data = $sql->result();
		$perms = array();
		foreach( $data as $row )
		{
			$pK = strtolower($this->getPermKeyFromID($row->permID));
			if ($pK == '') { continue; }
			if ($row->value === '1') {
				$hP = true;
			} else {
				$hP = false;
			}
			$perms[] = array('perm' => $pK,'inheritted' => true,'value' => $hP,'name' => $this->getPermNameFromID($row->permID),'id' => $row->permID);
		}
		return $perms;
	}

	function getUserPerms($userID) {
		//$strSQL = "SELECT * FROM `".DB_PREFIX."user_perms` WHERE `userID` = " . floatval($userID) . " ORDER BY `addDate` ASC";
		$perms = array();
		if($userID){
			$this->ci->db->where('userID',floatval($userID));
			$this->ci->db->order_by('addDate','asc');
			$sql = $this->ci->db->get('acl_user_perms');
			if($sql){
				$data = $sql->result();
				$perms = array();
				foreach( $data as $row )
				{
					$pK = strtolower($this->getPermKeyFromID($row->permID));
					if ($pK == '') { continue; }
					if ($row->value == '1') {
						$hP = true;
					} else {
						$hP = false;
					}
					$perms[] = array('perm' => $pK,'inheritted' => false,'value' => $hP,'name' => $this->getPermNameFromID($row->permID),'id' => $row->permID);
				}
				
			}
		}
		return $perms;
	}
	
	function hasRole($roleID) {
		foreach($this->userRoles as $k => $v)
		{
			if (floatval($v) === floatval($roleID))
			{
				return true;
			}
		}
		return false;
	}

	function hasPermission($permKey) {
		//debug($this->perms);
		$permKey = strtolower($permKey);
		if (array_key_exists($permKey,$this->perms))
		{
			if ($this->perms[$permKey]['value'] === '1' || $this->perms[$permKey]['value'] === true)
			{
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	function getAllUsers($format='ids') {
		$format = strtolower($format);

		$this->ci->db->order_by('username','asc');
		//$sql = $this->ci->db->get('perm_data');
		$sql = $this->ci->db->get('acl_users');
		$data = $sql->result();

		$resp = array();
		foreach( $data as $row )
		{
			if ($format == 'full'){
				$role = self::getUserRoles($row->id);
				$str_role = '';
				foreach ($role as $r)
				{
					$role_name = self::getRoleNameFromID($r);
					$str_role .= $role_name . ', ';
				}
				$str_role = trim($str_role, ', ');				
				$resp[] = array('id' => $row->id, 'role' => $str_role, 'username' => $row->username,  'last_name' => $row->last_name,  'first_name' => $row->first_name, 'email' => $row->email, 'account_num' => $row->account_num);
			} else {
				$resp[] = $row->id;
			}
		}
		return $resp;
	}
	
	# added by jerwin espiritu
	function getAllUsersByRole($roleId = 0){
		$resp = array();
		if($roleId){
			# get userId
			$sql = "select ar.roleName as role, u.* from (
                      select au.id, au.username, au.last_name, au.first_name, au.email, aur.roleID from acl_users as au , acl_user_roles as aur where au.id = aur.userID and aur.roleID = {$roleId}
                    ) as u left join acl_roles as ar on ar.ID = u.roleID";
			$query = $this->ci->db->query($sql);
			if($query->num_rows()>0) {
				$resp = $query->result();
				return $resp;
			}
			else{
				return $resp;
			}
		}
		else {
			return $resp;
		}
	}
	#
	
	function createRole($data)
	{
		$insert_id = 0;
		if (!empty($data))
		{
			$this->ci->db->insert('acl_roles', $data);
			$insert_id = $this->ci->db->insert_id();
		}
		return $insert_id;
	}
	function createRolePermission($data = array()){
		if($data){
			$this->ci->db->insert('acl_role_perms', $data);
			$insert_id = $this->ci->db->insert_id();
		}
		return $insert_id;
	}
	
	function updateRole($data, $reference){
		if(!empty($data)){
			$this->ci->db->where("ID", $reference);
			$this->ci->db->update('acl_roles', $data);
		}
	}
	
	function createPermission($data)
	{
		$insert_id = 0;
		if (!empty($data))
		{
			$this->ci->db->insert('acl_permissions', $data);
			$insert_id = $this->ci->db->insert_id();
		}
		return $insert_id;
	}
	
	function assignUserRole($userID, $roleID)
	{
		$this->ci->db->where('userID', $userID);
		$this->ci->db->delete('acl_user_roles');
		
		$this->ci->db->insert('acl_user_roles', array('userID' => $userID, 'roleID' => $roleID, 'addDate' => date('Y-m-d H:i:s')));
	}
	
	function assignUserPermission($userID, $permissionID)
	{
		$this->ci->db->where('userID', $userID);
		$this->ci->db->delete('acl_user_perms');
		
		$this->ci->db->insert('acl_user_perms', array('userID' => $userID, 'permID' => $permissionID, 'addDate' => date('Y-m-d H:i:s')));
	}
	
	function removeUserPermission($userID){
		$this->ci->db->where('userID', $userID);
		$this->ci->db->delete('acl_user_perms');
	}
	
	function getUser($userID){
		$this->ci->db->where('id',$userID);
		$sql = $this->ci->db->get('acl_users');
		if($sql->num_rows()>0){
			return $sql->result();
		}
		else{
			return array();
		}
	}
	
	
}