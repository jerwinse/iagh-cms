<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cloudmsngr {

	var $service_id, $poll_table, $feedback_table, $registration_table, $content_category_id, $service_group_id;
	var $account_data = array();
	
    public function __construct($params)
    {
        $this->ci =& get_instance();    
        $this->account_data = $params;
        $this->ci->config->load('cloudmsngr');           
    }
	
    public function CreateBasic($tenant_id)
    {
    	// Create tenant's service handler
		$this->service_id = self::CreateService($tenant_id);
		self::ReferenceAccountService($tenant_id, $this->service_id);		
		
		// Create billing information
		self::CreateBillingInformation($tenant_id);
				
    	/*
    	 * Create keyword group (Service Routes)
    	 * 	 a. Follow-Up Ticket Number
    	 * 	 b. Coupons: CODE <coupon code>
    	 * 	 c. Registration: REG <name>/<age>/<gender>/<address>/
    	 * 	 d. Poll: POLL <answer>
    	 * 	 e. Invalid Keyword
    	 * 	 f. Help
    	 *   g. AUTOFOLLOW
    	 *   h. Feedback Admin Reply
    	 *   i. CloudMsngr Touch Point Enrollment
    	 *   j. CloudMsngr Account Link
    	 *   k. Feedback: FBACK <tenant> 
    	 */
    	
    	$this->ci->load->model('cmservice_model');
    	
    	// Prepare user input tables 1-Poll 3-Feedback 5-Registration    	
    	$this->poll_table = $this->ci->cmservice_model->CreateUserInputTable($this->account_data['main_keyword'], 1);     	
    	$this->feedback_table = $this->ci->cmservice_model->CreateUserInputTable($this->account_data['main_keyword'], 3);
    	$this->registration_table = $this->ci->cmservice_model->CreateUserInputTable($this->account_data['main_keyword'], 5);
    	
    	// Prepare Content category
    	$this->content_category_id = $this->ci->cmservice_model->CreateContentCategory($this->account_data['main_keyword']);
    	
    	// Create service group
    	$this->service_group_id = $this->ci->cmservice_model->CreateServiceGroup($this->account_data['main_keyword']);
    	
    	// Assign Content Category
    	$this->ci->cmservice_model->AssignContentCategory($tenant_id, $this->content_category_id);
    	
    	self::ServiceRouteA($this->service_id);
    	self::ServiceRouteB($this->service_id);
    	self::ServiceRouteC($this->service_id);
    	self::ServiceRouteD($this->service_id);
    	self::ServiceRouteE($this->service_id);
    	self::ServiceRouteF($this->service_id);
    	self::ServiceRouteG($this->service_id);
    	self::ServiceRouteH($this->service_id);
    	self::ServiceRouteI($this->service_id);
    	self::ServiceRouteJ($this->service_id);
    	self::ServiceRouteK($this->service_id);
    	
    	self::deploy();
    }    
    
    private function CreateService($tenant_id)
    {
    	$this->ci->load->model('cmservice_model');
    	return $this->ci->cmservice_model->CreateService($tenant_id, $this->account_data);    	    	
    }
    
    private function CreateBillingInformation($tenant_id)
    {
    	$this->ci->load->model('cmservice_model');
    	
    	// Create billing service numbers
    	//$man_code = 'F';
    	$tan_code = $this->ci->cmservice_model->CreateAccessNumber($this->account_data['twitter_access_num']);
    	$yan_code = $this->ci->cmservice_model->CreateAccessNumber($this->account_data['ym_access_num']);
    	$gan_code = $this->ci->cmservice_model->CreateAccessNumber($this->account_data['xmpp_access_num']);
    	$fan_code = $this->ci->cmservice_model->CreateAccessNumber($this->account_data['facebook_access_num']);
    	
    	// Create billing code
    	//$mo_code = 'G';
    	$to_code = 'T';
    	$yo_code = 'Y';
    	$go_code = 'X';
    	$fo_code = 'F';
    	
    	$tr_code = '0000';
    	
    	$rs_code = 'A';
    	
    	$ta_code = '00';
    	$tb_code = '00';
    	
    	// twitter bill_info
    	$twitter_bill_info = $to_code . $tan_code . $tr_code . $rs_code . $ta_code . $tb_code;
    	$ym_bill_info = $yo_code . $yan_code . $tr_code . $rs_code . $ta_code . $tb_code;
    	$xmpp_bill_info = $go_code . $gan_code . $tr_code . $rs_code . $ta_code . $tb_code;
    	$facebook_bill_info = $fo_code . $fan_code . $tr_code . $rs_code . $ta_code . $tb_code;
    	
    	// no need to create mobile billcode, use the default 2910 billcode and assign to tenant
    	$this->ci->cmservice_model->AssignBillInfo($tenant_id, 'GF0000ACSPFREE', 'mobile');
    	$this->account_data['mobile_billcode'] = 'GF0000ACSPFREE';
    	
    	$data = array(
    		'description' => $this->account_data['name'] . ' Twitter Billcode', 
    		'code' => $twitter_bill_info,
    		'operator' => 'TWITTER',
    		'service_num' => $this->account_data['twitter_access_num'],
    		'tariffid_a' => $ta_code,
    		'tariffid_b' => $tb_code,
    		'tariff' => '0.00',
    		'revshare' => '0.00',
    		'operator_code' => $to_code,
    		'service_num_code' => $tan_code,
    		'revshare_code' => 'A'
    	);    	
    	$this->ci->cmservice_model->CreateBillInfo($data);
    	$this->ci->cmservice_model->AssignBillInfo($tenant_id, $twitter_bill_info, 'twitter');
    	$this->account_data['twitter_billcode'] = $twitter_bill_info;

    	$data = array(
    		'description' => $this->account_data['name'] . ' YM Billcode', 
    		'code' => $ym_bill_info,
    		'operator' => 'YM',
    		'service_num' => $this->account_data['ym_access_num'],
    		'tariffid_a' => $ta_code,
    		'tariffid_b' => $tb_code,
    		'tariff' => '0.00',
    		'revshare' => '0.00',
    		'operator_code' => $yo_code,
    		'service_num_code' => $yan_code,
    		'revshare_code' => 'A'
    	);    	
    	$this->ci->cmservice_model->CreateBillInfo($data);
    	$this->ci->cmservice_model->AssignBillInfo($tenant_id, $ym_bill_info, 'ym');
    	$this->account_data['ym_billcode'] = $ym_bill_info;
    	
    	$data = array(
    		'description' => $this->account_data['name'] . ' Gtalk Billcode', 
    		'code' => $xmpp_bill_info,
    		'operator' => 'XMPP',
    		'service_num' => $this->account_data['xmpp_access_num'],
    		'tariffid_a' => $ta_code,
    		'tariffid_b' => $tb_code,
    		'tariff' => '0.00',
    		'revshare' => '0.00',
    		'operator_code' => $go_code,
    		'service_num_code' => $gan_code,
    		'revshare_code' => 'A'
    	);
    	$this->ci->cmservice_model->CreateBillInfo($data);
    	$this->ci->cmservice_model->AssignBillInfo($tenant_id, $xmpp_bill_info, 'xmpp');
    	$this->account_data['xmpp_billcode'] = $xmpp_bill_info;
    	
    	$data = array(
    		'description' => $this->account_data['name'] . ' Facebook Billcode', 
    		'code' => $facebook_bill_info,
    		'operator' => 'FACEBOOK',
    		'service_num' => $this->account_data['facebook_access_num'],
    		'tariffid_a' => $ta_code,
    		'tariffid_b' => $tb_code,
    		'tariff' => '0.00',
    		'revshare' => '0.00',
    		'operator_code' => $fo_code,
    		'service_num_code' => $fan_code,
    		'revshare_code' => 'A'
    	);
    	$this->ci->cmservice_model->CreateBillInfo($data);
    	$this->ci->cmservice_model->AssignBillInfo($tenant_id, $facebook_bill_info, 'facebook');
    	$this->account_data['facebook_billcode'] = $facebook_bill_info;
    }
    
    private function ReferenceAccountService($tenant_id, $service_id)
    {
    	$this->ci->load->model('cloudmsngr_model');
    	$this->ci->cloudmsngr_model->ReferenceAccountService($tenant_id, $service_id);
    }
    
    private function CreateServiceRoute($service_id, $type)
    {
    	$this->ci->load->model('cmservice_model');
    	return $this->ci->cmservice_model->CreateServiceRoute($service_id, $type, $this->account_data);
    }
    
    private function CreateKeyword($type, $service_route_id, $keyword)
    {
    	$this->ci->load->model('cmservice_model');
    	$this->ci->cmservice_model->CreateKeyword($type, $service_route_id, $keyword, $this->account_data);
    }
    
    private function CreateKeyword2($keyword)
    {
    	$this->ci->load->model('cmservice_model');
    	return $this->ci->cmservice_model->CreateKeyword2($keyword);
    }
    
    private function AssignKeywordOperator($keyword_id, $operator)
    {
    	$this->ci->load->model('cmservice_model');
    	$this->ci->cmservice_model->AssignKeywordOperator($keyword_id, $operator);
    }
    
    private function CreateScenario($order, $service_route_id, $scenario)    
    {
    	$this->ci->load->model('cmservice_model');
    	return $this->ci->cmservice_model->CreateScenario($order, $service_route_id, $scenario);
    }
    
    private function AssignEvalScenario($scenario_id, $action_group_id)
    {
    	$this->ci->load->model('cmservice_model');
    	return $this->ci->cmservice_model->AssignEvalScenario($scenario_id, $action_group_id);    	
    }
    
    private function AssignMo($scenario_id, $action_group_id)
    {
    	$this->ci->load->model('cmservice_model');
    	return $this->ci->cmservice_model->AssignMo($scenario_id, $action_group_id);    	
    }
    
    private function CreateContent($type = 'sms', $sub_type = 'text', $description = '', $content = '', $category_id)
    {
    	$this->ci->load->model('cmservice_model');
		$product_code = $this->ci->cmservice_model->GenerateProductCode($type);
		$content = array(
			'name' => $this->account_data['main_keyword'] . ' ' . $description,
			'description' => $this->account_data['main_keyword'] . ' ' . $description,
			'product_code' => $product_code,
			'content' => $content,
			'type' => $type,
			'sub_type' => $sub_type,
			'category_id' => $category_id,
			'start_date' => '0000-00-00',
			'end_date' => '0000-00-00'
		);		
    	$this->ci->cmservice_model->CreateContent($content);

    	return $product_code;
    }
        
    private function ServiceRouteA($service_id)
    {
    	$kg_a = self::CreateServiceRoute($service_id, 'Follow-Up Ticket Number');
    	$kg_a_ctr = 1;
    	// A keywords
		$access_keys = $this->ci->config->item('access_numbers');
		$operators = $this->ci->config->item('operators');
		$key = 'UPDATE';
		foreach ($access_keys as $item) 
		{
			$keyword_data = array('service_route_id' => $kg_a, 'service_num' => $this->account_data[$item]);			
			$keyword_data['keyword'] = $key;
			if ($item == 'mobile_access_num')
				$keyword_data['keyword'] = $key . ' ' . $this->account_data['main_keyword'];
			$keyword_data = array_merge($keyword_data, $this->ci->config->item('kg_a_keywords'));
			
			$keyword_id = self::CreateKeyword2($keyword_data);
			self::AssignKeywordOperator($keyword_id, $operators[$item]);
		}    	
		
    	// A scenarios
    	// Update: Reg (Ticket No. is Valid)
    	$kg_a_s1 = self::CreateScenario($kg_a_ctr++, $kg_a, 'Update: Reg (Ticket No. is Valid)');
		// ... configuration
		// ON_EVAL
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;    
		self::Action113($g, $g_ctr++);
		self::Action109($g, $g_ctr++);
		self::Action121($g, $this->feedback_table, $g_ctr++);
		self::AssignEvalScenario($kg_a_s1, $g);
		
		// ON_MO
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1; 
		$content = "Your request has been processed. Please expect update for Ticket No: #ChunkRest#.";
		$product_code = self::CreateContent('sms', 'text', 'Update: Reg - Valid Ticket Number', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);
		self::AssignMo($kg_a_s1, $g);   
		
    	// Update: NotReg (Ticket No. is Valid)
    	$kg_a_s2 = self::CreateScenario($kg_a_ctr++, $kg_a, 'NotReg (Ticket No. is Valid)');
		// ... configuration
		// ON_EVAL    	
    	$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;    
    	self::Action121($g, $this->feedback_table, $g_ctr++);
    	self::AssignEvalScenario($kg_a_s2, $g);
    	
    	// ON_MO
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1; 
		$content = "Expect an update for Ticket No: #ChunkRest#.\n\nMore options await if you register to: myprofile.cloudmsngr.com";
		$product_code = self::CreateContent('sms', 'text', 'Update: NotReg - Valid Ticket Number', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);
		self::AssignMo($kg_a_s2, $g);   
		    	
    	// Update: Invalid Ticket Number
    	$kg_a_s3 = self::CreateScenario($kg_a_ctr++, $kg_a, 'Update: Invalid Ticket Number');
		// ... configuration
		// ON_EVAL    	    	
    	$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action21($g, $g_ctr++);
		self::AssignEvalScenario($kg_a_s3, $g);
		
		// ON_MO
    	$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;		
		$content = "You sent in an invalid ticket number.";
		$product_code = self::CreateContent('sms', 'text', 'Update - Invalid Ticket Number', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);
		self::AssignMo($kg_a_s3, $g);    	
    }    
    
    private function ServiceRouteB($service_id)
    {
    	$kg_b = self::CreateServiceRoute($service_id, 'Coupons: CODE &lt;coupon code&gt;');
    	$kg_b_ctr = 1; 
    	// B keywords
		$access_keys = $this->ci->config->item('access_numbers');
		$operators = $this->ci->config->item('operators');
		$key = 'CODE';
		foreach ($access_keys as $item) 
		{
			$keyword_data = array('service_route_id' => $kg_b, 'service_num' => $this->account_data[$item]);			
			$keyword_data['keyword'] = $key;
			if ($item == 'mobile_access_num')
				$keyword_data['keyword'] = $key . ' ' . $this->account_data['main_keyword'];
			$keyword_data = array_merge($keyword_data, $this->ci->config->item('kg_a_keywords'));
			
			$keyword_id = self::CreateKeyword2($keyword_data);
			self::AssignKeywordOperator($keyword_id, $operators[$item]);
		}    

		// B scenarios
		// Valid Coupon - Not Yet Claimed
		$kg_b_s1 = self::CreateScenario($kg_b_ctr++, $kg_b, 'Valid Coupon - Not Yet Claimed');
		// ... configuration
		// ON_EVAL
    	$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		$params = array(
			'tableName' => 'CouponCodes',
			'code' => '#ChunkRest#',
			'status' => 1
		);		
		self::Action119($g, $params, $g_ctr++);
		self::AssignEvalScenario($kg_b_s1, $g);
		
		// ON_MO
    	$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action120($g, $g_ctr++);
		$content = "Coupon Code: #CouponCode# has been verified. This serves as your verification receipt.";
		$product_code = self::CreateContent('sms', 'text', 'Coupon Code - Not Yet Claimed', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);
		self::AssignMo($kg_b_s1, $g);
		
		// Valid Coupon - Already Claimed
		$kg_b_s2 = self::CreateScenario($kg_b_ctr++, $kg_b, 'Valid Coupon - Already Claimed');
		// ... configuration
		// ON_EVAL
    	$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		$params = array(
			'tableName' => 'CouponCodes',
			'code' => '#ChunkRest#',
			'status' => 2
		);		
		self::Action119($g, $params, $g_ctr++);
		self::AssignEvalScenario($kg_b_s2, $g);
		
		// ON_MO
    	$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		$content = "The coupon code: #CouponCode# has already been claimed.";
		$product_code = self::CreateContent('sms', 'text', 'Coupon Code - Already Claimed', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);
		self::AssignMo($kg_b_s2, $g);		
		
		// Invalid Coupon Code
		$kg_b_s3 = self::CreateScenario($kg_b_ctr++, $kg_b, 'Invalid Coupon Code');
		// ... configuration
		// ON_EVAL
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action21($g, $g_ctr++);
		self::AssignEvalScenario($kg_b_s3, $g);
		
		// ON_MO	
    	$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		$content = "You sent in an invalid coupon code. Please check coupon code and try again.";
		$product_code = self::CreateContent('sms', 'text', 'Coupon Code - Invalid Coupon Code', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);
		self::AssignMo($kg_b_s3, $g);
    }

    private function ServiceRouteC($service_id)
    {
    	$kg_c = self::CreateServiceRoute($service_id, 'Registration: REG &lt;name&gt;/&lt;age&gt;/&lt;gender&gt;/&lt;address&gt;/');
    	$kg_c_ctr = 1; 

    	// C keywords
		$access_keys = $this->ci->config->item('access_numbers');
		$operators = $this->ci->config->item('operators');
		$key = 'REG';
		foreach ($access_keys as $item) 
		{
			$keyword_data = array('service_route_id' => $kg_c, 'service_num' => $this->account_data[$item]);			
			$keyword_data['keyword'] = $key;
			if ($item == 'mobile_access_num')
				$keyword_data['keyword'] = $key . ' ' . $this->account_data['main_keyword'];
			$keyword_data = array_merge($keyword_data, $this->ci->config->item('kg_c_keywords'));
			
			$keyword_id = self::CreateKeyword2($keyword_data);
			self::AssignKeywordOperator($keyword_id, $operators[$item]);
		} 

		// C scenarios
		// Already Registered: Registered to CM
		$kg_c_s1 = self::CreateScenario($kg_c_ctr++, $kg_c, 'Already Registered: Registered to CM');
		// ... configuration
		// ON_EVAL
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action113($g, $g_ctr++);
		self::Action109($g, $g_ctr++);
		self::Action7($g, $g_ctr++);
		self::AssignEvalScenario($kg_c_s1, $g);
		
		// ON_MO		
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		$content = "You are already registered. No need to register again.";
		$product_code = self::CreateContent('sms', 'text', 'Registration:Reg - Already Reg', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);
		self::AssignMo($kg_c_s1, $g);
				
		// Already Registered: Not Registered to CM
		$kg_c_s2 = self::CreateScenario($kg_c_ctr++, $kg_c, 'Already Registered: Not Registered to CM');
		// ... configuration
		// ON_EVAL
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action7($g, $g_ctr++);
		self::AssignEvalScenario($kg_c_s2, $g);
		
		// ON_MO		
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		$content = "You are already registered. More options and exciting deals awaits if you register to: myprofile.cloudmsngr.com";
		$product_code = self::CreateContent('sms', 'text', 'Registration:NotReg - Already Reg', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);
		self::AssignMo($kg_c_s2, $g);	
				
		// Valid Registration: Registered to CM
		$kg_c_s3 = self::CreateScenario($kg_c_ctr++, $kg_c, 'Valid Registration: Registered to CM');
		// ... configuration
		// ON_EVAL
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action113($g, $g_ctr++);
		self::Action109($g, $g_ctr++);
		self::Action9($g, 'NAME/AGE/GENDER/ADDRESS', $g_ctr++);
		self::AssignEvalScenario($kg_c_s3, $g);
		
		// ON_MO
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action51($g, $g_ctr++);
		$content = "You are now registered.";
		$product_code = self::CreateContent('sms', 'text', 'Registration: Reg- Acknowledgement', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);
		self::AssignMo($kg_c_s3, $g);	
		
		// Valid Registration: Not Registered to CM
		$kg_c_s4 = self::CreateScenario($kg_c_ctr++, $kg_c, 'Valid Registration: Not Registered to CM');
		// ... configuration
		// ON_EVAL
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action9($g, 'NAME/AGE/GENDER/ADDRESS', $g_ctr++);
		self::AssignEvalScenario($kg_c_s4, $g);
		
		// ON_MO		
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action51($g, $g_ctr++);
		$content = "You are now registered. More options and exciting deals awaits if you register to: myprofile.cloudmsngr.com";
		$product_code = self::CreateContent('sms', 'text', 'Registration: NotReg - Acknowledgment', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);
		self::AssignMo($kg_c_s4, $g);	
		
		// Incomplete Registration
		$kg_c_s5 = self::CreateScenario($kg_c_ctr++, $kg_c, 'Incomplete Registration');
		// ... configuration
		// ON_EVAL
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action21($g, $g_ctr++);
		self::AssignEvalScenario($kg_c_s5, $g);
		
		// ON_MO		
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		$content = "You sent in an incomplete registration info. Make sure you sent the required details and format.";
		$product_code = self::CreateContent('sms', 'text', 'Registration- Incomplete', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);		
		self::AssignMo($kg_c_s5, $g);
    }
    
    private function ServiceRouteD($service_id)
    {
    	$kg_d = self::CreateServiceRoute($service_id, 'Poll: POLL &lt;answer&gt;');
    	$kg_d_ctr = 1;

    	// D keywords
		$access_keys = $this->ci->config->item('access_numbers');
		$operators = $this->ci->config->item('operators');
		$key = 'POLL';
		foreach ($access_keys as $item) 
		{
			$keyword_data = array('service_route_id' => $kg_d, 'service_num' => $this->account_data[$item]);			
			$keyword_data['keyword'] = $key;
			if ($item == 'mobile_access_num')
				$keyword_data['keyword'] = $key . ' ' . $this->account_data['main_keyword'];
			$keyword_data = array_merge($keyword_data, $this->ci->config->item('kg_d_keywords'));
			
			$keyword_id = self::CreateKeyword2($keyword_data);
			self::AssignKeywordOperator($keyword_id, $operators[$item]);
		} 

		// D scenarios  
		// Valid Poll: Registered to CM
		$kg_d_s1 = self::CreateScenario($kg_d_ctr++, $kg_d, 'Valid Poll: Registered to CM');
		// ... configuration
		// ON_EVAL
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action113($g, $g_ctr++);
		self::Action109($g, $g_ctr++);
		self::Action9($g, 'POLL', $g_ctr++);		
		self::AssignEvalScenario($kg_d_s1, $g);
		
		// ON_MO
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action19($g, $g_ctr++);
		$content = "Thanks for joining our poll.";
		$product_code = self::CreateContent('sms', 'text', 'Poll: Reg- Acknowledgement', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);				
		self::AssignMo($kg_d_s1, $g);

		// Valid Poll: Not Registered to CM
		$kg_d_s2 = self::CreateScenario($kg_d_ctr++, $kg_d, 'Valid Poll: Not Registered to CM');
		// ... configuration
		// ON_EVAL
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action9($g, 'POLL', $g_ctr++);
		self::AssignEvalScenario($kg_d_s2, $g);
		
		// ON_MO
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		$content = "Thanks for joining our poll. More options and exciting deals awaits if you register to: myprofile.cloudmsngr.com";
		$product_code = self::CreateContent('sms', 'text', 'Poll: NotReg- Acknowledgement', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);				
		self::AssignMo($kg_d_s2, $g);
				
		// Incomplete Poll
		$kg_d_s3 = self::CreateScenario($kg_d_ctr++, $kg_d, 'Incomplete Poll');
		// ... configuration
		// ON_EVAL
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action21($g, $g_ctr++);
		self::AssignEvalScenario($kg_d_s3, $g);
		
		// ON_MO
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		$content = "You sent an incomplete poll entry. Please review details sent.";
		$product_code = self::CreateContent('sms', 'text', 'Poll - Incomplete', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);		
		self::AssignMo($kg_d_s3, $g);
    }

    private function ServiceRouteE($service_id)
    {
    	$kg_e = self::CreateServiceRoute($service_id, 'Invalid Keyword');
    	$kg_e_ctr = 1;

    	// E keywords
		$access_keys = $this->ci->config->item('access_numbers');
		$operators = $this->ci->config->item('operators');		
		foreach ($access_keys as $item) 
		{
			$keyword_data = array('keyword' => '*', 'service_route_id' => $kg_e, 'service_num' => $this->account_data[$item]);			
			$keyword_data = array_merge($keyword_data, $this->ci->config->item('kg_e_keywords'));
			
			$keyword_id = self::CreateKeyword2($keyword_data);
			self::AssignKeywordOperator($keyword_id, $operators[$item]);
		} 
    	
		// E scenarios
		// Help
		$kg_e_s1 = self::CreateScenario($kg_e_ctr++, $kg_e, 'Help');
		// ... configuration
		// ON_EVAL		
		// ON_MO
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		$content = "Thank you for your interest. Know more of our services, please log-on to myprofile.cloudmsngr.com.";
		$product_code = self::CreateContent('sms', 'text', 'Default Invalid Reply', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);		
		self::AssignMo($kg_e_s1, $g);
    }

    private function ServiceRouteF($service_id)
    {
    	$kg_f = self::CreateServiceRoute($service_id, 'Help');
    	$kg_f_ctr = 1;

    	// F keywords
		$access_keys = $this->ci->config->item('access_numbers');
		$operators = $this->ci->config->item('operators');		
		foreach ($access_keys as $item) 
		{
			$keyword_data = array('keyword' => 'HELP', 'service_route_id' => $kg_f, 'service_num' => $this->account_data[$item]);			
			$keyword_data = array_merge($keyword_data, $this->ci->config->item('kg_f_keywords'));
			
			$keyword_id = self::CreateKeyword2($keyword_data);
			self::AssignKeywordOperator($keyword_id, $operators[$item]);
		} 
    	
		// F scenarios
		// Help
		$kg_f_s1 = self::CreateScenario($kg_f_ctr++, $kg_f, 'Help');
		// ... configuration
		// ON_EVAL		
		// ON_MO
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		$content = "Send us feedback:\nFBACK <message>\nRegister:\nREG <name>/<age>/<gender>/<address>\nJoin poll:\nPOLL <answer>\n\nVisit: cloudmsngr.com";
		$product_code = self::CreateContent('sms', 'text', 'Service - Help', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);		
		self::AssignMo($kg_f_s1, $g);
    }    
    
    private function ServiceRouteG($service_id)
    {
    	$kg_g = self::CreateServiceRoute($service_id, 'AUTOFOLLOW');
    	$kg_g_ctr = 1;

    	// G keywords
		$keyword_data = array('keyword' => 'AUTOFOLLOW', 'service_route_id' => $kg_g, 'service_num' => $this->account_data['twitter_access_num']);			
		$keyword_data = array_merge($keyword_data, $this->ci->config->item('kg_g_keywords'));
			
		$keyword_id = self::CreateKeyword2($keyword_data);
		self::AssignKeywordOperator($keyword_id, 'twitter');
    	
		// F scenarios
		// Help
		$kg_g_s1 = self::CreateScenario($kg_g_ctr++, $kg_g, 'AUTOFOLLOW');
		// ... configuration
		// ON_EVAL		
		// ON_MO
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		$content = "#ChunkRest#";
		$product_code = self::CreateContent('command', 'follow', 'Twitback - Auto Follow', $content, $this->content_category_id);
		self::Action23($g, $product_code, $g_ctr++);		
		self::AssignMo($kg_g_s1, $g);
    }        
    
    private function ServiceRouteH($service_id)
    {
    	$kg_h = self::CreateServiceRoute($service_id, 'Feedback Admin Reply');
    	$kg_h_ctr = 1;

    	// H keywords
		$keyword_data = array('keyword' => 'FEEDBACK_REPLY', 'service_route_id' => $kg_h, 'service_num' => '0000');			
		$keyword_data = array_merge($keyword_data, $this->ci->config->item('kg_h_keywords'));
			
		$keyword_id = self::CreateKeyword2($keyword_data);
		self::AssignKeywordOperator($keyword_id, 'solucient');
    	
		// H scenarios
		// Send Feedback Reply
		$kg_h_s1 = self::CreateScenario($kg_h_ctr++, $kg_h, 'Send Feedback Reply');
		// ... configuration
		// ON_EVAL		
		// ON_MO
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		$content = "#ChunkRest#";
		$product_code = self::CreateContent('sms', 'text', 'CloudMsngr Feedback - Admin Reply', $content, $this->content_category_id);
		self::Action23_2($g, $product_code, $g_ctr++);		
		self::AssignMo($kg_h_s1, $g);
    }     
    
    private function ServiceRouteI($service_id)
    {
    	$kg_i = self::CreateServiceRoute($service_id, 'CloudMsngr Touch Point Enrollment');
    	$kg_i_ctr = 1;

    	// I keywords
		$access_keys = $this->ci->config->item('access_numbers');
		$operators = $this->ci->config->item('operators');
		$key = 'SOCIALS';
		foreach ($access_keys as $item) 
		{
			$keyword_data = array('service_route_id' => $kg_i, 'service_num' => $this->account_data[$item]);			
			$keyword_data['keyword'] = $key;
			if ($item == 'mobile_access_num')
				$keyword_data['keyword'] = $key . ' ' . $this->account_data['main_keyword'];
			$keyword_data = array_merge($keyword_data, $this->ci->config->item('kg_i_keywords'));
			
			$keyword_id = self::CreateKeyword2($keyword_data);
			self::AssignKeywordOperator($keyword_id, $operators[$item]);
		} 

		// I scenarios
		// Valid Link, Touch Point Account Exists: Reg to CM
		$kg_i_s1 = self::CreateScenario($kg_i_ctr++, $kg_i, 'Valid Link, Touch Point Account Exists: Reg to CM');
		// ... configuration
		// ON_EVAL
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action114($g, $g_ctr++);
		self::Action113($g, $g_ctr++);
		self::Action109($g, $g_ctr++);
		self::AssignEvalScenario($kg_i_s1, $g);		
		// ON_MO
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action108($g, $g_ctr++);
		$content = "You have already linked your social accounts. You may now use any of your social accounts to transact.";
		$product_code = self::CreateContent('sms', 'text', 'Socials: Reg - Valid &amp; TP Exist', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);						
		$content = "#ChunkRest#";
		$product_code = self::CreateContent('command', 'follow', 'Twitback - Auto Follow', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);		
		self::AssignMo($kg_i_s1, $g);
		
		// Valid Link, Touch Point Account Exists: NotReg to CM
		$kg_i_s2 = self::CreateScenario($kg_i_ctr++, $kg_i, 'Valid Link, Touch Point Account Exists: NotReg to CM');
		// ... configuration
		// ON_EVAL
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action114($g, $g_ctr++);
		self::Action113($g, $g_ctr++);
		self::AssignEvalScenario($kg_i_s2, $g);		
		// ON_MO
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action108($g, $g_ctr++);
		$content = "You have already linked your social accounts. More options and exciting deals awaits if you register to: myprofile.cloudmsngr.com";
		$product_code = self::CreateContent('sms', 'text', 'Socials: NotReg - Valid &amp; TP Exist', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);						
		$content = "#ChunkRest#";
		$product_code = self::CreateContent('command', 'follow', 'Twitback - Auto Follow', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);
		self::AssignMo($kg_i_s2, $g);
				
		// Valid Link, Touch Point Account Doesn't Exist
		$kg_i_s3 = self::CreateScenario($kg_i_ctr++, $kg_i, 'Valid Link, Touch Point Account Doesn\'t Exist');
		// ... configuration
		// ON_EVAL
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action114($g, $g_ctr++);
		self::AssignEvalScenario($kg_i_s3, $g);		
		// ON_MO
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action112($g, $g_ctr++);
		self::Action108_2($g, $g_ctr++);
		self::Action108($g, $g_ctr++);
		$content = "You have successfully linked your social accounts. More options and deals awaits if you register to: myprofile.cloudmsngr.com";
		$product_code = self::CreateContent('sms', 'text', 'Socials:  NotReg - Valid &amp; TP Dont Exist', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);						
		$content = "#ChunkRest#";
		$product_code = self::CreateContent('command', 'follow', 'Twitback - Auto Follow', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);		
		self::AssignMo($kg_i_s3, $g);
				
		// Invalid Link, Touch Point Account Exists
		$kg_i_s4 = self::CreateScenario($kg_i_ctr++, $kg_i, 'Invalid Link, Touch Point Account Exists');
		// ... configuration
		// ON_EVAL
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action113($g, $g_ctr++);
		self::AssignEvalScenario($kg_i_s4, $g);		
		// ON_MO
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		$content = "The details and/or accounts you sent is not valid. Please make sure you sent the correct format and details.";
		$product_code = self::CreateContent('sms', 'text', 'Socials - Invalid TP Link', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);								
		self::AssignMo($kg_i_s4, $g);
				
		// Invalid Link, Touch Point Account Doesn't Exist
		$kg_i_s5 = self::CreateScenario($kg_i_ctr++, $kg_i, 'Invalid Link, Touch Point Account Doesn\'t Exist');
		// ... configuration
		// ON_EVAL
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action21($g, $g_ctr++);
		self::AssignEvalScenario($kg_i_s5, $g);		
		// ON_MO
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		$content = "The details and/or accounts you sent is not valid. Please make sure you sent the correct format and details.";
		$product_code = self::CreateContent('sms', 'text', 'Socials - Invalid TP Link', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);										
		self::AssignMo($kg_i_s5, $g);
    }
    
    private function ServiceRouteJ($service_id)
    {
    	$kg_j = self::CreateServiceRoute($service_id, 'CloudMsngr Account Link');
    	$kg_j_ctr = 1;

    	// J keywords
		$access_keys = $this->ci->config->item('access_numbers');
		$operators = $this->ci->config->item('operators');
		$key = 'LINK';
		foreach ($access_keys as $item) 
		{
			$keyword_data = array('service_route_id' => $kg_j, 'service_num' => $this->account_data[$item]);			
			$keyword_data['keyword'] = $key;
			if ($item == 'mobile_access_num')
				$keyword_data['keyword'] = $key . ' ' . $this->account_data['main_keyword'];
			$keyword_data = array_merge($keyword_data, $this->ci->config->item('kg_j_keywords'));
			
			$keyword_id = self::CreateKeyword2($keyword_data);
			self::AssignKeywordOperator($keyword_id, $operators[$item]);
		} 

		// J scenarios
		// TouchPoint and Account Already Linked
		$kg_j_s1 = self::CreateScenario($kg_j_ctr++, $kg_j, 'TouchPoint and Account Already Linked');
		// ... configuration
		// ON_EVAL
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action113($g, $g_ctr++);
		self::Action109($g, $g_ctr++);		
		self::AssignEvalScenario($kg_j_s1, $g);				
		// ON_MO
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		$content = "Sorry, we can't process your request. Your account is already linked.";
		$product_code = self::CreateContent('sms', 'text', 'Link- Account is Already Link', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);										
		self::AssignMo($kg_j_s1, $g);
				
		// Account Already Registered
		$kg_j_s2 = self::CreateScenario($kg_j_ctr++, $kg_j, 'Account Already Registered');
		// ... configuration
		// ON_EVAL
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action106($g, $g_ctr++);		
		self::AssignEvalScenario($kg_j_s2, $g);				
		// ON_MO
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		$content = "The account you are trying to link is already in use.";
		$product_code = self::CreateContent('sms', 'text', 'Link - Account Already Registered', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);
		self::AssignMo($kg_j_s2, $g);
				
		// Account Not Yet Registered - TP Linked
		$kg_j_s3 = self::CreateScenario($kg_j_ctr++, $kg_j, 'Account Not Yet Registered - TP Linked');
		// ... configuration
		// ON_EVAL
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;		
		self::Action118($g, $g_ctr++);
		self::Action113($g, $g_ctr++);
		self::AssignEvalScenario($kg_j_s3, $g);				
		// ON_MO
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action107($g, $g_ctr++);
		$content = "You have just successfully linked your account. You may now enjoy the mobile services we offer.";
		$product_code = self::CreateContent('sms', 'text', 'Link - Account Successfully Linked', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);		
		self::AssignMo($kg_j_s3, $g);
						
		// Account Not Yet Registered - TP Not Linked
		$kg_j_s4 = self::CreateScenario($kg_j_ctr++, $kg_j, 'Account Not Yet Registered - TP Not Linked');
		// ... configuration
		// ON_EVAL
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action118($g, $g_ctr++);
		self::AssignEvalScenario($kg_j_s4, $g);				
		// ON_MO
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action112($g, $g_ctr++);
		self::Action107($g, $g_ctr++);
		self::Action108_2($g, $g_ctr++);
		$content = "You have just successfully linked your account. You may now enjoy the mobile services we offer.";
		$product_code = self::CreateContent('sms', 'text', 'Link - Account Successfully Linked', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);			
		self::AssignMo($kg_j_s4, $g);
				
		// Invalid User Name
		$kg_j_s5 = self::CreateScenario($kg_j_ctr++, $kg_j, 'Invalid User Name');
		// ... configuration
		// ON_EVAL
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;	
		self::Action21($g, $g_ctr++);	
		self::AssignEvalScenario($kg_j_s5, $g);				
		// ON_MO
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		$content = "Sorry, you have entered an invalid account. Please try again.";
		$product_code = self::CreateContent('sms', 'text', 'Link - Invalid Account', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);					
		self::AssignMo($kg_j_s5, $g);				
    }
    
	private function ServiceRouteK($service_id)
	{
    	$kg_k = self::CreateServiceRoute($service_id, 'Feedback: FBACK &lt;tenant&gt;');
    	$kg_k_ctr = 1;

    	// K keywords
		$access_keys = $this->ci->config->item('access_numbers');
		$operators = $this->ci->config->item('operators');
		$key = 'FBACK';
		foreach ($access_keys as $item) 
		{
			$keyword_data = array('service_route_id' => $kg_k, 'service_num' => $this->account_data[$item]);			
			$keyword_data['keyword'] = $key;
			if ($item == 'mobile_access_num')
				$keyword_data['keyword'] = $key . ' ' . $this->account_data['main_keyword'];
			$keyword_data = array_merge($keyword_data, $this->ci->config->item('kg_k_keywords'));
			
			$keyword_id = self::CreateKeyword2($keyword_data);
			self::AssignKeywordOperator($keyword_id, $operators[$item]);
		} 

		// K scenarios
		// Valid Feedback (NAME/REACTION): Reg to CM
		$kg_k_s1 = self::CreateScenario($kg_k_ctr++, $kg_k, 'Valid Feedback (NAME/REACTION): Reg to CM');
		// ... configuration
		// ON_EVAL
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action113($g, $g_ctr++);
		self::Action109($g, $g_ctr++);
		self::Action9($g, 'NAME/REACTION', $g_ctr++);
		self::AssignEvalScenario($kg_k_s1, $g);
		// ON_MO
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action19_2($g, $g_ctr++);
		$content = "Thanks for sending your feedback. Ticket Ref. No.: #TicketNumber#";
		$product_code = self::CreateContent('sms', 'text', 'Feedback: Reg - Acknowledgement', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);			
		self::AssignMo($kg_k_s1, $g);		
				
		// Valid Feedback (NAME/REACTION): NotYetReg to CM
		$kg_k_s2 = self::CreateScenario($kg_k_ctr++, $kg_k, 'Valid Feedback (NAME/REACTION): NotYetReg to CM');
		// ... configuration
		// ON_EVAL
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action9($g, 'NAME/REACTION', $g_ctr++);
		self::AssignEvalScenario($kg_k_s2, $g);
		// ON_MO
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action19_2($g, $g_ctr++);
		$content = "Thanks for sending your feedback. Ticket Ref No.: #TicketNumber#. More options await if you register: myprofile.cloudmsngr.com";
		$product_code = self::CreateContent('sms', 'text', 'Feedback: NotReg- Acknowledgement', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);		
		self::AssignMo($kg_k_s2, $g);		
		
		// Valid Feedback (REACTION): Reg to CM
		$kg_k_s3 = self::CreateScenario($kg_k_ctr++, $kg_k, 'Valid Feedback (REACTION): Reg to CM');
		// ... configuration
		// ON_EVAL
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action113($g, $g_ctr++);
		self::Action109($g, $g_ctr++);
		self::Action9($g, 'REACTION', $g_ctr++);
		self::AssignEvalScenario($kg_k_s3, $g);
		// ON_MO
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action19_3($g, $g_ctr++);
		$content = "Thanks for sending your feedback. Ticket Ref. No.: #TicketNumber#";
		$product_code = self::CreateContent('sms', 'text', 'Feedback: Reg - Acknowledgement', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);		
		self::AssignMo($kg_k_s3, $g);		
				
		// Valid Feedback (REACTION): NotYetReg to CM
		$kg_k_s4 = self::CreateScenario($kg_k_ctr++, $kg_k, 'Valid Feedback (REACTION): NotYetReg to CM');
		// ... configuration
		// ON_EVAL
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action9($g, 'REACTION', $g_ctr++);
		self::AssignEvalScenario($kg_k_s4, $g);
		// ON_MO
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action19_3($g, $g_ctr++);
		$content = "Thanks for sending your feedback. Ticket Ref No.: #TicketNumber#. More options await if you register: myprofile.cloudmsngr.com";
		$product_code = self::CreateContent('sms', 'text', 'Feedback: NotReg- Acknowledgement', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);		
		self::AssignMo($kg_k_s4, $g);		
				
		// Incomplete Reaction
		$kg_k_s5 = self::CreateScenario($kg_k_ctr++, $kg_k, 'Incomplete Reaction');
		// ... configuration
		// ON_EVAL
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		self::Action21($g, $g_ctr++);
		self::AssignEvalScenario($kg_k_s5, $g);
		// ON_MO
		$g = $this->ci->cmservice_model->CreateActionGroup();
		$g_ctr = 1;
		$content = "Please make sure you sent the complete details for your feedback.";
		$product_code = self::CreateContent('sms', 'text', 'Feedback - Incomplete', $content, $this->content_category_id);
		self::Action115($g, $product_code, $g_ctr++);				
		self::AssignMo($kg_k_s5, $g);			
	}
    
    
    private function Action7($action_group_id, $order = 0)
    {
    	$params = array(
    		'tablename' => $this->registration_table,
    		'service_group_id' => $this->service_group_id,
    		'key' => $this->account_data['main_keyword'],
    		'range' => 'FOREVER'
    	);
    	$data = array('action_group_id' => $action_group_id, 'action_id' => 7, 'bill_info' => '', 'event_trap' => 'I', 'params' => serialize($params), 'v_order' => $order);
    	$this->ci->load->model('cmservice_model');
    	return $this->ci->cmservice_model->ActionConfiguration($data);    	
    }
    
    private function Action9($action_group_id, $pattern, $order = 0)
    {
    	$params = array(
    		'info_parse' => '#ChunkRest#',
    		'pattern' => $pattern
    	);
    	$data = array('action_group_id' => $action_group_id, 'action_id' => 9, 'bill_info' => '', 'event_trap' => 'I', 'params' => serialize($params), 'v_order' => $order);
    	$this->ci->load->model('cmservice_model');
    	return $this->ci->cmservice_model->ActionConfiguration($data);
    }  
    
    private function Action19($action_group_id, $order = 0)
    {
    	$params = array(
    		'tablename' => $this->poll_table,
    		'show' => $this->account_data['main_keyword'],
    		'pattern' => 'POLL'
    	);
    	$data = array('action_group_id' => $action_group_id, 'action_id' => 19, 'bill_info' => '', 'event_trap' => 'I', 'params' => serialize($params), 'v_order' => $order);
    	$this->ci->load->model('cmservice_model');
    	return $this->ci->cmservice_model->ActionConfiguration($data);    	
    }
    
    private function Action19_2($action_group_id, $order = 0)
    {
    	$params = array(
    		'tablename' => $this->feedback_table,
    		'show' => $this->account_data['main_keyword'],
    		'pattern' => 'NAME/REACTION',
    		'generateTicketNumber' => 1
    	);
    	$data = array('action_group_id' => $action_group_id, 'action_id' => 19, 'bill_info' => '', 'event_trap' => 'I', 'params' => serialize($params), 'v_order' => $order);
    	$this->ci->load->model('cmservice_model');
    	return $this->ci->cmservice_model->ActionConfiguration($data);    	
    }
    
    private function Action19_3($action_group_id, $order = 0)
    {
    	$params = array(
    		'tablename' => $this->feedback_table,
    		'show' => $this->account_data['main_keyword'],
    		'pattern' => 'REACTION',
    		'generateTicketNumber' => 1
    	);
    	$data = array('action_group_id' => $action_group_id, 'action_id' => 19, 'bill_info' => '', 'event_trap' => 'I', 'params' => serialize($params), 'v_order' => $order);
    	$this->ci->load->model('cmservice_model');
    	return $this->ci->cmservice_model->ActionConfiguration($data);    	
    }
    
    private function Action21($action_group_id, $order = 0)
    {
    	$params = array();
    	$data = array('action_group_id' => $action_group_id, 'action_id' => 21, 'bill_info' => '', 'event_trap' => 'I', 'params' => serialize($params), 'v_order' => $order);
    	$this->ci->load->model('cmservice_model');
    	return $this->ci->cmservice_model->ActionConfiguration($data);	    	    	
    }
    
    private function Action23($action_group_id, $product_code, $order = 0)
    {
		$params = array(
			'ctr_options' => 1,
			'check_operator' => 0,
			'replace_suffix' => 1,			
			'telcontent' => array(
				'twitter' => array(
					'product_code' => $product_code,
					'bill_info' => $this->account_data['twitter_billcode'],
					'access_number' => $this->account_data['twitter_access_num']
				)
			)
		); 	    	
    	$data = array('action_group_id' => $action_group_id, 'action_id' => 23, 'bill_info' => '', 'event_trap' => 'I', 'params' => serialize($params), 'v_order' => $order);
    	$this->ci->load->model('cmservice_model');
    	return $this->ci->cmservice_model->ActionConfiguration($data);	    	    	 	
    }
    
    private function Action23_2($action_group_id, $product_code, $order = 0)
    {
    	$params = array(
		    'ctr_options' => 4,
		    'to' => '#Chunk2#',
		    'check_operator' => 1,
		    'replace_suffix' => 1,
		    'telcontent' => array(
    			'twitter' => array(
    				'product_code' => $product_code,
    				'bill_info' => $this->account_data['twitter_billcode'],
    				'access_number' => $this->account_data['twitter_access_num'],
    			),
    			'globe' => array(
    				'product_code' => $product_code,
    				'bill_info' => $this->account_data['mobile_billcode'],
    				'access_number' => $this->account_data['mobile_access_num']
    			),
    			'ym' => array(
    				'product_code' => $product_code,
    				'bill_info' => $this->account_data['ym_billcode'],
    				'access_number' => $this->account_data['ym_access_num']
    			),
    			'xmpp' => array(
    				'product_code' => $product_code,
    				'bill_info' => $this->account_data['xmpp_billcode'],
    				'access_number' => $this->account_data['xmpp_access_num']
    			)
    		)
    	);
    	$data = array('action_group_id' => $action_group_id, 'action_id' => 23, 'bill_info' => '', 'event_trap' => 'I', 'params' => serialize($params), 'v_order' => $order);
    	$this->ci->load->model('cmservice_model');
    	return $this->ci->cmservice_model->ActionConfiguration($data);	    	    	 	    		
    }
    
    private function Action51($action_group_id, $order = 0)
    {
    	$params = array(
    		'tablename' => $this->registration_table,
    		'service_group_id' => $this->service_group_id,
    		'pattern' => 'NAME/AGE/GENDER/ADDRESS',
    		'show' => $this->account_data['main_keyword']
    	);
    	$data = array('action_group_id' => $action_group_id, 'action_id' => 51, 'bill_info' => '', 'event_trap' => 'I', 'params' => serialize($params), 'v_order' => $order);
    	$this->ci->load->model('cmservice_model');
    	return $this->ci->cmservice_model->ActionConfiguration($data);	    	    	
    }
    
    private function Action106($action_group_id, $order = 0)
    {
    	$params = array('account' => 'CloudMsngr', 'accountName' => '#ChunkRest#');	    	
    	$data = array('action_group_id' => $action_group_id, 'action_id' => 106, 'bill_info' => '', 'event_trap' => 'I', 'params' => serialize($params), 'v_order' => $order);
    	$this->ci->load->model('cmservice_model');
    	return $this->ci->cmservice_model->ActionConfiguration($data);    	
    }
    
    private function Action107($action_group_id, $order = 0)
    {
    	$params = array(
    		'account' => 'CloudMsngr',
    		'accountName' => '#ChunkRest#',
    		'status' => 1
    	);	    	
    	$data = array('action_group_id' => $action_group_id, 'action_id' => 107, 'bill_info' => '', 'event_trap' => 'I', 'params' => serialize($params), 'v_order' => $order);
    	$this->ci->load->model('cmservice_model');
    	return $this->ci->cmservice_model->ActionConfiguration($data);    	    	
    }
    
    private function Action108_2($action_group_id, $order = 0)
    {
    	$params = array('touchPointType' => '#Operator#');
    	$data = array('action_group_id' => $action_group_id, 'action_id' => 108, 'bill_info' => '', 'event_trap' => 'I', 'params' => serialize($params), 'v_order' => $order);
    	$this->ci->load->model('cmservice_model');
    	return $this->ci->cmservice_model->ActionConfiguration($data);    	
    }
    
    private function Action108($action_group_id, $order = 0)
    {
    	$params = array();
    	$data = array('action_group_id' => $action_group_id, 'action_id' => 108, 'bill_info' => '', 'event_trap' => 'I', 'params' => serialize($params), 'v_order' => $order);
    	$this->ci->load->model('cmservice_model');
    	return $this->ci->cmservice_model->ActionConfiguration($data);	    	    	    	
    }
    
    private function Action109($action_group_id, $order = 0)
    {
    	$params = array('account' => 'CloudMsngr', 'status' => 1);
    	$data = array('action_group_id' => $action_group_id, 'action_id' => 109, 'bill_info' => '', 'event_trap' => 'I', 'params' => serialize($params), 'v_order' => $order);
    	$this->ci->load->model('cmservice_model');
    	return $this->ci->cmservice_model->ActionConfiguration($data);	    	
    }
    
    private function Action112($action_group_id, $order = 0)
    {
    	$params = array('account' => 'CloudMsngr', 'status' => 1);
    	$data = array('action_group_id' => $action_group_id, 'action_id' => 112, 'bill_info' => '', 'event_trap' => 'I', 'params' => serialize($params), 'v_order' => $order);
    	$this->ci->load->model('cmservice_model');
    	return $this->ci->cmservice_model->ActionConfiguration($data);    	
    }
    
    private function Action113($action_group_id, $order = 0)
    {
    	$params = array('account' => 'CloudMsngr', 'status' => 1);
    	$data = array('action_group_id' => $action_group_id, 'action_id' => 113, 'bill_info' => '', 'event_trap' => 'I', 'params' => serialize($params), 'v_order' => $order);
    	$this->ci->load->model('cmservice_model');
    	return $this->ci->cmservice_model->ActionConfiguration($data);	
    }
    
    private function Action114($action_group_id, $order = 0)
    {
		$params = array('data' => '#ChunkRest#');
    	$data = array('action_group_id' => $action_group_id, 'action_id' => 114, 'bill_info' => '', 'event_trap' => 'I', 'params' => serialize($params), 'v_order' => $order);
    	$this->ci->load->model('cmservice_model');
    	return $this->ci->cmservice_model->ActionConfiguration($data);			
    }
    
    private function Action115($action_group_id, $product_code, $order = 0)
    {
		$params = array(
			'ctr_options' => 4,
			'check_operator' => 0,
			'replace_suffix' => 1,
			'tenant' => 'CloudMsngr',
			'telcontent' => array(
				'twitter' => array(
					'product_code' => $product_code,
					'bill_info' => $this->account_data['twitter_billcode'],
					'access_number' => $this->account_data['twitter_access_num']
				),
				'globe' => array(
					'product_code' => $product_code,
					'bill_info' => $this->account_data['mobile_billcode'],
					'access_number' => $this->account_data['mobile_access_num'],
				),
				'xmpp' => array(
					'product_code' => $product_code,
					'bill_info' => $this->account_data['xmpp_billcode'],
					'access_number' => $this->account_data['xmpp_access_num'],				
				),
				'ym' => array(
					'product_code' => $product_code,
					'bill_info' => $this->account_data['ym_billcode'],
					'access_number' => $this->account_data['ym_access_num'],				
				)
			)
		); 	
    	$data = array('action_group_id' => $action_group_id, 'action_id' => 115, 'bill_info' => '', 'event_trap' => 'I', 'params' => serialize($params), 'v_order' => $order);
    	$this->ci->load->model('cmservice_model');    	
    	return $this->ci->cmservice_model->ActionConfiguration($data);		
    }
    
    private function Action118($action_group_id, $order = 0)
    {
    	$params = array('username' => '#ChunkRest#');
    	$data = array('action_group_id' => $action_group_id, 'action_id' => 118, 'bill_info' => '', 'event_trap' => 'I', 'params' => serialize($params), 'v_order' => $order);
    	$this->ci->load->model('cmservice_model');    	
    	return $this->ci->cmservice_model->ActionConfiguration($data);    	
    }
    
    private function Action119($action_group_id, $params, $order = 0)
    {
    	$data = array('action_group_id' => $action_group_id, 'action_id' => 119, 'bill_info' => '', 'event_trap' => 'I', 'params' => serialize($params), 'v_order' => $order);
    	$this->ci->load->model('cmservice_model');    	
    	return $this->ci->cmservice_model->ActionConfiguration($data);				
	}
    
	private function Action120($action_group_id, $order = 0)
	{
		$params = array(
			'tableName' => 'CouponCodes',
			'code' => '#CouponCodeID#',
			'status' => 2
		);
    	$data = array('action_group_id' => $action_group_id, 'action_id' => 120, 'bill_info' => '', 'event_trap' => 'I', 'params' => serialize($params), 'v_order' => $order);
    	$this->ci->load->model('cmservice_model');    	
    	return $this->ci->cmservice_model->ActionConfiguration($data);
	}
	
    private function Action121($action_group_id, $table, $order = 0)
    {
    	$params = array('tableName' => $table, 'ticketNumber' => '#ChunkRest#');
    	$data = array('action_group_id' => $action_group_id, 'action_id' => 121, 'bill_info' => '', 'event_trap' => 'I', 'params' => serialize($params), 'v_order' => $order);
    	$this->ci->load->model('cmservice_model');
    	return $this->ci->cmservice_model->ActionConfiguration($data);	    	
    }
    
    private function deploy()
    {
    	$this->ci->load->library('Upstage');
		
		// Defaults..
		$result = "0|0";
		$this->ci->load->database('system', TRUE);

		$services = array($this->service_id);
		$instance = 1;
		$content_categories = array($this->content_category_id);
		$system_options = array('bill_info', 'user_input_tables');
		$profiling_tables = array($this->registration_table, $this->feedback_table, $this->poll_table);

		// Set the instance chosen
		$this->ci->upstage->set_instance($instance);
		
		// Create the script to be upstaged to the System DB
		$script = "";
		if( $services != "" ) $script .= $this->ci->upstage->create_upstage_script($services);
		if( $system_options != "" ) $script .= $this->ci->upstage->system_upstage($system_options);
		if( $content_categories != "" ) $script .= $this->ci->upstage->content_upstage($content_categories);
				
		$html = '';
		if( trim($script) != "" )
		{
			$result = $this->ci->upstage->execute_upstage($script,"system");
			$oknok = explode("|",$result);
			$html .= "OK: ".$oknok[0]."<br />";
			$html .= "NOK: ".$oknok[1]."<br />";
		}

		// Create the script to be upstaged to the Profile DB
		$script = "";
		
		if( $profiling_tables != "" ) $script .= $this->ci->upstage->profiling_upstage($profiling_tables);
		
		if( trim($script) != "" )
		{
			$result = $this->ci->upstage->execute_upstage($script,"profile");
			$oknok = explode("|",$result);
			$html .= "OK: ".$oknok[0]."<br />";
			$html .= "NOK: ".$oknok[1]."<br />";
		}
		
		$this->ci->load->model('cmservice_model');
		$this->ci->cmservice_model->BuildrRestart();		    
    }
}