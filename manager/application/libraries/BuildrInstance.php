<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * BuildrInstance Class
 *
 * This class represents a buildr instance used for upstaging
 * 
 *
 * @author      mi0ng
 * @package     C Platform Upstaging Library
 * @copyright   2010 Solucient Inc.
 * @link        http://www.solucientinc.com
 * @since       Version 0.1
 */

class BuildrInstance {
	
	private $id;
	private $name;
	private $description;
	private $ip;
	private $server_user;
	private $server_pass;
	private $mysql_user;
	private $mysql_pass;
	private $db_name;
	
	public function set_values($id,$name,$description,$ip,$server_user,$server_pass,$mysql_user,$mysql_pass,$db_name)
	{
		$this->id = $id;
		$this->name = $name;
		$this->description = $description;
		$this->ip = $ip;
		$this->server_user = $server_user;
		$this->server_pass = $server_pass;
		$this->mysql_user = $mysql_user;
		$this->mysql_pass = $mysql_pass;
		$this->db_name = $db_name;
	}
	
	# Simplified getter/setter
	public function set($variable, $value)
	{
		$this->$variable = $value;
	}
	
	public function get($variable)
	{
		return $this->$variable;
	}
}