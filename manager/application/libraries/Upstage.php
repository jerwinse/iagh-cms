<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Upstage Class
 *
 * This class handles the upstaging feature of the C Platform to be used in the C Web App
 * 
 *
 * @author      mi0ng
 * @package     C Platform Upstaging Library
 * @copyright   2010 Solucient Inc.
 * @link        http://www.solucientinc.com
 * @since       Version 0.1
 */

require_once("BuildrInstance.php");

class Upstage {
	
	private $CI;
	private $upstage_model;
	private $services_model;
	private $service_routes_model;
	private $keyword_operators_model;
	private $keyword_aliases_model;
	private $action_configurations_model;
	private $actions_model;
	private $contents_model;
	
	public $config;
	private $instance;
	private $remote_model;
	
	public function __construct()
	{
		$this->CI = & get_instance();
		$this->CI->load->model('Upstage_model');
		$this->CI->load->model('Services_model');
		$this->CI->load->model('Service_routes_model');
		$this->CI->load->model('Keyword_operators_model');
		$this->CI->load->model('Keyword_aliases_model');
		$this->CI->load->model('Action_configurations_model');
		$this->CI->load->model('Actions_model');
		$this->CI->load->model('Contents_model');
		$this->upstage_model = &$this->CI->Upstage_model;
		$this->services_model = &$this->CI->Services_model;
		$this->service_routes_model = &$this->CI->Service_routes_model;
		$this->keyword_operators_model = &$this->CI->Keyword_operators_model;
		$this->keyword_aliases_model = &$this->CI->Keyword_aliases_model;
		$this->action_configurations_model = &$this->CI->Action_configurations_model;
		$this->actions_model = &$this->CI->Actions_model;
		$this->contents_model = &$this->CI->Contents_model;
		
		$this->CI->load->config('upstaging',TRUE,TRUE);
		$this->config = $this->CI->config->item('upstaging');
	}
	
	public function create_upstage_script($services = array())
	{
		# Create Script for services
		
		if(!is_array($services))
		{
			if( is_numeric($services) )
			{
				$services = array($services);
			}
			else 
			{
				$services = array();
			}
		}
		
		$script = "";
		
		foreach ($services as $service)
		{
			#$script .= "<br />#################   UPSTAGING SERVICE ID $service #######################<br />";
			# Delete this service first and all its routes, scenarios, keywords
			$script .= $this->service_cleanup($service);
			
			# Create the service upstage script
			$script .= $this->service_upstage($service);
		}
		
		return $script;
	}
	
	public function content_upstage($content_categories = array())
	{
		$script = "";
		
		if( !is_array($content_categories) )
		{
			if( is_numeric($content_categories) )
			{
				$content_categories = array($content_categories);
			}
			else 
			{
				$content_categories = array();
			}
		}
		
		# Contents
		foreach ($content_categories as $content_category_id)
		{
			$this->upstage_model->set_table(CONTENT_CATEGORIES);
			$content_category = $this->upstage_model->Find(array("id" => $content_category_id));
			$content_category = $content_category[0];
			
			$contents = $this->contents_model->Find(array("category_id" => $content_category->id));
			foreach ($contents as $content)
			{
				$script .= $this->separator(0);
				$script .= $this->create_delete_statement(CONTENTS,array("product_code" => $content->product_code));
				
				$filtered_content = $this->filter($content,CONTENTS);
				
				$script .= $this->separator(0);
				$script .= $this->create_insert_statement(CONTENTS,$filtered_content);
			}
		}
		
		# Scheduled Contents
		$script .= $this->separator(0);
		$script .= $this->create_truncate_statement(SCHEDULED_CONTENTS);
		
		$this->upstage_model->set_table(SCHEDULED_CONTENTS);
		//$where = array("end_date >=" => date("Y-m-d H:i:s") );
		$where = array("end_date >=" => date("Y-m-d H:i:s", mktime(date("H"),date("i"),date("s"),date("m")-1,date("d"),date("Y"))));
		$sched_contents = $this->upstage_model->Find($where);
		foreach ($sched_contents as $sched_content)
		{		
			# Insert
			$filtered_sched = $this->filter($sched_content,SCHEDULED_CONTENTS);
			$script .= $this->separator(0);
			$script .= $this->create_insert_statement(SCHEDULED_CONTENTS,$filtered_sched);
		}

		return $script;
	}
	
	public function system_upstage($options)
	{
		$script = "";
		
		if( in_array("actions", $options) )
		{
			# Actions
			$script .= $this->separator(0);
			$script .= $this->create_truncate_statement(ACTIONS);
			
			$actions = $this->actions_model->Find();
			foreach ($actions as $action)
			{			
				# Insert
				$filtered_action = $this->filter($action,ACTIONS);
				$script .= $this->separator(1);
				$script .= $this->create_insert_statement(ACTIONS,$filtered_action);
			}
		}
		
		if( in_array("error_codes", $options) )
		{
			# Error_codes
			$script .= $this->separator(0);
			$script .= $this->create_truncate_statement(ERROR_CODES);
			$this->upstage_model->set_table(ERROR_CODES);
			$error_codes = $this->upstage_model->Find();
			foreach ($error_codes as $error_code)
			{		
				# Insert
				$filtered_error_code = $this->filter($error_code,ERROR_CODES);
				$script .= $this->separator(1);
				$script .= $this->create_insert_statement(ERROR_CODES,$filtered_error_code);
			}
		}
		
		if( in_array("access_number", $options) )
		{
			# access_number
			$script .= $this->separator(0);
			$script .= $this->create_truncate_statement(ACCESS_NUMBERS);
			$this->upstage_model->set_table(ACCESS_NUMBERS);
			$access_numbers = $this->upstage_model->Find();
			foreach ($access_numbers as $access_number)
			{		
				# Insert
				$filtered_access_number = $this->filter($access_number,ACCESS_NUMBERS);
				$script .= $this->separator(1);
				$script .= $this->create_insert_statement(ACCESS_NUMBERS,$filtered_access_number);
			}
		}
		
		if( in_array("msisdn_info", $options) )
		{
			# Msisdn info
			$script .= $this->separator(0);
			$script .= $this->create_truncate_statement(MSISDN_INFO);
			$this->upstage_model->set_table(MSISDN_INFO);
			$msisdn_infos = $this->upstage_model->Find();
			foreach ($msisdn_infos as $msisdn_info)
			{
				# Insert
				$filtered_msisdn_info = $this->filter($msisdn_info,MSISDN_INFO);
				$script .= $this->separator(1);
				$script .= $this->create_insert_statement(MSISDN_INFO,$filtered_msisdn_info);
			}
		}
		
		if( in_array("operators", $options) )
		{
			# Operators
			$script .= $this->separator(0);
			$script .= $this->create_truncate_statement(OPERATORS);
			$this->upstage_model->set_table(OPERATORS);
			$operators = $this->upstage_model->Find();
			foreach ($operators as $operator)
			{
				# Insert
				$filtered_operator = $this->filter($operator,OPERATORS);
				$script .= $this->separator(1);
				$script .= $this->create_insert_statement(OPERATORS,$filtered_operator);
			}
		}
		
		if( in_array("bill_info", $options) )
		{
			# billing info
			$script .= $this->separator(0);
			$script .= $this->create_truncate_statement(BILLING_INFO);
			
			$this->upstage_model->set_table(BILLING_INFO);
			$error_codes = $this->upstage_model->Find();
			foreach ($error_codes as $error_code)
			{		
				# Insert
				$filtered_error_code = $this->filter($error_code,BILLING_INFO);
				$script .= $this->separator(1);
				$script .= $this->create_insert_statement(BILLING_INFO,$filtered_error_code);
			}
		}
		
		if( in_array("tagging", $options) )
		{
			# tagging info
			$script .= $this->separator(0);
			$script .= $this->create_truncate_statement(TAGGING);
			
			$this->upstage_model->set_table(TAGGING);
			$tags = $this->upstage_model->Find();
			foreach ($tags as $tag)
			{		
				# Insert
				$filtered_tag = $this->filter($tag,TAGGING);
				$script .= $this->separator(1);
				$script .= $this->create_insert_statement(TAGGING,$filtered_tag);
			}
		}
		
		if( in_array("user_input_tables", $options) )
		{
			# user_input_tables
			$script .= $this->separator(0);
			$script .= $this->create_truncate_statement(USER_INPUT_TABLES);
			
			$this->upstage_model->set_table(USER_INPUT_TABLES);
			$tables = $this->upstage_model->Find();
			foreach ($tables as $table)
			{		
				# Insert
				$filtered_table = $this->filter($table,USER_INPUT_TABLES);
				$script .= $this->separator(1);
				$script .= $this->create_insert_statement(USER_INPUT_TABLES,$filtered_table);
			}
		}
		
		if( in_array("user_input_types", $options) )
		{
			# user_input_types
			$script .= $this->separator(0);
			$script .= $this->create_truncate_statement(USER_INPUT_TYPES);
			
			$this->upstage_model->set_table(USER_INPUT_TYPES);
			$types = $this->upstage_model->Find();
			foreach ($types as $type)
			{		
				# Insert
				$filtered_type = $this->filter($type,USER_INPUT_TYPES);
				$script .= $this->separator(1);
				$script .= $this->create_insert_statement(USER_INPUT_TYPES,$filtered_type);
			}
		}
		
		if( in_array("keyword_cleanup", $options) )
		{
			# keyword_cleanup
			$script .= $this->separator(0);
			
			// select all valid routes (id)
			$service_routes = $this->service_routes_model->Find();
			
			$ids = "( 0";
			
			foreach ($service_routes as $route)
			{
				$ids .= ", ".$route->id;
			}
			
			$ids .= " )";
			
			// Delete all keywords where route_id not in those from above
			$sql = "DELETE FROM keywords WHERE service_route_id NOT IN ".$ids;
			
			
			
			$this->upstage_model->set_table(KEYWORDS);
			$types = $this->upstage_model->Find();
			foreach ($types as $type)
			{		
				# Insert
				$filtered_type = $this->filter($type,USER_INPUT_TYPES);
				$script .= $this->separator(1);
				$script .= $this->create_insert_statement(USER_INPUT_TYPES,$filtered_type);
			}
		}
		
		return $script;
	}
	
	public function service_upstage($service_id)
	{		
		$upstage_string = "";
		
		# The service object will be in the first element of this array
		$service = $this->services_model->Find(array("id"=>$service_id));
		
		if( !isset($service[0]) )
		{
			# The service id specified does not exist
			return $upstage_string;
		}
		
		$this->upstage_model->log_service_upstage($service_id,$this->instance->id);
		
		$filtered_services = $this->filter($service[0],SERVICES);
		
		$upstage_string .= $this->separator(0);
		$upstage_string .= $this->create_insert_statement(SERVICES,$filtered_services);
		
		# Get all the service_routes of this service
		$routes = $this->services_model->DisplayRoutes($service[0]->id);
		foreach ($routes as $route) 
		{
			# Based on routes, get scenarios
			$scenarios = array();
			$keywords = array();
			
			$filtered_route = $this->filter($route,SERVICE_ROUTES);
			
			$upstage_string .= $this->separator(1);
			$upstage_string .= $this->create_insert_statement(SERVICE_ROUTES,$filtered_route);
			
			# Get all the keywords and scenarios of this route
					
			$keywords = $this->service_routes_model->DisplayKeywords($route->id);
			foreach ($keywords as $keyword)
			{
				$keyword_operators = array();
				$keyword_aliases = array();
				
				$filtered_keyword = $this->filter($keyword,KEYWORDS);
				
				$upstage_string .= $this->separator(2);
				$upstage_string .= $this->create_insert_statement(KEYWORDS,$filtered_keyword);
				
				# Get all the keyword_operators of this keyword
				$keyword_operators = $this->keyword_operators_model->Find(array('keyword_id'=>$keyword->id));
				
				foreach ($keyword_operators as $keyword_operator)
				{
					$filtered_keyword_operator = $this->filter($keyword_operator,KEYWORD_OPERATORS);
					
					$upstage_string .= $this->separator(3);
					$upstage_string .= $this->create_insert_statement(KEYWORD_OPERATORS,$filtered_keyword_operator);
				}
				
				# Get all the keyword_aliases of this keyword
				$keyword_aliases = $this->keyword_aliases_model->Find(array('keyword_id'=>$keyword->id));
				
				foreach ($keyword_aliases as $keyword_alias)
				{
					$filtered_keyword_alias = $this->filter($keyword_alias,KEYWORD_ALIASES);
					
					$upstage_string .= $this->separator(3);
					$upstage_string .= $this->create_insert_statement(KEYWORD_ALIASES,$filtered_keyword_alias);
				}
			}
			
			$scenarios = $this->service_routes_model->DisplayScenarios($route->id);
			foreach ($scenarios as $scenario)
			{
				$filtered_scenario = $this->filter($scenario,SCENARIOS);
				
				$upstage_string .= $this->separator(2);
				$upstage_string .= $this->create_insert_statement(SCENARIOS,$filtered_scenario);
				
				if( $scenario->on_eval_scenario > 0 )
				{
					$upstage_string .= $this->separator(3);
					$upstage_string .= $this->create_insert_statement(ACTION_GROUPS,array("id" => $scenario->on_eval_scenario), TRUE);
					
					# Get all action_configurations of this action group
					$action_configurations = array();
					$action_configurations = $this->action_configurations_model->Find(array("action_group_id" => $scenario->on_eval_scenario));
					
					foreach ($action_configurations as $action_configuration) 
					{
						$filtered_action_configuration = $this->filter($action_configuration,ACTION_CONFIGURATIONS);
						
						$upstage_string .= $this->separator(4);
						$upstage_string .= $this->create_insert_statement(ACTION_CONFIGURATIONS,$filtered_action_configuration);
					}
				}
				
				if( $scenario->on_mo > 0 )
				{
					$upstage_string .= $this->separator(3);
					$upstage_string .= $this->create_insert_statement(ACTION_GROUPS,array("id" => $scenario->on_mo), TRUE);
					
					# Get all action_configurations of this action group
					$action_configurations = array();
					$action_configurations = $this->action_configurations_model->Find(array("action_group_id" => $scenario->on_mo));
					
					foreach ($action_configurations as $action_configuration) 
					{
						$filtered_action_configuration = $this->filter($action_configuration,ACTION_CONFIGURATIONS);
						
						$upstage_string .= $this->separator(4);
						$upstage_string .= $this->create_insert_statement(ACTION_CONFIGURATIONS,$filtered_action_configuration);
					}
				}
				
				if( $scenario->on_dlr > 0 )
				{
					$upstage_string .= $this->separator(3);
					$upstage_string .= $this->create_insert_statement(ACTION_GROUPS,array("id" => $scenario->on_dlr), TRUE);
					
					# Get all action_configurations of this action group
					$action_configurations = array();
					$action_configurations = $this->action_configurations_model->Find(array("action_group_id" => $scenario->on_dlr));
					
					foreach ($action_configurations as $action_configuration) 
					{
						$filtered_action_configuration = $this->filter($action_configuration,ACTION_CONFIGURATIONS);
						
						$upstage_string .= $this->separator(4);
						$upstage_string .= $this->create_insert_statement(ACTION_CONFIGURATIONS,$filtered_action_configuration);
					}
				}
				
				if( $scenario->on_dlr_nack > 0 )
				{
					$upstage_string .= $this->separator(3);
					$upstage_string .= $this->create_insert_statement(ACTION_GROUPS,array("id" => $scenario->on_dlr_nack), TRUE);
					
					# Get all action_configurations of this action group
					$action_configurations = array();
					$action_configurations = $this->action_configurations_model->Find(array("action_group_id" => $scenario->on_dlr_nack));
					
					foreach ($action_configurations as $action_configuration) 
					{
						$filtered_action_configuration = $this->filter($action_configuration,ACTION_CONFIGURATIONS);
						
						$upstage_string .= $this->separator(4);
						$upstage_string .= $this->create_insert_statement(ACTION_CONFIGURATIONS,$filtered_action_configuration);
					}
				}
			}
		}
		
		return $upstage_string;
	}
	
	public function profiling_upstage($tables)
	{
		$script = "";
		foreach ($tables as $table)
		{
			$structure = $this->upstage_model->build_table_structure($table);
		
			$script .= $this->separator(0);	
			$script .= $this->create_user_input_table($table, $structure['type'], $structure['fields']);
		}
		
		return $script;
	}
	
	public function service_cleanup($service_id)
	{
		$cleanup_string = "";
		
		$cleanup_string .= $this->separator(0);
		$cleanup_string .= $this->create_delete_statement(SERVICES,array("id" => $service_id));
		
		
		$cleanup_string .= $this->separator(1);
		$cleanup_string .= $this->create_delete_statement(SERVICE_ROUTES,array("service_id" => $service_id));
		
		# Based on service id, get routes
		$routes = $this->services_model->DisplayRoutes($service_id);
		foreach ($routes as $route) 
		{
			
			# Based on routes, get scenarios
			$scenarios = array();
			$keywords = array();
			
			$scenarios = $this->service_routes_model->DisplayScenarios($route->id);
			$keywords = $this->service_routes_model->DisplayKeywords($route->id);
			
			$cleanup_string .= $this->separator(2);
			$cleanup_string .= $this->create_delete_statement(KEYWORDS,array("service_route_id" => $route->id));
			
			foreach ($keywords as $keyword)
			{
				$cleanup_string .= $this->separator(3);
				$cleanup_string .= $this->create_delete_statement(KEYWORD_OPERATORS,array("keyword_id" => $keyword->id));
				
				$cleanup_string .= $this->separator(3);
				$cleanup_string .= $this->create_delete_statement(KEYWORD_ALIASES,array("keyword_id" => $keyword->id));
			}
			
			$cleanup_string .= $this->separator(2);
			$cleanup_string .= $this->create_delete_statement(SCENARIOS,array("service_route_id" => $route->id));
			
			foreach ($scenarios as $scenario)
			{
				# Based on scenarios, get action groups ( on_eval_scenario, on_mo, on_dlr, on_dlr_nack )
				if( $scenario->on_eval_scenario > 0 )
				{
					$cleanup_string .= $this->separator(3);
					$cleanup_string .= $this->create_delete_statement(ACTION_GROUPS,array("id" => $scenario->on_eval_scenario));
					$cleanup_string .= $this->separator(4);
					$cleanup_string .= $this->create_delete_statement(ACTION_CONFIGURATIONS,array("action_group_id" => $scenario->on_eval_scenario));
				}
				
				if( $scenario->on_mo > 0 )
				{
					$cleanup_string .= $this->separator(3);
					$cleanup_string .= $this->create_delete_statement(ACTION_GROUPS,array("id" => $scenario->on_mo));
					$cleanup_string .= $this->separator(4);
					$cleanup_string .= $this->create_delete_statement(ACTION_CONFIGURATIONS,array("action_group_id" => $scenario->on_mo));
				}
				
				if( $scenario->on_dlr > 0 )
				{
					$cleanup_string .= $this->separator(3);
					$cleanup_string .= $this->create_delete_statement(ACTION_GROUPS,array("id" => $scenario->on_dlr));
					$cleanup_string .= $this->separator(4);
					$cleanup_string .= $this->create_delete_statement(ACTION_CONFIGURATIONS,array("action_group_id" => $scenario->on_dlr));
				}
				
				if( $scenario->on_dlr_nack > 0 )
				{
					$cleanup_string .= $this->separator(3);
					$cleanup_string .= $this->create_delete_statement(ACTION_GROUPS,array("id" => $scenario->on_dlr_nack));
					$cleanup_string .= $this->separator(4);
					$cleanup_string .= $this->create_delete_statement(ACTION_CONFIGURATIONS,array("action_group_id" => $scenario->on_dlr_nack));
				}
				
			}
		}
		
		// Delete all keywords and its aliases that don't have parent routes by selecting all id of routes from all services
		$route_ids = array();
		
		
		
		return $cleanup_string;
	}
	
	public function set_instance($instance_id)
	{
		$this->instance = $this->upstage_model->get_instance($instance_id);
	}
	
	public function execute_upstage($script, $db_type)
	{
		# Connect to remote server
		if( strcasecmp($db_type,"profile") == 0 )
		{
			$config['hostname'] = $this->instance->profile_ip;
			$config['username'] = $this->instance->profile_mysql_user;
			$config['password'] = $this->instance->profile_mysql_pass;
			$config['database'] = $this->instance->profile_db_name;
		}
		else 
		{
			$config['hostname'] = $this->instance->system_ip;
			$config['username'] = $this->instance->system_mysql_user;
			$config['password'] = $this->instance->system_mysql_pass;
			$config['database'] = $this->instance->system_db_name;
		}
		
		$config['dbdriver'] = "mysql";
		$config['dbprefix'] = "";
		$config['pconnect'] = FALSE;
		$config['db_debug'] = TRUE;
		
		//debug($this->instance);
		//debug($config);
		
		$this->CI->load->model('Remote_model', '', $config);
		$this->remote_model = &$this->CI->Remote_model;
		$this->remote_model->load_db($config);

		# Explode script per line
		$aSQL = explode("\n", $script);
		
		# Counter
		$ok_counter = 0;
		$nok_counter = 0;
		
		# For each line, execute via mysql
		foreach ($aSQL as $sql)
		{
			if( trim($sql) == "" ) continue;
			
			log_message('debug',"SQL:".trim($sql));
			if( $this->remote_model->execute(trim($sql)) )
			{
				++$ok_counter;
			}
			else 
			{
				++$nok_counter;
			}
		}
		
		return "$ok_counter|$nok_counter";
	}
	
	public function separator($level)
	{
		#$separator = "<br />"; // Use this for testing when output to screen
		$separator = "\n"; // Use this for production when output to file
		
		for ($i = 0; $i < $level; $i++ )
		{
			#$separator .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			$separator .= "";
		}
		
		return $separator;
	}
	
	public function create_truncate_statement($table)
	{
		return "TRUNCATE TABLE `$table`";
	}
	
	public function create_insert_statement($table, $data_array, $ignore = false)
	{
		$fields = "";
		$values = "";
		$table = mysql_real_escape_string($table);
		/*
		if( !is_array($data_array) || count($data_array) < 1 )
		{
			# No statement to create so return blank
			return "";
		}
		*/
		foreach ($data_array as $field => $value)
		{
			//`create_by`,`create_date`,`update_by`,`update_date`
//			if ($field != 'create_by' && $field != 'create_date' && $field != 'update_by' && $field != 'create_date')
//			{
				$fields .= "`".mysql_real_escape_string($field)."`,";
				$values .= "'".mysql_real_escape_string($value)."',";
//			}
		}
		$fields = trim($fields,",");
		$values = trim($values,",");
		if( !$ignore )
		{
			$sql = "INSERT INTO `$table` ($fields) VALUES ($values)";
		}
		else 
		{
			$sql = "INSERT IGNORE INTO `$table` ($fields) VALUES ($values)";
		}
		
		return $sql;
	}
	
	public function create_delete_statement($table,$conditions)
	{
		$condition = "";
		$table = mysql_real_escape_string($table);
		
		foreach ($conditions as $field => $value )
		{
			$condition .= "`".mysql_real_escape_string($field)."` = '".mysql_real_escape_string($value)."' AND ";
		}
		
		$condition = substr($condition,0,strlen($condition)-4);
		
		$sql = "DELETE FROM `$table` WHERE $condition";
		
		return $sql;
	}
	
	public function create_update_statement($table, $data, $conditions)
	{
		$condition = "";
		$value_pairs = "";
		$table = mysql_real_escape_string($table);
		
		foreach ($data as $field => $value )
		{
			$value_pairs .= "`".mysql_real_escape_string($field)."` = '".mysql_real_escape_string($value)."',";
		}
		
		$value_pairs = trim($value_pairs,",");
		
		foreach ($conditions as $field => $value )
		{
			$condition .= "`".mysql_real_escape_string($field)."` = '".mysql_real_escape_string($value)."' AND ";
		}
		
		$condition = substr($condition,0,strlen($condition)-4);
		
		$sql = "UPDATE `$table` SET $value_pairs WHERE $condition";
		
		return $sql;
	}
	
	public function create_user_input_table($table, $type, $fields)
	{
		$script = "CREATE TABLE IF NOT EXISTS `".$type['prefix'].$table."` (";
		$script .= "`id` bigint(11) unsigned NOT NULL auto_increment,";
		$script .= "`msisdn` varchar(13) NOT NULL default '0',";		
		
		if( strcasecmp($type['type'],"poll") == 0 || strcasecmp($type['type'],"react") == 0 || strcasecmp($type['type'],"vote") == 0 || strcasecmp($type['type'],"donate") == 0 )
		{
			$script .= "`service_id` int(11) unsigned NOT NULL,";
			$script .= "`key` varchar(45) NOT NULL,";
			$script .= "`operator` varchar(12) default NULL,";
		}
		else if( strcasecmp($type['type'],"reg") == 0 )
		{
			$script .= "`service_group_id` int(11) unsigned NOT NULL,";
			$script .= "`key` varchar(45) NOT NULL,";
			$script .= "`operator` varchar(12) default NULL,";
		}
		else if( strcasecmp($type['type'],"entry") == 0 )
		{
			$script .= "`keyword` varchar(45) NOT NULL,";
			$script .= "`telco` varchar(12) default NULL,";
			$script .= "`message` varchar(255) default NULL,";
			$script .= "`isvalid` char(1) default '0',";
			$script .= "`rafflenumber` int(11) default '0',";
		}
		else if( strcasecmp($type['type'],"promoreg") == 0 )
		{
			$script .= "`telco` varchar(12) default NULL,";
			$script .= "`name` varchar(50) default NULL,";
			$script .= "`age` int(11) default '0',";
			$script .= "`gender` varchar(6) default NULL,";
			$script .= "`address` text,";
		}
		
		$script .= "`timestamp` datetime default '0000-00-00 00:00:00',";
		
		if( strcasecmp($type['type'],"poll") == 0 || strcasecmp($type['type'],"react") == 0 || strcasecmp($type['type'],"reg") == 0 || strcasecmp($type['type'],"donate") == 0)
		{
			$script .= "`pattern` varchar(255) NOT NULL,";
		}
		else if( strcasecmp($type['type'],"vote") == 0 )
		{
			$script .= "`action` varchar(45) NOT NULL default 'save',";
			$script .= "`votes` int(11) unsigned NOT NULL,";
		}
		
		if( strcasecmp($type['type'],"react") == 0 )
		{
			$script .= "`flag` tinyint(2) NOT NULL default '0',";
			$script .= "`aired` tinyint(2) NOT NULL default '0',";
		}
		
		if( isset($fields) && is_array($fields) )
		{
			foreach ($fields as $field => $data_type)
			{
				$script .= "`$field` $data_type,";
			}
		}
		
		$script .= "PRIMARY KEY  (`id`)";
		$script .= ")";
		
		return $script;
	}
	
	private function filter($array, $table)
	{
		if( isset($this->config['filter'][$table]) && is_array($this->config['filter'][$table]) && !empty($this->config['filter'][$table]) )
		{
			if( isset($array) )
			{
				if( $array instanceof stdClass )
				{
					foreach ($array as $key => $value )
					{
						if( !in_array($key, $this->config['filter'][$table]) )
						{
							unset($array->$key);
						}
					}
				}
				else if( is_array($array) )
				{
					foreach ($array as $key => $value )
					{
						if( !in_array($key, $this->config['filter'][$table]) )
						{
							unset($array[$key]);
						}
					}
				}
			}
		}
		
		return $array;
	}
	
	public function quick_content_upstage($id, $instance)
	{
		$script = $this->separator(0);
		$script .= $this->create_delete_statement(CONTENTS,array("id" => $id));
		
		$content = $this->contents_model->Find(array("id" => $id));
		
		$filtered_content = $this->filter($content[0],CONTENTS);
		
		$script .= $this->separator(0);
		$script .= $this->create_insert_statement(CONTENTS,$filtered_content);
		
		$config['hostname'] = $instance->system_ip;
		$config['username'] = $instance->system_mysql_user;
		$config['password'] = $instance->system_mysql_pass;
		$config['database'] = $instance->system_db_name;
		$config['dbdriver'] = "mysql";
		$config['dbprefix'] = "";
		$config['pconnect'] = FALSE;
		$config['db_debug'] = TRUE;
		
		$this->CI->load->model('Remote_model', '', $config);
		$this->remote_model = &$this->CI->Remote_model;
		$this->remote_model->load_db($config);
		
		# Explode script per line
		$aSQL = explode("\n", $script);
		
		# Counter
		$ok_counter = 0;
		$nok_counter = 0;
		
		# For each line, execute via mysql
		foreach ($aSQL as $sql)
		{
			if( trim($sql) == "" ) continue;
			
			log_message('debug',"SQL:".trim($sql));
			if( $this->remote_model->execute(trim($sql)) )
			{
				++$ok_counter;
			}
			else 
			{
				++$nok_counter;
			}
		}
		
		if( $nok_counter > 0 )
		{
			log_message('error',$nok_counter." error(s) occurred. ".$ok_counter." were successful.");
			return false;
		}
		
		log_message('debug',$ok_counter." sql statement(s) was/were successfully executed.");
		
		$ip = $instance->memcache_ip;
		$port = $instance->memcache_port;
		log_message('debug','Clearing cache in '.$ip.":".$port);
		$memcache = new Memcache();
		$memcache->connect($ip,$port);
		$memcache->flush();
		
		return true;
	}
	
	public function clear_cache()
	{
		$ip = $this->instance->memcache_ip;
		$port = $this->instance->memcache_port;
		log_message('debug','Clearing cache in '.$ip.":".$port);
		$memcache = new Memcache();
		$memcache->connect($ip,$port);
		$memcache->flush();
	}
}
