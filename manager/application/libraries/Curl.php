<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 * @author        	Jerwin Espiritu
 * @license         Solucient Inc.
 * @link			http://solucientinc.com
 */
class Curl {
	public  $baseUrl = "http://devapi.iagh.solucientinc.com";
	private $queryString;
	private $url;
	private $papiCall;
	private $method;
	public $options = array();

	
	public function __construct(){
	}
	
	private function setQueryString($params){
		$data = $params;
		if(is_array($data)){
			if(!empty($data)){
				foreach ($data as $key => $value){
					$this->queryString[] = urlencode($key) ."=". urlencode($value);
				}
				return true;
			}
		}
		else{
			if($params != ""){
				$this->queryString[] = $params;
			}
			return true;
		}
		return false;
	}
	
	public function PAPIConfig($params){
		$valid = true;
		$res['message'] = "";
		$res['valid'] = $valid;
		if(!isset($params['method'])){
			$res['message'] = "Please assign method GET or POST, ex. \$params['method']='GET'\n";
			$valid = false;
			$res['valid'] = $valid;
		}
		if(!isset($params['data'])){
			$res['message'] .= "Please assign data parameters\n";
			$valid = false;
			$res['valid'] = $valid;
		}
		if(!isset($params['papiCall'])){
			$res['message'] .= "Please assign PAPI function\n";
			$valid = false;
			$res['valid'] = $valid;
		}
		if($valid){
			# set method type
			$this->method = isset($params['method'])?strtoupper($params['method']):"GET";
			# setup PAPI call
			$papiCall = strtolower($params['papiCall']);
			$this->papiCall = str_ireplace(".","/",$papiCall);
			$this->papiCall = "/{$this->papiCall}/";
			# setup query string		
			$this->setQueryString($params['data']);
			$res['message'] = "Ready to run!";
		}
		return $res;
	}
	
	public function execute(){
		$response = array();
		
		$curl_handle = curl_init();
		$queryString =  implode("&",$this->queryString);
		if($this->method == "GET"){
			$this->url = $this->baseUrl.$this->papiCall."?".$queryString;
		}
		else{
			$this->url = $this->baseUrl.$this->papiCall;
		}
		# default options
		$options = array
		(
		    CURLOPT_URL=>$this->url,
		    CURLOPT_HEADER=>0,
		    CURLOPT_RETURNTRANSFER=>1
		);
		if($this->method=="POST"){
			$options[CURLOPT_POSTFIELDS] = $queryString;
			$options[CURLOPT_POST]=1;
		}
		else{
		    $options[CURLOPT_POST]=0;
		}
		# additional options
		if(!empty($this->options)){
			foreach ($this->options as $key => $value){
				$options[$key] = $value;
			}
		}
		
		curl_setopt_array($curl_handle,$options);
		$PAPIResponse = curl_exec($curl_handle);
		
		curl_close($curl_handle);
		if($PAPIResponse){
			$response['error'] = 0;
			$response['message'] = $PAPIResponse;
		}
		else {
			$response['error'] = 0;
			$response['message'] = "No response, please check your configuration";
		}
		return $response;
	}
}