<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Members extends CI_Controller {

	private $permission = array();
	private $userPermission = "";
	
	public function __construct()
	{
		parent::__construct();

		$this->tal->title = $this->config->item('title');
		$this->tal->base_url = substr(base_url(), 0, -1);

		# check if session expires
		if(!$this->ion_auth->user()->result()){
			exit;
		}
		
		$this->user = $this->ion_auth->user()->result();
		$this->user[0]->fullname = $this->user[0]->first_name . ' ' . $this->user[0]->last_name;
		$config = array('userID' => $this->user[0]->id);
		$this->load->library('acl', $config);
//		debug($this->acl);
		if (!$this->acl->userRoles) {
			exit;
		}
		else{
			# get role permission
			$this->permission = $this->acl->getRolePerms($this->acl->userRoles[0]);
			$userPermission = $this->acl->getUserPerms($this->acl->userID);
			$this->userPermission = $userPermission[0]['id'];
		}
	}
	
	public function index(){
		$data = array();
		$resp = array();
		$rolePermission = "";
		$users = array();
		foreach($this->permission as $index => $val){
			$rolePermission = strtolower($val['name']);
		}
		if($this->userPermission <= 2){
			if($rolePermission!=""){
				switch($rolePermission){
					case "superuser":
						$allUsers = $this->acl->getAllUsers('full');
						for($i=0; $i<count($allUsers); $i++){
							if($allUsers[$i]['id']!=$this->acl->userID){
								$users[] = $allUsers[$i];
							}
						}
						break;
					case "administrator":
						$resp = $this->acl->getAllUsersByRole($this->acl->userRoles[0]);
						for($i=0; $i<count($resp); $i++){
							if($resp[$i]->id != $this->acl->userID){
								$users[] = get_object_vars($resp[$i]);
							}
						}					
						break;
				}
			}
		}
		else {
			redirect('404');			
		}
//		$data = $this->acl->getAllUsers('full');
		$data['isAllowed'] = $this->userPermission==1||$this->userPermission==2?true:false;
		$data['data'] = $users;
		$this->load->view('members/index.zpt', $data);
	}	
	
	public function create(){
		
		if($this->userPermission <= 2){
			$data = array();
			$rolePermission = "";
			$permissions = array();
			
			$permissions = $this->acl->getAllPerms('full');
			
			foreach($this->permission as $index => $val){
				$rolePermission = strtolower($val['name']);
			}
			
			if($rolePermission == "superuser"){
				$roles = array();		
				$roles = $this->acl->getAllRoles('full');
			}
			
			if($rolePermission == "administrator"){
				$roles = $this->acl->userRoles[0];
			}
			
			$data['isAddRequest'] = true;
			$data['roles'] = $roles;
			$data['permissions'] = $permissions;
			$this->load->view('members/memberForm.zpt', $data);
		}
	}
	
	public function edit(){
		if($this->userPermission <= 2){
			if(isset($_GET['id'])){
				$userId = $_GET['id'];
				$data = array();
				$rolePermission = "";
				$currentRole = 0;
				$currentPermission = 0;
				
				$permissions = array();
				
				$permissions = $this->acl->getAllPerms('full');
				
				foreach($this->permission as $index => $val){
					$rolePermission = strtolower($val['name']);
				}
				
				if($rolePermission == "superuser"){
					$roles = array();		
					$roles = $this->acl->getAllRoles('full');
				}
				
				if($rolePermission == "administrator"){
					$roles = $this->acl->userRoles[0];
				}
				# get User Info
				$userInfo = $this->acl->getUser($userId); 
				$userInfo = get_object_vars($userInfo[0]);
				
				$currentRole = $this->acl->getUserRoles($userId);
				$currentRole = isset($currentRole[0])?$currentRole[0]:0;
				$currentPermission = $this->acl->getUserPerms($userId);
				$currentPermission = isset($currentPermission[0]['id'])?$currentPermission[0]['id']:0;
				
				$data['user'] = $userInfo;
				$data['isAddRequest'] = false;
				$data['currentRole'] = $currentRole;
				$data['currentPermission'] = $currentPermission;
				$data['roles'] = $roles;
				$data['permissions'] = $permissions;
				$this->load->view('members/memberForm.zpt', $data);
			}
			else {
				exit;
			}
		}
	}
	
	public function settings(){
		$userId = $this->acl->userID;
		if($userId)	{
			$data = array();
			$permissions = "";
			$roles = "";
			$userInfo = array();
			# get User Info
			$userInfo = $this->acl->getUser($userId);
			$userInfo = get_object_vars($userInfo[0]);			
			$roles = $this->acl->userRoles[0];
			$currentPermission = $this->acl->getUserPerms($userId);
			$currentPermission = isset($currentPermission[0]['id'])?$currentPermission[0]['id']:0;
			$data['user'] = $userInfo;
			$data['roles'] = $roles;
			$data['permissions'] = $currentPermission;
			$this->load->view('members/myAccount.zpt', $data);
		}
	}
	
	public function docreate(){
		$res = array();
		$res['status'] = 0;
		$res['message'] = "";
		$data = array();
		$message = "";
		
		$post = $this->input->post('data');
		$param = json_decode($post);
		
		foreach ($param as $item){
			$data[$item->name] = $item->value;
		}

		/*
		 * Parameter check
		 * 1. Does username already exists?
		 * 2. Does email already in use?
		 * 3. Password matched?
		*/
		$code = 0;
		$c_user = 1;
		$c_pass = 3;
		$c_email = 5;
		
		// Does username exists?
		if ($this->ion_auth->username_check($data['username']))
			$code = $c_user;
			
		// Password matched?
		if (strcmp($data['password'], $data['check']) != 0)
			$code = $code?$code:$c_pass;

		// Does email already in use?
		if ($this->ion_auth->email_check($data['email']))
			$code = $code?$code:$c_email;
			
		$code = trim($code, '|');
		
		$response = array();
		if ($code == 0){
			$response['status'] = 'ACK';

			// You've come this far. Congratulations! Create the account now.
			$username = $data['username'];
			$password = $data['password'];
			$email = $data['email'];
			
			$member_data = array(
				'first_name' => $data['first_name'],
				'last_name' => $data['last_name']
			);		
			$id = $this->ion_auth->register($username, $password, $email, $member_data);
			$this->acl->assignUserRole($id, $data['role']);
			$this->acl->assignUserPermission($id, $data['permission']);
			$res['status'] = 1;
			$res['message'] = "You have successfully created a role.";
		}
		else {
			$response['status'] = 'NACK';			
			$response['code'] = $code;
			$res['status'] = 0;
			switch ($code){
				case 1:
					$message = "Sorry, username already exists!";
					break;
				case 3:
					$message = "The password and confirm password do not match!";
					break;
				case 5:
					$message = "Email address already in use!";
			}
			$res['message'] = $message;
		}
		
		print_r(json_encode($res));
	}
	
	public function doedit(){
		$post = $this->input->post('data');
		$param = json_decode($post);
		$res = array();
		$res['status'] = 0;
		$data = array();
		
		foreach ($param as $item){
			$data[$item->name] = $item->value;
		}

		/*
		 * Parameter check
		 * 1. Does username already exists?
		 * 2. Does email already in use?
		 * 3. Password matched?
		*/
		$code = 0;
		$c_user = 1;
		$c_pass = 3;
		$c_email = 5;
		
		# get User Info
		$userInfo = $this->acl->getUser($data['userid']);
		$userInfo = get_object_vars($userInfo[0]);

		
		if($userInfo['username']!=$data['username']){
			// Does username exists?
			if ($this->ion_auth->username_check($data['username']))
				$code = $c_user;
		}
			
		// Password matched?
		if (strcmp($data['password'], $data['check']) != 0)
			$code = $code?$code:$c_pass;

		if($userInfo['email']!=$data['email']){
			// Does email already in use?
			if ($this->ion_auth->email_check($data['email']))
				$code = $code?$code:$c_email;
		}
		
		if ($code == 0){
			// You've come this far. Congratulations! Create the account now.
			$username = $data['username'];
			$password = $data['password'];
			$email = $data['email'];
			
			$newData = array(
				'first_name' => $data['first_name'],
				'last_name' => $data['last_name'],
				'username' => $data['username'],
				'password' => $data['password'],
				'email' => $data['email']
			);
			
			$this->ion_auth->updateUser($data['userid'], $newData);
			$this->acl->assignUserRole($data['userid'], $data['role']);
			$this->acl->assignUserPermission($data['userid'], $data['permission']);
			$res['status'] = 1;
			$res['message'] = "Changes have been successfully saved!.";
		}
		else {
			$res['status'] = 0;
			switch ($code){
				case 1:
					$message = "Sorry, username already exists! " . $data['userid'];
					break;
				case 3:
					$message = "The password and confirm password do not match!";
					break;
				case 5:
					$message = "Email address already in use!";
			}
			$res['message'] = $message;
		}
		
		print_r(json_encode($res));		
	}
	
	
	public function deleteSelected(){
		$res = array();
		$res['status'] = 0;
		$res['message'] = "";
		if(isset($_POST['id']) && !empty($_POST['id'])){
			$id = explode(",", $_POST['id']);			
			for($i=0; $i<count($id); $i++){
				$this->acl->removeUserPermission($id[$i]);
				$this->ion_auth->delete_user($id[$i]);
			}
			$res['status'] = 1;
		}
		print_r(json_encode($res));		
		
	}

}