<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller{
	
	public function __construct(){
		parent::__construct();
		$this->tal->title = $this->config->item('title');
		$this->tal->base_url = substr(base_url(), 0, -1);	
						
		# check if session expires
		if(!$this->ion_auth->user()->result()){
			exit;
		}
		
		$this->user = $this->ion_auth->user()->result();		
		$this->user[0]->fullname = $this->user[0]->first_name . ' ' . $this->user[0]->last_name;
		$config = array('userID' => $this->user[0]->id);			
		$this->load->library('acl', $config);		
		
		if (!$this->acl->hasRole(1)) {
			exit;
		}				
	}
	
	public function index(){
		# dashboard model
		$this->tal->display('dashboard/index.zpt');		
	}
}