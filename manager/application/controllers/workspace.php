<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Workspace extends CI_Controller {

	private $workspaceId;
	private $userPermission;
	private $permission;
	
	public function __construct(){
		parent::__construct();
		$this->tal->title = $this->config->item('title');
		$this->tal->base_url = substr(base_url(), 0, -1);	
		$this->user = $this->ion_auth->user()->result();
		if(!$this->ion_auth->user()->result()){
			exit;
		}
		$this->user[0]->fullname = $this->user[0]->first_name . ' ' . $this->user[0]->last_name;
		$config = array('userID' => $this->user[0]->id);			
		$this->load->library('acl', $config);
		$this->load->library('curl');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url', 'file'));
		if (!$this->acl->userRoles) {
			exit;
		}
		else{
			$this->workspaceId = $this->acl->userRoles[0];
			# get role permission
			$this->permission = $this->acl->getRolePerms($this->acl->userRoles[0]);
			$userPermission = $this->acl->getUserPerms($this->acl->userID);
			$this->userPermission = $userPermission[0]['id'];			
		}
	}
	
	public function papiCall($method="GET", $function="", $options=array()){
		$papi = array();
		$papi['method'] = $method;
		$papi['papiCall'] = $function;
		$papi['data'] = $options;
		$this->curl->PAPIConfig($papi);
		$res = $this->curl->execute();
		return json_decode($res['message']);
	}
	
	public function viewContactDetails(){
		$options = array("ID"=>$this->workspaceId);
		$apiRes = $this->papiCall("GET", "Workspace.Get", $options);
		$data = array();
		if($apiRes->Status){
			$data['workspace'] = $apiRes->Workspace;
		}
		$this->load->view('workspace/contactDetails.zpt', $data);
	}
	
	public function doEditWorkspace(){
		$res = array();
		$res['status'] = 0;
		$res['message'] = "Unable to update workplace details";
		$options =array();
		if(!empty($_POST['data'])){
			$input = json_decode($_POST['data']);
			$params = array();
			foreach($input as $item){
				$params[$item->name] = $item->value;
			}
			$options = array("ID"=>$this->workspaceId,
							 "Name"=>$params['name'],
							 "Contact"=>$params['contact'],
							 "ContactEmail"=>$params['contactEmail'],
							 "ContactPhone"=>$params['contactPhone'],
							 "ITAdmin"=>$params['ITAdmin'],
							 "ITAdminEmail"=>$params['ITAdminEmail'],
							 "ITAdminPhone"=>$params['ITAdminPhone']
							 );
			$apiRes = $this->papiCall("POST", "Workspace.Update", $options);
			if($apiRes->Status){
				$res['status'] = 1;
				$res['message'] = "Changes has been saved successfully!";
			}
		}
		print_r(json_encode($res));
	}
	
	public function viewLocations(){
		$data['location'] = array();
		$apiRes = $this->papiCall("GET", "Location.GetAll", array("WorkspaceID"=>$this->workspaceId));
		if($apiRes->Status){
			$data['locations'] = $apiRes->Locations;
		}
		$this->load->view('workspace/location.zpt', $data);
	}
	
	public function viewDepartments(){
		$data['departments'] = array();
		$apiRes = $this->papiCall("GET", "Department.GetAll", array("WorkspaceID"=>$this->workspaceId));
		if($apiRes->Status){
			$data['departments'] = $apiRes->Departments;
		}
		$this->load->view('workspace/department.zpt', $data);
	}
	
	public function addLocation(){
		$data = array();
		# add request
		$data['isAddRequest'] = true;
		$this->load->view('workspace/locationForm.zpt', $data);
	}
	
	public function editLocation(){
		$data = array();
		if(isset($_GET['id'])){
			$data['isAddRequest'] = false;
			$options = array("ID"=>$_GET['id']);
			$apiRes = $this->papiCall("GET", "Location.Get", $options);
			if($apiRes->Status){
				$data['location'] = $apiRes->Location;
			}
			$this->load->view('workspace/locationForm.zpt', $data);
		}
	}
	
	public function saveLocation(){
		$res = array();
		$res['status'] = 0;
		$res['message'] = "Failed to save location!";
		if(isset($_POST['data'])){
			$params = json_decode($_POST['data']);
			$dataParams = array();
			foreach($params as $item){
				$dataParams[$item->name] = $item->value;
			}
			# add request
			if($dataParams['locationId'] == "0"){
				$options = array("Name"=>$dataParams['name'], "WorkspaceID"=>$this->workspaceId);
				$apiRes = $this->papiCall("POST", "Location.Create", $options);
				if($apiRes->Status){
					$res['status'] = 1;
					$res['message'] = "New location was added!";
				}
			}
			# edit request
			else{
				$options = array("ID"=>$dataParams['locationId'], "Name"=>$dataParams['name'], "Workspace"=>$this->workspaceId);
				$apiRes = $this->papiCall("POST", "Location.Update", $options);
				if($apiRes->Status){
					$res['status'] = 1;
					$res['message'] = "Changes has been successfully saved!";
				}
			}
		}
		print_r(json_encode($res));
	}
	
	public function addDepartment(){
		$data = array();
		# add request
		$data['isAddRequest'] = true;
		$this->load->view('workspace/departmentForm.zpt', $data);
	}
	
	public function editDepartment(){
		$data = array();
		if(isset($_GET['id'])){
			$data['isAddRequest'] = false;
			$options = array("ID"=>$_GET['id']);
			$apiRes = $this->papiCall("GET", "Department.Get", $options);
			if($apiRes->Status){
				$data['department'] = $apiRes->Department;
			}
			$this->load->view('workspace/departmentForm.zpt', $data);
		}
	}
	
	public function saveDepartment(){
		$res = array();
		$res['status'] = 0;
		$res['message'] = $_POST['data'];
		if(isset($_POST['data'])){
			$params = json_decode($_POST['data']);
			$dataParams = array();
			foreach($params as $item){
				$dataParams[$item->name] = $item->value;
			}
			# add request
			if($dataParams['departmentId'] == "0"){
				$options = array("Name"=>$dataParams['name'], "WorkspaceID"=>$this->workspaceId);
				$apiRes = $this->papiCall("POST", "Department.Create", $options);
				if($apiRes->Status){
					$res['status'] = 1;
					$res['message'] = "New Department was added!";
				}
			}
			# edit request
			else{
				$options = array("ID"=>$dataParams['departmentId'], "Name"=>$dataParams['name'], "Workspace"=>$this->workspaceId);
				$apiRes = $this->papiCall("POST", "Department.Update", $options);
				if($apiRes->Status){
					$res['status'] = 1;
					$res['message'] = "Changes has been successfully saved!";
				}
			}
		}
		print_r(json_encode($res));
	}
	
	public function organizationSettings(){
		$data['domains'] = array();
		$options = array("ID"=>$this->workspaceId);
		$apiRes = $this->papiCall("GET", "Workspace.GET", $options);
		if($apiRes->Status){
			$domain_ = $apiRes->Workspace->Domains;
			$data['domains'] = explode(",", $domain_);
		}
		$this->load->view('workspace/organizationSettings.zpt', $data);
	}
	
	public function saveDomain(){
		$domain = "";
		$res = array();
		$res['status'] = 0;
		$res['message'] = "";
		$params = json_decode($_POST['data']);
		$data = array();
		foreach ($params as $item){
			$data[] = $item->value;
		}
		$domain = implode(",", $data);
		$options = array("ID"=>$this->workspaceId, "Domains"=>$domain);
		$apiRes = $this->papiCall("POST", "Workspace.Update", $options);
		if($apiRes->Status){
			$res['status'] = 1;
			$res['message'] = "Domain successfully saved!";
		}
		print_r(json_encode($res));
	}
}