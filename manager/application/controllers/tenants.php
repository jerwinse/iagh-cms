<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tenants extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
				
		$this->tal->title = $this->config->item('title');
		$this->tal->base_url = substr(base_url(), 0, -1);	
				
		$this->user = $this->ion_auth->user()->result();		
		$this->user[0]->fullname = $this->user[0]->first_name . ' ' . $this->user[0]->last_name;
		$config = array('userID' => $this->user[0]->id);			
		$this->load->library('acl', $config);		
		if (!$this->acl->hasRole($this->user[0]->id)) 
		{
			exit;
		}		
	}
	
	public function index()
	{		
		$this->load->model('cloudmsngr_model');
		$data = $this->cloudmsngr_model->getAllTenants();
		$this->tal->data = $data;
		$this->tal->display('tenants/index.zpt');		
	}
		
	public function create()
	{
		$this->tal->display('tenants/create.zpt');
	}
	
	public function docreate()
	{
		$post = $this->input->post('data');
		$param = json_decode($post);
		$data = array();
		foreach ($param as $item)
		{
			$data[$item->name] = $item->value;
		}
		$data['end_date'] = date('Y-m-d H:i:s', strtotime($data['end_date']));		
		$this->load->model('cloudmsngr_model');
		$id = $this->cloudmsngr_model->CreateTenant($data);
				
		$response = array('status' => 'NACK', 'message' => 'Cannot save record.');
		if ($id) {
			$this->load->library('cloudmsngr', $data);
			$this->cloudmsngr->CreateBasic($id);			
			$response = array('status' => 'ACK', 'message' => 'Record successfully created.');
		}
		
		$this->tal->data = json_encode($response);
		$this->tal->display('common/structure.zpt');
	}

}