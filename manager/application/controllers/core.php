<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Core extends CI_Controller{
	
	private $workspaceId;
	
	public function __construct(){
		parent::__construct();
		$this->tal->title = $this->config->item('title');
		$this->tal->base_url = substr(base_url(), 0, -1);	
		
		# check if session expires
		if(!$this->ion_auth->user()->result()){
			exit;
		}
		
		$this->user = $this->ion_auth->user()->result();		
		$this->user[0]->fullname = $this->user[0]->first_name . ' ' . $this->user[0]->last_name;
		$config = array('userID' => $this->user[0]->id);			
		$this->load->library('acl', $config);		
		
		if (!$this->acl->userRoles){
			exit;
		}
		else{
			$this->workspaceId = $this->acl->userRoles[0];
		}			
	}

	public function papiCall($method="GET", $function="", $options=array()){
		$papi = array();
		$papi['method'] = $method;
		$papi['papiCall'] = $function;
		$papi['data'] = $options;
		$this->curl->PAPIConfig($papi);
		$res = $this->curl->execute();
		return json_decode($res['message']);
	}
	
	public function login(){
		$options =array();
		$sectionId = 72;
		$data = array();
		$options = array("WorkspaceID"=>$this->workspaceId, "TypeID"=>3, "SectionID"=>$sectionId);
		$papiResult = $this->papiCall("GET", "Content.GetAll", $options);
		if($papiResult->Status){
			$data['videos'] = $papiResult->Content;
		}
		$this->load->view('core/login.zpt', $data);
	}
	
	public function saveVideoPage(){
		$res = array();
		$res['status'] = 0;
		$options = array();
		# edit video
		if($_POST['videoId']){
			$videoId = $_POST['videoId'];
			$options = array(
								"ID"=>$videoId,
								"TypeID"=>3,
								"Name"=>$_POST['title'],
								"Description"=>$_POST['description'],
								"URL"=>$_POST['url'],
								"ThumbnailURL"=>$_POST['thumbnailUrl'],
								"WorkspaceID"=>$this->workspaceId
							);
			$this->papiCall("POST", "Content.Update", $options);
			$res['status'] = 1;
		}
		# add video
		else{
			$sectionId = $_POST['sectionId'];
			$options = array(
								"TypeID"=>3,
								"Status"=>1,
								"Description"=>$_POST['description'],
								"Name"=>$_POST['title'],
								"URL"=>$_POST['url'],
								"ThumbnailURL"=>$_POST['thumbnailUrl'],
								"WorkspaceID"=>$this->workspaceId,
								"SectionID"=>$sectionId
							);
			$papiResult = $this->papiCall("POST", "Content.Create", $options);
			$res['status'] = 1;
		}
		print_r(json_encode($res));
	}
	
	public function home(){
		$options =array();
		$sectionId = 71;
		$data = array();
		$options = array("WorkspaceID"=>$this->workspaceId, "TypeID"=>3, "SectionID"=>$sectionId);
		$papiResult = $this->papiCall("GET", "Content.GetAll", $options);
		if($papiResult->Status){
			$data['videos'] = $papiResult->Content;
		}
		$this->load->view('core/home.zpt', $data);
	}
	
	public function addVideo(){
		if(isset($_GET['page'])){
			$page = $_GET['page'];
		}
		$sectionId = $page == "login" ? 72 : 71;
		$data['sectionId'] = $sectionId;
		$data['callback'] = $page;
		$data['isAddRequest'] = true;
		$data['workspaceId'] = $this->workspaceId;
		$this->load->view('core/videoForm.zpt', $data);
	}
	
	public function editVideo(){
		$data = array();
		$data['isAddRequest'] = false;
		$page = isset($_GET['page'])?$_GET['page']:"";
		if(isset($_GET['id'])){
			$videoId = $_GET['id'];
			$options = array();
			$options = array("ID"=>$videoId);
			$papiResult = $this->papiCall("GET", "Content.Get", $options);
			if($papiResult->Status){
				$data['video'] = $papiResult->Video;
			}
		}
		$data['callback'] = $page;
		$this->load->view('core/videoForm.zpt', $data);
	}
	
	public function deleteVideo(){
		$res = array();
		$res['status'] = 0;
		if(isset($_POST['id']) && !empty($_POST['id'])){
			$papiCall = $_POST['papiCall'];
			$id = explode(",", $_POST['id']);
			for($i=0; $i<count($id); $i++){
				$this->papiCall("POST", $papiCall, array("ID"=>$id[$i]));
			}
			$res['status'] = 1;			
		}
		print_r(json_encode($res));
	}
	
	public function publishVideo(){
		$res = array();
		$res['status'] = 0;
		$videoId = isset($_POST['videoId'])?$_POST['videoId']:"";
		$workspaceId = $this->workspaceId;
		if($videoId){
			$papiResult = $this->papiCall("GET", "Content.GetAll", array("TypeID"=>3, "WorkspaceID"=>$workspaceId));
			if($papiResult->Status){
				$content = $papiResult->Content;
				# set all status to 0
				for($i=0; $i<count($content);$i++){
					$this->papiCall("POST", "Content.Update", array("ID"=>$content[$i]->ID, "Status"=>0));
				}
			}
			# set status to 1
			$papiResult = $this->papiCall("POST", "Content.Update", array("ID"=>$videoId, "Status"=>1));
			$res['status'] = $papiResult->Status;
		}
		print_r(json_encode($res));
	}
}