<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Moodlifter extends CI_Controller{
	
	private $workspaceId;
	
	public function __construct(){
		parent::__construct();
		$this->tal->title = $this->config->item('title');
		$this->tal->base_url = substr(base_url(), 0, -1);	
		$this->user = $this->ion_auth->user()->result();
		if(!$this->ion_auth->user()->result()){
			exit;
		}
		$this->user[0]->fullname = $this->user[0]->first_name . ' ' . $this->user[0]->last_name;
		$config = array('userID' => $this->user[0]->id);			
		$this->load->library('acl', $config);
		$this->load->library('curl');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url', 'file'));
		if (!$this->acl->userRoles) {
			exit;
		}
		else{
			$this->workspaceId = $this->acl->userRoles[0];
		}
	}
	
	public function papiCall($method="GET", $function="", $options=array()){
		$papi = array();
		$papi['method'] = $method;
		$papi['papiCall'] = $function;
		$papi['data'] = $options;
		$this->curl->PAPIConfig($papi);
		$res = $this->curl->execute();
		return json_decode($res['message']);
	}
	
	private function verifyMimeType($mimeType=""){
		$mimeType = strtolower($mimeType);
		$res = array();
		$valid = 0;
		$typeId = 0; 
		# Types (1=Txt, 2=Image, 3=Video and 4=PDF)
		switch ($mimeType){
			case "text/css":
				$typeId = 1; $valid = 1;
				break;
			case "image/gif":
				$typeId = 2; $valid = 1;
				break;
			case "image/png":
				$typeId = 2; $valid = 1;
				break;
			case "image/jpg":
				$typeId = 2; $valid = 1;
				break;
			case "image/jpeg":
				$typeId = 2; $valid = 1;
				break;
			case "image/pjpeg":
				$typeId = 2; $valid = 1;
				break;
			case "video/mpeg":
				$typeId = 3; $valid = 1;
				break;
			case "video/quicktime":
				$typeId = 3; $valid = 1;
				break;
			case "video/x-msvideo":
				$typeId = 3; $valid = 1;
				break;
			case "video/3gpp":
				$typeId = 3; $valid = 1;
				break;
			case "video/x-flv":
				$typeId = 3; $valid = 1;
				break;
			case "application/pdf":
				$typeId = 4; $valid = 1;
				break;
			case "application/x-pdf":
				$typeId = 4; $valid = 1;
				break;
			case "application/acrobat":
				$typeId = 4; $valid = 1;
				break;
			case "applications/vnd.pdf":
				$typeId = 4; $valid = 1;
				break;
			case "text/pdf":
				$typeId = 4; $valid = 1;
				break;
			case "text/x-pdf":
				$typeId = 4; $valid = 1;
				break;
			case "application/x-download":
				$typeId = 4; $valid = 1;
				break;
		}
		$res['valid'] = $valid;
		$res['typeId'] = $typeId;
		return $res;
	}
	public function deleteSelectedRows(){
		$res = array();
		$res['status'] = 0;
		if(isset($_POST['id']) && !empty($_POST['id'])){
			$papiCall = $_POST['papiCall'];
			$id =explode(",", $_POST['id']);			
			for($i=0; $i<count($id); $i++){
				$this->papiCall("POST", $papiCall, array("ID"=>$id[$i]));
			}
			$res['status'] = 1;
		}
		print_r(json_encode($res));		
	}	
	# Message Composer
	public function composeMessage(){
		$messageArray[] = array("id" => 31, "name" => "Ok, I totally scewed up");
        $messageArray[] = array("id" => 32, "name" => "I'm Sorry");
        $messageArray[] = array("id" => 33, "name" => "You're Awesome");
        $messageArray[] = array("id" => 34, "name" => "You're Alright");
        $messageArray[] = array("id" => 35, "name" => "I Luv Ya Man");
        $messageArray[] = array("id" => 36, "name" => "What I Like About You");
        $messageArray[] = array("id" => 37, "name" => "I could use a boost");
        $messageArray[] = array("id" => 38, "name" => "High Five");
        $messageArray[] = array("id" => 39, "name" => "Thinking of you today");
        $messageArray[] = array("id" => 40, "name" => "I Love You");
        $messageArray[] = array("id" => 41, "name" => "You Rock");
        $messageArray[] = array("id" => 42, "name" => "If I haven't told you lately");

		$data['messageCounter'] = count($messageArray);
		$data['messages'] = $messageArray;
		$this->load->view('moodlifter/messageComposer.zpt', $data);
	}
	
	# Alert Messages
	# Add message
	public function addMessage(){
		$newMessage = true;
		$data['newMessage'] = $newMessage;
		$this->load->view('moodlifter/messageForm.zpt', $data);
	}
	
	# Edit message
	public function editMessage(){		
		$newMessage = false;		
		if(isset($_GET['sectionId'])){			
			$sectionId = $_GET['sectionId'];
			$message = "hello world!";
			$params['method'] = "GET";
			$params['papiCall'] = "Section.Get";
			$url = "http://iagh.solucientinc.com";
			
			$messageArray = array();
			$params['data'] = array("ID"=>$sectionId);
			$this->curl->baseUrl = $url;
			$this->curl->PAPIConfig($params);	
			$res = $this->curl->execute();
			$res = json_decode($res['message']);
			
			$messageArray = array("id" => $res->Section->ID, "name" => $res->Section->Name);
		}
		$data['message'] = $messageArray;
		$data['newMessage'] = $newMessage;
		$this->load->view('moodlifter/messageForm.zpt', $data);		
	}
	
	# Save message
	public function saveMessage(){
		if(isset($_GET['sectionId'])){
			$sectionId = $_GET['sectionId'];
			$message = "hello world!";
			$params['method'] = "GET";
			$params['papiCall'] = "Section.Update";
			$url = "http://iagh.solucientinc.com";
			
			$messageArray = array();
			$params['data'] = array("ID"=>$sectionId);
			$this->curl->baseUrl = $url;
			$this->curl->PAPIConfig($params);	
		}
	}
	
	public function viewSubCategory(){
		$data = array();
		$sectionId = 0;
		if(isset($_GET['sectionId'])){
			$sectionId = $_GET['sectionId'];
			$options = array();
			$options = array("SectionID"=>$sectionId);
			
/*			$papiResult = $this->papiCall("GET", "Message.GetBySection", $options);
			if($papiResult->Status){
				$data['subcategories'] = $papiResult->Messages;
			}
*/
			$papiResult = $this->papiCall("GET", "SubSection.GetAll", $options);
			if($papiResult->Status){
				$data['subcategories'] = $papiResult->SubSections;
			}
		}
		$data['sectionId'] = $sectionId;
		$this->load->view('moodlifter/subcategory.zpt', $data);
	}
	
	public function addSubCategory(){
		$data['isAddRequest'] = true;
		$data['subcategoryId'] = 0;
		$data['sectionId'] = $_GET['sectionId'];
		$this->load->view('moodlifter/subCategoryForm.zpt', $data);		
	}
	
	public function editSubCategory(){
		$sectionId = isset($_GET['sectionId'])?$_GET['sectionId']:0;
		$subcategoryId = 0;
		
		if(isset($_GET['subcategoryId'])){
			$subcategoryId = $_GET['subcategoryId'];
			$options = array();
			$options = array("ID"=>$subcategoryId);
//			$papiResult = $this->papiCall("GET", "Message.GET", $options);
//			$data['message'] = $papiResult->Message;
			$papiResult = $this->papiCall("GET", "SubSection.GET", $options);
			$data['message'] = $papiResult->SubSection;
		}
		
		$data['isAddRequest'] = false;
		$data['sectionId'] = $sectionId;
		$data['subcategoryId'] = $subcategoryId;
		$this->load->view('moodlifter/subCategoryForm.zpt', $data);		
	}
	
	# js call: genAjaxPost
	public function saveSubCategory(){
		$res = array();
		$res['status'] = 0;
		$res['message'] = "";
				
		if(!empty($_POST['data'])){
			
			$dataParams = array();
			$post = $_POST['data'];
			$param = json_decode($post);
			
			foreach ($param as $item){
				$dataParams[$item->name] = $item->value;
			}
			
			$sectionId = $dataParams['sectionId'];
			$subcategoryId = $dataParams['subcategoryId'];
			$subject = $dataParams['subject'];
//			$message = $dataParams['message'];
			
			$options = array();
			if($subcategoryId){
/*				
				$options = array(
									"ID"=>$subcategoryId,
									"Subject"=>$subject,
									"Message"=>$message,
									"SectionID"=>$sectionId,
									"TypeID"=>2,
									"LocationID"=>4
								);			
				$papiResult =$this->papiCall("POST","Message.Update",$options);
*/
				$options = array(
									"ID"=>$subcategoryId,
									"Name"=>$subject,
									"SectionID"=>$sectionId
								);			
				$papiResult =$this->papiCall("POST","SubSection.Update",$options);
				$res['message'] = "Changes has been successfully saved";
			}
			else{
/*				
				$options = array(
									"SectionID"=>$sectionId,
									"Subject"=>$subject,
									"Message"=>$message,
									"TypeID"=>2,
									"LocationID"=>4
								);
				$papiResult = $this->papiCall("POST","Message.Create",$options);
*/
				$options = array(
									"SectionID"=>$sectionId,
									"Name"=>$subject
								);
				$papiResult = $this->papiCall("POST","SubSection.Create",$options);
				$res['message'] = "Sub-category has been successfully saved";
				
			}
			if($papiResult->Status)
				$res['status'] = 1;
		}
		print_r(json_encode($res));
	}	
	
	public function addAlertMessage(){
		$sectionId = $_GET['sectionId'];		
		$data['originatingPage'] = $_GET['originatingPage'];
		$data['sectionId'] = $sectionId;
		$data['isAddRequest'] = true;
		$this->load->view('moodlifter/messageAlertForm.zpt', $data);
	}
	
	public function editAlertMessage(){
		$data['isAddRequest'] = false;
		$messages = array();
		if(isset($_GET)){
			$originationPage = $_GET['originatingPage'];
			$sectionId = $_GET['sectionId'];
			$messageId = $_GET['messageId'];
			$papiResult = $this->papiCall("GET", "Message.Get", array("ID"=>$messageId));
			if($papiResult->Status){
				$messages = $papiResult->Message;
			}
		}
		$data['sectionId'] = $sectionId;
		$data['originatingPage'] = $originationPage;
		$data['message'] = $messages;
		$this->load->view('moodlifter/messageAlertForm.zpt', $data);		
	}
	
	# js call: genAjaxPost
	public function saveAlertMessage(){
		$res = array();
		$res['status'] = 0;
		$res['message'] = "";
		
		if(!empty($_POST['data'])){
			$dataParams = array();
			$post = $this->input->post('data');
			$param = json_decode($post);
			
			foreach ($param as $item){
				$dataParams[$item->name] = $item->value;
			}
			
			$sectionId = $dataParams['sectionId'];
			$subject = $dataParams['subject'];
			$message = $dataParams['message'];
			$nameKey = "";
			
			switch ($sectionId){
				case 3:
					$nameKey = "Moodlifter.Generic";
					break;
				case 4:
					$nameKey = "Moodlifter.FiveToThrive";
					break;
				case 5:
					$nameKey = "Moodlifter.Challenges";
					break;
			}
			# Edit Request
			if($dataParams['messageId'] > 0){
				$messageId = $dataParams['messageId'];
				$papiResult = $this->papiCall("POST", "Message.Update", array("ID"=>$messageId, "Subject"=>$subject, "Message"=>$message));				
			}
			# Add Request
			else{
				$papiResult = $this->papiCall("POST", "Message.Create", array(	"SectionID"=>$sectionId,
																				"Subject"=>$subject,
																				"Message"=>$message,
																				"TypeID"=>2,
																				"LocationID"=>4
																			));
				# create alert
				if($papiResult->Status){
					$papiResult = $this->papiCall("POST", "Alert.Create", array("WorkspaceID"=>$this->workspaceId, "MessageID"=>$papiResult->Message->ID, "NameKey"=>$nameKey));
				}
			}
			$res['status'] = $papiResult->Status + 0;
			if($res['status']){
				$res['message'] = "Message Alert has been saved successfully!";
			}
		}
		
		print_r(json_encode($res));
	}
	
	public function messageAlert(){
		$options = array();
		$sectionID = 3;
		$options = array("SectionID"=>$sectionID);
		$messages = array();
		$messageCount = 0;
		$papiResult = $this->papiCall("GET", "Message.GetBySection", $options);
		if($papiResult->Status){
			$messages = $papiResult->Messages;
			$messageCount = 1;
		}
		$data['originatingPage'] = "messageAlert";
		$data['sectionId'] = $sectionID;
		$data['messages'] = $messages;
		$data['messageCount'] = $messageCount;
		$this->load->view('moodlifter/messageAlert.zpt', $data);
	}
	
	public function fiveForThriveAlert(){
		$options = array();
		$sectionID = 4;
		$options = array("SectionID"=>$sectionID);
		$messages = array();
		$messageCount = 0;
		$papiResult = $this->papiCall("GET", "Message.GetBySection", $options);
		if($papiResult->Status){
			$messages = $papiResult->Messages;
			$messageCount = 1;
		}		
		$data['sectionId'] = $sectionID;
		$data['originatingPage'] = "fiveForTriveAlert";
		$data['messageCount'] = $messageCount;
		$data['messages'] = $messages;
		$this->load->view('moodlifter/fiveForThriveAlert.zpt', $data);
	}
	
	public function challengeAlert(){
		$options = array();
		$sectionID = 5;
		$options = array("SectionID"=>$sectionID);
		$messages = array();
		$messageCount = 0;
		$papiResult = $this->papiCall("GET", "Message.GetBySection", $options);
		if($papiResult->Status){
			$messages = $papiResult->Messages;
			$messageCount = 1;
		}		
		$data['sectionId'] = $sectionID;
		$data['originatingPage'] = "challengeAlert";
		$data['messageCount'] = $messageCount;
		$data['messages'] = $messages;
		$this->load->view('moodlifter/challengeAlert.zpt', $data);
	}
	
	public function goodNewsVideoItems(){
		$sectionId = 73;
		$options = array("WorkspaceID" => $this->workspaceId, "TypeID"=>3, "SectionID"=>$sectionId);
		$res = $this->papiCall("GET", "Content.GetAll", $options);
		$data['videos'] = $res->Content;
		$this->load->view('moodlifter/goodNewsVideoItems.zpt', $data);
	}
	
	public function goodNewsAddVideo(){
		$data['isAddRequest'] = true;
		$data['workspaceId'] = $this->workspaceId;
		$data['videoId'] = 0;
		$this->load->view('moodlifter/goodNewsVideoForm.zpt', $data);
	}
	
	public function editGoodNewsVideo(){
		$data = array();
		if(isset($_GET['id'])){
			$videoId = $_GET['id'];
			$options = array("ID"=>$videoId);
			$res = $this->papiCall("GET", "Content.Get", $options);
			$data['videoId'] = $videoId;
			$data['video'] = $res->Video;
			$data['isAddRequest'] = false;
		}
		$this->load->view('moodlifter/goodNewsVideoForm.zpt', $data);
	}

	# js call: genAjaxPost()
	public function saveGoodNewsVideo(){
		$res = array();
		$res['status'] = 0;
		$res['message'] = "";
		
		if(isset($_POST['data'])){
			$param = array();
			$post = $this->input->post('data');
			$param = json_decode($post);
			$dataParams = array();
			
			foreach($param as $item){
				$dataParams[$item->name] = $item->value;
			}
			
			$videoId = $dataParams['videoId'] + 0;
			$name = $dataParams['title'];
			$description = $dataParams['description'];
			$url = $dataParams['url'];
			$thumbnailUrl = $dataParams['thumbnailUrl'];
			$sectionId = 73;
			$typeId = 3; # TypeId for Video
		
			# edit video
			if($videoId>0){
				$options = array("ID"=>$videoId, "WorkspaceID"=>$this->workspaceId, "Status"=>1, "Name"=>$name, "Description"=>$description, "URL"=>$url, "ThumbnailURL"=>$thumbnailUrl);
				$papiResult = $this->papiCall("POST", "Content.Update", $options);
				if($papiResult->Status){
					$res['status'] = 1;
					$res['message'] = "Changes in video details had been successfully saved";
				}
			}
			# add new video
			else{
				$options = array("TypeID"=>$typeId, "Status"=>1, "Description"=>$description, "Name"=>$name, "URL"=>$url, "ThumbnailURL"=>$thumbnailUrl, "WorkspaceID"=>$this->workspaceId, "SectionID"=>$sectionId);
				$papiResult = $this->papiCall("POST", "Content.Create", $options);
				if($papiResult->Status){
					$res['status'] = 1;
					$res['message'] = "You have successfully added a good news video.";
				}
			}
		}
		print_r(json_encode($res));
	}
	
	public function goodNewsPlaylist(){
		$playlist = array();
		$data = array();
				
		$options = array("WorkspaceID"=>$this->workspaceId);
		$papiResults = $this->papiCall("GET", "Playlist.GetAll", $options);
		if($papiResults->Status){
			$data['playlists'] = $papiResults->Playlist;
		}
		$this->load->view('moodlifter/goodNewsPlaylist.zpt', $data);
	}
	
	public function addGoodNewsPlaylist(){
		$sectionId = 73;
		$data = array();
		$videos = array();
		$data['formType'] = "add";
		$dataTable = "";
		
		$options =array("TypeID"=>3, "WorkspaceID"=>$this->workspaceId, "SectionID"=>$sectionId);
		$papiResults = $this->papiCall("GET", "Content.GetAll", $options);
		if($papiResults->Status){
			$videos = $papiResults->Content;
			for($i=0; $i<count($videos); $i++){
				$dataTable .= '<tr><td><input type="checkbox" name="' . ucwords($videos[$i]->Name) . '" id="video-list" value="'.$videos[$i]->ID.'"/>&nbsp;' . ucwords($videos[$i]->Name) . '</td></tr>';
			}
		}		
		$data['dataTable'] = $dataTable;
		$this->load->view('moodlifter/goodNewsPlaylistForm.zpt', $data);
	}
	
	public function editGoodNewsPlaylist(){
		$data = array();
		$sectionId = 73;
		$dataTable = "";
		$playlist = array();
		$arrayCurrentVideoList = array();
		$currentVideoList = "";
		
		$options = array();
		if(isset($_GET['id'])){
			$playlistId = $_GET['id'];
			$options = array("ID"=>$playlistId);
			$papiResults = $this->papiCall("GET", "Playlist.Get", $options);
			if($papiResults->Status){
				$playlist = $papiResults->Playlist;
				
				$options = array();
				$options = array("PlaylistID"=>$playlistId);
				$papiResult = $this->papiCall("GET", "Playlist.Content.GetAll", $options);
				
				if($papiResult->Status){
					$videoIds = array();
					for($i=0; $i<count($papiResult->PlaylistContent); $i++){
						$videoIds[] = $papiResult->PlaylistContent[$i]->ID;
						$arrayCurrentVideoList [] = '<li class="ui-state-default" id="vlist" name="'.$papiResult->PlaylistContent[$i]->ID.'"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'. ucwords($papiResult->PlaylistContent[$i]->Name) . ' </li>';
					}
					$data['strVideoId'] = implode(",",$videoIds);
					$papiResult = $this->papiCall("GET", "Content.GetAll", array("TypeID"=>3, "WorkspaceID"=>$this->workspaceId, "SectionID"=>$sectionId));
					if($papiResult->Status){
						$videos = $papiResult->Content;
						for($i=0; $i<count($videos); $i++){
							$check = in_array($videos[$i]->ID, $videoIds)?"checked":"";
//							if(in_array($videos[$i]->ID, $videoIds)){
//								$currentVideoList .= '<div id="video-box" style="margin-left:30px;"><input type="checkbox" name="assignedVideo" id="assignedVideo" checked" disabled="true"/>&nbsp;&nbsp;'.$videos[$i]->Name.'<input type="hidden" name="video-list-hidden" id="video-list-hidden" value="'. ucwords($videos[$i]->Name) .'"/></div>';
//							}
							$dataTable .= '<tr><td><input type="checkbox" name="' . ucwords($videos[$i]->Name) . '" id="video-list" '.$check.' value="'.$videos[$i]->ID.'"/>&nbsp;' . ucwords($videos[$i]->Name) . '</td></tr>';
						}						
					}
					$currentVideoList = implode("", $arrayCurrentVideoList);
				}
			}
		}
		$data['playlist'] = $playlist;
		$data['formType'] = "edit";
		$data['dataTable'] = $dataTable;
		$data['currentVideoList'] = $currentVideoList;
		$this->load->view('moodlifter/goodNewsPlaylistForm.zpt', $data);
	}
	
	public function saveGoodNewsPlaylist(){
		$res = array();
		$res['status'] = 0;
		
		# Edit Playlist
		if(($_POST['playlistId'])){
			$playlistId = isset($_POST['playlistId'])?$_POST['playlistId']:"";
			$name = isset($_POST['title'])?$_POST['title']:"";
			$description = isset($_POST['description'])?$_POST['description']:"";
			$videoId = isset($_POST['videoId'])?$_POST['videoId']:"";
			$workspaceId = $this->workspaceId;
			$options = array(
								"ID"=>$playlistId,
								"Name"=>$name,
								"Description"=>$description,
								"WorkspaceID"=>$workspaceId
							);
			$papiResult = $this->papiCall("POST", "Playlist.Update", $options);
						
			if($papiResult->Status){				
				$currentVideoId = isset($_POST['currentVideoId'])?$_POST['currentVideoId']:"";
				# delete current videos in playlist
				if(!empty($currentVideoId)){
					$this->papiCall("POST", "Playlist.Content.Remove", array("ContentID"=>$currentVideoId, "PlaylistID"=>$playlistId));
				}
				if($videoId != "0"){
					if($papiResult->Status){
						# add new list of videos
						$papiResult = $this->papiCall("POST", "Playlist.Content.Add", array("PlaylistID"=>$playlistId, "ContentID"=>$videoId));
					}
					$res['status'] = $papiResult->Status + 0;
				}
				else{
					$res['status'] = 1;
				}
			}
			else {
				$res['status'] = $papiResult->Status + 0;
			}
			
		}
		# Add Playlist
		else{
			$name = $_POST['title'];
			$description = $_POST['description'];
			$videoId = $_POST['videoId'];
			$status = 0;
			$workspaceId = $this->workspaceId;
			$options = array(
								"Name"=>$name,
								"Description"=>$description,
								"Status"=>$status,
								"WorkspaceID"=>$workspaceId
							);
			$papiResults = $this->papiCall("POST", "Playlist.Create", $options);
			$playlistId = $papiResults->Playlist->ID;
			$videoIds = explode(",", $videoId);
							
			for($i=0; $i<count($videoIds); $i++){
				$options = array();
				$options = array(
									"PlaylistID"=>$playlistId,
									"ContentID"=>$videoIds[$i]
								);
				$this->papiCall("POST", "Playlist.Content.Add", $options);
			}
			$res['status'] = 1;
		}
		print_r(json_encode($res));
	}	
	
	public function publishGoodNewsPlaylist(){
		$res = array();
		$res['status'] = 0;
		if(isset($_POST)){
			$currentPublishedId = explode(",", $_POST['playlistId']);
			$papiResult = $this->papiCall("GET", "Playlist.GetAll", array("WorkspaceID"=>$this->workspaceId));
			if($papiResult->Status){
				$playlistId = $papiResult->Playlist;
				for($i=0; $i<count($playlistId); $i++){
					if(in_array($playlistId[$i]->ID, $currentPublishedId)){
						$this->papiCall("POST", "Playlist.Update", array("ID"=>$playlistId[$i]->ID, "Status"=>1));
					}
					else {
						$this->papiCall("POST", "Playlist.Update", array("ID"=>$playlistId[$i]->ID, "Status"=>0));
					}
				}				
			}
			$res['status'] = 1;
		}
		print_r(json_encode($res));
	}

	public function secretVault(){
		$secretVault = array();
		$data = array();
				
		$options = array("WorkspaceID"=>$this->workspaceId);
		$papiResult = $this->papiCall("GET", "Vault.GetAll", $options);
		if($papiResult->Status){
			$data['vaults'] = $papiResult->Vaults;
		}
		$this->load->view('moodlifter/secretVault.zpt', $data);		
	}
	
	public function addSecretVault(){
		$sectionId = 75;
		$data = array();
		$data['isAddRequest'] = true;
		$dataTable = "";
		$papiResult = $this->papiCall("GET", "Content.GetAll", array("WorkspaceID"=>$this->workspaceId, "SectionID"=>$sectionId));
		if($papiResult->Status){
			$videos = $papiResult->Content;
			for($i=0; $i<count($videos); $i++){
				$dataTable .= '<tr><td><input type="checkbox" name="' . ucwords($videos[$i]->Name) . '" id="vault-list" value="'.$videos[$i]->ID.'"/>&nbsp;' . ucwords($videos[$i]->Name) . '</td></tr>';
			}						
		}				
		$data['dataTable'] = $dataTable;
		$this->load->view('moodlifter/secretVaultForm.zpt', $data);
	}
	
	public function addSecretVaultItems(){
		$sectionId = 75;
		$data = array();
		$data['sectionId'] = $sectionId;
		$this->load->view('moodlifter/vaultItemsForm.zpt', $data);
	}
	
	public function saveVaultItems(){
		$res = array();
		$error = "";
		$res['status'] = 0;		
		$sectionId = 75;
		if(!empty($_POST['name']) && !empty($_POST['description'])){
			
			$videoUrl = $_POST['videoUrl'];
			$name = $_POST['name'] = $_POST['name'];
			$description = $_POST['description'];
			$thumbnail = $_POST['thumbnail'];
			$workspaceId = $this->workspaceId;
			
			if($videoUrl != "" || $thumbnail != ""){
				$papiResult = $this->papiCall("POST", "Content.Create", array("TypeID"=>3, "Status"=>1, "Description"=>$description, "Name"=>$name, "URL"=>$videoUrl, "WorkspaceID"=>$workspaceId, "SectionID"=>$sectionId, "ThumbnailURL"=>$thumbnail));
				$res['status'] = $papiResult->Status + 0;
			}
			else{
				$fileElementName = 'uploader';
				if(!empty($_FILES[$fileElementName]['error'])){
					switch($_FILES[$fileElementName]['error'])
					{
						case '1':
							$error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
							break;
						case '2':
							$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
							break;
						case '3':
							$error = 'The uploaded file was only partially uploaded';
							break;
						case '4':
							$error = 'No file was uploaded.';
							break;
			
						case '6':
							$error = 'Missing a temporary folder';
							break;
						case '7':
							$error = 'Failed to write file to disk';
							break;
						case '8':
							$error = 'File upload stopped by extension';
							break;
						case '999':
						default:
							$error = 'No error code avaiable';
					}
				}
				elseif(empty($_FILES['uploader']['tmp_name']) || $_FILES['uploader']['tmp_name'] == 'none'){
					$error = 'No file was uploaded..';
				}
				else {
					if (file_exists("uploads/" . $_FILES["uploader"]["name"])){
						$error = $_FILES["uploader"]["name"] . " already exists. ";
					}
					else{
						$mimeTypes = $this->verifyMimeType($_FILES['uploader']['type']);
						
						if($mimeTypes['valid']){
							if(move_uploaded_file($_FILES["uploader"]["tmp_name"],"uploads/" . $_FILES["uploader"]["name"])){
								$typeId = $mimeTypes['typeId'];
								$url = base_url() ."uploads/" . $_FILES['uploader']['name'];
								$papiResult = $this->papiCall("POST", "Content.Create", array("TypeID"=>$typeId, "Status"=>1, "Description"=>$description, "Name"=>$name, "URL"=>$url, "WorkspaceID"=>$workspaceId, "SectionID"=>$sectionId, "ThumbnailURL"=>$thumbnail));
								if($papiResult->Status){
									$res['status'] = 1;
								}
								else {
									$res['status'] = 0;
									# remove uploaded file
									@unlink("uploads/" .$_FILES['uploader']['name']);
								}
							}
							else{
								$error = "Unable to upload file!";
							}
						}
						else{
							$error = "Cannot upload the file. The format is not supported";
						}
					}
				}
			}
		}
		else {
			$error = "Fill all required fields";
		}
		$res['error'] = $error;
		print_r(json_encode($res));
	}
	
	public function editSecretVault(){
		$sectionId = 75;
		$data = array();
		$vaultDetails = array();
		$vaultItems = array();
		$currentVaultList = "";
		$dataTable = "";
		if($_GET['id']){
			$vaultId = $_GET['id'];
			$papiResult = $this->papiCall("GET", "Vault.Content.GetAll", array("VaultID"=>$vaultId));
			if($papiResult->Status){
				$vaultDetails = $papiResult->Vault;
				$vaultItems = $papiResult->VaultItems;
					$vaultIds = array();
					for($i=0; $i<count($vaultItems); $i++){
						$vaultIds[] = $vaultItems[$i]->ID;
					}
					$data['strVaultId'] = implode(",",$vaultIds);
					$papiResult = $this->papiCall("GET", "Content.GetAll", array("WorkspaceID"=>$this->workspaceId, "SectionID"=>$sectionId));
					if($papiResult->Status){
						$videos = $papiResult->Content;
						for($i=0; $i<count($videos); $i++){
							$check = in_array($videos[$i]->ID, $vaultIds)?"checked":"";
							if(in_array($videos[$i]->ID, $vaultIds)){
								$currentVaultList .= '<div id="vault-box" style="margin-left:30px;"><input type="checkbox" name="assignedVaultItems" id="assignedVaultItems" checked disabled="true"/>&nbsp;&nbsp;'.$videos[$i]->Name.'<input type="hidden" name="video-list-hidden" id="video-list-hidden" value="'. ucwords($videos[$i]->Name) .'"/></div>';
							}
							$dataTable .= '<tr><td><input type="checkbox" name="' . ucwords($videos[$i]->Name) . '" id="vault-list" '.$check.' value="'.$videos[$i]->ID.'"/>&nbsp;' . ucwords($videos[$i]->Name) . '</td></tr>';
						}						
					}				
			}
		}
		$data['isAddRequest'] = false;
		$data['vault'] = $vaultDetails;
		$data['vaultItems'] = $vaultItems;
		$data['dataTable'] = $dataTable;
		$data['currentVaultList'] = $currentVaultList;
		$this->load->view("moodlifter/secretVaultForm.zpt", $data);
	}
	
	public function saveSecretVault(){
		$res = array();
		$res['status'] = 0;
		$name = isset($_POST['title'])?$_POST['title']:"";
		$description = isset($_POST['description'])?$_POST['description']:"";
		$token = isset($_POST['token'])?$_POST['token']:"";
		$currentVaultId = isset($_POST['currentVaultId'])?trim($_POST['currentVaultId']):"";
		$imageUrl = isset($_POST['imageUrl'])?$_POST['imageUrl']:"";
		$newVaultItems = isset($_POST['vaultItems'])?trim($_POST['vaultItems']):"";
		$workspaceId = $this->workspaceId;
		# edit request for secret vault
		if(isset($_POST['vaultId']) && $_POST['vaultId'] != 0){
			# update vault details
			$vaultId = trim($_POST['vaultId']) + 0;
			$options = array(
								"ID"=>$vaultId,
								"Name"=>$name,
								"Description"=>$description,
								"Cost"=>$token,
								"ImageURL"=>$imageUrl,
								"WorkspaceID"=>$workspaceId
							);
			$papiResult = $this->papiCall("POST", "Vault.Update", $options);
			if($papiResult->Status){
				if($currentVaultId !=""){
					# delete existing vault items
					$papiResult = $this->papiCall("POST", "Vault.Content.Remove", array("ContentID"=>$currentVaultId, "VaultID"=>$vaultId));
					$res['status'] = $papiResult->Status + 0;
				}
				if(!empty($newVaultItems) && $newVaultItems !=""){
					# insert new vault items
					$papiResult = $this->papiCall("POST", "Vault.Content.Add", array("ContentID"=>$newVaultItems, "VaultID"=>$vaultId, "Status"=>0));
					$res['status'] = $papiResult->Status + 0;
				}				
			}
		}
		# add request for secret vault
		else{
			$papiResult = $this->papiCall("POST", "Vault.Create", array("Name"=>$name, "ImageURL"=>$imageUrl, "Cost"=>$token, "Status"=>0, "Description"=>$description, "WorkspaceID"=>$workspaceId));
			if($papiResult->Status){
				# add vault items in secret vault
				$vaultId = $papiResult->Vault->ID;
				if($vaultId){
					if(!empty($newVaultItems) && $newVaultItems !=""){
						# insert new vault items
						$papiResult = $this->papiCall("POST", "Vault.Content.Add", array("ContentID"=>$newVaultItems, "VaultID"=>$vaultId, "Status"=>0));
						$res['status'] = $papiResult->Status + 0;
					}
				}
			}
			$res['status'] = $papiResult->Status;
		}
		print_r(json_encode($res));
	}	
	
	public function publishSecretVault(){
		$res = array();
		$res['status'] = 0;
		$vaultId = array();
		if(isset($_POST)){
			$currentPublishedId = explode(",", $_POST['vaultId']);
			$papiResult = $this->papiCall("GET", "Vault.GetAll", array("WorkspaceID"=>$this->workspaceId));
			if($papiResult->Status){
				$vaultId = $papiResult->Vaults;
				for($i=0; $i<count($vaultId); $i++){
					if(in_array($vaultId[$i]->ID, $currentPublishedId)){
						$this->papiCall("POST", "Vault.Update", array("ID"=>$vaultId[$i]->ID, "Status"=>1));
					}
					else {
						$this->papiCall("POST", "Vault.Update", array("ID"=>$vaultId[$i]->ID, "Status"=>0));
					}
				}				
			}
			$res['status'] = 1;
		}
		print_r(json_encode($res));
	}
	
	public function challenges(){
		$sectionId = 74;
		$challenges = array();
		$data = array();
				
		$options = array("WorkspaceID"=>$this->workspaceId, "ParentID"=>0);
		$papiResult = $this->papiCall("GET", "Challenge.GetAll", $options);
		if($papiResult->Status){
			$data['challenges'] = $papiResult->Challenges;
		}
		$this->load->view('moodlifter/challenges.zpt', $data);
	}
	public function addParentChallenge(){
		$data = array();
		$parentId = 0;
		$data['isAddRequest'] = true;
		$data['parentId'] = $parentId;
		$this->load->view('moodlifter/challengeForm.zpt', $data);
	}
	public function editParentChallenge(){
		$data = array();
		$challengeDetails = array();
		$parentId = 0;
		$data['isAddRequest'] = false;
		if(isset($_GET['parentId'])){
			$parentId = $_GET['parentId'];
			$papiResult = $this->papiCall("GET", "Challenge.Get", array("ID"=>$parentId));
			if($papiResult->Status){
				$challengeDetails = $papiResult->Challenge;
			}
		}
		$data['challenge'] = $challengeDetails;
		$data['parentId'] = $parentId;
		$this->load->view('moodlifter/challengeForm.zpt', $data);
	}
	public function saveParentChallenge(){
		$res = array();
		$res['status'] = 0;
		$res['message'] = "";
		if(isset($_POST['data'])){
			
			$post = $this->input->post('data');
			$param = json_decode($post);
			$dataParams = array();
			
			foreach($param as $item){
				$dataParams[$item->name] = $item->value;
			}						
			
			$parenId = trim($dataParams['parentId']) + 0;
			$name = $dataParams['name']			;
			$instruction = $dataParams['instruction'];
			$videoInstruction = $dataParams['videoInstruction'];
			$avatar = $dataParams['avatar'];
			$welcomeMessage = $dataParams['welcomeMessage'];
			# edit request
			if($parenId){
				$papiResult = $this->papiCall("POST", "Challenge.Update", array("ID"=>$parenId, "Name"=>$name, "Instruction"=>$instruction, "VideoInstruction"=>$videoInstruction, "AvatarURL"=>$avatar, "WelcomeMessage"=>$welcomeMessage));
				$res['status'] = $papiResult->Status + 0;
			}
			# add request
			else {
				$papiResult = $this->papiCall("POST", "Challenge.Create", array("Name"=>$name, "TokenCount"=>0, "Status"=>0, "Instruction"=>$instruction, "VideoInstruction"=>$videoInstruction, "AvatarURL"=>$avatar, "WelcomeMessage"=>$welcomeMessage, "WorkspaceID"=>$this->workspaceId));
				$res['status'] = $papiResult->Status + 0;
			}
			if($res['status']){
				$res['message'] = "Challenge has been successfully saved!";
			}
		}
		print_r(json_encode($res));
	}
	public function publishChallenge(){
		$res = array();
		$res['status'] = 0;
		$vaultId = array();
		if(isset($_POST)){
			$currentPublishedId = explode(",", $_POST['challengeId']);
			$papiResult = $this->papiCall("GET", "Challenge.GetAll", array("WorkspaceID"=>$this->workspaceId));
			if($papiResult->Status){
				$challenges = $papiResult->Challenges;
				for($i=0; $i<count($challenges); $i++){
					if(in_array($challenges[$i]->ID, $currentPublishedId)){
						$this->papiCall("POST", "Challenge.Update", array("ID"=>$challenges[$i]->ID, "Status"=>1));
					}
					else {
						$this->papiCall("POST", "Challenge.Update", array("ID"=>$challenges[$i]->ID, "Status"=>0));
					}
				}				
			}
			$res['status'] = 1;
		}
		print_r(json_encode($res));
	}

	# deleted in page challenged.zpt ------------
	public function subChallenges(){
		$data = array();
		$sectionId = 74;
		$subChallenges = array();
		$parentChallenges = array();
		$dataTable = array();
		$workspaceId = $this->workspaceId;
		$papiResult = $this->papiCall("GET", "Challenge.GetAll", array(""));
		if($papiResult->Status){
			$challenges = $papiResult->Challenges;
			for($i=0; $i<count($challenges); $i++){
				if($challenges[$i]->ParentID>0){
					$subChallenges[] = array(	"ID"=>$challenges[$i]->ID,
												"Name"=>$challenges[$i]->Name,
												"TokenCount"=>$challenges[$i]->TokenCount,
												"ParentID"=>$challenges[$i]->ParentID
											);
				}
				else{
					$parentChallenges[] = array("ID"=>$challenges[$i]->ID, "Name"=>$challenges[$i]->Name);
				}
			}
		}
		if(!empty($subChallenges)){
			
			for($i=0; $i<count($subChallenges); $i++){
				$dataTable[] = "<tr>";
				$sel = "<select id='parentChallenge'>";
				for($j=0; $j<count($parentChallenges); $j++){
					$selected = $subChallenges[$i]['ParentID'] == $parentChallenges[$j]['ID']?"selected='selected'":"";
					$sel .= "<option value='{$subChallenges[$i]['ID']}|{$parentChallenges[$j]['ID']}' {$selected} >{$parentChallenges[$j]['Name']}</option>";
				}
				$sel .="</select>";
				$dataTable[] = "<td><input type='checkbox' name='pubChk' id='pubChk' value='{$subChallenges[$i]['ID']}' /> &nbsp;";
				$dataTable[] = "<span onmouseover=\"moodlifter.displayButtons('subCat{$subChallenges[$i]['ID']}')\">{$subChallenges[$i]['Name']}</span>";
				$dataTable[] = "<div class=\"subCat{$subChallenges[$i]['ID']}\" style=\"display:none; width:400px; height:40px; margin-top:10px;\">";
				$dataTable[] = "<a href=\"#/moodlifter/editSubChallenge/?subChallengeId={$subChallenges[$i]['ID']}\" class=\"button icon-with-text\" title=\"Edit Challenge\"><img src=\"/images/navicons-small/135.png\" alt=\"\"/>Edit SubChallenge</a>&nbsp;";
				$dataTable[] = "<a href=\"#\" class=\"button icon-with-text\" title=\"Close\" onclick=\"moodlifter.hideButtons('subCat{$subChallenges[$i]['ID']}')\"><img src=\"/images/navicons-small/134.png\" alt=\"\"/>Close</a></div></td>";
				$dataTable[] = "<td>{$subChallenges[$i]['TokenCount']}</td>";
				$dataTable[] = "<td>{$sel}</td>";
				$dataTable[] = "</tr>";
			}
			
		}
		$data['subChallenges'] = $subChallenges;
		$data['dataTable'] = implode("", $dataTable);
		$this->load->view('moodlifter/subChallenge.zpt', $data);
	}
	# deleted in page challenged.zpt ------------
	
	public function viewParentSubChallenges(){
		$data = array();
		$sectionId = 74;
		$subChallenges = array();
		$parentChallenges = array();
		$parentId = 0;
		$dataTable = array();
		if(isset($_GET['parentId'])){
			$parentId = $_GET['parentId'];
			$workspaceId = $this->workspaceId;
			$papiResult = $this->papiCall("GET", "Challenge.GetAll", array(""));
			if($papiResult->Status){
				$challenges = $papiResult->Challenges;
				for($i=0; $i<count($challenges); $i++){
					if($challenges[$i]->ParentID == $parentId){
						$subChallenges[] = array(	"ID"=>$challenges[$i]->ID,
													"Name"=>$challenges[$i]->Name,
													"TokenCount"=>$challenges[$i]->TokenCount,
													"ParentID"=>$challenges[$i]->ParentID
												);
					}
					if(!$challenges[$i]->ParentID){
						$parentChallenges[] = array("ID"=>$challenges[$i]->ID, "Name"=>$challenges[$i]->Name);
					}
				}
			}
		}
		
		if(!empty($subChallenges)){
			
			for($i=0; $i<count($subChallenges); $i++){
				$dataTable[] = "<tr>";
				$sel = "<select id='parentChallenge'>";
				for($j=0; $j<count($parentChallenges); $j++){
					$selected = $subChallenges[$i]['ParentID'] == $parentChallenges[$j]['ID']?"selected='selected'":"";
					$sel .= "<option value='{$subChallenges[$i]['ID']}|{$parentChallenges[$j]['ID']}' {$selected} >{$parentChallenges[$j]['Name']}</option>";
				}
				$sel .="</select>";
				$dataTable[] = "<td><input type='checkbox' name='pubChk' id='pubChk' value='{$subChallenges[$i]['ID']}' /> &nbsp;";
				$dataTable[] = "<span onmouseover=\"moodlifter.displayButtons('subCat{$subChallenges[$i]['ID']}')\">{$subChallenges[$i]['Name']}</span>";
				$dataTable[] = "<div class=\"subCat{$subChallenges[$i]['ID']}\" style=\"display:none; width:400px; height:40px; margin-top:10px;\">";
				$dataTable[] = "<a href=\"#/moodlifter/editSubChallenge/?subChallengeId={$subChallenges[$i]['ID']}\" class=\"button icon-with-text\" title=\"Edit Challenge\"><img src=\"/images/navicons-small/135.png\" alt=\"\"/>Edit SubChallenge</a>&nbsp;";
				$dataTable[] = "<a href=\"#\" class=\"button icon-with-text\" title=\"Close\" onclick=\"moodlifter.hideButtons('subCat{$subChallenges[$i]['ID']}')\"><img src=\"/images/navicons-small/134.png\" alt=\"\"/>Close</a></div></td>";
				$dataTable[] = "<td>{$subChallenges[$i]['TokenCount']}</td>";
				$dataTable[] = "<td>{$sel}</td>";
				$dataTable[] = "</tr>";
			}
			
		}
		$data['parentId'] = $parentId;
		$data['subChallenges'] = $subChallenges;
		$data['dataTable'] =implode("", $dataTable);
		$this->load->view('moodlifter/subChallenge.zpt', $data);
	}

	public function addSubChallenge(){
		$data = array();		
		$parentId = isset($_GET['parentId'])?$_GET['parentId']:0;
		$parentChallenges = array();
		$selectMenu = array();
		$papiResult = $this->papiCall("GET", "Challenge.GetAll", array("ParentID"=>0));
		if($papiResult->Status){
			$parentChallenges = $papiResult->Challenges;
			$selectMenu[] = "<select id='parentChallenge' name='parentId'>";
			for($i=0; $i<count($parentChallenges); $i++){
				$selected = $parentChallenges[$i]->ID == $parentId?"selected":"";
				$selectMenu[] = "<option value='{$parentChallenges[$i]->ID}' {$selected}>{$parentChallenges[$i]->Name}</option>";
			}
			$selectMenu[] = "</select>";
		}		
		$data['isAddRequest'] = true;
		$data['subChallengeId'] = 0;
		$data['parentId'] = $parentId;
		$data['parentMenu'] = implode("", $selectMenu);
		$this->load->view('moodlifter/subChallengeForm.zpt', $data);
	}
	public function editSubChallenge(){
		$data = array();
		$challengeDetails = array();
		$subChallengeId = 0;
		$selectMenu = array();
		$parentChallenges = array();
		
		$data['isAddRequest'] = false;
		if(isset($_GET['subChallengeId'])){
			$subChallengeId = $_GET['subChallengeId'];
			$papiResult = $this->papiCall("GET", "Challenge.Get", array("ID"=>$subChallengeId));
			if($papiResult->Status){
				$challengeDetails = $papiResult->Challenge;
				$parentId = $challengeDetails->ParentID;
			}
		}
		
		$papiResult = $this->papiCall("GET", "Challenge.GetAll", array("ParentID"=>0));		
		if($papiResult->Status){
			$parentChallenges = $papiResult->Challenges;
			$selectMenu[] = "<select id='parentChallenge' name='parentId'>";
			for($i=0; $i<count($parentChallenges); $i++){
				$selected = $parentChallenges[$i]->ID==$parentId?"selected='selected'":"";
				$selectMenu[] = "<option value='{$parentChallenges[$i]->ID}' {$selected}>{$parentChallenges[$i]->Name}</option>";
			}
			$selectMenu[] = "</select>";
		}		
		
		$data['parentMenu'] = implode("", $selectMenu);
		$data['subChallengeId'] = $subChallengeId;
		$data['challenge'] = $challengeDetails;
		$data['parentId'] = $parentId;
		$this->load->view('moodlifter/subChallengeForm.zpt', $data);
	}
	
	# js call: genAjaxPost
	public function saveSubChallenge(){
		$res = array();
		$res['status'] = 0;
		$res['message'] = "";
		if(isset($_POST['data'])){
			$post = $this->input->post('data');
			$param = json_decode($post);
			$dataParams = array();
			
			foreach($param as $item){
				$dataParams[$item->name] = $item->value;
			}
			
			$parentId = trim($dataParams['parentId']) + 0;
			$name = $dataParams['name']			;
			$instruction = $dataParams['instruction'];
			$videoInstruction = $dataParams['videoInstruction'];
			$avatar = $dataParams['avatar'];
			$token = $dataParams['token'];
			$subChallengeId = $dataParams['subChallengeId'];
			
			# edit request
			if($subChallengeId){
				$papiResult = $this->papiCall("POST", "Challenge.Update", array(
																				"ID"=>$subChallengeId, 
																				"Name"=>$name, 
																				"Instruction"=>$instruction,
																				"VideoInstruction"=>$videoInstruction,
																				"AvatarURL"=>$avatar,
																				"TokenCount"=>$token, 
																				"ParentID"=>$parentId
																				));
				$res['status'] = $papiResult->Status + 0;
			}
			# add request
			else {
				$papiResult = $this->papiCall("POST", "Challenge.Create", array(
																				"Name"=>$name, 
																				"Instruction"=>$instruction,
																				"VideoInstruction"=>$videoInstruction,
																				"AvatarURL"=>$avatar,
																				"TokenCount"=>$token, 
																				"Status"=>1, 
																				"ParentID"=>$parentId,
																				"WorkspaceID"=>$this->workspaceId
																				));
				$res['status'] = $papiResult->Status + 0;
			}
			if($papiResult->Status){
				$res['message'] = "Challenges has been successfully saved!";
			}
		}
		print_r(json_encode($res));
	}
	public function applySubChallengeMapping(){
		$res = array();
		$res['status'] = 0;
		if($_POST['id']){
			$id = explode(",", $_POST['id']);
			for($i=0; $i<count($id); $i++){
				$childParentId = explode("|", $id[$i]);
				$childId = $childParentId[0];
				$parentId = $childParentId[1];
				$this->papiCall("POST", "Challenge.Update", array("ID"=>$childId, "ParentID"=>$parentId));
			}
			$res['status'] = 1;
		}
		print_r(json_encode($res));
	}
}