<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usermanagement extends CI_Controller {
	
	private $workspaceId;
	private $userPermission;
	private $permission;
	
	public function __construct(){
		parent::__construct();
		$this->tal->title = $this->config->item('title');
		$this->tal->base_url = substr(base_url(), 0, -1);	
		$this->user = $this->ion_auth->user()->result();
		if(!$this->ion_auth->user()->result()){
			exit;
		}
		$this->user[0]->fullname = $this->user[0]->first_name . ' ' . $this->user[0]->last_name;
		$config = array('userID' => $this->user[0]->id);			
		$this->load->library('acl', $config);
		$this->load->library('curl');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url', 'file'));
		if (!$this->acl->userRoles) {
			exit;
		}
		else{
			$this->workspaceId = $this->acl->userRoles[0];
			# get role permission
			$this->permission = $this->acl->getRolePerms($this->acl->userRoles[0]);
			$userPermission = $this->acl->getUserPerms($this->acl->userID);
			$this->userPermission = $userPermission[0]['id'];			
		}
	}
	
	public function papiCall($method="GET", $function="", $options=array()){
		$papi = array();
		$papi['method'] = $method;
		$papi['papiCall'] = $function;
		$papi['data'] = $options;
		$this->curl->PAPIConfig($papi);
		$res = $this->curl->execute();
		return json_decode($res['message']);
	}
	
	public function listUsers(){
		$data['users'] = array();
		$options = array("ID"=>$this->workspaceId);
		$apiRes = $this->papiCall("GET", "Workspace.Users.GetAll", $options);
		if($apiRes->Status){
			$data['users'] = $apiRes->Users;
		}
		$this->load->view('usermanagement/users.zpt', $data);
	}
	
	public function addUser(){
		$data['isAddRequest'] = true;
		$this->load->view('usermanagement/userForm.zpt', $data);
	}
	
	public function editUser(){
		$data['isAddRequest']  = false;
		if(isset($_GET['id'])){
			$id = $_GET['id'];
			$options = array("UserID"=>$id, "WorkspaceID"=>$this->workspaceId);
			$apiRes = $this->papiCall("GET", "User.Workspace.Access.Check", $options);
			if($apiRes->Status){
				$data['user'] = $apiRes->User;
				$data['userWorkspace'] = $apiRes->UserWorkspace;
			}
			$this->load->view('usermanagement/userForm.zpt', $data);
		}
	}
	
	public function saveUser(){
		$res['status'] = 0;
		$validDomain = false;
		
		if(isset($_POST['data'])){
			$params = json_decode($_POST['data']);
			$dataParam = array();
			foreach($params as $item){
				$dataParam[$item->name] = $item->value;
			}
			
			$input_ = explode("@", $dataParam['email']);
			if(isset($input_[1])){
				$domainName = $input_[1];
				$apiRes = $this->papiCall("GET", "Workspace.Get", array("ID"=>$this->workspaceId));
				if($apiRes->Status){
					$domains = (!empty($apiRes->Workspace->Domains)?explode(",", $apiRes->Workspace->Domains):array());
					
					if(in_array($domainName, $domains)){
						$validDomain = true;
					}
					else{
						$res['message'] = "Domain is not allowed";
					}
				}
			}
			else{
				$res['message'] = "Enter valid email address";
			}
			
			if($validDomain){
				$options = array(
					"Email" => $dataParam['email'],
					"Timezone" => $dataParam['timezone'],
					"TimeMorning" => $dataParam['timemorning'],
					"TimeNoon" => $dataParam['timenoon'],
					"TimeNight" => $dataParam['timenight'],
					"FirstName" => $dataParam['firstname'],
					"LastName" => $dataParam['lastname'],
					"AboutMe" => $dataParam['aboutme'],
					"ImageURL" => $dataParam['imageurl'],
					"Birthday" => $dataParam['birthday'],
					"Country" => $dataParam['country'],
					"Gender" => $dataParam['gender'],
					"MiddleName" => $dataParam['middlename']
				);
				
				$userWorkspaceOptions = array(
					"Title" => $dataParam['title'],
					"Position" => $dataParam['position'],
					"EmployeeID" => $dataParam['employeeid'],
					"DepartmentID" => $dataParam['departmentid'],
					"SupervisorID" => $dataParam['supervisorid']
				);
				
				# new user
				if($dataParam['userId'] == "0"){
					$options['Password'] = $dataParam['password'];
					$apiRes = $this->papiCall("POST", "User.Create", $options);
					if($apiRes->Status){
						if($apiRes->User->ID){
							$userID = $apiRes->User->ID;
							$grantOptions = array("UserID"=>$userID, "WorkspaceID"=>$this->workspaceId);
							$apiRes = $this->papiCall("POST", "User.Workspace.Access.Grant", $grantOptions);
							if($apiRes->Status){							
								$userWorkspaceOptions['UserID'] = $userID;
								$userWorkspaceOptions['WorkspaceID'] = $this->workspaceId;
								$apiRes = $this->papiCall("POST", "User.Workspace.Access.Edit", $userWorkspaceOptions);
								if($apiRes->Status){
									$res['status'] = 1;
									$res['message'] = "User successfully added!";
								}
								else{
									$res['message'] = $apiRes->StatusMessage;
								}
							}
							else{
								$res['message'] = $apiRes->StatusMessage;
							}
						}
					}
					else{
						$res['message'] = $apiRes->StatusMessage;
						$res['status'] = 0;
					}
					
				}
				# edit user
				else{
					$options['ID'] = $dataParam['userId'];
					$apiRes = $this->papiCall("POST", "User.Update", $options);
					if($apiRes->Status){
						$userWorkspaceOptions['UserID'] = $dataParam['userId'];
						$userWorkspaceOptions['WorkspaceID'] = $this->workspaceId;
						$apiRes = $this->papiCall("POST", "User.Workspace.Access.Edit", $userWorkspaceOptions);
						if($apiRes->Status){
							$res['status'] = 1;
							$res['message'] = "Details have been successfully saved!";
						}
						else{
							$res['message'] = $apiRes->StatusMessage;
						}
					}
					else{
						$res['message'] = $apiRes->StatusMessage;
					}
				}
			}
		}
		print_r(json_encode($res));
	}
	
	public function deleteSelected(){
		$res = array();
		$res['status'] = 0;
		$res['message'] = "";
		if(isset($_POST['id']) && !empty($_POST['id'])){
			$papiCall = $_POST['papiCall'];
			$id =explode(",", $_POST['id']);
			for($i=0; $i<count($id); $i++){
				$apiRes = $this->papiCall("POST", $papiCall, array("UserID"=>$id[$i], "WorkspaceID"=>$this->workspaceId));
			}
			$res['status'] = 1;
		}
		print_r(json_encode($res));		
	}
	
	public function checkAvailability(){
		$email = $_POST['email'];
		$res['status'] = 0;
		$res['message'] = "";
		if(empty($email) || $email == ""){
			$res['message'] = "Enter valid email address";
		}
		else{
			$userId = $_POST['userid'];
			$input_ = explode("@", $email);
			if(isset($input_[1])){
				$domainName = $input_[1];
				$apiRes = $this->papiCall("GET", "Workspace.Users.GetAll", array("ID"=>$this->workspaceId));
				if($apiRes->Status){
					$domains = (!empty($apiRes->Workspace->Domains)?explode(",", $apiRes->Workspace->Domains):array());
					
					if(in_array($domainName, $domains)){
						$apiRes = $this->papiCall("GET", "User.GetAll", array());
						if($apiRes->Status){
							$valid = true;
							for($i=0; $i<count($apiRes->Users); $i++){
								if($email == $apiRes->Users[$i]->Email){
									if($userId != $apiRes->Users[$i]->ID){
										$valid = false;
										break;
									}
								}
							}
						}
						if($valid){
							$res['message'] = "{$email} is available!";
						}
						else{
							$res['message'] = "{$email} is not available!";
						}
					}
					else{
						$res['message'] = "Domain is not allowed";
					}
				}
			}
			else{
				$res['message'] = "Enter valid email address";
			}
		}
		print_r(json_encode($res));
	}
	
	public function emailNotification(){
		$this->load->view('usermanagement/emailNotification.zpt');
	}
	
	public function sendNotification(){
		$res['status'] = 0;
		$res['message'] = "Failed to send verification code";
		$input = json_decode($_POST['data']);
		$param = array();
		foreach ($input as $item){
			$param[$item->name] = $item->value;
		}
		$email_ = explode("@", $param['email']);
		if(isset($email_[1])){
			$option = array("ID"=>$this->workspaceId);
			$apiRes = $this->papiCall("GET", "Workspace.Users.GetAll", $option);
			if($apiRes->Status){
				$users = $apiRes->Users;
				$emailExists = false;
				for($i=0; $i<count($users); $i++){
					if($param['email'] == $users[$i]->UserID->Email){
						$emailExists = true;
					}
				}
				if($emailExists){
					$subject = "Verification Code";
					$message = "This is a test message";
					$from = "admin@iagh.com";
					$to = $param['email'];
					
					$config = array(
					    'protocol' => 'mail'
					);
					
					$this->load->library('email', $config);
					
					$this->email->from($from);
					$this->email->to($to);
					$this->email->subject($subject);
					$this->email->message($message);
					
					if($this->email->send()){
						$res['status'] = 1;
						$res['message'] = "Email notification sent";
					}
					else{
						$res['message'] = "Unable to send message";
					}
				}
				else{
					$res['message'] = "Email does not exist";
				}
			}
			else{
				$res['message'] = $apiRes->StatusMessage;
			}
		}
		else {
			$res['message'] = "Enter valid email address";
		}
		print_r(json_encode($res));
	}
}