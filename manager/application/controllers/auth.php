<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->tal->title = $this->config->item('title');
		$this->tal->base_url = substr(base_url(), 0, -1);		
	}
	
	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			$message = $this->session->flashdata('message');
			$this->tal->error = false;
			if (trim(strip_tags($message)) == 'Incorrect Login'){
				$this->tal->error = true;
			}
			$this->tal->display('common/login.zpt');			
		}
		else
		{
			$user = $this->ion_auth->user()->result();
			$config = array('userID' => $user[0]->id);			
			$this->load->library('acl',$config);
			$userPermission = $this->acl->userPerms[0]['id'];
			
			if($this->acl->userRoles){
				$this->load->model('iagh_model');
				# get user's group
				$info = $this->iagh_model->getPageGrouping($this->acl->userRoles[0], $userPermission);
				if($info){
					$ladingPage = $info[0]->landing_page;
					redirect($ladingPage, 'refresh');
				}
			}
		}
	}
	
	public function login()
	{
		$account_num =  $this->input->post('account_num');
		$identity =  $this->input->post('username');
		$password =  $this->input->post('password');
		$remember = (bool) $this->input->post('remember');
		
		if (!$this->ion_auth->login($identity, $password))
		{	
			$this->session->set_flashdata('message', $this->ion_auth->errors());		
			redirect('auth', 'refresh');
		}
		else
		{
			$user = $this->ion_auth->user()->result();
			$config = array('userID' => $user[0]->id);			
			$this->load->library('acl',$config);
			$userPermission = $this->acl->userPerms[0]['id'];
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			if($this->acl->userRoles){
				$this->load->model('iagh_model');
				$info = $this->iagh_model->getPageGrouping($this->acl->userRoles[0], $userPermission);
				if($info){
					$ladingPage = $info[0]->landing_page;
					redirect($ladingPage, 'refresh');
				}
				else {
					$message = $this->session->flashdata('message');
					$this->tal->error = true;
					$this->tal->display('common/login.zpt');
				}
			}
		}
	}

	public function logout()
	{
		$logout = $this->ion_auth->logout();
		redirect('auth', 'refresh');
	}
	
}