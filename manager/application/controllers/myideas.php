<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Myideas extends CI_Controller{
	
	private $workspaceId;
	
	public function __construct(){
		parent::__construct();
		
		$this->user = $this->ion_auth->user()->result();
		if(!$this->ion_auth->user()->result()){
			exit;
		}
		$this->user[0]->fullname = $this->user[0]->first_name . ' ' . $this->user[0]->last_name;
		$config = array('userID' => $this->user[0]->id);			
		$this->load->library('acl', $config);
		$this->load->library('curl');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url', 'file'));
		if (!$this->acl->userRoles) {
			exit;
		}
		else{
			$this->workspaceId = $this->acl->userRoles[0];
		}		
	} # -- function __construct

	public function papiCall($method="GET", $function="", $options=array()){
		$papi = array();
		$papi['method'] = $method;
		$papi['papiCall'] = $function;
		$papi['data'] = $options;
		$this->curl->PAPIConfig($papi);
		$res = $this->curl->execute();
		return json_decode($res['message']);
	} # -- function papiCall
	
	public function viewVideoApproval(){
		$data = array();
		$this->load->view('myideas/videoApproval.zpt', $data);
	} #-- function index
	
	public function approveVideos(){
		$data = array();
		$data['content'] = array();
		$options = array(	"WorkspaceID"=>$this->workspaceId,
							"TypeID"=>3,
							"UserGenFlag"=>1,
							"PublishedFlag"=>1,
							);
		$papiResult = $this->papiCall("GET", "Content.GetAll", $options);
		if($papiResult->Status){
			$data['content'] = $papiResult->Content;
		}
		$this->load->view('myideas/approvedVideos.zpt', $data);
		
	} # -- function approveVideo
	
	public function viewRejectedVieos(){
		$data = array();
		$data['content'] = array();
		$options = array(	"WorkspaceID"=>$this->workspaceId,
							"TypeID"=>3
//							"UserGenFlag"=>1,
//							"PublishedFlag"=>0
							);
		$papiResult = $this->papiCall("GET", "Content.GetAll", $options);
		if($papiResult->Status){
			$data['content'] = $papiResult->Content;
		}
		$this->load->view('myideas/rejectedVideos.zpt', $data);
	} # -- function viewRejectedVideos
	
	
	public function featurePlaylist(){
		$data = array();
		$data['playlists'] = array();
				
		$options = array("WorkspaceID"=>$this->workspaceId, "Type"=>1);
		$papiResults = $this->papiCall("GET", "Playlist.GetAll", $options);
		if(isset($papiResults->Status)){
			if($papiResults->Status)
				$data['playlists'] = $papiResults->Playlist;
		}
		$this->load->view('myideas/featuredIdea.zpt', $data);
	} # -- function featurePlaylist
		
	public function addFeaturePlaylist(){
		$sectionId = 73;
		$data = array();
		$videos = array();
		$data['formType'] = "add";
		$dataTable = "";
		
		$options =array(
							"TypeID"=>3, 
							"WorkspaceID"=>$this->workspaceId, 
							"SectionID"=>$sectionId
						);
		$papiResults = $this->papiCall("GET", "Content.GetAll", $options);
		if($papiResults->Status){
			$videos = $papiResults->Content;
			for($i=0; $i<count($videos); $i++){
				$dataTable .= '<tr><td><input type="checkbox" name="' . ucwords($videos[$i]->Name) . '" id="video-list" value="'.$videos[$i]->ID.'"/>&nbsp;' . ucwords($videos[$i]->Name) . '</td></tr>';
			}
		}		
		$data['dataTable'] = $dataTable;
		$this->load->view('myideas/featurePlaylistForm.zpt', $data);
	}
	
	public function editFeaturePlaylist(){
		$data = array();
		$sectionId = 73;
		$dataTable = "";
		$playlist = array();
		$arrayCurrentVideoList = array();
		$currentVideoList = "";
		
		$options = array();
		if(isset($_GET['id'])){
			$playlistId = $_GET['id'];
			$options = array("ID"=>$playlistId);
			$papiResults = $this->papiCall("GET", "Playlist.Get", $options);
			if($papiResults->Status){
				$playlist = $papiResults->Playlist;
				
				$options = array();
				$options = array("PlaylistID"=>$playlistId);
				$papiResult = $this->papiCall("GET", "Playlist.Content.GetAll", $options);
				
				if($papiResult->Status){
					$videoIds = array();
					for($i=0; $i<count($papiResult->PlaylistContent); $i++){
						$videoIds[] = $papiResult->PlaylistContent[$i]->ID;
						$arrayCurrentVideoList [] = '<li class="ui-state-default" id="vlist" name="'.$papiResult->PlaylistContent[$i]->ID.'"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'. ucwords($papiResult->PlaylistContent[$i]->Name) . ' </li>';
					}
					$data['strVideoId'] = implode(",",$videoIds);
					$options = array(
										"TypeID"=>3, 
										"WorkspaceID"=>$this->workspaceId,
										"UserGenFlag"=>1,
										"PublishedFlag"=>1,
										"SectionID"=>$sectionId
									);
					$papiResult = $this->papiCall("GET", "Content.GetAll", $options);
					if($papiResult->Status){
						$videos = $papiResult->Content;
						for($i=0; $i<count($videos); $i++){
							$check = in_array($videos[$i]->ID, $videoIds)?"checked":"";
							$dataTable .= '<tr><td><input type="checkbox" name="' . ucwords($videos[$i]->Name) . '" id="video-list" '.$check.' value="'.$videos[$i]->ID.'"/>&nbsp;' . ucwords($videos[$i]->Name) . '</td></tr>';
						}						
					}
					$currentVideoList = implode("", $arrayCurrentVideoList);
				}
			}
		}
		$data['playlist'] = $playlist;
		$data['formType'] = "edit";
		$data['dataTable'] = $dataTable;
		$data['currentVideoList'] = $currentVideoList;
		$this->load->view('myideas/featurePlaylistForm.zpt', $data);
	} # -- function editGoodNewsPlaylist
	
	public function saveFeaturePlaylist(){
		$res = array();
		$res['status'] = 0;
		
		# Edit Playlist
		if(($_POST['playlistId'])){
			$playlistId = isset($_POST['playlistId'])?$_POST['playlistId']:"";
			$name = isset($_POST['title'])?$_POST['title']:"";
			$description = isset($_POST['description'])?$_POST['description']:"";
			$videoId = isset($_POST['videoId'])?$_POST['videoId']:"";
			$workspaceId = $this->workspaceId;
			$options = array(
								"ID"=>$playlistId,
								"Name"=>$name,
								"Description"=>$description,
								"WorkspaceID"=>$workspaceId,
								"Type"=>1
							);
			$papiResult = $this->papiCall("POST", "Playlist.Update", $options);
						
			if($papiResult->Status){				
				$currentVideoId = isset($_POST['currentVideoId'])?$_POST['currentVideoId']:"";
				# delete current videos in playlist
				if(!empty($currentVideoId)){
					$this->papiCall("POST", "Playlist.Content.Remove", array("ContentID"=>$currentVideoId, "PlaylistID"=>$playlistId));
				}
				if($videoId != "0"){
					if($papiResult->Status){
						# add new list of videos
						$papiResult = $this->papiCall("POST", "Playlist.Content.Add", array("PlaylistID"=>$playlistId, "ContentID"=>$videoId));
					}
					$res['status'] = $papiResult->Status + 0;
				}
				else{
					$res['status'] = 1;
				}
			}
			else {
				$res['status'] = $papiResult->Status + 0;
			}
			
		}
		# Add Playlist
		else{
			$name = $_POST['title'];
			$description = $_POST['description'];
			$videoId = $_POST['videoId'];
			$status = 0;
			$workspaceId = $this->workspaceId;
			$options = array(
								"Name"=>$name,
								"Description"=>$description,
								"Status"=>$status,
								"WorkspaceID"=>$workspaceId,
								"Type"=>1
							);
			$papiResults = $this->papiCall("POST", "Playlist.Create", $options);
			$playlistId = $papiResults->Playlist->ID;
			$videoIds = explode(",", $videoId);
							
			for($i=0; $i<count($videoIds); $i++){
				$options = array();
				$options = array(
									"PlaylistID"=>$playlistId,
									"ContentID"=>$videoIds[$i]
								);
				$this->papiCall("POST", "Playlist.Content.Add", $options);
			}
			$res['status'] = 1;
		}
		print_r(json_encode($res));
	} # -- saveFeaturePlaylist
	
	public function publishFeaturePlaylist(){
		$res = array();
		$res['status'] = 0;
		if(isset($_POST)){
			$currentPublishedId = explode(",", $_POST['playlistId']);
			$papiResult = $this->papiCall("GET", "Playlist.GetAll", array("WorkspaceID"=>$this->workspaceId, "Type"=>1));
			if($papiResult->Status){
				$playlistId = $papiResult->Playlist;
				for($i=0; $i<count($playlistId); $i++){
					if(in_array($playlistId[$i]->ID, $currentPublishedId)){
						$this->papiCall("POST", "Playlist.Update", array("ID"=>$playlistId[$i]->ID, "Status"=>1));
					}
					else {
						$this->papiCall("POST", "Playlist.Update", array("ID"=>$playlistId[$i]->ID, "Status"=>0));
					}
				}				
			}
			$res['status'] = 1;
		}
		print_r(json_encode($res));
	} # -- function publishFeaturePlayslist	
	
}