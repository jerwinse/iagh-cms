<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cpanel extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		if (!$this->ion_auth->logged_in())
		{
			redirect('/auth/login');
		}
		else
		{
			$this->tal->title = $this->config->item('title');
			$this->tal->base_url = substr(base_url(), 0, -1);	
	
			$this->user = $this->ion_auth->user()->result();		
			$this->user[0]->fullname = $this->user[0]->first_name . ' ' . $this->user[0]->last_name;
			$config = array('userID' => $this->user[0]->id);			
			$this->load->library('acl', $config);	
		}		
	}
	
	public function admin()
	{
		if ($this->acl->userRoles[0]) {
			$this->load->model('iagh_model');
			$userPermission = $this->acl->userPerms[0]['id'];
			$panelPage = $this->iagh_model->getPageGrouping($this->acl->userRoles[0], $userPermission);
			$panelPage = $panelPage[0]->panel_page;
			$this->tal->user = $this->user[0];
			$this->tal->display($panelPage);			
		}
		else{
			redirect('404');
		}
	}
	
	public function tenant()
	{
		if ($this->acl->hasRole(2)) 
		{
			$this->tal->user = $this->user[0];
			$this->tal->display('cpanel/tenant.zpt');			
		}
		else
		{
			redirect('404');
		}		
	}

		
}