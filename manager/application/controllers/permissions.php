<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Permissions extends CI_Controller {

	private $permission;
	private $userPermission;
	
	public function __construct(){
		parent::__construct();
				
		$this->tal->title = $this->config->item('title');
		$this->tal->base_url = substr(base_url(), 0, -1);	
				
		# check if session expires
		if(!$this->ion_auth->user()->result()){
			exit;
		}
		
		$this->user = $this->ion_auth->user()->result();		
		$this->user[0]->fullname = $this->user[0]->first_name . ' ' . $this->user[0]->last_name;
		$config = array('userID' => $this->user[0]->id);			
		$this->load->library('acl', $config);

		if (!$this->acl->userRoles) {
			exit;
		}
		else{
			# get role permission
			$this->permission = $this->acl->getRolePerms($this->acl->userRoles[0]);
			$userPermission = $this->acl->getUserPerms($this->acl->userID);
			$this->userPermission = $userPermission[0]['id'];
		}
	}
	
	public function index()
	{
		$data = $this->acl->getAllPerms('full');		
		$this->tal->data = $data;
		$this->tal->display('permissions/index.zpt');		
	}
	
	public function create()
	{
		$this->tal->display('permissions/create.zpt');
	}
	
	public function docreate()
	{
		$post = $this->input->post('data');
		$param = json_decode($post);
		$data = array();
		foreach ($param as $item)
		{
			$data[$item->name] = $item->value;
		}
		$id = $this->acl->createPermission($data);
		$response = array('status' => 'NACK', 'message' => 'Cannot save record.');
		if ($id) {
			$response = array('status' => 'ACK', 'message' => 'Record successfully created.');
		}
		
		$this->tal->data = json_encode($response);
		$this->tal->display('common/structure.zpt');
	}

}