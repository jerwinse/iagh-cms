<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Roles extends CI_Controller {

	private $permission;
	private $userPermission;
	
	public function __construct()
	{
		parent::__construct();
				
		$this->tal->title = $this->config->item('title');
		$this->tal->base_url = substr(base_url(), 0, -1);	
				
		# check if session expires
		if(!$this->ion_auth->user()->result()){
			exit;
		}
		
		$this->user = $this->ion_auth->user()->result();		
		$this->user[0]->fullname = $this->user[0]->first_name . ' ' . $this->user[0]->last_name;
		$config = array('userID' => $this->user[0]->id);			
		$this->load->library('acl', $config);		

		if (!$this->acl->userRoles) {
			exit;
		}
		else{
			# get role permission
			$this->permission = $this->acl->getRolePerms($this->acl->userRoles[0]);
			$userPermission = $this->acl->getUserPerms($this->acl->userID);
			$this->userPermission = $userPermission[0]['id'];
		}
	}
	
	public function index()
	{
		$data = array();
		$roles = array();
		$roles = $this->acl->getAllRoles('full');
		$data['roles'] = $roles;
		$this->load->view('roles/index.zpt', $data);
	}
		
	public function create()
	{
		$data = array();
		$data['isAddRequest'] = true;
		$role[] = array('id'=>0);
		$data['role'] = $role;
		$permissions = $this->acl->getAllPerms('full');
		
		foreach($permissions as $index => $val){
			$rolePermission = strtolower($val['name']);
		}
		$data['permissions'] = $permissions;
		$data['currentPermission'] = 0;
		$this->load->view('roles/roleForm.zpt', $data);
	}
	
	public function docreate()
	{
		$post = $this->input->post('data');
		$param = json_decode($post);
		$data = array();
		foreach ($param as $item)
		{
			$data[$item->name] = $item->value;
		}
		$id = $this->acl->createRole($data);
		$response = array('status' => 'NACK', 'message' => 'Cannot save record.');
		if ($id) {
			$response = array('status' => 'ACK', 'message' => 'Record successfully created.');
		}
		
		$this->tal->data = json_encode($response);
		$this->tal->display('common/structure.zpt');
	}
	
	public function edit(){		
		if(isset($_GET['roleId'])){
			$roleInfo = array();
			$roleId = $_GET['roleId'];
			$curPermId = $this->acl->getRolePerms($roleId);
			$roles = $this->acl->getAllRoles('full');
			if($roles){
				for($i=0; $i<count($roles); $i++){
					if($roles[$i]['id']==$roleId){
						$roleInfo[] = $roles[$i];
					}
				}
			}
			
			$permissions = $this->acl->getAllPerms('full');
			
			foreach($permissions as $index => $val){
				$rolePermission = strtolower($val['name']);
			}
			
			$data['currentPermission'] = $curPermId[0]['perm'];
			$data['permissions'] = $permissions;
			$data['isAddRequest'] = false;
			$data['role'] = $roleInfo;
			$this->load->view('roles/roleForm.zpt', $data);
		}
	}
	
	public function saveRole(){
		$res = array();
		$res['status'] = 0;
		$res['message'] = "";
		$post = $this->input->post('data');
		$params = json_decode($post);
		$dataParams = array();
		foreach($params as $item){
			$dataParams[$item->name] = $item->value;
		}
		
		$roleName = array("roleName"=>$dataParams['roleName']);
		# Edit Request
		if($dataParams['roleId']>0){
			$this->acl->updateRole($roleName, $dataParams['roleId']);
			$this->acl->updateRolePermission($dataParams['roleId'], $dataParams['permission']);
			$res = array("status"=> 1, "message"=>"Role has been successfully saved!");
		}
		# Add Request
		else {			
			$id = $this->acl->createRole($roleName);
			if ($id) {
				$rolePermissionData = array("roleID"=>$id, "permID"=>$dataParams['permission']);
				$permID = $this->acl->createRolePermission($rolePermissionData);
				$res = array('status' => 1, 'message' => 'Record successfully created.');
			}
			else {
				$res = array('status' => 0, 'message' => 'Cannot save record.');
			}
		}
		print_r(json_encode($res));
	}
	
	public function deleteSelected(){
		$res = array();
		$res['status'] = 0;
		$res['message'] = "Failed to delete Role";
		if(isset($_POST['id']) && !empty($_POST['id'])){			
			$id = explode(",", $_POST['id']);
			$counter = 0;
			for($i=0; $i<count($id); $i++){
				$roleMembers = $this->acl->getRoleMembers($id[$i]);
				if(!empty($roleMembers)){
					$counter++;
				}
			}
			
			if($counter>0){
				$res = array("status"=>0, "message"=>"Role is currently in use");
			}
			else{
				for($i=0; $i<count($id); $i++){
					$this->acl->removeRole($id[$i]);
				}
				$res = array("status"=>1, "message"=>"Role has been deleted");
			}
		}
		print_r(json_encode($res));
	}
}