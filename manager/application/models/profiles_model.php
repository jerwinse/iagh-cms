<?
class Profiles_model extends CI_Model 
{
	public function __construct()
    {
        parent::__construct();
	}
	
	public function getAccountTouchPoints($account_id)
	{
		$this->profiles_db = $this->load->database('profiles', TRUE);		
		$this->profiles_db->select('AccountTouchPoints.touchPointType, AccountTouchPoints.touchPointId');
		$this->profiles_db->from('3rdPartyAccounts');
		$this->profiles_db->where('3rdPartyAccounts.accountName', $account_id);
		$this->profiles_db->join('TouchPointHandler', 'TouchPointHandler.id = 3rdPartyAccounts.touchPointHandlerId');
		$this->profiles_db->join('AccountTouchPoints', 'AccountTouchPoints.accountId = TouchPointHandler.id');		

		$query = $this->profiles_db->get();		
		$result = $query->result();
		
		$this->profiles_db->close();
		return $result;
	}
	
	public function checkTouchPoint($tp, $type)
	{
		$this->profiles_db = $this->load->database('profiles', TRUE);
		$this->profiles_db->select('accountId');
		$this->profiles_db->from('AccountTouchPoints');	
		$this->profiles_db->where('touchPointId', $tp);
		$this->profiles_db->where('touchPointType', $type);
		
		$query = $this->profiles_db->get();		
		$result = $query->result();
		
		$this->profiles_db->close();
		if (empty($result))
			return 0;
			
		return $result[0]->accountId;		
	}
	
	public function checkAccount($username)
	{
		$this->profiles_db = $this->load->database('profiles', TRUE);
		$this->profiles_db->select('touchPointHandlerId');
		$this->profiles_db->from('3rdPartyAccounts');	
		$this->profiles_db->where('accountName', $username);
		
		$query = $this->profiles_db->get();		
		$result = $query->result();
		
		$this->profiles_db->close();
		if (empty($result))
			return 0;
			
		return $result[0]->touchPointHandlerId;		
	}

	public function createAccount($data)
	{
		$this->profiles_db = $this->load->database('profiles', TRUE);
		$this->profiles_db->insert('3rdPartyAccounts', $data); 
		$id = $this->profiles_db->insert_id(); 
		$this->profiles_db->close();
		
		return $id;
	}
	
	public function createHandler($data)
	{
		$this->profiles_db = $this->load->database('profiles', TRUE);
		$this->profiles_db->insert('TouchPointHandler', $data);
		$id = $this->profiles_db->insert_id(); 
		$this->profiles_db->close();
		
		return $id;
	}

	public function createTouchPoint($data)
	{
		$this->profiles_db = $this->load->database('profiles', TRUE);
		$this->profiles_db->insert('AccountTouchPoints', $data);
		$id = $this->profiles_db->insert_id(); 
		$this->profiles_db->close();
		
		return $id;		
	}
	public function update($table, $data, $reference)
	{
		$this->profiles_db = $this->load->database('profiles', TRUE);
		$this->profiles_db->update($table, $data, $reference);
		$this->profiles_db->close();	
	}
}