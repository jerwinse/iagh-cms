<?php

class Iagh_model extends CI_Model{
	
	private $db;
	
	public function __construct(){
		parent::__construct();
		$this->db = $this->load->database('system', true);
	}
	
	public function getPageGrouping($roleId = "", $permissionId = 0){
		if(!empty($roleId)){
			$this->db->where(array('role_id'=>$roleId, 'permission_id'=> $permissionId));
			$query = $this->db->get('page_grouping');
			if($query->num_rows()){
				return $query->result();
			}
			else {
				return array();
			}
		}
	}
}
?>