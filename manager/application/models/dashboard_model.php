<?php


class Dashboard_model extends CI_Model{
	
	private $db;
	
	public function __construct(){
		parent::__construct();
		$this->db = $this->load->database('trans', TRUE);
	}
	
	public function getTopFollowers(){
		$sql = "select name, hits from (
				    select trans.*, acc.name from (
				       select service_id, count(distinct destination_number) as hits from transaction_db_DEV.mttrans group by service_id
				    ) as trans
				   left join web_app.cloudmsngr_accounts as acc on acc.service_id = trans.service_id
				) as r where name IS NOT NULL ORDER BY hits desc";
		$query = $this->db->query($sql);
		return $query->num_rows()>0?$query->result():array();
	}
	
	public function getTotalHits(){
		$sql = "select name, hits from (
				    select trans.*, acc.name from (
				       select service_id, count(distinct transaction_number) as hits from transaction_db_DEV.mttrans group by service_id
				    ) as trans
				   left join web_app.cloudmsngr_accounts as acc on acc.service_id = trans.service_id
				) as r where name IS NOT NULL ORDER BY hits desc";
		$query = $this->db->query($sql);
		return $query->num_rows()>0?$query->result():array();
	}
	
	public function getOperatorTotalHits(){
//		$sql = "SELECT operator, COUNT(distinct transaction_number) AS hits FROM mttrans GROUP BY operator";
		$this->db->select("operator, COUNT(DISTINCT transaction_number) AS hits");
		$this->db->group_by("operator");
		$query = $this->db->get("mttrans");
		return $query->num_rows()>0?$query->result():array();
	}
	
	public function getTodayHits(){
		$today = date('Y-m-d');
		$sql = "SELECT date_format(date_time,'%H') AS h, COUNT(distinct transaction_number) AS hits FROM mttrans WHERE DATE(date_time)='{$today}' GROUP BY h";
//		$query = $this->db->select("date_format(date_time,'%H') AS h, COUNT(distinct transaction_number) AS hits")->from("mttrans")->where(array("DATE(date_time)"=>"'{$today}'"))->group_by("h");
//		$this->db->where(array("DATE(date_time)"=>"'{$today}'"));
//		$this->db->group_by("h");
//		$query = $this->db->get("mttrans");
		
		$query = $this->db->query($sql);
		return $query->num_rows()>0?$query->result():array();
	}
	
	public function getTPMonthlyHits(){
		$sql = "select operator AS name, date_format(date_time,'%Y-%m') AS d, count(distinct transaction_number) as hits from mttrans group by operator, d";
		$query = $this->db->query($sql);
		return $query->num_rows()>0?$query->result():array();
	}
	
	public function getTenantMonthlyHits(){
		$sql = "select name, hits, d from (
				    select trans.*, acc.name from (
				       select service_id, DATE_FORMAT(date_time, '%Y-%m') AS d, count(distinct transaction_number) as hits from transaction_db_DEV.mttrans group by service_id, d
				    ) as trans
				   left join web_app.cloudmsngr_accounts as acc on acc.service_id = trans.service_id
				) as r where name IS NOT NULL ORDER BY hits desc";
		$query = $this->db->query($sql);
		return $query->num_rows()>0?$query->result():array();
	}
	
	public function getFeedbackTables(){
		$sql = "SELECT table_name AS tableName FROM INFORMATION_SCHEMA.TABLES
					WHERE table_schema = 'buildr_profiles'
				  	AND table_name LIKE 'react_%_feedback%'
				";
		$query = $this->db->query($sql);
		return $query->num_rows()>0?$query->result():array();
	}
	
	public function getFeedbackHits($field = "severity", $status="low"){
		$feedback = $this->getFeedbackTables();
		$sqlTables = array();
		if($feedback){
			for($i=0; $i<count($feedback); $i++){
				$sqlTables[] = "SELECT * FROM buildr_profiles.{$feedback[$i]->tableName}";
			}
			$sql = implode(" union ", $sqlTables);
			$sql = "SELECT operator, COUNT(*) AS hits, {$field} FROM ({$sql}) AS f WHERE {$field} like '{$status}' group by operator, {$field}";
			$query = $this->db->query($sql);
			return $query->num_rows()>0?$query->result():array();
		}
	}
}