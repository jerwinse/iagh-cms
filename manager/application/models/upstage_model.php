<?
class Upstage_model extends CI_Model
{
	public $db;
	private $table_name;
	
	
	public function __construct()
	{
        parent::__construct();
        $this->db = $this->load->database('system', TRUE);        	
	}
	
	public function set_table($table)
	{
		$this->table_name = $table;
	}
	
	public function get_table()
	{
		return $this->table_name;
	}
	
	public function log_service_upstage( $service_id, $buildr_instance_id )
	{
		$sql = "INSERT IGNORE INTO deployed_services ( service_id, buildr_instance_id ) VALUES ( $service_id, $buildr_instance_id )";
				
		if( !$this->db->simple_query($sql) )
		{
			log_message('error','SQL ERROR - '.$sql);
		}
	}
	
	public function get_instances()
	{
		$instances = array();
		#$this->db->where_in('product_code', $content_id);
		$this->db->order_by('id');
		$query = $this->db->get('buildr_instances');
		foreach($query->result() as $row)
		{
			$instances[] = $row;
		}
		return $instances;
	}
	
	public function get_instance($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('buildr_instances');
		
		return $query->row();
	}
	
	public function build_table_structure($tablename)
	{
		# Return array
		$structure = array();
		$structure['name'] = $tablename;
		
		# Get table info
		$this->db->where('name', $tablename);
		$query = $this->db->get('user_input_tables');
		$table_info = $query->row();
		
		# get table type
		$this->db->where('id', $table_info->type_id);
		$query = $this->db->get('user_input_types');
		$table_info->type = $query->row();
		$structure['type'] = $query->row_array();
		
		$structure['prefix'] = $table_info->type->prefix;
		
		# Get table fields
		$this->db->where('table_id', $table_info->id);
		$query = $this->db->get('user_input_table_fields');
		$structure['fields'] = array();
		
		foreach ($query->result() as $row)
		{
			# Get each field
			$this->db->where('id', $row->field_id);
			$query2 = $this->db->get('user_input_fields');
			
			$field = $query2->row();
			$structure['fields'][$field->field] = $field->datatype;
			
		}
		
		return $structure;
	}
	
	public function Find($reference = array(), $returns = "", $options = array())
	{
		$data = array();
		if(is_array($reference))
		{
			
			$ref_id = $this->GetReferenceId($reference, $options);
			if(!empty($ref_id))
			{
				$this->db->where_in('id', $ref_id);
				$query = $this->db->get($this->table_name);
				foreach($query->result() as $row)
				{
					$data[] = $row;
				}
			}
		}
		return $data;
	}
	
	public function GetReferenceId($reference = array(), $options = array())
	{
		$ref_id = array();
		
		if(is_array($reference) && !empty($reference))
		{
			foreach($reference as $col => $val)
			{
				if(is_array($val) && !empty($val))
				{
					$this->db->where_in($col, $val);
				}
				else
				{
					if(!empty($val))
					$this->db->where($col, $val);
				}
			}
		}
		else
		{
			$this->db->where($reference);
		}		
		$query = $this->db->get($this->table_name);
		
		if(!empty($options))
		{
			if(array_key_exists('order_by', $options))
			{
				foreach($options['order_by'] as $field => $sort_option)
				{
					$this->db->order_by($field, $sort_option);
				}
			}
			if(array_key_exists('group_by', $options))
			{
				$this->db->group_by($options['group_by']);
			}
			if(array_key_exists('limit', $options))
			{
				if(is_array($options['limit']))
				{
					if(count($options['limit'])==2)
					{
						$this->db->limit($options['limit'][0],$options['limit'][1]);
					}
					else
					{
						$this->db->limit($options['limit'][0]);
					}
				}
				else
				{
					$this->db->limit($options['limit']);
				}
			}
		}
		foreach($query->result() as $row)
		{
			$ref_id[] = $row->id;
		}
		return $ref_id;
	}
}
?>
