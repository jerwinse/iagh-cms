<?
class Cloudmsngr_model extends CI_Model 
{
	
	public function __construct()
    {
        parent::__construct();        
        $this->config->load('cloudmsngr');
	}
	
	public function BuildrRestart()
	{
		$this->db = $this->load->database('system', TRUE);
		$data = array('status' => 1);
		$this->db->where('id', 1);
		$this->db->update('tmp_buildr_restart', $data); 	
		$this->db->close();	
	}
	
	public function GetAllTenants($page = 0)
	{
		$this->db = $this->load->database('system', TRUE);
		if ($page != 0)
		{
			$item = $this->config->item('cloudmsngr');
			$offset = $item['itemperpage'] * ($page - 1);
			$this->db->limit($item['itemperpage'], $offset);	
		}
		
		$this->db->order_by('name asc');
		 		
		$query = $this->db->get('cloudmsngr_accounts');
		$result = $query->result();
		$this->db->close();
		
		return $result;
	}
	
	public function CreateTenant($data)
	{
		$this->db = $this->load->database('system', TRUE);
		$insert_id = 0;
		if(!empty($data))
		{
			$this->db->select('account_num');
			$this->db->order_by('account_num', 'desc'); 
			$this->db->limit(1);
			$query = $this->db->get('cloudmsngr_accounts');		
			$result = $query->result();		
			$account_num = $result[0]->account_num;			
			$data['account_num'] = ++$account_num;
			
			$this->db->insert('cloudmsngr_accounts', $data);			
			$insert_id = $this->db->insert_id();
		}
		$this->db->close();		
		return $insert_id;				
	}
	
	public function ReferenceAccountService($tenant_id, $service_id)
	{
		$this->db = $this->load->database('system', TRUE);
		$data = array('account_id' => $tenant_id, 'service_id' => $service_id, 'type' => 'cloudmsngr');
		$this->db->insert('cloudmsngr_account_services', $data);
		$this->db->close();		
	}
}
