<?
class Remote_model extends CI_Model
{	
	var $db;
	var $table_name;
	
	# by defaul table name is services
	function __construct()
	{
        parent::__construct();
	}
		
	function load_db($config)
	{
		#$this->load->database($config);
		$this->db = $this->load->database($config, TRUE);
	}
	
	public function execute($sql)
	{
		return $this->db->query($sql);
	}

	public function getInsertId()
	{
		return $this->db->insert_id();
	}

}
?>
