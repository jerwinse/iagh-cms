$(document).ready(function(){	
	jQuery.fn.center = function () {
	    this.css("position","absolute");
	    this.css("top", ( $(window).height() - this.outerHeight() ) / 4+$(window).scrollTop() + "px");
	    this.css("left", ( $(window).width() - this.outerWidth() ) / 4+$(window).scrollLeft() + "px");
	    return this;
	}
});

var account = {
	deleteSelected: function(checkBox, controller, param, callback){	
		var items = [];
		var i=0;
		$("input[id='" + checkBox + "']:checked").each(function(){
			items.push($(this).val());
			i++;
		});
		if(i>0){			
	    	var html = '<h3>Delete!</h3><p>These items will be permanently deleted and cannot be recovered. Are you sure? &nbsp;&nbsp;<a href="javascript:;" onclick="account.deleteSelected(\'' + checkBox + '\', \'' + controller + '\', \'yes\', \'' + callback + '\')">Yes</a>&nbsp;&nbsp;/&nbsp;&nbsp;<a href="javascript:;" onclick="account.deleteSelected(\'' + checkBox + '\', \'' + controller + '\', \'no\', \'' + callback + '\')">No</a></p>';
	    	$('#delMessage').html(html);
	    	$('#delMessage').removeClass('error');
	    	$('#delMessage').addClass('message');
	    	$('#delMessage').addClass('success');
			switch(param){
				case '':
					$('#delMessage').show(500);
				break;
				case 'no':
					$('#delMessage').hide(500);
				break;
				case 'yes':
					$('#delMessage').hide(300);
					if(i>0){
				        $.ajax(controller, {
				        	type: 'post',
				        	data: {
								'id':items.toString()
				        	},
				            dataType: "json",
				            cache: false,
				            beforeSend: function(jqXHR, settings) {
				            	$('#message').html('');
				            	$('#message').removeClass('error');
				            	$('#message').removeClass('success');
				            	
				            	$('#message').html('<h3>Please wait</h3><p>Deleting record...</p>');
				            	$('#message').addClass('message');
				            	$('#message').addClass('info');
				            	$('#message').fadeIn();
				            },
				            complete: function(jqXHR, textStatus) {            	
				                var response = jQuery.parseJSON(jqXHR.responseText);
				                if (response.status == 1)
				                {
				                	var html = '<h3>Success!</h3><p>Done deleting record/s</p>';
				                	$('#message').html(html);
				                	$('#message').removeClass('error');
				                	$('#message').addClass('message');
				                	$('#message').addClass('success');
				                	$('#message').fadeIn().delay(10000).fadeOut('slow');
						            if(callback==''){
						            	location.reload();
						            }
						            else{
						            	window.location.href = callback;	
						            }				                	
				                }
				                else
				                {
				                	var html = '<h3>Error!</h3><p>There was an error deleting the record.</p><p>' + response.message + '</p>';
				                	$('#message').html(html);
				                	$('#message').addClass('message');
				                	$('#message').addClass('error');
				                	$('#message').fadeIn().delay(10000).fadeOut('slow');
				                }
				            }
				        });					
					}
		
				break;
			}
		}
	}
}

var core = {
	deleteSelected: function(checkBox, papiCall, param){		
		var items = [];
		var i=0;
		$("input[id='" + checkBox + "']:checked").each(function(){
			items.push($(this).val());
			i++;
		});
		if(i>0){			
	    	var html = '<h3>Delete!</h3><p>These items will be permanently deleted and cannot be recovered. Are you sure? &nbsp;&nbsp;<a href="javascript:;" onclick="core.deleteSelected(\'' + checkBox + '\', \'' + papiCall + '\', \'yes\')">Yes</a>&nbsp;&nbsp;/&nbsp;&nbsp;<a href="javascript:;" onclick="core.deleteSelected(\'' + checkBox + '\', \'' + papiCall + '\', \'no\')">No</a></p>';
	    	$('#delMessage').html(html);
	    	$('#delMessage').removeClass('error');
	    	$('#delMessage').addClass('message');
	    	$('#delMessage').addClass('success');
			switch(param){
				case '':
					$('#delMessage').show(500);
				break;
				case 'no':
					$('#delMessage').hide(500);
				break;
				case 'yes':
					$('#delMessage').hide(300);
					if(i>0){
				        $.ajax('/core/deleteVideo', {
				        	type: 'post',
				        	data: {
								'id':items.toString(),
								'papiCall':papiCall
				        	},
				            dataType: "json",
				            cache: false,
				            beforeSend: function(jqXHR, settings) {
				            	$('#message').html('');
				            	$('#message').removeClass('error');
				            	$('#message').removeClass('success');
				            	
				            	$('#message').html('<h3>Please wait</h3><p>Deleting record...</p>');
				            	$('#message').addClass('message');
				            	$('#message').addClass('info');
				            	$('#message').fadeIn();
				            },
				            complete: function(jqXHR, textStatus) {            	
				                var response = jQuery.parseJSON(jqXHR.responseText);
				                if (response.status == 1)
				                {
				                	var html = '<h3>Success!</h3><p>Done deleting record/s</p>';
				                	$('#message').html(html);
				                	$('#message').removeClass('error');
				                	$('#message').addClass('message');
				                	$('#message').addClass('success');
				                	$('#message').fadeIn().delay(10000).fadeOut('slow');
				                }
				                else
				                {
				                	var html = '<h3>Error!</h3><p>There was an error deleting the record.</p>';
				                	$('#message').html(html);
				                	$('#message').addClass('message');
				                	$('#message').addClass('error');
				                	$('#message').fadeIn().delay(10000).fadeOut('slow');
				                }
				                location.reload();
				            }
				        });					
					}
		
				break;
			}
		}
	},
	
	publishVideo: function(){
		var items = [];
		var i=0;
		$("input[name='video[]']:checked").each(function(){
			items.push($(this).val());
			i++;
		});
		if(i>1){
			alert("Multiple videos are not allowed");
		}
		else if(i<1){
			alert("To publish a video, simply check the box");
		}
		else{
			$.ajax("/core/publishVideo/",{
				type:'post',
				data:{
					'videoId':items.toString()
				},
				dataType:'json',
				cache:false,
	            beforeSend: function(jqXHR, settings) {
	            	$('#message').html('');
	            	$('#message').removeClass('error');
	            	$('#message').removeClass('success');
	            	
	            	$('#message').html('<h3>Please wait</h3><p>Saving record...</p>');
	            	$('#message').addClass('message');
	            	$('#message').addClass('info');
	            	$('#message').fadeIn();
	            },
	            complete: function(jqXHR, textStatus) {            	
	                var response = jQuery.parseJSON(jqXHR.responseText);
	                if (response.status == 1)
	                {
	                	var html = '<h3>Success!</h3><p>Video has been published successfully!</p>';
	                	$('#message').html(html);
	                	$('#message').removeClass('error');
	                	$('#message').addClass('message');
	                	$('#message').addClass('success');
	                	$('#message').fadeIn().delay(10000).fadeOut('slow');
	                }
	                else
	                {
	                	var html = '<h3>Error!</h3><p>There was an error saving the record.</p>';
	                	$('#message').html(html);
	                	$('#message').addClass('message');
	                	$('#message').addClass('error');
	                	$('#message').fadeIn().delay(10000).fadeOut('slow');
	                }
	                location.reload();
	            }			
			});
		}
	},
	
	saveVideoPage: function(requestType){
		var title = $("#form-title").val();
		var description = $("#form-description").val();
		var url = $("#form-url").val();
		var thumbnailUrl = $("#form-thumbnail-url").val();
		var callback = $("#callback-page").val();
		var callbackPage = "";
		var message = "";
		var ajaxUrl = '/core/saveVideoPage/';
		var videoId = 0;
		var page = "";
		var sectionId = 0;
		
		if(callback == "login"){
			callbackPage = "admin#/core/login";
			page = "login";
		}
		else{
			callbackPage = "admin#/core/home";
			page = "home";
		}
		if(requestType == "add"){
			message = "You have successfully added a " + page + " video.";
			sectionId = $("#section-id").val();
		}
		else{
			videoId = $("#video-id").val();
			message = "Changes in " + page + " video details had been saved successfully";
		}
		
        $.ajax(ajaxUrl, {
        	type: 'post',
        	data: {
				'title'			: title,
				'description'	: description,
				'url'			: url,
				'thumbnailUrl'	: thumbnailUrl,
				'videoId'		: videoId,
				'sectionId'		: sectionId
        	},
            dataType: "json",
            cache: false,
            beforeSend: function(jqXHR, settings) {
            	$('#message').html('');
            	$('#message').removeClass('error');
            	$('#message').removeClass('success');
            	
            	$('#message').html('<h3>Please wait</h3><p>Saving record...</p>');
            	$('#message').addClass('message');
            	$('#message').addClass('info');
            	$('#message').fadeIn();
            },
            complete: function(jqXHR, textStatus) {            	
                var response = jQuery.parseJSON(jqXHR.responseText);
                if (response.status == 1)
                {
                	var html = '<h3>Success!</h3><p>' + message + '</p>';
                	$('#message').html(html);
                	$('#message').removeClass('error');
                	$('#message').addClass('message');
                	$('#message').addClass('success');
                	$('#message').fadeIn().delay(10000).fadeOut('slow');
                }
                else
                {
                	var html = '<h3>Error!</h3><p>There was an error saving the record.</p>';
                	$('#message').html(html);
                	$('#message').addClass('message');
                	$('#message').addClass('error');
                	$('#message').fadeIn().delay(10000).fadeOut('slow');
                }
                window.location.href = callbackPage;
            }
        });
	}
};

var moodlifter = {
	isValid:1,
	displayButtons:function(divName) {
		$("."+divName).show(500);
	},
	hideButtons: function(divName){
		$("."+divName).hide(400);
	},		
	
//	Message Composer
//  params: add / edit
	saveMessageComposed: function(param){
		var subject = $("#form-subject").val();
		if(param == "add"){
			var ajaxUrl = '/moodlifter/saveMessage/';
		}
		else{
			var ajaxUrl = '/moodlifter/saveMessage/?sectionId=' + $("#form-section-id").val();
		}
		
        $.ajax(ajaxUrl, {
        	type: 'post',
        	data: {
				'subject': subject
        	},
            dataType: "json",
            cache: false,
            beforeSend: function(jqXHR, settings) {
            	$('#message').html('');
            	$('#message').removeClass('error');
            	$('#message').removeClass('success');
            	
            	$('#message').html('<h3>Please wait</h3><p>Saving record...</p>');
            	$('#message').addClass('message');
            	$('#message').addClass('info');
            	$('#message').fadeIn();
            },
            complete: function(jqXHR, textStatus) {            	
                var response = jQuery.parseJSON(jqXHR.responseText);                
                if (response.status == 1)
                {
                	var html = '<h3>Success!</h3><p>Your message has been saved successfully</p>';
                	$('#message').html(html);
                	$('#message').removeClass('error');
                	$('#message').addClass('message');
                	$('#message').addClass('success');
                	$('#message').fadeIn().delay(10000).fadeOut('slow');
                }
                else
                {
                	var html = '<h3>Error!</h3><p>There was an error saving the record.</p>';
                	$('#message').html(html);
                	$('#message').addClass('message');
                	$('#message').addClass('error');
                	$('#message').fadeIn().delay(10000).fadeOut('slow');
                }
                window.location.href = "admin#/moodlifter/composeMessage";
            }
        });

	},
		
	assignVideo: function(){
		var items = [];
		var data = $('#video-box').val();
		$('#sortable').html('');
//		$('<div id="assigned-video-list"></div>').appendTo("#playlist-main-container");
//		$("#assigned-video-container").append('<ul id="sortable" class="ui-sortable" unselectable="on" style="-moz-user-select:none;"></ul>');
		$("input[id='video-list']:checked").each(function(){
//			alert($(this).attr('name'));
			var div = $('<li class="ui-state-default" id="vlist" name="'+$(this).val()+'"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>' + $(this).attr('name') + '</li>');
			$('#sortable').append(div);
		});
		
		$("#modalContainer").hide(500);
	},
	
	publishGoodNewsPlaylist: function(){
		var items = [];
		$("input[id='pubChk']:checked").each(function(){
			items.push($(this).val());
		});
		$.ajax("/moodlifter/publishGoodNewsPlaylist/",{
			type:'post',
			data:{
				'playlistId':items.toString()
			},
			dataType:'json',
			cache:false,
            beforeSend: function(jqXHR, settings) {
            	$('#message').html('');
            	$('#message').removeClass('error');
            	$('#message').removeClass('success');
            	
            	$('#message').html('<h3>Please wait</h3><p>Saving record...</p>');
            	$('#message').addClass('message');
            	$('#message').addClass('info');
            	$('#message').fadeIn();
            },
            complete: function(jqXHR, textStatus) {            	
                var response = jQuery.parseJSON(jqXHR.responseText);
                if (response.status == 1)
                {
                	var html = '<h3>Success!</h3><p>Playlist has been published successfully!</p>';
                	$('#message').html(html);
                	$('#message').removeClass('error');
                	$('#message').addClass('message');
                	$('#message').addClass('success');
                	$('#message').fadeIn().delay(10000).fadeOut('slow');
                }
                else
                {
                	var html = '<h3>Error!</h3><p>There was an error saving the record.</p>';
                	$('#message').html(html);
                	$('#message').addClass('message');
                	$('#message').addClass('error');
                	$('#message').fadeIn().delay(10000).fadeOut('slow');
                }
                location.reload();
            }			
		});
	},
	
	savePlaylist: function(param){
		var title = $("#form-title").val();
		var description = $("#form-description").val();
		var playlistId = 0;
		var currentVideoId = $("#currentVideoId").val();
		
		var items = [];		
		var i=0;
		
		$("li[id='vlist']").each(function(){
			items.push($(this).attr('name'));
			i++;
		});
		
		var ajaxUrl = '/moodlifter/saveGoodNewsPlaylist/';
		
		if(param == "edit"){
			playlistId = $("#playlist-id").val();
		}
        $.ajax(ajaxUrl, {
        	type: 'post',
        	data: {
				'title':title,
				'description':description,
				'videoId':items?items.toString():0,
				'currentVideoId':currentVideoId,
				'playlistId':playlistId
        	},
            dataType: "json",
            cache: false,
            beforeSend: function(jqXHR, settings) {
            	$('#message').html('');
            	$('#message').removeClass('error');
            	$('#message').removeClass('success');
            	
            	$('#message').html('<h3>Please wait</h3><p>Saving record...</p>');
            	$('#message').addClass('message');
            	$('#message').addClass('info');
            	$('#message').fadeIn();
            },
            complete: function(jqXHR, textStatus) {            	
                var response = jQuery.parseJSON(jqXHR.responseText);
                if (response.status == 1)
                {
                	var html = '<h3>Success!</h3><p>Playlist has been saved successfully!</p>';
                	$('#message').html(html);
                	$('#message').removeClass('error');
                	$('#message').addClass('message');
                	$('#message').addClass('success');
                	$('#message').fadeIn().delay(10000).fadeOut('slow');
                }
                else
                {
                	var html = '<h3>Error!</h3><p>There was an error saving the record.</p>';
                	$('#message').html(html);
                	$('#message').addClass('message');
                	$('#message').addClass('error');
                	$('#message').fadeIn().delay(10000).fadeOut('slow');
                }
                window.location.href = "admin#/moodlifter/goodNewsPlaylist";
            }
        });
	},

	assignVaultItems: function(){
		var items = [];
		var data = $('#vault-box').val();
		$('#assigned-vault-container').html('');
		$('<div id="assigned-vault-list"></div>').appendTo("#vault-main-container");
		$("input[id='vault-list']:checked").each(function(){
			var div = $('<div id="vault-box"><input type="checkbox" name="assignedVaultItems" id="assignedVaultItems" checked" disabled="true" value="'+$(this).val()+'"/>&nbsp;&nbsp;' + $(this).attr('name') + '</div>');
			$('#assigned-vault-container').append(div);
		});
		
		$("#modalContainer").hide(500);
	},
	
	saveVaultItems: function(){
    	$('#message').html('');
    	$('#message').removeClass('error');
    	$('#message').removeClass('success');
    	
    	$('#message').html('<h3>Please wait</h3><p>Saving record...</p>');
    	$('#message').addClass('message');
    	$('#message').addClass('info');
    	$('#message').fadeIn().delay(1000).fadeOut('slow');
    	
		var form_data = JSON.stringify($('#form-vault-items').serializeArray());
    	var name = $("#form-name").val();
    	var description = $("#form-description").val();
    	var thumbnail = $("#form-thumbnail-url").val();
    	var videoUrl = $("#form-video-url").val();
		$.ajaxFileUpload({
				url:'/moodlifter/saveVaultItems',
				secureuri:false,
				fileElementId:'uploader',
				dataType: 'json',
				data:{
					'videoUrl'		: videoUrl,
					'name' 			: name,
					'description'	: description,
					'thumbnail' 	: thumbnail
				},
				success: function (data, status){
					if(typeof(data.status) != 'undefined'){
						if(data.status != 0){
		                	var html = '<h3>Success!</h3><p>Vault Item has been saved successfully!</p>';
		                	$('#message').html(html);
		                	$('#message').removeClass('error');
		                	$('#message').addClass('message');
		                	$('#message').addClass('success');
		                	$('#message').fadeIn().delay(10000).fadeOut('slow');
		                	window.location.href = "admin#/moodlifter/secretVault";
						}
						else{
		                	var html = '<h3>Error!</h3><p>There was an error saving the record. </p><p>' + data.error + '</p>';
		                	$('#message').html(html);
		                	$('#message').addClass('message');
		                	$('#message').addClass('error');
		                	$('#message').fadeIn().delay(10000).fadeOut('slow');	
//		                	window.setTimeout(function(){location.reload()},3000)
						}
					}
				},
				error: function (data, status, e){
                	var html = '<h3>Error!</h3><p>There was an error saving the record.  </p><p>' + data.error + '</p>';
                	$('#message').html(html);
                	$('#message').addClass('message');
                	$('#message').addClass('error');
                	$('#message').fadeIn().delay(1000).fadeOut('slow');
                	window.setTimeout(function(){location.reload()},3000)
				}
			});
	},
	
	saveSecretVault: function(param){
		var title = $("#form-title").val();
		var description = $("#form-description").val();
		var token = $("#form-token").val();
		var imageUrl = $("#form-image-url").val();
		var currentVaultId = $("#currentVaultId").val();
		var vaultId = 0;
		var items = [];		
		var i=0;
		$("input[id='vault-list']:checked").each(function(){
			items.push($(this).val());
			i++;
		});
		if(param == "edit"){
			vaultId = $("#vault-id").val();
		}
        $.ajax('/moodlifter/saveSecretVault/', {
        	type: 'post',
        	data: {
				'title'			: title,
				'description'	: description,
				'vaultId'		: vaultId,
				'token'			: token,
				'imageUrl'		: imageUrl,
				'currentVaultId': currentVaultId,
				'vaultItems'	: items.toString(),
				'vaultId'		: vaultId
        	},
            dataType: "json",
            cache: false,
            beforeSend: function(jqXHR, settings) {
            	$('#message').html('');
            	$('#message').removeClass('error');
            	$('#message').removeClass('success');
            	
            	$('#message').html('<h3>Please wait</h3><p>Saving record...</p>');
            	$('#message').addClass('message');
            	$('#message').addClass('info');
            	$('#message').fadeIn();
            },
            complete: function(jqXHR, textStatus) {            	
                var response = jQuery.parseJSON(jqXHR.responseText);
                if (response.status == 1)
                {
                	var html = '<h3>Success!</h3><p>Secret Vault has been saved successfully!</p>';
                	$('#message').html(html);
                	$('#message').removeClass('error');
                	$('#message').addClass('message');
                	$('#message').addClass('success');
                	$('#message').fadeIn().delay(10000).fadeOut('slow');
                }
                else
                {
                	var html = '<h3>Error!</h3><p>There was an error saving the record.</p>';
                	$('#message').html(html);
                	$('#message').addClass('message');
                	$('#message').addClass('error');
                	$('#message').fadeIn().delay(10000).fadeOut('slow');
                }
                window.location.href = "admin#/moodlifter/secretVault";
            }
        });		
	},

	deleteSelected: function(checkBox, papiCall, param){		
		var items = [];
		var i=0;
		$("input[id='" + checkBox + "']:checked").each(function(){
			items.push($(this).val());
			i++;
		});
		if(i>0){
	    	var html = '<h3>Delete!</h3><p>These items will be permanently deleted and cannot be recovered. Are you sure? &nbsp;&nbsp;<a href="javascript:;" onclick="moodlifter.deleteSelected(\'' + checkBox + '\', \'' + papiCall + '\', \'yes\')">Yes</a>&nbsp;&nbsp;/&nbsp;&nbsp;<a href="javascript:;" onclick="moodlifter.deleteSelected(\'' + checkBox + '\', \'' + papiCall + '\', \'no\')">No</a></p>';
	    	$('#delMessage').html(html);
	    	$('#delMessage').removeClass('error');
	    	$('#delMessage').addClass('message');
	    	$('#delMessage').addClass('success');
			switch(param){
				case '':
					$('#delMessage').show(500);
				break;
				case 'no':
					$('#delMessage').hide(500);
				break;
				case 'yes':
					$('#delMessage').hide(300);
					if(i>0){
				        $.ajax('/moodlifter/deleteSelectedRows', {
				        	type: 'post',
				        	data: {
								'id':items.toString(),
								'papiCall':papiCall
				        	},
				            dataType: "json",
				            cache: false,
				            beforeSend: function(jqXHR, settings) {
				            	$('#message').html('');
				            	$('#message').removeClass('error');
				            	$('#message').removeClass('success');
				            	
				            	$('#message').html('<h3>Please wait</h3><p>Deleting record...</p>');
				            	$('#message').addClass('message');
				            	$('#message').addClass('info');
				            	$('#message').fadeIn();
				            },
				            complete: function(jqXHR, textStatus) {            	
				                var response = jQuery.parseJSON(jqXHR.responseText);
				                if (response.status == 1)
				                {
				                	var html = '<h3>Success!</h3><p>Done deleting record/s</p>';
				                	$('#message').html(html);
				                	$('#message').removeClass('error');
				                	$('#message').addClass('message');
				                	$('#message').addClass('success');
				                	$('#message').fadeIn().delay(10000).fadeOut('slow');
				                }
				                else
				                {
				                	var html = '<h3>Error!</h3><p>There was an error deleting the record.</p>';
				                	$('#message').html(html);
				                	$('#message').addClass('message');
				                	$('#message').addClass('error');
				                	$('#message').fadeIn().delay(10000).fadeOut('slow');
				                }
				                location.reload();
				            }
				        });					
					}
		
				break;
			}
		}
	},
	
	publishSecretVault: function(){
		var items = [];
		$("input[id='pubChk']:checked").each(function(){
			items.push($(this).val());
		});
		$.ajax("/moodlifter/publishSecretVault/",{
			type:'post',
			data:{
				'vaultId':items.toString()
			},
			dataType:'json',
			cache:false,
            beforeSend: function(jqXHR, settings) {
            	$('#message').html('');
            	$('#message').removeClass('error');
            	$('#message').removeClass('success');
            	
            	$('#message').html('<h3>Please wait</h3><p>Publishing vault...</p>');
            	$('#message').addClass('message');
            	$('#message').addClass('info');
            	$('#message').fadeIn();
            },
            complete: function(jqXHR, textStatus) {            	
                var response = jQuery.parseJSON(jqXHR.responseText);
                if (response.status == 1)
                {
                	var html = '<h3>Success!</h3><p>Playlist has been published successfully!</p>';
                	$('#message').html(html);
                	$('#message').removeClass('error');
                	$('#message').addClass('message');
                	$('#message').addClass('success');
                	$('#message').fadeIn().delay(10000).fadeOut('slow');
                }
                else
                {
                	var html = '<h3>Error!</h3><p>There was an error saving the record.</p>';
                	$('#message').html(html);
                	$('#message').addClass('message');
                	$('#message').addClass('error');
                	$('#message').fadeIn().delay(10000).fadeOut('slow');
                }
                location.reload();
            }			
		});
	},
	
	publishChallenge: function(){
		var items = [];
		$("input[id='pubChk']:checked").each(function(){
			items.push($(this).val());
		});
		$.ajax("/moodlifter/publishChallenge/",{
			type:'post',
			data:{
				'challengeId':items.toString()
			},
			dataType:'json',
			cache:false,
            beforeSend: function(jqXHR, settings) {
            	$('#message').html('');
            	$('#message').removeClass('error');
            	$('#message').removeClass('success');
            	
            	$('#message').html('<h3>Please wait</h3><p>Publishing Challenges...</p>');
            	$('#message').addClass('message');
            	$('#message').addClass('info');
            	$('#message').fadeIn();
            },
            complete: function(jqXHR, textStatus) {            	
                var response = jQuery.parseJSON(jqXHR.responseText);
                if (response.status == 1)
                {
                	var html = '<h3>Success!</h3><p>Challenges has been published successfully!</p>';
                	$('#message').html(html);
                	$('#message').removeClass('error');
                	$('#message').addClass('message');
                	$('#message').addClass('success');
                	$('#message').fadeIn().delay(10000).fadeOut('slow');
                }
                else
                {
                	var html = '<h3>Error!</h3><p>There was an error saving the record.</p>';
                	$('#message').html(html);
                	$('#message').addClass('message');
                	$('#message').addClass('error');
                	$('#message').fadeIn().delay(10000).fadeOut('slow');
                }
                location.reload();
            }
		});
	},

	applySubChallengeMapping: function(){
		var items = [];
		var counter = 0;
		$('select[id="parentChallenge"] > option:selected').each(function() {
//    		alert($(this).text() + ' ' + $(this).val());
			items.push($(this).val());
			counter++;
		});
		
		$.ajax("/moodlifter/applySubChallengeMapping/",{
			type:'post',
			data:{
				"id":items.toString()
			},
			dataType:'json',
			cache:false,
            beforeSend: function(jqXHR, settings) {
            	$('#message').html('');
            	$('#message').removeClass('error');
            	$('#message').removeClass('success');
            	
            	$('#message').html('<h3>Please wait</h3><p>Mapping SubChallenge...</p>');
            	$('#message').addClass('message');
            	$('#message').addClass('info');
            	$('#message').fadeIn();
            },
            complete: function(jqXHR, textStatus) {            	
                var response = jQuery.parseJSON(jqXHR.responseText);
                if (response.status == 1)
                {
                	var html = '<h3>Success!</h3><p>Mapping successfully finished!</p>';
                	$('#message').html(html);
                	$('#message').removeClass('error');
                	$('#message').addClass('message');
                	$('#message').addClass('success');
                	$('#message').fadeIn().delay(10000).fadeOut('slow');
                }
                else
                {
                	var html = '<h3>Error!</h3><p>There was an error saving the record.</p>';
                	$('#message').html(html);
                	$('#message').addClass('message');
                	$('#message').addClass('error');
                	$('#message').fadeIn().delay(10000).fadeOut('slow');
                }
                location.reload();
            }
		});				
	}
};


var myideas = {
	publishFeaturePlaylist: function(){
		var items = [];
		$("input[id='pubChk']:checked").each(function(){
			items.push($(this).val());
		});
		$.ajax("/myideas/publishFeaturePlaylist/",{
			type:'post',
			data:{
				'playlistId':items.toString()
			},
			dataType:'json',
			cache:false,
            beforeSend: function(jqXHR, settings) {
            	$('#message').html('');
            	$('#message').removeClass('error');
            	$('#message').removeClass('success');
            	
            	$('#message').html('<h3>Please wait</h3><p>Saving record...</p>');
            	$('#message').addClass('message');
            	$('#message').addClass('info');
            	$('#message').fadeIn();
            },
            complete: function(jqXHR, textStatus) {            	
                var response = jQuery.parseJSON(jqXHR.responseText);
                if (response.status == 1)
                {
                	var html = '<h3>Success!</h3><p>Playlist has been published successfully!</p>';
                	$('#message').html(html);
                	$('#message').removeClass('error');
                	$('#message').addClass('message');
                	$('#message').addClass('success');
                	$('#message').fadeIn().delay(10000).fadeOut('slow');
                }
                else
                {
                	var html = '<h3>Error!</h3><p>There was an error saving the record.</p>';
                	$('#message').html(html);
                	$('#message').addClass('message');
                	$('#message').addClass('error');
                	$('#message').fadeIn().delay(10000).fadeOut('slow');
                }
                location.reload();
            }			
		});
	},
	
	assignVideo: function(){
		var items = [];
		var data = $('#video-box').val();
		$('#sortable').html('');
		$("input[id='video-list']:checked").each(function(){
			var div = $('<li class="ui-state-default" id="vlist" name="'+$(this).val()+'"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>' + $(this).attr('name') + '</li>');
			$('#sortable').append(div);
		});
		
		$("#modalContainer").hide(500);
	},
		
	savePlaylist: function(param){
		var title = $("#form-title").val();
		var description = $("#form-description").val();
		var playlistId = 0;
		var currentVideoId = $("#currentVideoId").val();
		
		var items = [];		
		var i=0;
		
		$("li[id='vlist']").each(function(){
			items.push($(this).attr('name'));
			i++;
		});
		
		var ajaxUrl = '/myideas/saveFeaturePlaylist/';
		
		if(param == "edit"){
			playlistId = $("#playlist-id").val();
		}
        $.ajax(ajaxUrl, {
        	type: 'post',
        	data: {
				'title':title,
				'description':description,
				'videoId':items?items.toString():0,
				'currentVideoId':currentVideoId,
				'playlistId':playlistId
        	},
            dataType: "json",
            cache: false,
            beforeSend: function(jqXHR, settings) {
            	$('#message').html('');
            	$('#message').removeClass('error');
            	$('#message').removeClass('success');
            	
            	$('#message').html('<h3>Please wait</h3><p>Saving record...</p>');
            	$('#message').addClass('message');
            	$('#message').addClass('info');
            	$('#message').fadeIn();
            },
            complete: function(jqXHR, textStatus) {            	
                var response = jQuery.parseJSON(jqXHR.responseText);
                if (response.status == 1)
                {
                	var html = '<h3>Success!</h3><p>Playlist has been saved successfully!</p>';
                	$('#message').html(html);
                	$('#message').removeClass('error');
                	$('#message').addClass('message');
                	$('#message').addClass('success');
                	$('#message').fadeIn().delay(10000).fadeOut('slow');
                }
                else
                {
                	var html = '<h3>Error!</h3><p>There was an error saving the record.</p>';
                	$('#message').html(html);
                	$('#message').addClass('message');
                	$('#message').addClass('error');
                	$('#message').fadeIn().delay(10000).fadeOut('slow');
                }
                window.location.href = "admin#/myideas/featurePlaylist";
            }
        });
	}
}

var workspace = {
	addDomain: function(){
		var i = $('input').size();
//        $('<div class="clearfix" id="d_'+i+'" style="width:100%; text-align:right;"><label for="form-name" class="form-label" style="width:150px;">Domain Name</label><div class="form-input"><input type="text" id="form-name" name="name" required="required" placeholder="" value=""/></div></div><div class="form-input"><a href="javascript:;" onclick="$(\'#d_'+i+'\').remove();" title="New Domain" class="button icon-with-text"><img src="/images/navicons-small/10.png" alt=""/>Delete</a></div>').fadeIn('slow').appendTo('.inputs');
		$('<div class="clearfix" id="d_' + i + '" style="width:100%; text-align:right;margin-right:10px;"><label for="form-name" class="form-label" style="width:150px;">Domain Name</label><div class="form-input"><input type="text" id="form-name" name="name" required="required" placeholder="" value=""/></div><div class="form-input" style="margin-right:20px;"><a href="javascript:;" onclick="$(\'#d_' + i + '\').remove();" title="New Domain" class="button icon-with-text"><img src="/images/navicons-small/135.png" alt=""/>Delete</a></div></div>').fadeIn('slow').appendTo('.inputs');
	}
	
};

var usermanagement = {
	validEmail:0,
	deleteSelected: function(checkBox, papiCall, param){
		var items = [];
		var i=0;
		$("input[id='" + checkBox + "']:checked").each(function(){
			items.push($(this).val());
			i++;
		});
		if(i>0){
	    	var html = '<h3>Delete!</h3><p>These items will be permanently deleted and cannot be recovered. Are you sure? &nbsp;&nbsp;<a href="javascript:;" onclick="usermanagement.deleteSelected(\'' + checkBox + '\', \'' + papiCall + '\', \'yes\')">Yes</a>&nbsp;&nbsp;/&nbsp;&nbsp;<a href="javascript:;" onclick="usermanagement.deleteSelected(\'' + checkBox + '\', \'' + papiCall + '\', \'no\')">No</a></p>';
	    	$('#delMessage').html(html).show(1000);
	    	$('#delMessage').removeClass('error');
	    	$('#delMessage').addClass('message');
	    	$('#delMessage').addClass('success');
			switch(param){
				case '':
					$('#delMessage').show(500);
				break;
				case 'no':
					$('#delMessage').hide(500);
				break;
				case 'yes':
					$('#delMessage').hide(300);
					if(i>0){
				        $.ajax('/usermanagement/deleteSelected', {
				        	type: 'post',
				        	data: {
								'id':items.toString(),
								'papiCall':papiCall
				        	},
				            dataType: "json",
				            cache: false,
				            beforeSend: function(jqXHR, settings) {
				            	$('#message').html('');
				            	$('#message').removeClass('error');
				            	$('#message').removeClass('success');
				            	
				            	$('#message').html('<h3>Please wait</h3><p>Deleting record...</p>');
				            	$('#message').addClass('message');
				            	$('#message').addClass('info');
				            	$('#message').fadeIn();
				            },
				            complete: function(jqXHR, textStatus) {            	
				                var response = jQuery.parseJSON(jqXHR.responseText);
				                if (response.status == 1)
				                {
				                	var html = '<h3>Success!</h3><p>Done deleting record/s</p>';
				                	$('#message').html(html);
				                	$('#message').removeClass('error');
				                	$('#message').addClass('message');
				                	$('#message').addClass('success');
				                	$('#message').fadeIn().delay(10000).fadeOut('slow');
				                }
				                else
				                {
				                	var html = '<h3>Error: ' + response.message + '</h3><p>There was an error deleting the record.</p>';
				                	$('#message').html(html);
				                	$('#message').addClass('message');
				                	$('#message').addClass('error');
				                	$('#message').fadeIn().delay(10000).fadeOut('slow');
				                }
				                setInterval(location.reload(),3000);
				                
				            }
				        });					
					}
		
				break;
			}
		}
	},

	checkAvailability: function(){
        $.ajax('/usermanagement/checkAvailability/', {
        	type: 'post',
        	data: {
				'email':$('#form-email').val(),
				'userid':$("#form-user-id").val()
        	},
            dataType: "json",
            beforeSend: function(jqXHR, settings) {
            	$('#availability-message').html('');
            	$('#availability-message').removeClass('error');
            	$('#availability-message').removeClass('success');
            	
            	$('#availability-message').html('Please wait, validating email address');
            	$('#availability-message').addClass('availability-message');
            	$('#availability-message').addClass('info');
            	$('#availability-message').fadeIn();
            },
            complete: function(jqXHR, textStatus) {            	
                var response = jQuery.parseJSON(jqXHR.responseText);
                if (response.status == 1){
                	var html = response.message;
                	$('#availability-message').html(html);
                	$('#availability-message').removeClass('error');
                	$('#availability-message').addClass('availability-message');
                	$('#availability-message').addClass('success');
                	$('#availability-message').fadeIn().delay(10000).fadeOut('slow');
                }
                else
                {
                	var html = response.message;
                	$('#availability-message').html(html);
                	$('#availability-message').addClass('availability-message');
                	$('#availability-message').addClass('error');
                	$('#availability-message').fadeIn().delay(10000).fadeOut('slow');
                }
            }
        });
	}
};
function displayDiv(param){
	if(param == "video"){
		$("#uploadDiv").hide(500);
		$("#videoDiv").show(500);
		document.getElementById('uploader').value = "";
	}
	else{
		$("#videoDiv").hide(500);		
		$("#uploadDiv").show(500);
		document.getElementById('form-video-url').value = "";
	}
}

function displayButtons(divName) {
	$("."+divName).show(500);
}

function hideButtons(divName){
	$("."+divName).hide(400);
}

function displayModal(){
	$("#modalContainer").center().show(500);
}


function hideModal(){
	$("#modalContainer").hide(500);
}

function displayPageLoader(){
	var html = '<h3><img id="loading-graphic" width="16" height="16" src="/images/ajax-loader-eeeeee.gif" />&nbsp;Loading..</h3><p>Please wait!</p>';
	$('#message').html(html);
	$('#message').removeClass('error');
	$('#message').addClass('message');
	$('#message').addClass('info');
	$('#message').fadeIn().delay(10000).fadeOut('slow');
//	window.location.href = "admin#/moodlifter/challenges";	
}

function genAjaxPost(formName, controller, callback) {
	var form_data = JSON.stringify($('#'+formName).serializeArray());
	
    $.ajax(controller, {
    	type: 'post',
    	data: {'data':form_data},
        dataType: "json",
        cache: false,
        beforeSend: function(jqXHR, settings) {
        	$('#message').html('');
        	$('#message').removeClass('error');
        	$('#message').removeClass('success');
        	
        	$('#message').html('<h3>Please wait</h3><p>Processing...</p>');
        	$('#message').addClass('message');
        	$('#message').addClass('info');
        	$('#message').fadeIn();
        },
        complete: function(jqXHR, textStatus) {
            var response = jQuery.parseJSON(jqXHR.responseText);  
            if (response.status == 1){
            	var html = '<h3>Success!</h3><p>' + response.message + '</p>';
            	$('#message').html(html);
            	$('#message').removeClass('error');
            	$('#message').addClass('message');
            	$('#message').addClass('success');
            	$('#message').fadeIn().delay(10000).fadeOut('slow');
	            if(callback==''){
	            	location.reload();
	            }
	            else{
	            	window.location.href = callback;	
	            }
            }
            else{
            	var html = '<h3>Error!</h3><p>There was an error processing your request.</p><p>' + response.message + '</p>';
            	$('#message').html(html);
            	$('#message').addClass('message');
            	$('#message').addClass('error');
            	$('#message').fadeIn().delay(10000).fadeOut('slow');
            }
        }
    });
}

