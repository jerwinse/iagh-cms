var global = {
	displayTransContainer: function(divName){
		var month = new Array("todayTransaction","January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
		
		for(var i=0; i<month.length; i++){
			$('#'+month[i]).hide(500);
		}
		$('#'+divName).show(500);
	},
	displayTabMenuReports: function(tabName){
		var tab = new Array("followers", "tenantHits", "touchpointHits", "feedbackSeverity", "feedbackStatus", "feedback");
		for(var i=0; i<tab.length; i++){
			$('#'+tab[i]).hide(500);
			$('#'+tab[i]+"Link").css({"font-color":"#000", "font-size":"20px"});
		}
//		$('#'+tabName+"Link").css("font-size:30px;color:#3C3C47;");
		switch(tabName){
			case "feedback":
				$('#feedbackLink').css({"font-size":"30px", "color":"RED"});
				$('#feedbackStatus').show(500);
			break;
			case "feedbackStatus":
				$('#feedbackLink').css({"font-size":"30px", "color":"RED"});
				$('#feedbackStatus').show(500);
			break;
			case "feedbackSeverity":
				$('#feedbackLink').css({"font-size":"30px", "color":"RED"});
				$('#feedbackSeverity').show(500);
			break;
			default:
				$('#'+tabName+"Link").css({"font-size":"30px", "color":"RED"});
				$('#'+tabName).show(500);
			break;
		}
	},
	displaySeverityHits: function(){
		$('#severityHits').show(500);
	}
};