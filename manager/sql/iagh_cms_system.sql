-- MySQL dump 10.13  Distrib 5.5.24, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: iagh_system
-- ------------------------------------------------------
-- Server version	5.5.24-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `page_grouping`
--

DROP TABLE IF EXISTS `page_grouping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_grouping` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `panel_page` varchar(150) DEFAULT NULL,
  `landing_page` varchar(150) DEFAULT NULL,
  `role_id` int(9) DEFAULT NULL,
  `permission_id` int(9) DEFAULT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url_roleid` (`landing_page`,`role_id`,`permission_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_grouping`
--

LOCK TABLES `page_grouping` WRITE;
/*!40000 ALTER TABLE `page_grouping` DISABLE KEYS */;
INSERT INTO `page_grouping` VALUES (1,'Solucient','cpanel/sol-admin.zpt','cpanel/admin#/dashboard',2,2,'2012-07-24 07:13:16'),(2,'IAGH','cpanel/iagh-admin.zpt','cpanel/admin#/dashboard',1,2,'2012-07-24 07:13:11'),(4,'Solucient','cpanel/sol-admin.zpt','cpanel/admin#/dashboard',2,1,'2012-07-24 07:13:19'),(5,'IAGH','cpanel/iagh-admin.zpt','cpanel/admin#/dashborad',1,1,'2012-07-24 07:13:13'),(6,'IAGH','cpanel/iagh.zpt','cpanel/admin#/dashboard',1,3,'2012-07-24 07:13:14'),(7,'Admin','cpanel/admin.zpt','cpanel/admin#/members/index',3,1,'2012-07-24 07:18:56');
/*!40000 ALTER TABLE `page_grouping` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-10-01 18:43:12
